﻿using Microsoft.AspNetCore.Http;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RedbullKSA.API.Extensions
{
    public static class HttpRequestExtensions
    {
        #region Constants
        private const string DEF_DEVICE_HEADER_KEY = "X-DeviceType";

        private static readonly List<string> SensitiveParameterKeys = new List<string>()
        {
            "login",
            "password",
            "confirmPassword",
        };

        private const string DEF_SEARCH_BODY_PARAMS_PATTERN = "(\"{0}\"): (\"\\S*\")";
        private const string DEF_SEARCH_QUERY_PARAMS_PATTERN = "(\"Key\":\"{0}\",\"Value\":\\[\"\\S*\"\\])";
        private const char DEF_REGEX_OR_CHARACTER = '|';
        #endregion

        /// <summary>
        /// Get client device name
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Device name or empty string if device name not provided in request</returns>
        public static SessionTokenType? GetClientDeviceType(this HttpRequest request)
        {
            return request.Headers.ContainsKey(DEF_DEVICE_HEADER_KEY)
                ? request.Headers[DEF_DEVICE_HEADER_KEY].ToString().StringToEnum<SessionTokenType>()
                : null;
        }

        /// <summary>
        /// Get user name
        /// </summary>
        /// <param name="context"></param>
        /// <returns>User name or empty string if user name not provided in request</returns>
        public static string GetUserName(this HttpContext context)
        {
            return context.User?.Identity?.Name ?? string.Empty;
        }

        /// <summary>
        /// Get request parameters as Json string
        /// </summary>
        /// <param name="context"></param>
        /// <returns>Json string of request parameters</returns>
        public static async Task<string> GetRequestParameters(this HttpContext context)
        {
            if (context.Request.Query.Count > 0)
            {
                var stringParameters = JsonSerializer.Serialize(context.Request.Query);
                stringParameters = RemoveSensitiveParameters(stringParameters);
                return stringParameters;
            }

            context.Request.EnableBuffering();

            var requestBody = string.Empty;
            using (var reader = new StreamReader(context.Request.Body, leaveOpen: true)) 
            {
                // resetting body position, in case, when happened exception after reading body in a controller
                ResetRequestBodyPosition(context);
                requestBody = await reader.ReadToEndAsync();
                requestBody = RemoveSensitiveParameters(requestBody);
                // IMPORTANT: Reset the request body stream position so the next middleware can read it
                ResetRequestBodyPosition(context);
            } 

            return requestBody;
        }

        #region Helper Methods
        private static void ResetRequestBodyPosition(HttpContext context)
        {
            if(context.Request.Body.Position != 0)
            {
                context.Request.Body.Position = 0;
            }

            return;
        }

        private static string RemoveSensitiveParameters(string parameters)
        {
            var patternBuilder = new StringBuilder();

            // building regex pattern based on sensitive parameter keys
            foreach (var parameterKey in SensitiveParameterKeys)
            {
                // adding pattern of body parameter
                patternBuilder.Append(string.Format(DEF_SEARCH_BODY_PARAMS_PATTERN, parameterKey));
                // addind 'OR' character to pattern
                patternBuilder.Append(DEF_REGEX_OR_CHARACTER);
                // adding pattern of query parameter
                patternBuilder.Append(string.Format(DEF_SEARCH_QUERY_PARAMS_PATTERN, parameterKey));

                if (SensitiveParameterKeys.Last() != parameterKey)
                {
                    patternBuilder.Append(DEF_REGEX_OR_CHARACTER);
                }
            }

            var regexPattern = patternBuilder.ToString();
            Regex regex = new Regex(@regexPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

            var clearedParameters = regex.Replace(parameters, string.Empty);

            return clearedParameters;
        }

        #endregion
    }
}
