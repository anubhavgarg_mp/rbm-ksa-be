﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using RedbullKSA.API.Extensions;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Threading.Tasks;
using RedbullKSA.Common.Enums;
using System.IO;
using System.Text.Json;
using RedbullKSA.Entities.API.Base;
using System.Net;
using System.Text;

namespace RedbullKSA.API.Middleware
{
    /// <summary>
    /// Main API exception handling middleware
    /// </summary>
    public class ApiExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private const string CONTENT_TYPE_HDR = "Content-Type";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="next"></param>
        /// <param name="loggerFactory"></param>
        public ApiExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger(GetType());
            _next = next;
        }

        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="context"></param>
        /// <param name="errorLogRepository"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context, IErrorLogEntryRepository errorLogRepository)
        {
            try
            {
                await _next(context);
            }
            catch (KSAException ex)
            {
                await LogError(context, ex, errorLogRepository);
                _logger.LogError(ex, $"'{context.Request.Path}' '{ex.Message}'");
                await SetResponseBody(context, From(ex), HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                await LogError(context, ex, errorLogRepository);
                _logger.LogError(ex, $"'{context.Request.Path}' '{ex.Message}'");
                await SetResponseBody(context, From(ex), HttpStatusCode.BadRequest);
            }
        }

        private async Task LogError(HttpContext context, Exception exception, IErrorLogEntryRepository errorLogRepository)
        {
            var isKSAException = exception is KSAException;

            var errorLogEntry = new ErrorLogEntry()
            {
                Device = context.Request.GetClientDeviceType(),
                UserName = context.GetUserName(),
                RequestUrl = context.Request.Path,
                RequestParameters = await context.GetRequestParameters(),
                ExceptionType = isKSAException ? ExceptionType.KSA : ExceptionType.System,
                ExceptionMessage = exception.Message,
                StackTrace = exception.StackTrace,
            };

            if (isKSAException)
            {
                var kSAException = (KSAException)exception;
                errorLogEntry.KSAExceptionCode = Convert.ToInt32(kSAException.Code);
                errorLogEntry.KSAExceptionType = kSAException.Code.GetType().Name;
            }

            await errorLogRepository.Add(errorLogEntry);
        }

        /// <summary>
        /// Write ErrorResponseModel to response body
        /// </summary>
        /// <param name="context"></param>
        /// <param name="error"></param>
        /// <param name="httpStatus"></param>
        /// <returns></returns>
        private async Task SetResponseBody(HttpContext context, ErrorResponseModelDTO error, HttpStatusCode httpStatus)
        {
            var originalBody = ReplaceWithMemoryStream(context.Response);

            var jsonString = JsonSerializer.Serialize(error);
            var bytes = Encoding.UTF8.GetBytes(jsonString);
            await context.Response.Body.WriteAsync(bytes, 0, bytes.Length);

            await ReturnBody(context.Response, originalBody);
        }

        /// <summary>
        /// Forms a error response object from KSA exception
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private ErrorResponseModelDTO From(KSAException ex)
            => new ErrorResponseModelDTO(ex);

        /// <summary>
        /// Forms a error response object from exception
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private ErrorResponseModelDTO From(Exception ex)
            => new ErrorResponseModelDTO(ex);

        /// <summary>
        /// Replaces HttpResponse Body stream with memory stream, thus stopping response streaming start and allowing to write to it
        /// </summary>
        /// <param name="response"></param>
        /// <returns>Original response Body stream</returns>
        private Stream ReplaceWithMemoryStream(HttpResponse response)
        {
            var original = response.Body;
            response.Body = new MemoryStream();
            return original;
        }

        /// <summary>
        /// Return original body to response, retaining data from MemoryStream
        /// </summary>
        /// <param name="response"></param>
        /// <param name="originalBody"></param>
        /// <returns></returns>
        private async Task ReturnBody(HttpResponse response, Stream originalBody)
        {
            response.StatusCode = (int)HttpStatusCode.BadRequest;
            if (!response.Headers.ContainsKey(CONTENT_TYPE_HDR))
                response.Headers.Add(CONTENT_TYPE_HDR, "application/json; charset=utf-8");

            response.Body.Seek(0, SeekOrigin.Begin);
            await response.Body.CopyToAsync(originalBody);
            response.Body = originalBody;
        }
    }
}
