﻿using Microsoft.AspNetCore.Http;
using RedbullKSA.API.Extensions;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Repositories.Contracts;
using System.Threading.Tasks;

namespace RedbullKSA.API.Middleware
{
    /// <summary>
    /// Main API for logging requests
    /// </summary>
    public class ApiLoggerMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="next"></param>
        public ApiLoggerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="context"></param>
        /// <param name="systemLogEntryRepository"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context, ISystemLogEntryRepository systemLogEntryRepository)
        {
            await LogRequest(context, systemLogEntryRepository);
            await _next(context);
        }

        private async Task LogRequest(HttpContext context, ISystemLogEntryRepository systemLogEntryRepository)
        {
            var systemLogEntry = new SystemLogEntry()
            {
                Device = context.Request.GetClientDeviceType(),
                UserName = context.GetUserName(),
                RequestUrl = context.Request.Path,
                RequestParameters = await context.GetRequestParameters(),
            };

            await systemLogEntryRepository.Add(systemLogEntry);
        }
    }
}
