﻿using System;

namespace RedbullKSA.API.Attributes
{
    /// <summary>
    /// Basic Auth attribute
    /// </summary>
    public class BasicAuthAttribute : Attribute
    {
    }
}
