﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using RedbullKSA.API.Attributes;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Helpers;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Identity;
using RedbullKSA.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.API.Filters
{
    /// <summary>
    /// Filter for token authorization
    /// </summary>
    public class SessionTokenAuthorizeFilter : IAsyncAuthorizationFilter
    {
        private readonly ILogger _logger;
        private readonly IServiceProvider _services;
        private FilterContext _context;
        private List<SessionTokenAuthorizeAttribute> AuthAttributes;
        private List<SessionTokenAuthorizeAttribute> MethodAuthAttributes;
        private List<SessionTokenAuthorizeAttribute> ControllerAttributes;
        private List<SessionTokenAuthorizeAttribute> MethodAttributes;
        private bool MethodHasAllowAnonymous { get; set; }
        private bool MethodHasAllowGuest { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="loggerFactory"></param>
        public SessionTokenAuthorizeFilter(IServiceProvider services, ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger(GetType());
            _services = services;
            AuthAttributes = new List<SessionTokenAuthorizeAttribute>();
            MethodAuthAttributes = new List<SessionTokenAuthorizeAttribute>();
            ControllerAttributes = new List<SessionTokenAuthorizeAttribute>();
            MethodAttributes = new List<SessionTokenAuthorizeAttribute>();
        }

        /// <summary>
        /// Main authorization method
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
#pragma warning disable 1998
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            _context = context;

            GetAttributes();

            if (DoNeedAuthorize())
                Authorize();
        }
#pragma warning restore 1998

        private bool DoNeedAuthorize()
        {
            var method = ((ControllerActionDescriptor)_context.ActionDescriptor).MethodInfo;

            // Anonymous overrides controller attributes
            var methodAuthAttributes = MethodAuthAttributes.Any() ? MethodAuthAttributes : AuthAttributes;
            return MethodHasAllowAnonymous ? false : methodAuthAttributes.Any() || MethodHasAllowGuest;
        }

        /// <summary>
        /// Checks:
        /// if IIdentity is SessionTokenIdentity
        /// if SessionToken didn't expire
        /// if user has claims mentioned in SessionTokenAuthorizeAttribute attributes
        /// if SessionToken user type is correct
        /// </summary>
        private void Authorize()
        {
            var passScheme = _context.HttpContext.User.Identity.IsAuthenticated && _context.HttpContext.User.Identity is SessionTokenIdentity;

            if (!passScheme)
                throw new KSAException(KSAExceptionCode.General.Unauthorized);

            if (((SessionTokenIdentity)_context.HttpContext.User.Identity).IsGuest && !MethodHasAllowGuest)
                throw new KSAException(KSAExceptionCode.General.Unauthorized);

            var identity = (SessionTokenIdentity)_context.HttpContext.User.Identity;
            if (!identity.IsValid)
                throw new KSAException(KSAExceptionCode.General.Unauthorized);

            ValidateClaims(identity);
            ValidateAuthAttributes(identity);
        }

        private void ValidateClaims(SessionTokenIdentity identity)
        {
            var validIdentityUserClaims = identity.UserClaims.Where(i => !string.IsNullOrWhiteSpace(i.Value) && !ConversionHelper.InterpretFalseBool(i.Value));

            var requiredControllerClaims = ControllerAttributes.Where(v => v.RequiredClaims != null).SelectMany(v => v.RequiredClaims);
            CheckAttributes(requiredControllerClaims, validIdentityUserClaims);

            var requiredMethodClaims = MethodAttributes.Where(v => v.RequiredClaims != null).SelectMany(v => v.RequiredClaims);
            CheckAttributes(requiredMethodClaims, validIdentityUserClaims);
        }

        private void ValidateAuthAttributes(SessionTokenIdentity identity)
        {
            var validIdentityTokenType = _services.GetSessionTokenService().ParseUserType(identity.SessionToken.Token);

            var requiredControllerTokenTypes = AuthAttributes.Select(v => v.TokenType).FirstOrDefault();
            CheckTokenTypes(requiredControllerTokenTypes, validIdentityTokenType);

            var requiredMethodTokenTypes = MethodAuthAttributes.Select(v => v.TokenType).FirstOrDefault();
            CheckTokenTypes(requiredMethodTokenTypes, validIdentityTokenType);
        }

        private void CheckAttributes(IEnumerable<AdminClaimType> claimTypes, IEnumerable<UserClaim> validIdentityUserClaims)
        {
            if (!claimTypes.Any())
                return;

            var hasCommonClaim = claimTypes.Intersect(validIdentityUserClaims.Select(v => v.ClaimType)).Any();
            if (!hasCommonClaim)
                throw new KSAException(KSAExceptionCode.General.Unauthorized);
        }

        private void CheckTokenTypes(SessionTokenType tokenType, SessionTokenType validSessionTokenType)
        {
            if (tokenType == 0)
                return;

            if( !tokenType.HasFlag(validSessionTokenType))
            {
                throw new KSAException(KSAExceptionCode.General.Unauthorized);
            }
        }

        /// <summary>
        /// Gets all SessionTokenAuthorizeAttribute attributes from controller and method
        /// </summary>
        private void GetAttributes()
        {
            var actionDescriptor = (ControllerActionDescriptor)_context.ActionDescriptor;

            var controller = actionDescriptor.ControllerTypeInfo;
            var controllerAttributes = controller.GetCustomAttributes(typeof(SessionTokenAuthorizeAttribute), true);

            var method = actionDescriptor.MethodInfo;
            var methodAttributes = method.GetCustomAttributes(typeof(SessionTokenAuthorizeAttribute), true);

            MethodHasAllowAnonymous = method.GetCustomAttributes(typeof(AllowAnonymousAttribute), false).Count() > 0;
            MethodHasAllowGuest = method.GetCustomAttributes(typeof(AllowGuestAttribute), false).Count() > 0;

            foreach (var atr in controllerAttributes)
            {
                AuthAttributes.Add((SessionTokenAuthorizeAttribute)atr);
                ControllerAttributes.Add((SessionTokenAuthorizeAttribute)atr);
            }

            foreach (var atr in methodAttributes)
            {
                MethodAuthAttributes.Add((SessionTokenAuthorizeAttribute)atr);
                MethodAttributes.Add((SessionTokenAuthorizeAttribute)atr);
            }
        }
    }
}
