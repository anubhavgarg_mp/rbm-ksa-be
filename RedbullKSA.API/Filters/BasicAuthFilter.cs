﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.API.Attributes;
using RedbullKSA.Common.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.API.Filters
{
    /// <summary>
    /// Basic auth filter (for SMS, etc.)
    /// </summary>
    public class BasicAuthFilter : IAsyncAuthorizationFilter
    {
        /// <summary>
        /// Services
        /// </summary>
        private readonly IServiceProvider _services;

        /// <summary>
        /// Configuration
        /// </summary>
        private readonly ConfigurationSettings _config;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        public BasicAuthFilter(IServiceProvider services)
        {
            _services = services;
            _config = _services.GetRequiredService<ConfigurationSettings>();
        }

        /// <summary>
        /// Default method
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
#pragma warning disable 1998
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            if (!HasBasicAuthAttribute(context))
                return;

            // Let through local requests, no need to block developer
            if (_config.General.IsLocal())
                return;

            string authHeader = context.HttpContext.Request.Headers["Authorization"];

            if (HasBasicAuthHeader(authHeader) && IsAuthenticated(authHeader))
                return;

            // Return authentication type (causes browser to show login dialog)
            context.HttpContext.Response.Headers["WWW-Authenticate"] = "Basic";
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            context.Result = new UnauthorizedResult();

            return;
        }
#pragma warning restore 1998

        private bool HasBasicAuthHeader(string authHeader)
        {
            if (!string.IsNullOrEmpty(authHeader) && authHeader.StartsWith("Basic "))
                return true;

            return false;
        }

        private bool IsAuthenticated(string authHeader)
        {
            var endcodedCredentials = authHeader.Split(' ')[1]?.Trim();

            var credentials = Encoding.UTF8.GetString(Convert.FromBase64String(endcodedCredentials));

            if (_config.General.BasicAuth == credentials)
                return true;

            return false;
        }

        private bool HasBasicAuthAttribute(FilterContext context)
        {
            var actionDescriptor = (ControllerActionDescriptor)context.ActionDescriptor;

            var method = actionDescriptor.MethodInfo;
            var methodAttributes = method.GetCustomAttributes(typeof(BasicAuthAttribute), true);

            // Method attributes override controller attribues
            if (methodAttributes.Any())
                return true;

            var controller = actionDescriptor.ControllerTypeInfo;
            var controllerAttributes = controller.GetCustomAttributes(typeof(BasicAuthAttribute), true);

            if (controllerAttributes.Any())
                return true;

            return false;
        }
    }
}
