﻿using Microsoft.AspNetCore.Authorization;
using NJsonSchema;
using NSwag;
using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;
using RedbullKSA.API.Attributes;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using RedbullKSA.Entities.Enums;
using System;

namespace RedbullKSA.API.Filters
{
    public class AddRequiredHeaderParameter : IOperationProcessor
    {
        public bool Process(OperationProcessorContext context)
        {
            var allowAnonymous = context.ControllerType.GetCustomAttribute<AllowAnonymousAttribute>() != null
                || context.MethodInfo.GetCustomAttribute<AllowAnonymousAttribute>() != null;

            var allowGuest = context.ControllerType.GetCustomAttribute<AllowGuestAttribute>() != null
                || context.MethodInfo.GetCustomAttribute<AllowGuestAttribute>() != null;

            if (allowAnonymous)
            {
                context.OperationDescription.Operation.Security = new List<OpenApiSecurityRequirement>();
            }

            if (!allowGuest)
            {
                var security = context.OperationDescription.Operation.Security.FirstOrDefault(x => x.ContainsKey("GUEST"));
                if (security != null)
                {
                    context.OperationDescription.Operation.Security.Remove(security);
                }
            }

            context.OperationDescription.Operation.Parameters.Add(
                new OpenApiParameter
                {
                    Name = "X-DeviceType",
                    Kind = OpenApiParameterKind.Header,
                    IsRequired = true,
                    Schema = new JsonSchema()
                    {
                        Type = JsonObjectType.String
                    },
                    Description = $"Possible values: {string.Join(", ", Enum.GetNames(typeof(SessionTokenType)))}",
                    Default = SessionTokenType.Web.ToString()
                });

            context.OperationDescription.Operation.Parameters.Add(
                new OpenApiParameter
                {
                    Name = "X-Language",
                    Kind = OpenApiParameterKind.Header,
                    IsRequired = true,
                    Schema = new JsonSchema
                    {
                        Type = JsonObjectType.String,
                    },
                    Description = $"Possible values: {string.Join(", ", Enum.GetNames(typeof(ServiceLanguage)))}",
                    Default = ServiceLanguage.AR.ToString()
                });

            return true;
        }
    }
}