﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to manage administrator users
    /// </summary>
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.AdministratorsView)]
    public class AdministratorController : BaseWebController
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public AdministratorController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _userRepository = services.GetUserRepository();
            _userService = services.GetUserService();
        }

        /// <summary>
        /// Get administrator users based on submitted parameters
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET
        ///
        /// </remarks>
        /// <param in="query" name="parameters"></param>
        /// <returns>Returns administrator grid view list</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<AdministratorGridView>))]
        public async Task<IActionResult> GetGridView([FromQuery] AdministratorParameters parameters) => Ok(await _userRepository.GetAdministratorGridView(parameters));

        /// <summary>
        /// Get administrator user totals based on submitted parameters
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /totals
        ///
        /// </remarks>
        /// <param in="query" name="parameters"></param>
        /// <returns>Returns administrator grid totals</returns>
        [HttpGet("totals")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(AdministratorTotals))]
        public async Task<IActionResult> GetTotals([FromQuery] AdministratorParameters parameters) => Ok(await _userRepository.GetAdministratorTotals(parameters));

        /// <summary>
        /// Add a new administrator user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///         "firstName": "string"
        ///         "lastName": "string"
        ///         "email": "string"
        ///         "phoneNumber": "+37065456456"
        ///         "permissions": [
        ///             "manageAdministrators"
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <param in="body" name="details"></param>
        /// <returns>Returns the new administrator user details</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, typeof(AdministratorDetailView))]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.AdministratorsWrite)]
        public async Task<IActionResult> AddNew([FromBody] AdministratorDetailView details) => Ok(await _userService.AddAdministrator(details, GetCurrentUserId().Value));

        /// <summary>
        /// Get an administrator user's details and permissions
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /{id}
        ///
        /// </remarks>
        /// <param in="path" name="id"></param>
        /// <returns>Returns administrator's details and permissions</returns>
        [HttpGet("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(AdministratorDetailView))]
        public async Task<IActionResult> GetDetails(int id) => Ok(await _userService.GetAdministratorDetails(id));

        /// <summary>
        /// Edit an administrator user's details and permissions
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /{id}
        ///     {
        ///         "firstName": "string"
        ///         "lastName": "string"
        ///         "email": "string"
        ///         "phoneNumber": "+37065456456"
        ///         "status": "active"
        ///         "permissions": [
        ///             "visibility1",
        ///             "visibility2"
        ///         ]
        ///     }
        ///
        /// </remarks>
        /// <param in="path" name="id"></param>
        /// <param in="body" name="details"></param>
        /// <returns>Returns administrator's details and permissions</returns>
        [HttpPut("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(AdministratorDetailView))]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.AdministratorsWrite)]
        public async Task<IActionResult> EditDetails(int id, [FromBody] AdministratorDetailView details) => Ok(await _userService.EditAdministratorDetails(id, details, GetCurrentUserId().Value));

        /// <summary>
        /// Exports administrator grid view based on submitted parameters.
        /// !!! REQUIRES Session Token as 'Authorization' URL query parameter along with other parameters.
        /// F.e. url.com/api/administrator/xls?Authorization=Bearer 7JLWUWP5IIWPIXIRXAVH4JYXSOR7B6JPX2XXWJK4ZW5VPBDXQTSA====.AIAAAAA=.D4AAA
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet("xls")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(FileContentResult))]
        public async Task<IActionResult> GetXls([FromQuery] AdministratorParameters parameters)
        {
            parameters.StripPaging();
            var administrators = await _userRepository.GetAdministratorGridView(parameters);
            return ToXls(administrators.Select(a => new AdministratorXlsModel(a)), "administrators");
        }
    }
}
