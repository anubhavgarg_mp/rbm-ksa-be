﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Base
{
    /// <summary>
    /// Public external API controller
    /// </summary>
    [Consumes("application/json")]
    public class BaseApiController : BaseController
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public BaseApiController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            
        }

    }
}
