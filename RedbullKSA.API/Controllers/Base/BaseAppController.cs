﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.API.Extensions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Services.Base;
using System;

namespace RedbullKSA.API.Controllers.Base
{
    /// <summary>
    /// Base app controller
    /// </summary>
    public abstract class BaseAppController : BaseController
    {
        private const string X_LANGUAGE = "X-Language";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public BaseAppController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
        }

        /// <summary>
        /// Returns current client's id
        /// </summary>
        [NonAction]
        public int? GetCurrentClientId() => GetCurrentUserSessionToken()?.ClientId;

        /// <summary>
        /// Gets client's phone number
        /// </summary>
        [NonAction]
        public string GetClientPhoneNumber() => GetCurrentUserSessionToken()?.PhoneNumber;

        /// <summary>
        /// Gets client's language
        /// </summary>
        [NonAction]
        public ServiceLanguage GetClientLanguage() =>
                Request.Headers[X_LANGUAGE].ToString().StringToEnum<ServiceLanguage>();

        /// <summary>
        /// Gets client's app version
        /// </summary>
        [NonAction]
        public Version GetClientVersion() =>
            new Version(Request.Headers["X-AppVersion"].ToString());
    }
}
