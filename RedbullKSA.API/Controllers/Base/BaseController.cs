﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RedbullKSA.API.Extensions;
using RedbullKSA.Common.Configuration;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Identity;
using System;
using System.IO;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Base
{
    /// <summary>
    /// Base controller
    /// </summary>
    public abstract class BaseController : Controller
    {
        /// <summary>
        /// Services
        /// </summary>
        protected readonly IServiceProvider _services;

        /// <summary>
        /// Logger
        /// </summary>
        protected readonly ILogger _logger;

        /// <summary>
        /// Environment
        /// </summary>
        protected readonly IWebHostEnvironment _env;

        /// <summary>
        /// Configuration
        /// </summary>
        protected readonly ConfigurationSettings _config;

        /// <summary>
        /// Memory chach
        /// </summary>
        protected readonly IMemoryCache _cache;

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public BaseController(IServiceProvider services, IMemoryCache cache)
        {
            _services = services;
            _logger = _services.GetRequiredService<ILoggerFactory>().CreateLogger(this.GetType());
            _env = _services.GetRequiredService<IWebHostEnvironment>();
            _config = services.GetService<ConfigurationSettings>();
            _cache = cache;
        }

        /// <summary>
        /// Return current user session token
        /// </summary>
        [NonAction]
        public SessionToken GetCurrentUserSessionToken() => GetCurrentUserSessionTokenIdentity()?.SessionToken;

        /// <summary>
        /// Get Identity. Used all over the controller.
        /// </summary>
        [NonAction]
        public SessionTokenIdentity GetCurrentUserSessionTokenIdentity()
        {
            var identity = HttpContext?.User?.Identity;

            if (identity == null)
                return null;

            if (!(identity is SessionTokenIdentity))
                return null;

            var sesionTokenIdentity = (SessionTokenIdentity)identity;

            return sesionTokenIdentity;
        }

        /// <summary>
        /// Async copies a byte stream from the Request body into a byte array
        /// </summary>
        [NonAction]
        public async Task<byte[]> StreamToArray(Stream body)
        {
            var destination = new MemoryStream();

            await body.CopyToAsync(destination);

            using (destination)
                return destination.ToArray();
        }

        /// <summary>
        /// Gets client's type
        /// </summary>
        [NonAction]
        public SessionTokenType GetClientDeviceType() =>
            Request.GetClientDeviceType().Value;

    }
}
