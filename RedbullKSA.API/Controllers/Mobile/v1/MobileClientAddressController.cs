﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.AddressModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// Controller to manage client addresses
    /// </summary>
    [Route("api/mobile/v1/client/addresses")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileClientAddressController : BaseAppController
    {
        private readonly IClientAddressService _clientAddressService;

        public MobileClientAddressController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _clientAddressService = services.GetClientAddressService();
        }

        /// <summary>
        /// Create client address
        /// </summary>
        /// <param name="request">address</param>
        /// <returns>Client address</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ClientAddressDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<ClientAddressDTO>> Create([FromBody] CreateClientAddressRequest request) 
            => Ok(await _clientAddressService.Create(GetCurrentClientId(), request, GetClientLanguage()));

        /// <summary>
        /// Update client address
        /// </summary>
        /// <param name="request">address</param>
        /// <returns></returns>
        [HttpPut]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ClientAddressDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task Update([FromBody] UpdateClientAddressRequest request)
        {
            await _clientAddressService.Update(GetCurrentClientId(), request, GetClientLanguage());
        }

        /// <summary>
        /// Get client address by id
        /// </summary>
        /// <param name="id">API address id</param>
        /// <returns>Client address</returns>
        [HttpGet("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ClientAddressDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<ClientAddressDTO>> Get(int id)
        {
            return Ok(await _clientAddressService.Get(GetCurrentClientId(), id, GetClientLanguage()));
        }

        /// <summary>
        /// Get client shipping address
        /// </summary>
        /// <returns>Shipping address</returns>
        [HttpGet("shipping")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ClientAddressDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<ClientAddressDTO>> GetShippingAddress()
            => Ok(await _clientAddressService.GetShippingAddress(GetCurrentClientId(), GetClientLanguage()));

        /// <summary>
        /// Get all client addresses
        /// </summary>
        /// <returns>List of client address</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<ClientAddressDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<IEnumerable<ClientAddressDTO>>> GetAll()
            => Ok(await _clientAddressService.GetAll(GetCurrentClientId(), GetClientLanguage()));

        /// <summary>
        /// Delete client address
        /// </summary>
        /// <param name="id">API address id</param>
        /// <returns></returns>
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, typeof(void))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task Delete(int id)
        {
            await _clientAddressService.Delete(GetCurrentClientId(), id);
        }
    }
}
