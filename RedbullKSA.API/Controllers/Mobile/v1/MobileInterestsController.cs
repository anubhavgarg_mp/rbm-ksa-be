﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// Mobile controller to manage interests
    /// </summary>
    [Route("api/mobile/v1/interests")]
    [SessionTokenAuthorize(SessionTokenType.App)]
    public class MobileInterestsController : BaseAppController
    {
        private readonly IInterestRepository _interestRepository;
        private readonly IInterestService _interestService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileInterestsController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _interestRepository = services.GetInterestRepository();
            _interestService = services.GetInterestService();
        }

        /// <summary>
        /// Returns a list of interests for user to choose from
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET
        ///     
        /// </remarks>
        /// <returns>Object containing list of available interests together with information which ones are already selected</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ClientInterestsMobile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetInterests(WebParameters parameters)
        {
            return Ok(
                ClientInterestsMobile.FromInterestList(
                    await _interestRepository.Get(parameters), 
                    await _interestRepository.GetClientInterests(GetCurrentClientId().Value), 
                    GetClientLanguage()
                )
           );
        }

        /// <summary>
        /// Save client interests selections. If client already has selected interests, new selections will override old ones.
        /// </summary>
        /// <param name="request"></param>
        /// Sample request:
        /// 
        ///     POST /interests
        ///     {
        ///         "interests": [1, 2, 3]
        ///     }
        ///     
        /// <returns>HTTP200 if saving was successfull</returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> SaveSelected([FromBody] ClientInterestsRequest request)
        {
            await _interestService.SaveSelected(request, GetCurrentClientId().Value);

            return Ok();
        }
    }
}
