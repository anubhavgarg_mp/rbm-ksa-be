﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.CarouselModels;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// App controller to manage interests
    /// </summary>
    [Route("api/mobile/v1/carousel")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileCarouselController : BaseAppController
    {
        private readonly ICarouselEntityService _carouselEntityService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileCarouselController(IServiceProvider services, IMemoryCache cache) : base(services, cache) => 
            _carouselEntityService = services.GetCarouselEntityService();

        /// <summary>
        /// Get active carousel entities
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET
        ///
        /// </remarks>
        /// <returns>Returns a list of active carousel entities</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<IEnumerable<MobileCarouselEntity>>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetCarousel(WebParameters parameters) => 
            Ok(await _carouselEntityService.GetActive(parameters, GetClientLanguage()));
    }
}
