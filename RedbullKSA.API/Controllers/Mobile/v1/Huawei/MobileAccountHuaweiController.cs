﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1.Huawei
{
    /// <summary>
    /// App controller to query Huawei for Account information
    /// </summary>
    [Route("api/mobile/v1/huawei/account")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]

    public class MobileAccountHuaweiController : BaseAppController
    {
        private readonly IAccountHuaweiService _accountHuaweiService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileAccountHuaweiController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _accountHuaweiService = services.GetAccountHuaweiService();
        }

        /// <summary>
        /// This endpoint is used to query balance and free unit for account.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("queryBalanceAndFreeUnit")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(AccountBalanceAndFreeUnitDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> QueryBalanceAndFreeUnit(QueryBalanceAndFreeUnitRequest request)
            => Ok(await _accountHuaweiService.QueryBalanceAndFreeUnit(request));
    }
}
