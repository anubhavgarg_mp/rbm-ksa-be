﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models.SubscriberServices;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1.Huawei
{
    /// <summary>
    /// App controller to query Huawei for Subscriber information
    /// </summary>
    [Route("api/mobile/v1/huawei/subscriber")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileSubscriberHuaweiController : BaseAppController
    {
        private readonly ISubscriberHuaweiService _subscriberHuaweiService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileSubscriberHuaweiController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _subscriberHuaweiService = services.GetSubscriberHuaweiService();
        }

        /// <summary>
        /// This interface is used to change the subscriber’s information.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Order's information</returns>
        [HttpPost("changeSubscriberInformation")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ChangeSubInfoRspMsg))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> ChangeSubscriberInformation(ChangeSubscriberInformation request)
            => Ok(await _subscriberHuaweiService.ChangeSubscriberInformation(request));

        /// <summary>
        /// When creating a new customer or a new subscriber, NGBSS will generate the customer identity,
        /// subscriber identity, account identity, order identity etc. The external application may need
        /// these identity to fulfill the subsequence business process.
        /// </summary>
        /// <param name="serviceNumber">User's service number</param>
        /// <returns>List of entity ids</returns>
        [HttpGet("entityIds")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(QueryEntityIdsRspMsg))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> QueryEntityIds(string serviceNumber)
            => Ok(await _subscriberHuaweiService.QueryEntityIds(serviceNumber));

        /// <summary>
        /// Enables client to receive all information about subscriber
        /// </summary>
        /// <param name="request"></param>
        /// <returns>All subscriber's information</returns>
        [HttpGet("subscriberAllInformation")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(QuerySubAllInfoRspMsg))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> QuerySubscriberAllInformation([FromBody] QuerySubscriberAllInformationRequest request)
            => Ok(await _subscriberHuaweiService.QuerySubscriberAllInfo(request));

        /// <summary>
        /// Enables client to receive information list about subscriber
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Subscriber's info</returns>
        [HttpGet("subscriberMateDates")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(QuerySubMateDatesRspMsg))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> QuerySubscriberMateDates([FromBody] QuerySubscriberMateDatesRequest request)
            => Ok(await _subscriberHuaweiService.QuerySubscriberMateDates(request.Objects));
    }
}
