﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1.Huawei
{
    /// <summary>
    /// App controller to query Huawei Inventory endpoint
    /// </summary>
    [Route("api/mobile/v1/huawei/inventory")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileInventoryHuaweiController : BaseAppController
    {
        private readonly IInventoryHuaweiService _inventoryHuaweiService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileInventoryHuaweiController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _inventoryHuaweiService = services.GetInventoryHuaweiService();
        }

        /// <summary>
        /// Used to get a status of SIM card.
        /// </summary>
        /// <param name="iccid">ICCID of a SIM card</param>
        /// <param name="puk">PUK code of SIM card</param>
        /// <returns>SIM status</returns>
        [HttpGet("simStatus")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(int))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetSIMStatus([FromQuery][BindRequired] string iccid, string puk = null)
        {
            return Ok(await _inventoryHuaweiService.QuerySIMStatus(iccid, puk));
        }

        /// <summary>
        /// Used to get specific quantity of available service numbers.
        /// </summary>
        /// <param name="numberLevel">Either Platinum, Gold, Silver or Standard</param>
        /// <param name="pattern">Selection criteria for available number(pattern match).
        ///E.g.Pattern ‘%7777’ represents the service numbers with last four digits of '7777’.</param>
        /// <param name="count">Count of numbers to return. Default 20 if not provided</param>
        /// <returns>List of available service numbers</returns>
        [HttpGet("availableNumbers")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(List<long>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetAvailableNumbers([FromQuery][BindRequired] string numberLevel, string pattern = "", int count = 20) {
            return Ok(await _inventoryHuaweiService.QueryAvailableNumbers(pattern, numberLevel, count));
        }

        /// <summary>
        /// Used to get list of avalable number tier
        /// </summary>
        /// <returns>List of number tiers</returns>
        [HttpGet("availableNumberTiers")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(List<AvailableNumberTierDto>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public IActionResult GetAvailableNumberTiers() => Ok(_inventoryHuaweiService.AvailableNumberTiers());

        /// <summary>
        /// Used to pick or unpick MSISDN
        /// </summary>
        /// <returns>Integer idicating operation success. 1- success; 0 - partial success; -1 - failure.</returns>
        [HttpGet("operateMsisdn")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(int))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> OperateMsisdn([BindRequired] MsisdnOperationType operationType, [BindRequired] List<string> msisdns)
            => Ok(await _inventoryHuaweiService.QueryOperateMsisdn(operationType, msisdns));
    }
}
