﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1.Huawei
{
    /// <summary>
    /// App controller to query Huawei for customer information
    /// </summary>
    [Route("api/mobile/v1/huawei/customer")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileCustomerHuaweiController : BaseAppController
    {
        private readonly ICustomerHuaweiService _customerHuaweiService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileCustomerHuaweiController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _customerHuaweiService = services.GetCustomerHuaweiService();
        }

        /// <summary>
        /// Get customer information by Customer Id or Service Number. Only one parameter is required.
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="serviceNumber"></param>
        /// <returns>Customer's information</returns>
        [HttpGet("customerInformation")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(CustomerInformationDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> QueryCustomerInformation([FromQuery] string customerId = null, string serviceNumber = null)
        {
            return Ok(await _customerHuaweiService.QueryCustomerInformation(customerId, serviceNumber));
        }
    }
}
