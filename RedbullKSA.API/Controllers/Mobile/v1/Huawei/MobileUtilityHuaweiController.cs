﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1.Huawei
{
    /// <summary>
    /// App Controller to query Huawei Utility endpoints
    /// </summary>
    [Route("api/mobile/v1/huawei/utility")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileUtilityHuaweiController : BaseAppController
    {
        private readonly IUtilityHuaweiService _utilityHuaweiService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileUtilityHuaweiController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _utilityHuaweiService = services.GetUtilityHuaweiService();
        }

        /// <summary>
        /// Calculate one off charges for the order, i.e. business charge,
        /// offering subscription fee, MSISDN charge, SIM charge 
        /// </summary>
        /// <returns>Calculated one off charges</returns>
        [HttpPost("calOneOffFee")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OneOffFeeDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> CalOneOffFee([FromBody] CalOneOffFeeRequest request)
        {
            return Ok(await _utilityHuaweiService.QueryCalOneOffFee(request));
        }
    }
}
