﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models.RechargeServices;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1.Huawei
{
    /// <summary>
    /// App controller to query Huawei Recharge endpoint
    /// </summary>
    [Route("api/mobile/v1/huawei/recharge")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileRechargeHuaweiController : BaseAppController
    {
        private readonly IRechargeHuaweiService _rechargeService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        /// <param name="rechargeService"></param>
        public MobileRechargeHuaweiController(IServiceProvider services, IMemoryCache cache, IRechargeHuaweiService rechargeService) : base(services, cache) 
        {
            _rechargeService = rechargeService;
        }

        /// <summary>
        /// Used for a subscriber to recharge by the credit card, debit card or the cash.
        /// </summary>
        /// <param name="rechargeModel"></param>
        /// <returns>User's balance informaton</returns>
        [HttpPost("rechargeByCash")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(RechargeDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> RechargeByCash([FromBody] RechargeModel rechargeModel) => 
            Ok(await _rechargeService.QueryRechargeByCash(rechargeModel));

        /// <summary>
        /// Used for a subscriber to recharge by voucher
        /// </summary>
        /// <param name="rechargeModel"></param>
        /// <returns>User's balance informaton</returns>
        [HttpPost("rechargeByVoucher")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(RechargeByVoucherRspMsg))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> RechargeByVoucher([FromBody] RechargeVoucherModel rechargeModel) =>
            Ok(await _rechargeService.QueryRechargeByVoucher(rechargeModel));

        /// <summary>
        /// Used to query certain amount of recharge transactions
        /// </summary>
        /// <param name="rechargeLog"></param>
        /// <returns>List of recharge transactions</returns>
        [HttpPost("rechargeLog")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<RechargeLogEntryDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetRechargeLog([FromBody] RechargeLogModel rechargeLog) =>
            Ok(await _rechargeService.QueryRechargeLog(rechargeLog));
    }
}
