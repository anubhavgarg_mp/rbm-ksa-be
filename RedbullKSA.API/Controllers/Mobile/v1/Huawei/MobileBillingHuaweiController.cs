﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models.XMLResponses.BillingServices;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1.Huawei
{
    /// <summary>
    /// App Controller to query Huawei Billing endpoints
    /// </summary>
    [Route("api/mobile/v1/huawei/billing")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileBillingHuaweiController : BaseAppController
    {
        private readonly IBillingHuaweiService _billingHuaweiService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileBillingHuaweiController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _billingHuaweiService = services.GetBillingHuaweiService();
        }

        /// <summary>
        /// Query subsriber's unbilled amount
        /// </summary>
        /// <returns>Calculated one off charges</returns>
        [HttpPost("unbilledAmount")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(QueryUnbilledAmountRspMsg))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<QueryUnbilledAmountRspMsg>> QueryUnbilledAmount([FromBody] UnbilledAmountRequest request)
            => Ok(await _billingHuaweiService.QueryUnbilledAmount(request));
    }
}
