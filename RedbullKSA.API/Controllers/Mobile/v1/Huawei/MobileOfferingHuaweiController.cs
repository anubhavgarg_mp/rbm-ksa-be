﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to query Huawei Offering endpoint
    /// </summary>
    [Route("api/mobile/v1/huawei/offering")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobileOfferingHuaweiController : BaseWebController
    {
        private readonly IOfferingHuaweiService _offeringService;

        public MobileOfferingHuaweiController(IServiceProvider services, IMemoryCache cache, IOfferingHuaweiService offeringService) : base(services, cache)
        {
            _offeringService = offeringService;
        }

        #region PRIMARY
        /// <summary>
        /// Used to query the primary offering that subscriber has purchased.
        /// </summary>
        /// <param name="objectId">User's service number</param>
        /// <returns></returns>
        [HttpGet("purchasedPrimaryOffering")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(PurchasedOfferingDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetPurchasedPrimaryOffering([FromQuery] string objectId) =>
            Ok(await _offeringService.QueryPurchasedPrimaryOffering(objectId));

        /// <summary>
        /// Used to change user's primary offering
        /// </summary>
        /// <param name="primaryOffering">User's service number</param>
        /// <returns>Order's id</returns>
        [HttpPut("changePrimaryOffering")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> ChangePrimaryOffering([FromBody] ChangeOffering primaryOffering) =>
            Ok(await _offeringService.ChangePrimaryOffering(primaryOffering));

        /// <summary>
        /// Used to query available primary offerings
        /// </summary>
        /// <param name="objectId">User's service number</param>
        /// <returns>List of available primary offerings</returns>
        [HttpGet("availablePrimaryOfferings")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<AvailableOfferingDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> QueryAvailablePrimaryOfferings([FromQuery] string objectId) =>
            Ok(await _offeringService.QueryAvailablePrimaryOfferings(objectId));
        #endregion

        #region SUPPLEMENTARY
        /// <summary>
        /// Used to query supplementary offerings that subscriber has purchased.
        /// </summary>
        /// <param name="objectId">User's service number</param>
        /// <returns>List of purchased supplementary offerings</returns>
        [HttpGet("purchasedSupplementaryOfferings")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(PurchasedOfferingDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetSupplementaryOfferings([FromQuery] string objectId) =>
            Ok(await _offeringService.QueryPurchasedSupplementaryOfferings(objectId));

        /// <summary>
        /// Used to change user's supplementary offerings.
        /// </summary>
        /// <param name="supplementaryOffering">User's service number</param>
        /// <returns>Order's id</returns>
        [HttpPut("changeSupplementaryOffering")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(string))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> ChangeSupplementaryOffering([FromBody] ChangeOffering supplementaryOffering) =>
            Ok(await _offeringService.ChangeSupplementaryOffering(supplementaryOffering));

        /// <summary>
        /// Used to query available supplementary offerings
        /// </summary>
        /// <param name="objectId">User's service number</param>
        /// <returns>List of available supplementary offerings</returns>
        [HttpGet("availableSupplementaryOfferings")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<AvailableOfferingDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> QueryAvailableSupplementaryOfferings([FromQuery] string objectId) =>
            Ok(await _offeringService.QueryAvailableSupplementaryOfferings(objectId));

        /// <summary>
        /// Used to query all supplementary offerings
        /// </summary>
        /// <returns>List of all supplementary offerings</returns>
        [HttpGet("allSupplementaryOfferings")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<SupplementaryOfferingDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> QueryAllSupplementaryOfferings() =>
            Ok(await _offeringService.QueryAllSupplementaryOfferings());
        #endregion
    }
}
