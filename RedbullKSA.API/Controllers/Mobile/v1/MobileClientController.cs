﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// Controller to manage mobile users
    /// </summary>
    [Route("api/mobile/v1/user")]
    [SessionTokenAuthorize(SessionTokenType.App)]
    public class MobileClientController : BaseAppController
    {
        private readonly IClientService _clientService;
        private readonly ISessionTokenService _sessionTokenService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileClientController (IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _clientService = services.GetClientService();
            _sessionTokenService = services.GetSessionTokenService();
        }

        /// <summary>
        /// Returns client's profile
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /profile
        ///     
        /// </remarks>
        /// <returns>Returns clients profile</returns>
        [HttpGet("profile")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ClientProfileMobile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<ClientProfileMobile>> GetProfile() => Ok(await _clientService.GetMobileProfile(GetCurrentClientId()));

        /// <summary>
        /// Logs out client
        /// </summary>
        /// <remarks>
        /// Sample request
        /// 
        ///     POST /logout
        ///
        /// </remarks>
        /// <returns></returns>
        [HttpPost("logout")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> Logout()
        {
            await _sessionTokenService.LogoutClient(GetCurrentClientId().Value);
            return Ok();
        }

        /// <summary>
        /// Changes client's password
        /// </summary>
        /// <param in="body" name="changePasswordDto"></param>
        /// <returns></returns>
        [HttpPost("changePassword")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO changePasswordDto)
        {
            await _clientService.ChangePassword(changePasswordDto, GetCurrentClientId().Value);
            return Ok();
        }

    }
}
