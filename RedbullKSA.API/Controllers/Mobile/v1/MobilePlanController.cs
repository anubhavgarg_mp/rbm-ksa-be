﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.PlanModels.Mobile;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// Controller to manage client plans
    /// </summary>
    [Route("api/mobile/v1/plan")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobilePlanController : BaseAppController
    {
        private readonly IPlanService _planService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobilePlanController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _planService = services.GetPlanService();
        }

        /// <summary>
        /// Get all plans
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<PlansMobile>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<IEnumerable<PlansMobile>>> GetAllPlans()
            => Ok(await _planService.GetAvailablePlans(GetClientPhoneNumber(), GetClientLanguage()));
    }
}
