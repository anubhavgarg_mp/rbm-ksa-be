﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.Auth.Mobile;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// Handles mobile authentication
    /// </summary>
    [Route("api/mobile/v1/auth")]
    public class MobileAuthController : BaseAppController
    {
        private readonly IPhoneNumberConfirmationService _phoneNumberConfirmationService;
        private readonly IAuthService _authService;
        private readonly ISessionTokenService _sessionTokenService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileAuthController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _phoneNumberConfirmationService = services.GetPhoneNumberConfirmationService();
            _authService = services.GetAuthService();
            _sessionTokenService = services.GetSessionTokenService();
        }

        /// <summary>
        /// Log In user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /login
        ///     {
        ///        "login": "string"
        ///        "password": "string"
        ///     }
        ///
        /// </remarks>
        /// <param in="body" name="login"></param>
        /// <returns>Returns a newly created session token</returns>
        [HttpPost("login")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(TokenMobile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> Login([FromBody] LoginMobileWithPassword login)
        {
            var deviceType = GetClientDeviceType();
            return Ok(new TokenMobile(await _authService.Login(login, deviceType)));
        }

        /// <summary>
        /// Get guest token, which enables quering certain endpoints for unauthenticated user
        /// </summary>
        /// <returns>Guest session token</returns>
        [HttpGet("guestToken")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(TokenGuest))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GuestToken()
        {
            return Ok(new TokenGuest(await _sessionTokenService.GenerateGuestTokenMobile()));
        }

        /// <summary>
        /// Send confirmation code to provided phone number
        /// </summary>
        /// <param in="body" name="phoneNumber"></param>
        /// <returns></returns>
        [HttpPost("sendVerificationCode")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> SendConfirmationCode([FromBody] PhoneNumberMobile phoneNumber)
        {
            await _phoneNumberConfirmationService.SendConfirmationCode(phoneNumber, GetClientLanguage());
            return Ok();
        }

        /// <summary>
        /// Validates if verification code matches with a phone number
        /// and if it's a new user then returns null(to continue registration), otherwise returns session token
        /// </summary>
        /// <param in="body" name="login"></param>
        /// <returns></returns>
        [HttpPost("verifyCode")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(TokenMobile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> Login([FromBody] LoginMobile login)
        {
            var deviceType = GetClientDeviceType();
            return Ok(new TokenMobile(await _authService.Login(login, deviceType)));
        }

        /// <summary>
        /// Register a new user
        /// </summary>
        /// <param in="body" name="registration"></param>
        /// <returns>A newly created session token</returns>
        [HttpPost("register")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(TokenMobile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> Register([FromBody] RegistrationMobile registration)
        {
            //Gets language from X-Language header
            var language = GetClientLanguage();
            var deviceType = GetClientDeviceType();
            var client = await _authService.SignUp(registration, language);

            return Ok(new TokenMobile(await _sessionTokenService.GenerateToken(client.Id, registration.PhoneNumber, deviceType)));
        }
    }
}
