﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.PaymentModels.DTO;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// Controller that responsible for interaction with payment data
    /// </summary>
    [Route("api/mobile/v1")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobilePaymentController : BaseAppController
    {
        private readonly IPaymentService _paymentService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobilePaymentController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _paymentService = services.GetPaymentService();
        }

        /// <summary>
        /// Creates new sdk token of payment system
        /// </summary>
        /// <param name="deviceId">Unique device identifier generated from the Payment SDK.</param>
        /// <returns></returns>
        [SessionTokenAuthorize(SessionTokenType.App)]
        [HttpGet("sdkToken")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(SdkTokenDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> CreateSdkToken([FromQuery] string deviceId)
        {
            var userLanguage = GetClientLanguage().GetDescription();

            return Ok(await _paymentService.CreateSdkToken(deviceId, userLanguage));
        }
    }
}
