﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// Mobile controller to manage benefits
    /// </summary>
    [Route("api/mobile/v1/benefits")]
    [SessionTokenAuthorize(SessionTokenType.App)]
    public class MobileBenefitsController : BaseAppController
    {
        private readonly IBenefitRepository _benefitRepository;
        private readonly IBenefitService _benefitService;
        private readonly IRedeemedBenefitService _redeemedBenefitService;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileBenefitsController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _benefitRepository = services.GetBenefitRepository();
            _benefitService = services.GetBenefitService();
            _redeemedBenefitService = services.GetRedeemedBenefitService();
        }

        /// <summary>
        /// Returns client's redeemed benefits
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /history?dateFrom={from}&amp;dateTo={to}&amp;perPage={perPage}&amp;page={page}
        ///     
        /// </remarks>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet("history")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(RedeemedBenefitsHistoryMobile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetRedeemHistory(ExtendedWebParameters parameters)
        {
            var benefits = (await _benefitRepository.GetRedeemedBenefitsMobile(GetCurrentClientId().Value, parameters));
            var language = GetClientLanguage();

            foreach (var benefit in benefits)
                benefit.SetTitle(language);

            return Ok(new RedeemedBenefitsHistoryMobile(benefits));
        }

        /// <summary>
        /// Returns client's benefits offers
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /offers
        ///     
        /// </remarks>
        /// <returns></returns>
        [HttpGet("offers")] 
        [AllowGuest]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<BenefitOffersViewMobile>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetBenefitOffers(BenefitOfferParameters parameters) 
            => Ok(await _benefitService.GetBenefitOffersMobile(parameters, GetClientLanguage(), GetCurrentClientId()));

        /// <summary>
        /// Returns client's benefits details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /{id}
        ///     
        /// </remarks>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowGuest]
        [SwaggerResponse(HttpStatusCode.OK, typeof(BenefitDetailsMobile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetBenefitDetails(int id) 
            => Ok(await _benefitService.GetMobileDetails(id, GetClientLanguage()));

        /// <summary>
        /// Redeem benefit
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /redeem
        ///     {
        ///         "benefitId": 4,
        ///         "deliveryAddress": "street 12",
        ///         "questions" : [
        ///             {
        ///                 "questionId": 1,
        ///                 "questionAnswerIds": [1,2]
        ///             }
        ///         ]
        ///
        ///     }
        /// </remarks>
        /// <returns></returns>
        [HttpPost("redeem")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(RedeemedBenefitViewMobile))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> Redeem([FromBody] RedeemBenefitAddMobile redeem) 
            => Ok(await _redeemedBenefitService.Redeem(redeem, GetCurrentClientId().Value));
    }
}
