﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.OrderModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    [Route("api/mobile/v1/order")]
    [SessionTokenAuthorize(SessionTokenType.App)]
    public class MobileOrderController : BaseAppController
    {
        private readonly IOrderService _orderService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobileOrderController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _orderService = services.GetOrderService();
        }

        /// <summary>
        /// Used to save order information when client chooses home delivery/pick from store delivery options when activating sim.
        /// </summary>
        /// <returns></returns>
        [HttpPost("save")]
        [AllowGuest]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> SaveSimOrderInformation([FromBody] SimOrderRequest request)
        {
            if (request == null)
                throw new KSAException(KSAExceptionCode.General.RequestBodyIsNull);

            await _orderService.Add(GetCurrentClientId().Value, request);

            return Ok();
        }
    }
}
