﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.Portability;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Mobile.v1
{
    /// <summary>
    /// Mobile controller for number porting operations
    /// </summary>
    [Route("api/mobile/v1/portability")]
    [SessionTokenAuthorize(SessionTokenType.App | SessionTokenType.Web)]
    public class MobilePortabilityController : BaseAppController
    {
        private readonly INumberPortabilityService _portabilityService;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public MobilePortabilityController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _portabilityService = services.GetNumberPortabilityService();
        }

        /// <summary>
        /// Request a number to be ported
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("port")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(NumberPortingResponse))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> PortNumber([FromBody] PortNumberRequest request)
            => Ok(await _portabilityService.PortNumber(request, GetClientLanguage()));
    }
}
