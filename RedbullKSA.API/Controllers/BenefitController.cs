﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to manage benefits
    /// </summary>
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.BenefitsView)]
    public class BenefitController : BaseWebController
    {
        private readonly IBenefitRepository _benefitRepository;
        private readonly IBenefitService _benefitService;
        private readonly IRedeemedBenefitService _redeemedBenefitService;
        private readonly IRedeemedBenefitRepository _redeemedBenefitRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public BenefitController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _benefitRepository = services.GetBenefitRepository();
            _benefitService = services.GetBenefitService();
            _redeemedBenefitService = services.GetRedeemedBenefitService();
            _redeemedBenefitRepository = services.GetBenefitRedeemedRepository();
        }

        /// <summary>
        /// Gets benefits grid view with submitted parameters
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /benefit
        ///
        /// </remarks>
        /// <param name="parameters"></param>
        /// <returns>Returns benefit grid view</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<BenefitGridView>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetBenefitGridView([FromQuery] BenefitParameters parameters) 
            => Ok(await _benefitRepository.GetGridView(parameters));

        /// <summary>
        /// Gets benefit grid totals based on submitted parameters
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /benefit/totals
        ///
        /// </remarks>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet("totals")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(BenefitTotals))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetTotals([FromQuery] BenefitParameters parameters) 
            => Ok(await _benefitRepository.GetBenefitTotals(parameters));

        /// <summary>
        /// Gets benefit details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /benefit/{id}
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Returns benefit details</returns>
        [HttpGet("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(BenefitWithAttachmentsDetails))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetBenefitDetails(int id) 
            => Ok(await _benefitService.GetDetails(id));

        /// <summary>
        /// Add benefit with updating attachments
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /
        /// 
        ///    "availableQuantity": 10000,
        ///    "type": "experience",
        ///    "category": "giveaways",
        ///    "segments": [1, 15, 201],
        ///    "status": "active",
        ///    "distributionType": "guestList",
        ///    "isShownToGuests": true,
        ///    "lotteryIsRandom": false,
        ///    "startDate": "2019-04-03T00:00:00",
        ///    "endDate": "2019-04-01T00:00:00",
        ///    "eventDate": "2019-04-01T00:00:00",
        ///    "skuCode": "code",
        ///    "benefitEN": {
        ///        "name": "dsf",
        ///        "description": "description",
        ///        "secondaryDescription": "sec description",
        ///        "location": "location",
        ///        "teaser": {
        ///            "id": 34,
        ///            "fileUrl": "teaserUrl",
        ///            "category": "benefitTeaser",
        ///            "language": "en"
        ///        },
        ///        "main": {
        ///            "id": 23,
        ///            "fileUrl": "mainUrl",
        ///            "category": "benefitMain",
        ///            "language": "en"
        ///        },
        ///        "galleryCover": {
        ///            "id": 25,
        ///            "fileUrl": "coverUrl",
        ///            "category": "benefitGalleryCover",
        ///            "language": "en"
        ///        },
        ///        "header": {
        ///            "id": 32,
        ///            "fileUrl": "headerUrl",
        ///            "category": "benefitHeader",
        ///            "language": "en"
        ///        },
        ///        "galleryImages": [
        ///            {
        ///                "id": 27,
        ///                "fileUrl": "galleryImage",
        ///                "category": "benefitGalleryImage",
        ///                "language": "en"
        ///            },
        ///            {
        ///                "id": 29,
        ///                "fileUrl": "imageUrl",
        ///                "category": "benefitGalleryImage",
        ///                "language": "en"
        ///            }
        ///        ]
        ///    },
        ///    "benefitAR": {
        ///        "name": "name",
        ///        "description": "description",
        ///        "location": "loaction",
        ///        "secondaryDescription": "sec description",
        ///        "teaser": {
        ///            "id": 31,
        ///            "fileUrl": "Url",
        ///            "category": "benefitTeaser",
        ///            "language": "ar"
        ///        },
        ///        "main": {
        ///            "id": 24,
        ///            "fileUrl": "Url",
        ///            "category": "benefitMain",
        ///            "language": "ar"
        ///        },
        ///        "galleryCover": {
        ///            "id": 26,
        ///            "fileUrl": "Url",
        ///            "category": "benefitGalleryCover",
        ///            "language": "ar"
        ///        },
        ///        "header": {
        ///            "id": 33,
        ///            "fileUrl": "Url",
        ///            "category": "benefitHeader",
        ///            "language": "ar"
        ///        },
        ///        "galleryImages": [
        ///            {
        ///                "id": 28,
        ///                "fileUrl": "Url",
        ///                "category": "benefitGalleryImage",
        ///                "language": "ar"
        ///            },
        ///            {
        ///                "id": 30,
        ///                "fileUrl": "Url",
        ///                "category": "benefitGalleryImage",
        ///                "language": "ar"
        ///            },
        ///            {
        ///                "id": 35,
        ///                "fileUrl": "Url",
        ///                "category": "benefitGalleryImage",
        ///                "language": "ar"
        ///            }
        ///        ]
        ///    }
        ///     
        /// </remarks>
        /// <param name="details"></param>
        /// <param name="file"></param>
        /// <returns>Returns added benefit details</returns>
        [HttpPost]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.BenefitsWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(BenefitWithAttachmentsDetails))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> Add([FromForm] BenefitWithAttachmentsDetails details, IFormFile file) 
            => Ok(await _benefitService.Add(details, ReadVoucherBenefitExcel(file, details.Type), GetCurrentUserId().Value));

        /// <summary>
        /// Update benefit with attachments
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /{id}
        /// 
        ///    "id": 7,  
        ///    "availableQuantity": 10000,
        ///    "type": "experience",
        ///    "category": "giveaways",
        ///    "segments": [1, 15, 201],
        ///    "status": "active",
        ///    "distributionType": "guestList",
        ///    "lotteryIsRandom": false,
        ///    "isShownToGuests": true,
        ///    "startDate": "2019-04-03T00:00:00",
        ///    "endDate": "2019-04-01T00:00:00",
        ///    "eventDate": "2019-04-01T00:00:00",
        ///    "skuCode": "code",
        ///    "benefitEN": {
        ///        "name": "dsf",
        ///        "description": "description",
        ///        "secondaryDescription": "sec description",
        ///        "location": "location",
        ///        "teaser": {
        ///            "id": 34,
        ///            "fileUrl": "teaserUrl",
        ///            "category": "benefitTeaser",
        ///            "language": "en"
        ///        },
        ///        "main": {
        ///            "id": 23,
        ///            "fileUrl": "mainUrl",
        ///            "category": "benefitMain",
        ///            "language": "en"
        ///        },
        ///        "galleryCover": {
        ///            "id": 25,
        ///            "fileUrl": "coverUrl",
        ///            "category": "benefitGalleryCover",
        ///            "language": "en"
        ///        },
        ///        "header": {
        ///            "id": 32,
        ///            "fileUrl": "headerUrl",
        ///            "category": "benefitHeader",
        ///            "language": "en"
        ///        },
        ///        "galleryImages": [
        ///            {
        ///                "id": 27,
        ///                "fileUrl": "galleryImage",
        ///                "category": "benefitGalleryImage",
        ///                "language": "en"
        ///            },
        ///            {
        ///                "id": 29,
        ///                "fileUrl": "imageUrl",
        ///                "category": "benefitGalleryImage",
        ///                "language": "en"
        ///            }
        ///        ]
        ///    },
        ///    "benefitAR": {
        ///        "name": "name",
        ///        "description": "description",
        ///        "location": "loaction",
        ///        "secondaryDescription": "sec description",
        ///        "teaser": {
        ///            "id": 31,
        ///            "fileUrl": "Url",
        ///            "category": "benefitTeaser",
        ///            "language": "ar"
        ///        },
        ///        "main": {
        ///            "id": 24,
        ///            "fileUrl": "Url",
        ///            "category": "benefitMain",
        ///            "language": "ar"
        ///        },
        ///        "galleryCover": {
        ///            "id": 26,
        ///            "fileUrl": "Url",
        ///            "category": "benefitGalleryCover",
        ///            "language": "ar"
        ///        },
        ///        "header": {
        ///            "id": 33,
        ///            "fileUrl": "Url",
        ///            "category": "benefitHeader",
        ///            "language": "ar"
        ///        },
        ///        "galleryImages": [
        ///            {
        ///                "id": 28,
        ///                "fileUrl": "Url",
        ///                "category": "benefitGalleryImage",
        ///                "language": "ar"
        ///            },
        ///            {
        ///                "id": 30,
        ///                "fileUrl": "Url",
        ///                "category": "benefitGalleryImage",
        ///                "language": "ar"
        ///            },
        ///            {
        ///                "id": 35,
        ///                "fileUrl": "Url",
        ///                "category": "benefitGalleryImage",
        ///                "language": "ar"
        ///            }
        ///        ]
        ///    }
        ///     
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="details"></param>
        /// <returns>Returns updated benefit details</returns>
        [HttpPut("{id}")]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.BenefitsWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(BenefitWithAttachmentsDetails))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> Update(int id, [FromBody] BenefitWithAttachmentsDetails details) 
            => Ok(await _benefitService.Update(id, details, GetCurrentUserId().Value));

        /// <summary>
        /// Gets benefit redeemed details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /benefit/redeemed/{id}
        ///
        /// </remarks>
        /// <param name="id"></param>
        /// <returns>Returns benefit redeemed details</returns>
        [HttpGet("redeemed/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(BenefitRedeemedDetails))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetBenefitRedeemedDetails(int id) 
            => Ok(await _benefitRepository.GetBenefitRedeemedDetails(id));

        /// <summary>
        /// Gets a list of benefits that are eligible for selecting winners.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /benefit/eligible
        ///
        /// </remarks>
        /// <returns></returns>
        [HttpGet("eligible")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<BenefitEligibleSelectView>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetEligibleBenefits() => Ok(await _redeemedBenefitRepository.GetEligible());

        /// <summary>
        /// Randomly selects winners of a single selected benefit.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /benefit/pickwinners
        ///
        /// </remarks>
        /// <param in="query" name="id"></param>
        /// <param in="body" name="model"></param>
        /// <returns></returns>
        [HttpPost("{id}/pickwinners")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> PickWinners(int id, [FromBody] BenefitWinnerSelectModel model)
        {
            await _redeemedBenefitService.PickWinners(id, model.NumberOfWinners);

            return Ok();
        }

        /// <summary>
        /// Exports benefit grid view based on submitted parameters.
        /// !!! REQUIRES Session Token as 'Authorization' URL query parameter along with other parameters.
        /// F.e. url.com/api/benefit/xls?Authorization=Bearer 7JLWUWP5IIWPIXIRXAVH4JYXSOR7B6JPX2XXWJK4ZW5VPBDXQTSA====.AIAAAAA=.D4AAA
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet("xls")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(FileContentResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetXls([FromQuery] BenefitParameters parameters)
        {
            parameters.StripPaging();
            var benefits = await _benefitRepository.GetGridView(parameters);
            return ToXls(benefits.Select(a => new BenefitXlsModel(a)), "benefits");
        }

        [NonAction]
        private Stream ReadVoucherBenefitExcel(IFormFile file, BenefitType type)
        {
            if (type != BenefitType.Voucher)
                return null;

            if (file == null)
                throw new KSAException(KSAExceptionCode.Benefit.NoExcelFileImported);

            if (!file.FileName.EndsWith(".xlsx"))
                throw new KSAException(KSAExceptionCode.Upload.FileTypeInvalid);

            return file.OpenReadStream();
        }
    }
}
