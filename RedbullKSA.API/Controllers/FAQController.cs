﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.FAQModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to manage frequently asked questions
    /// </summary>
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.FAQView)]
    public class FAQController : BaseWebController
    {
        private readonly IFAQService _faqService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public FAQController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _faqService = services.GetFAQService();
        }

        /// <summary>
        /// Create FAQ
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.FAQWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(FAQDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<FAQDTO>> Create([FromBody]CreateFAQRequest request)
        {
            return Ok(await _faqService.Create(request));
        }

        /// <summary>
        /// Update FAQ
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.FAQWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(void))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task Update([FromBody]UpdateFAQRequest request)
        {
            await _faqService.Update(request);
        }

        /// <summary>
        /// Get list of FAQ
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<FAQView>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<IEnumerable<FAQView>>> GetAll() => Ok(await _faqService.GetAll());

        /// <summary>
        /// Get FAQ by id
        /// </summary>
        /// <param name="id">FAQ id</param>
        /// <returns></returns>
        [HttpGet("id")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(FAQDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<FAQDTO>> Get(int id) => await _faqService.Get(id);

        /// <summary>
        /// Delete FAQ
        /// </summary>
        /// <param name="id">FAQ id</param>
        /// <returns></returns>
        [HttpDelete]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.FAQWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(void))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task Delete(int id) => await _faqService.Delete(id);
    }
}
