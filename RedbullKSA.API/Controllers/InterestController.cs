﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.ClientModels;
using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to manage interests
    /// </summary>
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.InterestsView)]
    public class InterestController : BaseWebController
    {
        private readonly IInterestService _interestService;
        private readonly IInterestRepository _interestRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public InterestController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _interestService = services.GetInterestService();
            _interestRepository = services.GetInterestRepository();
        }

        /// <summary>
        /// Get interests based on submitted parameters
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET
        ///
        /// </remarks>
        /// <param in="query" name="parameters"></param>
        /// <returns>Returns interest grid view list</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<InterestGridView>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetGridView([FromQuery] InterestParameters parameters) => Ok(await _interestRepository.GetGridView(parameters));

        /// <summary>
        /// Get interest's details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /{id}
        ///
        /// </remarks>
        /// <param in="path" name="id"></param>
        /// <returns>Returns interest's details</returns>
        [HttpGet("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(InterestDetailsView))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetDetails(int id) => Ok(InterestDetailsView.FromInterest(await _interestRepository.Get(id)));

        /// <summary>
        /// Edit interest's details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /{id}
        ///     {
        ///         "name": {
        ///             "arabicName": "string",
        ///             "englishName": "string"
        ///         },
        ///         "englishImageId": 17,
        ///         "arabicImageId": 19,
        ///         "status": "active"
        ///     }
        ///
        /// </remarks>
        /// <param in="path" name="id"></param>
        /// <param in="body" name="details"></param>
        /// <returns>Returns the updated interest's details</returns>
        [HttpPut("{id}")]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.InterestsWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(InterestDetailsView))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> EditDetails(int id, [FromBody] InterestDTO details) => Ok(await _interestService.EditDetails(id, details, GetCurrentUserId().Value));

        /// <summary>
        /// Add a new interest
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST 
        ///     {
        ///         "name": {
        ///             "arabicName": "string",
        ///             "englishName": "string"
        ///         },
        ///         "englishImageId": 17,
        ///         "arabicImageId": 19,
        ///         "status": "active"
        ///     }
        ///
        /// </remarks>
        /// <param in="body" name="details"></param>
        /// <returns>Returns the newly created interest's details</returns>
        [HttpPost]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.InterestsWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(InterestDetailsView))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> AddInterest([FromBody] InterestDTO details) => Ok(await _interestService.AddInterest(details, GetCurrentUserId().Value));

        /// <summary>
        /// Gets interest totals based on parameters
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /totals 
        ///  
        /// </remarks>
        /// <param name="parameters"></param>
        /// <returns>Returns interest grid view totals</returns>
        [HttpGet("totals")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(InterestTotals))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetInterestTotals([FromQuery] InterestParameters parameters) => Ok(await _interestRepository.GetInterestTotals(parameters));

        /// <summary>
        /// Exports interest grid view based on submitted parameters.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpGet("xls")]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.InterestsView)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(FileContentResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetXls([FromQuery] InterestParameters parameters)
        {
            parameters.StripPaging();
            var interests = await _interestRepository.GetGridView(parameters);
            return ToXls(interests.Select(i => new InterestXlsModel(i)), "interests");
        }

        /// <summary>
        /// Exports users list for a single interest entity.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("xls/{id}")]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.InterestsView)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(FileContentResult))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetXlsForInterest(int id)
        {
            var interestClients = await _interestRepository.GetInterestClients(id);
            return ToXls(interestClients.Select(i => new ClientXlsModel(i)), "interest clients");
        }
    }
}
