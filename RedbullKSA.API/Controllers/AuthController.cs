﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.API.Models.Auth.Open;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to handle authentication
    /// </summary>
    [Route("api/[controller]")]
    public class AuthController : BaseWebController
    {
        private readonly IUserService _userService;
        private readonly ISessionTokenService _sessionTokenService;
        private readonly IAuthService _authService;
        private readonly IUserClaimRepository _userClaimRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public AuthController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _userService = services.GetUserService();
            _sessionTokenService = services.GetSessionTokenService();
            _authService = services.GetAuthService();
            _userClaimRepository = services.GetUserClaimRepository();
        }

        /// <summary>
        /// Request password reset by passing an email.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /requestPasswordReset
        ///     {
        ///        "email": "string"
        ///     }
        ///     
        /// </remarks>
        /// <param in="body" name="emailModel"></param>
        /// <returns>Returns status code 200 if successful</returns>
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        [HttpPost("requestPasswordReset")]
        [AllowAnonymous]
        public async Task<IActionResult> RequestPasswordReset([FromBody] EmailModel emailModel)
        {
            await _userService.RequestPasswordReset(emailModel.Email);

            return Ok();
        }

        /// <summary>
        /// Reset password by token.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /resetPassword
        ///     {
        ///        "password": "string",
        ///        "repeatPassword": "string",
        ///        "resetToken": "string"
        ///     }
        ///
        /// </remarks>
        /// <param in="body" name="resetPasswordModel"></param>
        /// <returns>Returns Status code 200 in success case</returns>
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        [HttpPost("resetPassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel resetPasswordModel)
        {
            await _userService.ResetPassword(resetPasswordModel);

            return Ok();
        }

        /// <summary>
        /// Log In user
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /login
        ///     {
        ///        "email": "string"
        ///        "password": "string"
        ///     }
        ///
        /// </remarks>
        /// <param in="body" name="login"></param>
        /// <returns>Returns a newly created session token</returns>
        [HttpPost("login")]
        [AllowAnonymous]
        [SwaggerResponse(HttpStatusCode.OK, typeof(TokenModel))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            var deviceType = GetClientDeviceType();
            return Ok(await _authService.Login(login, deviceType));
        }

        /// <summary>
        /// Logout user
        /// </summary>
        /// <returns></returns>
        [HttpPost("logout")]
        [SessionTokenAuthorize(SessionTokenType.Admin)]
        public async Task<IActionResult> Logout()
        {
            await _sessionTokenService.LogoutUser(GetCurrentUserId().Value, SessionTokenType.Admin);
            return Ok();
        }

        /// <summary>
        /// Check if a reset token for resetting/creating password is valid.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /tokenStatus
        ///     {
        ///         "resetToken": "string"
        ///     }
        ///     
        /// </remarks>
        /// <param in="body" name="token"></param>
        /// <returns>Returns boolean indicating if reset token is valid</returns>
        [SwaggerResponse(HttpStatusCode.OK, typeof(bool))]
        [HttpPost("tokenStatus")]
        [AllowAnonymous]
        public async Task<IActionResult> CheckTokenStatus([FromBody] ResetTokenModel token) => Ok(await _userService.GetResetTokenStatus(token));

        /// <summary>
        /// Get admin user claims by session token.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /claims
        ///     
        /// </remarks>
        /// <returns>Returns the iteration of current user claims</returns>
        [SwaggerResponse(HttpStatusCode.OK, typeof(AdminClaimModel))]
        [HttpGet("claims")]
        [SessionTokenAuthorize(SessionTokenType.Admin)]
        public async Task<IActionResult> GetClaims()
        {
            var userId = GetCurrentUserId();
            var claims = Enumerable.Empty<UserClaim>();

            if (userId.HasValue)
                claims = await _userClaimRepository.Get(userId.Value);

            return Ok(new AdminClaimModel(claims));
        }

        /// <summary>
        /// Generates new API key (needs warning on front-end)
        /// </summary>
        /// <returns></returns>
        [SwaggerResponse(HttpStatusCode.OK, typeof(ApiKeyModel))]
        [HttpPost("apiKey")]
        [SessionTokenAuthorize(SessionTokenType.Admin)]
        public async Task<IActionResult> GenerateApiKey()
        {
            var sessionToken = await _sessionTokenService.GenerateApiKeyToken(GetCurrentUserId().Value);

            return Ok(new ApiKeyModel(sessionToken, true));
        }
    }
}
