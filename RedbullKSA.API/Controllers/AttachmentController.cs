﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.AttachmentModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller for attachment handling
    /// </summary>
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Admin)]
    public class AttachmentController : BaseWebController
    {
        private readonly IAttachmentService _attachmentService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public AttachmentController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _attachmentService = services.GetAttachmentService();
        }

        /// <summary>
        /// Adds a new attachment entity and uploads the file to the local server storage
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /{en}/{planImage}
        ///     Content-Type: multipart/form-data
        ///     Body: Form data with attached file.    
        ///
        /// </remarks>
        /// <param name="language">Attachment language</param>
        /// <param name="category">Attachment category</param>
        /// <returns>The newly created entity as an Attachment class object</returns>
        [HttpPost("{language}/{category}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(Attachment))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<Attachment> UploadAttachment(ServiceLanguage language, AttachmentCategory category)
        {
            var result = await StreamToArray(Request.Form.Files[0].OpenReadStream());

            return await _attachmentService.CreateAttachment(result, category, GetCurrentUserId(), language);
        }
    }
}
