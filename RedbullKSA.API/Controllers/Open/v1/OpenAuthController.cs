﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.Auth.Open;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers.Open.v1
{
    /// <summary>
    /// Public auth controller
    /// </summary>
    [Route("api/open/v1/auth")]
    [AllowAnonymous]
    public class OpenAuthController : BaseApiController
    {
        private readonly IAuthService _authService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public OpenAuthController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _authService = services.GetAuthService();
        }

        /// <summary>
        /// Generates new API key
        /// </summary>
        /// <param name="details" in="body"></param>
        /// <returns></returns>
        [HttpPost("key")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(ApiKeyModel))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GenerateApiKey([FromBody] OpenLoginModel details)
        {
            var sessionToken = await _authService.Login(details);

            return Ok(new ApiKeyModel(sessionToken, true));
        }
    }
}
