﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.CarouselModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to manage the carousel
    /// </summary>
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.CarouselView)]
    public class CarouselController : BaseWebController
    {
        private readonly ICarouselEntityService _carouselEntityService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public CarouselController(IServiceProvider services, IMemoryCache cache) : base(services, cache) {
            _carouselEntityService = services.GetCarouselEntityService();
        }

        /// <summary>
        /// Get all carousel entities
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /all
        ///
        /// </remarks>
        /// <returns>Returns a list of all carousel entities</returns>
        [HttpGet("all")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<CarouselListViewEntity>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetCarouselEntities() =>
            Ok(await _carouselEntityService.GetAll());


        /// <summary>
        /// Edit carousel entity's details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /{id}
        ///     {
        ///         "Title" : {
        ///             "English" : "string",
        ///             "Arabic" : "string"
        ///         },
        ///         "Description" : {
        ///             "English" : "string",
        ///             "Arabic" : "string"
        ///         },
        ///         "Status" : "active",
        ///         "EnglishImageId" : 1,
        ///         "ArabicImageId" : 1
        ///     }
        ///
        /// </remarks>
        /// <param in="path" name="id"></param>
        /// <param in="body" name="details"></param>
        /// <returns>Returns the updated carousel entity's details</returns>
        [HttpPut("{id}")]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.CarouselWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(CarouselEntityDetailsView))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> EditCarouselEntity(int id, [FromBody] CarouselEntityDTO details) =>
            Ok(await _carouselEntityService.Update(id, details, GetCurrentUserId().Value));

        /// <summary>
        /// Get carousel entity's details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /{id}
        ///
        /// </remarks>
        /// <param in="path" name="id"></param>
        /// <returns>Returns the carousel entity's details</returns>
        [HttpGet("{id}")]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.CarouselWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(CarouselEntityDetailsView))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> GetEntityDetails(int id) => Ok(await _carouselEntityService.Get(id));

        /// <summary>
        /// Get carousel entity's details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST
        ///     {
        ///         "Title" : {
        ///             "English" : "string",
        ///             "Arabic" : "string"
        ///         },
        ///         "Description" : {
        ///             "English" : "string",
        ///             "Arabic" : "string"
        ///         },
        ///         "Status" : "active",
        ///         "EnglishImageId" : 1,
        ///         "ArabicImageId" : 1
        ///     }
        ///
        /// </remarks>
        /// <returns>Returns the new carousel entity's details</returns>
        [HttpPost]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.CarouselWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(CarouselEntityDetailsView))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<IActionResult> AddEntity([FromBody] CarouselEntityDTO details) => Ok(await _carouselEntityService.AddEntity(details, GetCurrentUserId().Value));
    }
}
