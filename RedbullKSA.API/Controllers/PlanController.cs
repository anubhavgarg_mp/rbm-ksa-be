﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.PlanModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to manage plans
    /// </summary>
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.PlansView)]
    public class PlanController : BaseWebController
    {
        private readonly IPlanService _planService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public PlanController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _planService = services.GetPlanService();
        }

        /// <summary>
        /// Create plan
        /// </summary>
        /// <param name="request">plan</param>
        /// <returns></returns>
        [HttpPost]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.PlansWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<PlanDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<PlanDTO>> CreatePlan([FromBody] CreatePlanRequest request)
        {
            request.UserId = GetCurrentUserId() ?? 0;
            return Ok(await _planService.Create(request));
        }

        /// <summary>
        /// Update plan
        /// </summary>
        /// <param name="request">plan</param>
        /// <returns></returns>
        [HttpPut]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.PlansWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(void))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task UpdatePlan([FromBody] UpdatePlanRequest request)
        {
            request.UserId = GetCurrentUserId() ?? 0;
            await _planService.Update(request);
        }

        /// <summary>
        /// Get all plans
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<PlanGridView>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<IEnumerable<PlanGridView>>> GetAllPlans()
            => Ok(await _planService.GetAllPlans());

        /// <summary>
        /// Get a plan details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /{id}
        ///
        /// </remarks>
        /// <param in="path" name="id"></param>
        /// <returns>Returns plan's details</returns>
        [HttpGet("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(PlanGridView))]
        public async Task<IActionResult> GetPlanDetails(int id)
            => Ok(await _planService.GetPlanDetails(id));
    }
}
