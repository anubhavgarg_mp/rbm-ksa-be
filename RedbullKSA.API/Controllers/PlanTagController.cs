﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Base;
using RedbullKSA.Entities.API.Models.PlanModels.Tags;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller to manage plan tags
    /// </summary>
    [Route("api/plan/tag")]
    [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.PlansView)]
    public class PlanTagController : BaseWebController
    {
        private readonly IPlanTagService _planTagService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public PlanTagController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _planTagService = services.GetPlanTagService();
        }

        /// <summary>
        /// Create plan tag
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.PlansWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(PlanTagDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<PlanTagDTO>> Create([FromBody] CreatePlanTagRequest request)
        {
            return Ok(await _planTagService.Create(request));
        }

        /// <summary>
        /// Update plan tag
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        [SessionTokenAuthorize(SessionTokenType.Admin, AdminClaimType.PlansWrite)]
        [SwaggerResponse(HttpStatusCode.OK, typeof(PlanTagDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task Update([FromBody] UpdatePlanTagRequest request)
        {
            await _planTagService.Update(request);
        }

        /// <summary>
        /// Get plan tag
        /// </summary>
        /// <param name="id">Tag id</param>
        /// <returns></returns>
        [HttpGet("id")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(PlanTagDTO))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<PlanTagDTO>> Get(int id)
        {
            return Ok(await _planTagService.Get(id));
        }

        /// <summary>
        /// Get all plan tags
        /// </summary>
        /// <returns>List of plan tags</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(IEnumerable<PlanTagDTO>))]
        [SwaggerResponse(HttpStatusCode.BadRequest, typeof(ErrorResponseModelDTO))]
        public async Task<ActionResult<IEnumerable<PlanTagDTO>>> GetAll()
        {
            return Ok(await _planTagService.GetAll());
        }
    }
}
