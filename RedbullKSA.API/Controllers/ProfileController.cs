﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using NSwag.Annotations;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Net;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    /// <summary>
    /// Web controller for user profile
    /// </summary>
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Admin)]
    public class ProfileController : BaseWebController
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public ProfileController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _userRepository = services.GetUserRepository();
            _userService = services.GetUserService();
        }

        /// <summary>
        /// Changes a user's password
        /// </summary>
        /// <param in="body" name="changePasswordDto"></param>
        /// <returns></returns>
        [HttpPost("changePassword")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(OkResult))]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO changePasswordDto)
        {
            await _userService.ChangePassword(changePasswordDto, GetCurrentUserId().Value);
            return Ok();
        }

        /// <summary>
        /// Gets user profile details
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET
        ///     
        /// </remarks>
        /// <returns>Returns users profile details</returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(UserProfileDetailsView))]
        public async Task<IActionResult> GetProfile() => Ok(new UserProfileDetailsView(await _userRepository.Get(GetCurrentUserId().Value)));
    }
}
