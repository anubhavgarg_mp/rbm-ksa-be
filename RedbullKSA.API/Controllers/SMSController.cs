﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.API.Attributes;
using RedbullKSA.API.Controllers.Base;
using RedbullKSA.Entities.API.Models.SmsModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.API.Controllers
{
    [Route("api/[controller]")]
    [SessionTokenAuthorize(SessionTokenType.Api)]
    public class SMSController : BaseWebController
    {
        private readonly ISmsService _smsService;

        /// <summary>
        /// Controller
        /// </summary>
        /// <param name="services"></param>
        /// <param name="cache"></param>
        public SMSController(IServiceProvider services, IMemoryCache cache) : base(services, cache)
        {
            _smsService = services.GetSmsService();
        }

        /// <summary>
        /// Endpoint to send SMS message from number A to number B
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("send")]
        public async Task<IActionResult> SendSMS([FromBody] SendSMSRequestModel request)
        {
            var message = await _smsService.Send(request);

            return Ok(new { Message = message });
        }

        /// <summary>
        /// Check if number is already present in the database
        /// </summary>
        /// <param name="msisdn">Number to check</param>
        /// <returns></returns>
        [HttpGet("isPresent")]
        public async Task<IActionResult> IsPresent(string msisdn)
            => Ok(await _smsService.IsPresent(msisdn));
    }
}
