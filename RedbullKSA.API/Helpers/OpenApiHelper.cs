﻿using NSwag;
using NSwag.AspNetCore;
using System;

namespace RedbullKSA.API.Helpers
{
    /// <summary>
    /// Helper class for API Documentation
    /// </summary>
    public class OpenApiHelper
    {
        // OpenApi const
        private const string JSON_ROUTE_UI3 = "./swagger/v1/swagger.json";
        private const string UI3_ROUTE = "/swagger";
        private const string DOCUMENT_NAME = "v1";

        // Redoc consts
        private const string REDOC_ROUTE = "/redoc";

        // Common consts
        private const string DESCRIPTION = "Red Bull KSA API";
        private const string VERSION = "1.0.0";
        private const string TITLE = "RBK";

        // OpenApi authorization
        private const string HEADER_TITLE = "HEADER";
        private const string HEADER_DESCRIPTION = "You can use authorization tokens";
        private const string HEADER_NAME = "Authorization";

        /// <summary>
        /// Configures OpenApi
        /// </summary>
        /// <returns></returns>
        public static Action<OpenApiDocumentMiddlewareSettings> OpenApiConfigure()
        {
            return configure =>
            {
                configure.DocumentName = DOCUMENT_NAME;
                configure.PostProcess = (document, arg) =>
                {
                    document.Schemes = new[] { OpenApiSchema.Https };
                    document.SecurityDefinitions.Add(HEADER_TITLE, new OpenApiSecurityScheme
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = HEADER_NAME,
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = HEADER_DESCRIPTION
                    }
                    );
                    document.SecurityDefinitions.Add("GUEST", new OpenApiSecurityScheme
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = HEADER_NAME,
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = HEADER_DESCRIPTION
                    });
                    document.Info.Description = DESCRIPTION;
                    document.Info.Version = VERSION;
                    document.Info.Title = TITLE;
                };

                configure.DocumentName = DOCUMENT_NAME;
            };
        }

        /// <summary>
        /// Configures Swagger UI
        /// </summary>
        /// <returns></returns>
        public static Action<SwaggerUi3Settings> SwaggerUi3Configure() => configure => configure.Path = UI3_ROUTE;

        /// <summary>
        /// Configures ReDoc
        /// </summary>
        /// <returns></returns>
        public static Action<ReDocSettings> ReDocConfigure()
        {
            return configure =>
            {
                configure.Path = REDOC_ROUTE;
                configure.DocumentPath = JSON_ROUTE_UI3;
            };
        }
    }
}
