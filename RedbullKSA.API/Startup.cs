﻿using FluentMigrator.Runner;
using Microsoft.ApplicationInsights.SnapshotCollector;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using NSwag.Generation.Processors.Security;
using RedbullKSA.API.Filters;
using RedbullKSA.API.Helpers;
using RedbullKSA.API.Middleware;
using RedbullKSA.Common.Configuration;
using RedbullKSA.Common.Helpers.Microsoft.Extensions.Configuration;
using RedbullKSA.Services.Base;
using System;
namespace RedbullKSA.API
{
    /// <summary>
    /// API startup
    /// </summary>
    public class Startup
    {
        private readonly IWebHostEnvironment _env;
        private IConfiguration Configuration { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IWebHostEnvironment env) => _env = env;

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {

            //Add config file as singleton
            services.AddScoped(v => new ConfigurationBuilder()
                .SetBasePath(_env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{_env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build());

            var config = services.BuildServiceProvider().CreateScope().ServiceProvider.GetService<IConfigurationRoot>();
            services.Configure<ConfigurationSettings>(config);
            services.AddTransient(s => s.GetService<IOptions<ConfigurationSettings>>().Value);

            // Repos and services
            services.ConfigureServices(config);
            services.AddNamedHttpClients(config);
            services.AddApplicationInsightsTelemetry(config);

            services.AddSnapshotCollector(sc => new SnapshotCollectorConfiguration()
            {
                IsEnabledInDeveloperMode = true,
                SnapshotsPerDayLimit = 50
            });

            services.AddMemoryCache();

            services.AddCors();

            var mvc = services.AddMvc(v =>
            {
                // Order is important here!!
                v.Filters.Add<BasicAuthFilter>();
                v.Filters.Add<SessionTokenAuthenticateFilter>();
                v.Filters.Add<SessionTokenAuthorizeFilter>();
                v.EnableEndpointRouting = false;
            });

            services.AddControllers().AddNewtonsoftJson(
                opt =>
                {
                    opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                    opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            // Fluent migrator   
            services
                .AddFluentMigratorCore()
                .ConfigureRunner(rb => rb
                    .AddSqlServer2012()
                    .WithGlobalConnectionString(config.GetDefaultConnection())
                    // Define the assembly containing the migrations
                    .ScanIn(typeof(M201901231233_FirstMigration).Assembly).For.Migrations())
                // Enable logging to console in the FluentMigrator way
                .AddLogging(lb => lb.AddFluentMigratorConsole())
                .BuildServiceProvider(false);

            // Migrate
            var servicesMigrationScope = services.BuildServiceProvider().CreateScope().ServiceProvider;
            UpdateDatabase(servicesMigrationScope);

            services.AddOpenApiDocument(options =>
            {
                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("HEADER"));
                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("GUEST"));
                options.OperationProcessors.Add(new AddRequiredHeaderParameter()); 
            });

            if (config.GetValue<bool>("General:UseSmppLicense"))
                services.AddSmppLicense();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var config = app.ApplicationServices.CreateScope().ServiceProvider.GetConfig();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();
            app.UseHttpsRedirection();

            app.UseMiddleware<ApiLoggerMiddleware>();
            app.UseMiddleware<ApiExceptionMiddleware>();

            app.UseCors(options => options.WithOrigins(config.Cors.Origins).AllowAnyMethod().AllowAnyHeader());

            app.UseMvc();

            app.UseOpenApi(OpenApiHelper.OpenApiConfigure()); // serve OpenAPI/Swagger documents
            app.UseSwaggerUi3(OpenApiHelper.SwaggerUi3Configure()); // serve Swagger UI
            app.UseReDoc(OpenApiHelper.ReDocConfigure()); // serve ReDoc UI
        }

        /// <summary>
        /// Update the database
        /// </summary>
        private static void UpdateDatabase(IServiceProvider serviceProvider)
        {
            // Instantiate the runner
            var runner = serviceProvider.GetRequiredService<IMigrationRunner>();

            // Execute the migrations
            runner.MigrateUp();
        }
    }
}
