﻿using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace RedbullKSA.Common.Helpers
{
    public static class ConversionHelper
    {
        public static bool InterpretBool(string val)
        {
            if (string.IsNullOrWhiteSpace(val))
                return false;

            val = val.ToLowerInvariant();

            List<string> booleans = new List<string> { "on", "true", "t", "y", "yes", "1", "enabled" };

            if (booleans.Contains(val))
                return true;

            return false;
        }

        public static bool InterpretFalseBool(string val)
        {
            if (string.IsNullOrWhiteSpace(val))
                return false;

            val = val.ToLowerInvariant();

            List<string> booleans = new List<string> { "", "0", "false", "f", "n", "no", "off", "disabled", "null" };

            if (booleans.Contains(val))
                return true;

            return false;
        }

        /// <summary>
        /// Turns XmlDocument into a specified class (P) using XML serialization class that represents Envelope (T)
        /// </summary>
        public static P ConvertSoapToRest<T, P>(XmlDocument document) where T : class
        {
            XmlReader reader = new XmlNodeReader(document);
            var serializer = new XmlSerializer(typeof(T));
            T result = (T)serializer.Deserialize(reader);

            // The type of a class that represents Envelope
            Type envelopeType = result.GetType();

            // Name of a class that represents EnvelopeBody
            string bodyName = envelopeType.GetFields()[0].Name;

            // Value of a class that represents EnvelopeBody
            var bodyField = envelopeType.GetField(bodyName).GetValue(result);

            // Value of a class that represents message
            var MessageField = bodyField.GetType().GetFields()[0].GetValue(bodyField);

            return (P)MessageField;
        }

        public static IEnumerable<int> ConvertEnumToIntArray<T>() => Enum.GetValues(typeof(T)).Cast<int>().ToList();
        
        public static DateTime ConvertHuaweiDateToDateTime(string date) {
            if (date.Length != 14) {
                throw new KSAException(KSAExceptionCode.Huawei.UnexpectedResponse);
            }

            string year = date.Substring(0, 4);
            string month = date.Substring(4, 2);
            string day = date.Substring(6, 2);
            string hours = date.Substring(8, 2);
            string minutes = date.Substring(10, 2);
            string seconds = date.Substring(12, 2);

            string dateString = $"{year}-{month}-{day} {hours}:{minutes}:{seconds}";
            return DateTime.ParseExact(dateString, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
        }

        public static string DateTimeToHuaweiDate(DateTime date) => date.ToString("yyyyMMddHHmmss");
    }
}
