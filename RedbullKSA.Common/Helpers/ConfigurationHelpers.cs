﻿using Microsoft.Extensions.Configuration;
using System;

namespace RedbullKSA.Common.Helpers
{
    // Wrong namespace for convenience
    namespace Microsoft.Extensions.Configuration
    {
        public static class ConfigurationHelper
        {
            private const string DEFAULT_CONNECTION = "DefaultConnection";

            public static string GetDefaultConnection(this IConfigurationRoot config) => config.GetConnectionString(DEFAULT_CONNECTION);

        }
    }
}
