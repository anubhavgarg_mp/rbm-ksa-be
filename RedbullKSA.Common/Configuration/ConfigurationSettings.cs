﻿namespace RedbullKSA.Common.Configuration
{
    public class ConfigurationSettings
    {
        public GeneralConfig General { get; set; }
        public EmailConfig Email { get; set; }
        public RandomOrgConfig RandomOrg { get; set; }
        public CorsConfig Cors { get; set; }
        public SessionTokenConfig SessionToken { get; set; }
        public ConnectionStringsConfig ConnectionStrings { get; set; }
        public HuaweiConfig Huawei { get; set; }

        public Smpp Smpp { get; set; }
        public PayFortConfig PayFortConfig { get; set; }
    }

    public class GeneralConfig
    {
        public string Environment { get; set; }
        public string DevelopmentOTPCode { get; set; }
        public string BackendUrl { get; set; }
        public string BasicAuth { get; set; }
        public string AttachmentsDirectory { get; set; }
        public bool UseSmppLicense { get; set; }

        public bool IsProduction() => Environment == "Production";
        public bool IsStaging() => Environment == "Staging";
        public bool IsDevelopment() => Environment == "Development";
        public bool IsLocal() => Environment == "Local";
    }

    public class EmailConfig
    {
        public string SmtpHost { get; set; }
        public string SmtpPassword { get; set; }
        public int SmtpPort { get; set; }
        public string SmtpUsername { get; set; }
        public string DisplayUsername { get; set; }
        public string SenderAddress { get; set; }
    }

    public class RandomOrgConfig
    {
        public string BaseUrl { get; set; }
        public string ApiKey { get; set; }
    }

    public class CorsConfig
    {
        public string[] Origins { get; set; }
    }

    public class SessionTokenConfig
    {
        public int LifetimeMinutes { get; set; }
        public int ByteCount { get; set; }
    }

    public class ConnectionStringsConfig
    {
        public string DefaultConnection { get; set; }
        public string RedisConnection { get; set; }
    }

    public class HuaweiConfig
    {
        public string Version { get; set; }
        public string Channel { get; set; }
        public string PartnerId { get; set; }
        public string TimeType { get; set; }
        public string AccessUser { get; set; }
        public string AccessPassword { get; set; }
        public string URL { get; set; }
    }

    public class Smpp
    {
        public string IP { get; set; }
        public int Port { get; set; }
        public string SystemId { get; set; }
        public string Password { get; set; }
        public string SourceNumber { get; set; }
    }

    public class PayFortConfig
    {
        public string MerchantIdentifier { get; set; }
        public string AccessCode { get; set; }
        public string SHAType { get; set; }
        public string SHARequestPhrase { get; set; }
        public string ApiUrl { get; set; }
    }
}
