﻿using System.ComponentModel;

namespace RedbullKSA.Common.Enums
{
    public enum ExceptionType
    {
        [Description("System Exception")]
        System = 0,

        [Description("KSA Exception")]
        KSA = 1,
    }
}
