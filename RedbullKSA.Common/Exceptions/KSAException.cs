﻿using Microsoft.Extensions.Logging;
using RedbullKSA.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RedbullKSA.Common.Exceptions
{
    public class KSAException : Exception, IKSAException
    {
        public Enum Code { get; set; }
        public LogLevel Level { get; set; } = LogLevel.None;
        public List<KSAFieldException> FieldExceptions { get; set; } = new List<KSAFieldException>();

        public KSAException() { }

        public KSAException(Enum code)
            : base(code.GetDescription()) => Code = code;

        public KSAException(Enum code, string message)
            : base($"{code.GetDescription()} {Environment.NewLine}{message}") => Code = code;

        public KSAException(Enum code, string message, LogLevel logLevel)
            : base($"{code.GetDescription()} {Environment.NewLine}{message}")
        {
            Code = code;
            Level = logLevel;
        }

        public void AddFieldException(Enum code) => FieldExceptions.Add(new KSAFieldException(code, code.GetDescription()));

        public void AddFieldException(Enum code, string fieldName) => FieldExceptions.Add(new KSAFieldException(code, fieldName));

        public void AddFieldException(Enum code, string fieldName, string value) => FieldExceptions.Add(new KSAFieldException(code, fieldName, value));

        public void ThrowExceptionIfExists()
        {
            if (FieldExceptions.Any())
                throw this;
        }
    }

    public class KSAFieldException : Exception, IKSAException
    {
        public Enum Code { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }

        public KSAFieldException(Enum code, string field) : base(code.GetDescription())
        {
            Code = code;
            Field = field;
        }

        public KSAFieldException(Enum code, string field, string value) : base(code.GetDescription())
        {
            Code = code;
            Field = field;
            Value = value;
        }
    }

    public interface IKSAException
    {
        Enum Code { get; set; }
    }
}
