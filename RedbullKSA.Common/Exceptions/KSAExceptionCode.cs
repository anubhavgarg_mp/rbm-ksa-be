﻿using System.ComponentModel;

namespace RedbullKSA.Common.Exceptions
{
    /// <summary>
    /// Exception codes of Redbull KSA API
    /// </summary>
    public static class KSAExceptionCode
    {
        public enum General
        {
            [Description("Unknown error.")]
            UnknownError = 99999,

            [Description("Invalid request.")]
            InvalidRequest = 10000,

            [Description("Username and/or password incorrect. Try again.")]
            UserPasswordIncorrect = 10001,

            [Description("Email or phone number and/or password incorrect. Try again" )]
            ClientPasswordIncorrect = 10002,

            [Description("Passwords do not match.")]
            PasswordsDoNotMatch = 10003,

            [Description("Password must be at least 8 characters long and contain both letters and numbers.")]
            PasswordDoesNotMeetRequirements = 10004,

            [Description("New password must be different from previous password.")]
            PasswordSameAsOld = 10005,

            [Description("Data validation failed.")]
            DataValidationFailed = 10006,

            [Description("Unauthorized.")]
            Unauthorized = 10007,

            [Description("Random generator has been corrupted!")]
            RandomGeneratorIsCorrupted = 10008,

            [Description("Request body is null.")]
            RequestBodyIsNull = 10009,

            [Description("Invalid attachment category")]
            InvalidAttachmentCategory = 10010,

            [Description("File not found")]
            FileNotFound = 10011
        }

        public enum Client
        {

            [Description("Invalid e-mail address.")]
            EmailInvalid = 20000,

            [Description("E-mail already exists.")]
            EmailExists = 20001,

            [Description("Invalid phone number.")]
            PhoneInvalid = 20002,

            [Description("Phone number already exists.")]
            PhoneExists = 20003,

            [Description("Client does not exist")]
            ClientDoesNotExist = 20004,

            [Description("Verification code expired")]
            VerificationCodeExpired = 20005,

            [Description("Border or Nationality ID not provided")]
            ClientIdNotProvided = 20006,

            [Description("Verification code incorrect")]
            VerificationCodeIncorrect = 20007

        }

        public enum User
        {
            [Description("User not found.")]
            NotFound = 21000,

            [Description("Invalid e-mail address.")]
            EmailInvalid = 21001,

            [Description("E-mail already exists.")]
            EmailExists = 21002,

            [Description("Invalid phone number.")]
            PhoneInvalid = 21003,

            [Description("User cannot have write permission without a corresponding view permission.")]
            ClaimInvalid = 21004,

            [Description("User does not have the required permission to use the API.")]
            NotApiUser = 21005
        }

        public enum Interest
        {
            [Description("Interest with such name already exists.")]
            InterestNameExists = 30000,

            [Description("Interest photo missing.")]
            InterestPhotoMissing = 30001,

            [Description("Interest name is required.")]
            InterestNameMissing = 30002,

            [Description("No interests were selected.")]
            NoneSelected = 30003
        }

        public enum Upload
        {
            [Description("File not found.")]
            FileNotFound = 40000,

            [Description("File type is invalid.")]
            FileTypeInvalid = 40001,

            [Description("Bad worksheet headers.")]
            BadWorksheetHeaders = 40002,

            [Description("File has duplicates.")]
            FileHasDuplicates = 40003,

            [Description("File contents are in wrong format.")]
            WrongFormat = 40004,

            [Description("Wrong attachment ratio.")]
            WrongAttachmentRatio = 40005,

            [Description("Failed to extract thumbnail from video.")]
            FFMPEGFailed = 40006
        }

        public enum Benefit
        {
            [Description("Name is invalid.")]
            NameIsInvalid = 50000,

            [Description("Description is invalid.")]
            DescriptionIsInvalid = 50001,

            [Description("Location is invalid.")]
            LocationIsInvalid = 50002,

            [Description("Start date is invalid.")]
            StartDateIsInvalid = 50003,

            [Description("End date is invalid.")]
            EndDateIsInvalid = 50004,

            [Description("Event date is invalid.")]
            EventDateIsInvalid = 50005,

            [Description("Available quantity is invalid.")]
            AvailableQtyIsInvalid = 50006,

            [Description("SKU code is invalid.")]
            SkuCodeIsInvalid = 50007,

            [Description("Distribution type is invalid.")]
            DistributionTypeIsInvalid = 50008,

            [Description("Button url is invalid.")]
            ButtonUrlIsInvalid = 50009,

            [Description("App url is invalid.")]
            AppUrlIsInvalid = 50010,

            [Description("No benefit gallery images are chosen.")]
            NoImagesAreChosen = 50011,

            [Description("No excel file imported (.xlsx).")]
            NoExcelFileImported = 50012,

            [Description("Benefit has already been redeemed.")]
            RedeemedBenefitAlreadyOccupied = 50013,

            [Description("Delivery address is invalid.")]
            DeliveryAddressIsInvalid = 50014,

            [Description("Benefit is not available anymore.")]
            BenefitIsNotAvailableAnymore = 50015,

            [Description("Benefit expired.")]
            OldBenefitOrInactive = 50016,

            [Description("There is already occupied redeemed benefit.")]
            RedeemedBenefitAlreadyExists = 50017,

            [Description("Benefit type is not allowed to be changed. Please to deactivate a benefit and create the new one.")]
            BenefitTypeIsNotAllowedToBeChanged = 50018,

            [Description("Secondary description is invalid.")]
            SecondaryDescriptionIsInvalid = 50019,

            [Description("Winners have already been selected for this benefit.")]
            WinnerAlreadySelected = 50020,

            [Description("Not enough participants for the provided ")]
            NotEnoughParticipants = 50021,

            [Description("Winner count must be 1 or more.")]
            InvalidWinnerCount = 50022,

            [Description("No clients have redeemed this benefit.")]
            NoRedeemedBenefits = 50023,

            [Description("Description teaser is invalid.")]
            DescriptionTeaserIsInvalid = 50024,

            [Description("Benefit with the provided ID was not found.")]
            BenefitNotFound = 50025,

            [Description("Email intro text is invalid.")]
            EmailIntroIsInvalid = 50026,

            [Description("Email instructions are invalid.")]
            EmailInstructionsAreInvalid = 50027
        }

        public enum Invitation
        {

            [Description("Phone number is not a valid number.")]
            InvalidPhoneNumber = 60000,
        }

        public enum Huawei
        {
            [Description("SIM does not exist in Huawei system")]
            SIMDoesNotExist = 70001,

            [Description("Unexpected response from huawei")]
            UnexpectedResponse = 70002,

            [Description("Huawei API is unreachable at the moment.")]
            APIUnreachable = 70003,

            [Description("Invalid customer information search parameters.")]
            InvalidCustomerSearchParameters = 70004,

            [Description("Invalid service number.")]
            InvalidServiceNumber = 70005,

            [Description("No subscriber information provided.")]
            NoSubscriberSearchParameters = 70006,

            [Description("Request body is null or incorrect.")]
            RequestBodyIsNullOrIncorrect = 70007,

            [Description("Provided object id was invalid")]
            InvalidObjectId = 70008,

            [Description("Huawei response failure")]
            ResponseFailure = 70009
        }

        public enum SMS
        {
            [Description("Failure connecting to SMPP server")]
            ConnectionFailure = 80001,

            [Description("Failure binding to SMPP server")]
            BindFailure = 80002,

            [Description("Unexpected error occured while trying to send SMS")]
            UnexpectedError = 80003,

            [Description("Failed to send a message")]
            MessageSendingFailed = 80004
        }

        public enum Payment 
        {
            [Description("Response from Payment API was unsuccessful")]
            ResponseUnsuccessful = 90001,

            [Description("Request to Payment API failed")]
            RequestFailed = 90002
        }

        public enum Plan
        {
            [Description("Plan tag not found")]
            TagNotFound = 100001,

            [Description("Plan tag ID not provided")]
            TagIdNotProvided = 100002,

            [Description("Plan not found")]
            PlanNotFound = 100003,

            [Description("Plan id not provided")]
            PlanIdNotProvided = 100004,

            [Description("Plan already exists")]
            PlanExists = 100005
        }

        public enum SupplementaryOffering
        {
            [Description("Supplementary offering not found")]
            NotFound = 110001
        }
        public enum ClientAddress
        {
            [Description("Address with such name already exists.")]
            AddressNameExists = 120001,

            [Description("An unexpected error has occurred when saving address.")]
            SaveUnexpectedError = 120002,

            [Description("Address id not provided")]
            AddressIdNotProvided = 120003,

            [Description("Address not found.")]
            NotFound = 120004
        }
        public enum FAQ
        {
            [Description("FAQ not found")]
            NotFound = 130001
        }

        public enum CarouselEntity
        {
            [Description("Carousel photo missing.")]
            CarouselPhotoMissing = 140001,

            [Description("Attachments with provided ID do not exist")]
            InvalidAttachmentId = 140002,

            [Description("Carousel entity not found")]
            CarouselEntityNotFound = 140003
        }
    }
}
