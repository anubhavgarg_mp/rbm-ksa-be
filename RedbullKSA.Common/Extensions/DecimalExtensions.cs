﻿using System;

namespace RedbullKSA.Common.Extensions
{
    public static class DecimalExtensions
    {
        public static decimal ConvertToGBPrice(this decimal huaweiPrice)
        {
            return Math.Round(huaweiPrice / 10000, 2);
        }
    }
}
