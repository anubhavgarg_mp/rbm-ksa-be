﻿using System;

namespace RedbullKSA.Common.Extensions
{
    public static class DateTimeExtensions
    {
        public static long? ToUnixTimeStamp(this DateTime? date) => date.HasValue ? ToUnixTimeStamp(date.Value) : (long?)null;

        public static long ToUnixTimeStamp(this DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            date = date.ToUniversalTime();
            return (int)(date.Subtract(epoch)).TotalSeconds;
        }
    }
}
