﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Services.Exporting.Contracts
{
    public interface IExcelExportService
    {
        byte[] Export<T>(string name, List<T> records);
    }
}
