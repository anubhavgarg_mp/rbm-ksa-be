﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.Auth.Mobile;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class PhoneNumberConfirmationService : BaseService, IPhoneNumberConfirmationService
    {
        private const string APPLE_USER_PHONE = "71000568";
        private const string APPLE_USER_CODE = "7979";

        private readonly IPhoneNumberConfirmationRepository _phoneNumberConfirmationRepository;
        private readonly IRandomGeneratorService _randomGeneratorService;
        private readonly ISmsService _smsService;

        public PhoneNumberConfirmationService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _phoneNumberConfirmationRepository = services.GetPhoneNumberConfirmationRepository();
            _randomGeneratorService = services.GetRandomGeneratorService();
            _smsService = services.GetSmsService();
        }

        public async Task SendConfirmationCode(PhoneNumberMobile login, ServiceLanguage language)
        {
            login.CleanUpInputs();

            login.ValidateNumber();

            // For APPLE reviews
            if (login.PhoneNumber == APPLE_USER_PHONE)
                return;

            if (!login.PhoneNumber.IsValidPhoneNumber())
                throw new KSAException(KSAExceptionCode.Client.PhoneInvalid);

            var confirmationCodeInteger = await _randomGeneratorService.Range(1, 9999);
            var confirmationCode = confirmationCodeInteger.ToStringWithLeadingZeroes(4);

            var message = language == ServiceLanguage.EN ? $"Hi, your code is {confirmationCode}. " +
                $"Use this code to complete the registration in the Red Bull MOBILE app." : $"{confirmationCode}";

            await _smsService.Send(login.PhoneNumber, message);

            await _phoneNumberConfirmationRepository.Add(new PhoneNumberConfirmation(login.PhoneNumber, confirmationCode));
        }

        public async Task ValidateConfirmationCode(string phoneNumber, string confirmationCode)
        {
            if (_config.General.IsStaging() || _config.General.IsDevelopment())
            {
                if (confirmationCode == _config.General.DevelopmentOTPCode)
                    return;
                else
                    throw new KSAException(KSAExceptionCode.Client.VerificationCodeIncorrect);
            }

            // For APPLE reviews
            if (phoneNumber == APPLE_USER_PHONE && confirmationCode == APPLE_USER_CODE)
                return;

            var latestConfirmation = await _phoneNumberConfirmationRepository.Get(phoneNumber) ?? 
                throw new KSAException(KSAExceptionCode.Client.PhoneInvalid);

            if (latestConfirmation.RetryCount == 0)
                throw new KSAException(KSAExceptionCode.Client.VerificationCodeExpired);

            if (latestConfirmation.ConfirmationCode != confirmationCode)
            {
                latestConfirmation.RetryCount--;
                await _phoneNumberConfirmationRepository.Update(latestConfirmation);

                if (latestConfirmation.RetryCount == 0)
                    throw new KSAException(KSAExceptionCode.Client.VerificationCodeExpired);
                else
                    throw new KSAException(KSAExceptionCode.Client.VerificationCodeIncorrect);
            }
        }
    }
}
