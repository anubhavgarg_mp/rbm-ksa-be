﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Database.Models.OfferingServices;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Comparers;
using RedbullKSA.Services.Helpers;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class OfferingHuaweiService : BaseHuaweiService, IOfferingHuaweiService
    {
        public OfferingHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache) { }

        #region PRIMARY
        public async Task<PurchasedOfferingDTO> QueryPurchasedPrimaryOffering(string objectId)
        {
            var response = await Post<QueryPurchasedPrimaryOffering_Rsp, QueryPurchasedPrimaryOfferingRspMsg>(
                HuaweiPaths.Offerings, HuaweiMessageBuilder.PurchasedPrimaryOfferingMessage(objectId));

            if (response.PrimaryOffering == null) {
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);
            }

            return new PurchasedOfferingDTO(
                response.PrimaryOffering.OfferingId.OfferingId,
                response.PrimaryOffering.OfferingId.PurchaseSeq, response.PrimaryOffering.OfferingName,
                response.PrimaryOffering.EffectiveDate, response.PrimaryOffering.ExpireDate);
        }

        public async Task<string> ChangePrimaryOffering(ChangeOffering primaryOffering)
        {
            if (primaryOffering == null)
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);

            var currentOffer = await QueryPurchasedPrimaryOffering(primaryOffering.ObjectId);

            var response = await Post<ChangePrimaryOffering_Rsp, ChangePrimaryOfferingRspMsg>(
                HuaweiPaths.Offerings,
                HuaweiMessageBuilder.ChangePrimaryOfferingMessage(primaryOffering.ObjectId, currentOffer.OfferingId, currentOffer.PurchaseSeq, primaryOffering.NewOfferId));

            return response.SubmitOrderResponse.OrderId;
        }

        public async Task<IEnumerable<AvailableOfferingDTO>> QueryAvailablePrimaryOfferings(string objectId)
        {
            var response = await Post<QueryAvailablePrimaryOffering_Rsp, QueryAvailablePrimaryOfferingRspMsg>(
                HuaweiPaths.Offerings,
                HuaweiMessageBuilder.AvailablePrimaryOfferingsMessage(objectId));

            return response.PrimaryOffering?.Select((x) => new AvailableOfferingDTO(x)) ?? new List<AvailableOfferingDTO>();
        }

        #endregion

        #region SUPPLEMENTARY
        public async Task<IEnumerable<PurchasedOfferingDTO>> QueryPurchasedSupplementaryOfferings(string objectId)
        {
            var response = await Post<QueryPurchasedSupplementaryOffering_Rsp, QueryPurchasedSupplementaryOfferingRspMsg>(
                HuaweiPaths.Offerings, HuaweiMessageBuilder.PurchasedSupplementaryOfferingMessage(objectId));

            return response.SupplementaryOffering?.Select(x =>
                new PurchasedOfferingDTO(
                    x.OfferingId.OfferingId, x.OfferingId.PurchaseSeq,
                    x.OfferingName, x.EffectiveDate, x.ExpireDate)) ?? new List<PurchasedOfferingDTO>();
        }

        public async Task<string> ChangeSupplementaryOffering(ChangeOffering supplementaryOffering) 
        {
            if (supplementaryOffering == null)
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);

            var response = await Post<ChangeSupplementaryOffering_Rsp, ChangeSupplementaryOfferingRspMsg>(
                HuaweiPaths.Offerings,
                HuaweiMessageBuilder.ChangeSupplementaryOfferingMessage(supplementaryOffering.ObjectId, supplementaryOffering.NewOfferId));

            return response.SubmitOrderResponse.OrderId;
        }

        public async Task<IEnumerable<AvailableOfferingDTO>> QueryAvailableSupplementaryOfferings(string objectId)
        {
            var response = await Post<QueryAvailableSupplementaryOffering_Rsp, QueryAvailableSupplementaryOfferingRspMsg>(
                HuaweiPaths.Offerings,
                HuaweiMessageBuilder.AvailableSupplementaryOfferingsMessage(objectId));

            return response.SupplementaryOffering?.Select((x) => new AvailableOfferingDTO(x)) ?? new List<AvailableOfferingDTO>();
        }

        public async Task<IEnumerable<SupplementaryOfferingDTO>> QueryAllSupplementaryOfferings()
        {
            var allSupplementaryOfferings = new List<SupplementaryOfferingDTO>();
            var primaryOfferings = await QueryAvailablePrimaryOfferings(null);
            var primaryOfferingIds = primaryOfferings.Select(o => o.OfferingId);

            foreach (var primaryOfferingId in primaryOfferingIds)
            {
                var response = await Post<QueryAvailableSupplementaryOffering_Rsp, QueryAvailableSupplementaryOfferingRspMsg>(
                    HuaweiPaths.Offerings,
                    HuaweiMessageBuilder.SupplementaryOfferingsMessage(primaryOfferingId));
                if (response.SupplementaryOffering != null && response.SupplementaryOffering.Any())
                {
                    var offerings = response.SupplementaryOffering.Select((x) => new SupplementaryOfferingDTO(x));
                    allSupplementaryOfferings.AddRange(offerings.Except(allSupplementaryOfferings, OfferingComparer.OfferingIdComparer));
                }
            }

            return allSupplementaryOfferings;
        }

        public async Task<SupplementaryOfferingDTO> QuerySupplementaryOffering(string offeringId)
        {
            var primaryOfferings = await QueryAvailablePrimaryOfferings(null);
            var primaryOfferingIds = primaryOfferings.Select(o => o.OfferingId);

            foreach (var primaryOfferingId in primaryOfferingIds)
            {
                var response = await Post<QueryAvailableSupplementaryOffering_Rsp, QueryAvailableSupplementaryOfferingRspMsg>(
                    HuaweiPaths.Offerings,
                    HuaweiMessageBuilder.SupplementaryOfferingsMessage(primaryOfferingId));
                if (response.SupplementaryOffering != null && response.SupplementaryOffering.Any())
                {
                    var offering = response.SupplementaryOffering.FirstOrDefault(o => o.OfferingId.OfferingId == offeringId);
                    if (offering != null)
                    {
                        return new SupplementaryOfferingDTO(offering);
                    }
                }
            }

            throw new KSAException(KSAExceptionCode.SupplementaryOffering.NotFound);
        }
        #endregion
    }
}
