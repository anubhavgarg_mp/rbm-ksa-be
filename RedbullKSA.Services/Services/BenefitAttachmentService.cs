﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Comparers;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.AttachmentModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Comparers;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class BenefitAttachmentService : BaseService, IBenefitAttachmentService
    {
        private readonly IAttachmentRepository _attachmentRepository;

        public BenefitAttachmentService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _attachmentRepository = services.GetAttachmentRepository();
        }

        //Update
        public async Task HandleAttachmentsOnUpdate(IEnumerable<AttachmentModel> attachments, int benefitId, ServiceLanguage language,
            IUnitOfWork uow = null)
        {
            if (!attachments.Any())
                throw new KSAException(KSAExceptionCode.Benefit.NoImagesAreChosen, $"{language}");

            var galleryImagesFromDb = await GetAttachmentRepository(uow).GetBenefitImages(benefitId, AttachmentCategory.BenefitGalleryImage, language);

            var tasks = new List<Task>();

            tasks.AddRange(DetachBenefitImages(galleryImagesFromDb, attachments.Where(x => x.Category == AttachmentCategory.BenefitGalleryImage), uow));
            tasks.AddRange(await AttachBenefitImages(galleryImagesFromDb, attachments.Where(x => x.Category == AttachmentCategory.BenefitGalleryImage), benefitId));

            //gets all new ids and old ids for all images attachments handling except GalleryImage type
            var newAndOldIds = attachments.Select(x => x.Id).ToList();
            newAndOldIds.AddRange(attachments.Where(x => x.ThumbnailId.HasValue).Select(x => x.ThumbnailId.Value));
            newAndOldIds.AddRange(AddOldIdsToList(attachments));

            var attachmentsDb = await GetAttachmentRepository(uow).Get(newAndOldIds);

            foreach (var attachmentModel in attachments)
            {
                if (!attachmentModel.OldId.HasValue) continue;

                var attachmentOld = attachmentsDb.FirstOrDefault(x => x.Id == attachmentModel.OldId);

                attachmentOld.DetachIds();
                tasks.Add(GetAttachmentRepository(uow).Update(attachmentOld));
                var attachmentNew = attachmentsDb.FirstOrDefault(x => x.Id == attachmentModel.Id);
                attachmentNew.BenefitId = benefitId;
                tasks.Add(GetAttachmentRepository(uow).Update(attachmentNew));

                if (attachmentModel.ThumbnailId.HasValue)
                {
                    var thumbnailNew = attachmentsDb.FirstOrDefault(x => x.Id == attachmentModel.ThumbnailId);
                    thumbnailNew.BenefitId = benefitId;
                    tasks.Add(GetAttachmentRepository(uow).Update(thumbnailNew));
                }

                if (!attachmentModel.OldThumbnailId.HasValue) continue;

                var thumbnailOld = attachmentsDb.FirstOrDefault(x => x.Id == attachmentModel.OldThumbnailId.Value);
                thumbnailOld.DetachIds();
                tasks.Add(GetAttachmentRepository(uow).Update(thumbnailOld));
            }

            await Task.WhenAll(tasks);
        }

        //Add
        public async Task HandleAttachmentsOnAdd(IEnumerable<AttachmentModel> attachments, int benefitId, ServiceLanguage language,
            IUnitOfWork uow = null)
        {
            if (!attachments.Any())
                throw new KSAException(KSAExceptionCode.Benefit.NoImagesAreChosen, $"{language}");

            var attachmentsDb = await GetAttachmentRepository(uow).Get(attachments.Select(x => x.Id));
            await AttachOnAdd(benefitId, attachmentsDb, uow);

            if (attachments.Any(a => a.ThumbnailId.HasValue))
            {
                var thumbnails = attachments.Where(a => a.ThumbnailId.HasValue).Select(t => t.ThumbnailId.Value);
                await HandleThumbnailAttachmentsOnAdd(thumbnails, benefitId, uow);
            }
        }

        private async Task HandleThumbnailAttachmentsOnAdd(IEnumerable<int> thumbnailIds, int benefitId,
            IUnitOfWork uow = null)
        {
            var attachments = await GetAttachmentRepository(uow).Get(thumbnailIds);

            await AttachOnAdd(benefitId, attachments, uow);
        }

        private async Task AttachOnAdd(int benefitId, IEnumerable<Attachment> attachmentsDb,
            IUnitOfWork uow = null)
        {
            var tasksToAttach = new List<Task>();

            foreach (var attachmentDb in attachmentsDb)
            {
                attachmentDb.BenefitId = benefitId;
                tasksToAttach.Add(GetAttachmentRepository(uow).Update(attachmentDb));
            }

            await Task.WhenAll(tasksToAttach);
        }

        #region Handle  benefit gallery images

        private List<int> AddOldIdsToList(IEnumerable<AttachmentModel> attachments)
        {
            var oldIds = new List<int>();

            foreach (var attachmentModel in attachments)
            {
                if (attachmentModel.OldId.HasValue)
                    oldIds.Add(attachmentModel.OldId.Value);

                if (attachmentModel.OldThumbnailId.HasValue)
                    oldIds.Add(attachmentModel.OldThumbnailId.Value);
            }

            return oldIds;
        }

        private List<Task> DetachBenefitImages(IEnumerable<Attachment> dbAttachments, IEnumerable<AttachmentModel> newAttachments,
            IUnitOfWork uow = null)
        {
            var tasks = new List<Task>();

            var attachmentDbModel = dbAttachments.Select(x => new AttachmentModel(x));
            var intersectDb = attachmentDbModel.Intersect(newAttachments, AttachmentComparer.Id);
            var detachedAttachments = attachmentDbModel.Except(intersectDb, AttachmentComparer.Id);

            foreach (var attachment in detachedAttachments)
            {
                var attachmentDb = dbAttachments.FirstOrDefault(x => x.Id == attachment.Id);
                attachmentDb.DetachIds();

                tasks.Add(GetAttachmentRepository(uow).Update(attachmentDb));
            }

            return tasks;
        }

        private async Task<List<Task>> AttachBenefitImages(IEnumerable<Attachment> dbAttachments, IEnumerable<AttachmentModel> newAttachments, int benefitId,
            IUnitOfWork uow = null)
        {
            var attachedtasks = new List<Task>();
            var dbAttachmentModel = dbAttachments.Select(x => new AttachmentModel(x));
            var attachedAttachments = newAttachments.Except(dbAttachmentModel, AttachmentComparer.Id);

            if (!attachedAttachments.Any())
                return attachedtasks;

            var attachedAttachmentsDb = await GetAttachmentRepository(uow).Get(attachedAttachments.Select(x => x.Id));

            foreach (var attachment in attachedAttachmentsDb)
            {
                attachment.BenefitId = benefitId;
                attachedtasks.Add(GetAttachmentRepository(uow).Update(attachment));
            }

            return attachedtasks;
        }

        #endregion

        private IAttachmentRepository GetAttachmentRepository(IUnitOfWork uow)
        {
            return uow != null ? uow.AttachmentRepository : _attachmentRepository;
        }
    }
}
