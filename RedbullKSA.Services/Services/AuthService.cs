﻿using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.API.Models.Auth.Mobile;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.Auth.Open;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Services.Services
{
    public class AuthService : BaseService, IAuthService
    {
        private readonly IUserRepository _userRepository;
        private readonly ISessionTokenService _sessionTokenService;
        private readonly IPhoneNumberConfirmationService _confirmationCodeService;
        private readonly IClientRepository _clientRepository;
        private readonly IPasswordService _passwordService;

        public AuthService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _userRepository = services.GetUserRepository();
            _sessionTokenService = services.GetSessionTokenService();
            _confirmationCodeService = services.GetPhoneNumberConfirmationService();
            _clientRepository = services.GetClientRepository();
            _passwordService = services.GetPasswordService();
        }

        public async Task<TokenModel> Login(LoginModel login, SessionTokenType sessionTokenType)
        {
            login.CleanUpInputs();

            var user = await _userRepository.Get(login.Email, true)
                ?? throw new KSAException(KSAExceptionCode.User.NotFound);

            var isValid = _passwordService.ValidatePassword(login.Password, user.Guid, user.Password);

            if (!isValid)
                throw new KSAException(KSAExceptionCode.General.UserPasswordIncorrect);

            var tokenModel = new TokenModel(await _sessionTokenService.GenerateAdminToken(user.Id))
            {
                Id = user.Id
            };

            return tokenModel;
        }

        #region mobile login/signup
        public async Task<SessionToken> Login(LoginMobileWithPassword login, SessionTokenType sessionTokenType) {
            login.CleanUpInputs();

            var client = await _clientRepository.GetByEmail(login.Login);

            if (client == null) {
                client = await _clientRepository.GetByPhoneNumber(login.Login.PhoneNumberToSaudiNational());
            }

            if (client == null) {
                throw new KSAException(KSAExceptionCode.Client.ClientDoesNotExist);
            }

            var isValid = _passwordService.ValidatePassword(login.Password, client.Guid, client.Password);

            if (!isValid)
                throw new KSAException(KSAExceptionCode.General.ClientPasswordIncorrect);

            return await _sessionTokenService.GenerateToken(client.Id, client.PhoneNumber, sessionTokenType);
        }

        public async Task<SessionToken> Login(LoginMobile login, SessionTokenType sessionTokenType)
        {
            login.CleanUpInputs();

            await _confirmationCodeService.ValidateConfirmationCode(login.PhoneNumber, login.Code);

            var user = await _clientRepository.GetByPhoneNumber(login.PhoneNumber);

            if (user == null)
                throw new KSAException(KSAExceptionCode.Client.ClientDoesNotExist);

            return await _sessionTokenService.GenerateToken(user.Id, login.PhoneNumber, sessionTokenType);
        }

        public async Task<Client> SignUp(RegistrationMobile registration, ServiceLanguage language)
        {
            registration.CleanUpInputs();
            RegistrationMobile.ValidateMobileUserInputs(registration);

            //Validate again a confirmation code with a phone number
            var validateCredentialsTask = ValidateCredentials(registration.PhoneNumber, registration.Email);

            await Task.WhenAll(validateCredentialsTask);

            Guid guid = Guid.NewGuid();
            var hashedPassword = _passwordService.HashPassword(registration.Password, guid);
            var client = Client.FromRegistrationInputs(registration, guid, language, hashedPassword);
            var dbClient = await _clientRepository.Add(client);

            return dbClient;
        }

        private async Task ValidateCredentials(string phoneNumber, string email)
        {
            var tasks = new List<Task>();
            tasks.Add(CheckIfPhoneExists(phoneNumber));
            tasks.Add(CheckIfEmailExists(email));

            await Task.WhenAll(tasks);
        }

        private async Task CheckIfPhoneExists(string phoneNumber)
        {
            var user = await _clientRepository.GetByPhoneNumber(phoneNumber);

            if (user != null)
                throw new KSAException(KSAExceptionCode.Client.PhoneExists);
        }

        private async Task CheckIfEmailExists(string email)
        {
            var user = await _clientRepository.GetByEmail(email);

            if (user != null)
                throw new KSAException(KSAExceptionCode.Client.EmailExists);
        }

        #endregion

        public async Task<SessionToken> Login(OpenLoginModel login)
        {
            login.CleanUpInputs();

            var user = await _userRepository.Get(login.Username, true)
                ?? throw new KSAException(KSAExceptionCode.User.NotFound);

            if (!user.IsApiUser)
                throw new KSAException(KSAExceptionCode.User.NotApiUser);

            var isValid = _passwordService.ValidatePassword(login.Password, user.Guid, user.Password);

            if (!isValid)
                throw new KSAException(KSAExceptionCode.General.UserPasswordIncorrect);

            return await _sessionTokenService.GenerateApiKeyToken(user.Id);
        }
    }
}
