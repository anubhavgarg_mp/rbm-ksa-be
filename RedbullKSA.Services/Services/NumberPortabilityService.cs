﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.Auth.Mobile;
using RedbullKSA.Entities.API.Models.EmailModels;
using RedbullKSA.Entities.API.Models.Portability;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using RedbullKSA.Services.Strategies.Email;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class NumberPortabilityService : BaseService, INumberPortabilityService
    {
        private readonly INumberPortabilityRepository _portabilityRepository;
        private readonly IAuthService _authService;
        private readonly IEmailSendingStrategy<PortingNumberEmailData> _emailStrategy;

        public NumberPortabilityService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
            _authService = serviceProvider.GetAuthService();
            _portabilityRepository = serviceProvider.GetNumberPortabilityRepository();
            _emailStrategy = serviceProvider.GetNumberPortingEmailStrategy();
        }

        public async Task<NumberPortingResponse> PortNumber(PortNumberRequest request, ServiceLanguage language)
        {
            if (request == null)
                throw new KSAException(KSAExceptionCode.General.DataValidationFailed);

            var client = await _authService.SignUp(new RegistrationMobile
            {
                NationalityId = request.NationalityId,
                PhoneNumber = request.ContactMsisdn,
                Email = request.Email,
                Password = request.Password,
                ConfirmPassword = request.ConfirmPassword
            }, language);

            var portedNumber = await _portabilityRepository.Add(new PortedNumber
            {
                ClientId = client.Id,
                MSISDN = request.CurrentMsisdn,
                CurrentOperator = request.CurrentOperator,
                Status = NumberPortingStatus.Started,
                RequestedAt = DateTime.UtcNow
            });

            await _emailStrategy.Send(new PortingNumberEmailData
            {
                ClientId = client.Id,
                PortingRequestId = portedNumber.Id,
                ContactMsisdn = client.PhoneNumber,
                PortedMsisdn = portedNumber.MSISDN
            });

            return new NumberPortingResponse() { Id = portedNumber.Id };
        }
    }
}
