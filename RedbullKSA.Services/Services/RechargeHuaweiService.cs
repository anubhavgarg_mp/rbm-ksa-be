﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Database.Models.RechargeServices;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Helpers;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class RechargeHuaweiService : BaseHuaweiService, IRechargeHuaweiService
    {
        public RechargeHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache) { }

        public async Task<RechargeDTO> QueryRechargeByCash(RechargeModel rechargeModel)
        {
            if (rechargeModel == null)
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);

            var response = await Post<RechargeByCash_Rsp, RechargeByCashRspMsg>(
                HuaweiPaths.Recharge, HuaweiMessageBuilder.RechargeByCashMessage(rechargeModel));

            return new RechargeDTO(response.BalanceInfo.OldBalanceAmt, response.BalanceInfo.NewBalanceAmt, response.BalanceInfo.Currency);
        }

        public async Task<RechargeByVoucherRspMsg> QueryRechargeByVoucher(RechargeVoucherModel rechargeModel)
        {
            if (rechargeModel == null)
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);

            var response = await Post<RechargeByVoucher_Rsp, RechargeByVoucherRspMsg>(
                HuaweiPaths.Recharge, HuaweiMessageBuilder.RechargeByVoucherMessage(rechargeModel));

            return response;
        }

        public async Task<IEnumerable<RechargeLogEntryDTO>> QueryRechargeLog(RechargeLogModel rechargeLog)
        {
            if (rechargeLog == null || rechargeLog.PagingInfo == null || rechargeLog.TimePeriod == null)
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);

            var response = await Post<QueryRechargeLog_Rsp, QueryRechargeLogRspMsg>(
                HuaweiPaths.Recharge, HuaweiMessageBuilder.QueryRechargeLog(rechargeLog));

            return response.RechargeLog?.Select(x => new RechargeLogEntryDTO(x)) ?? new List<RechargeLogEntryDTO>();

        }
    }
}
