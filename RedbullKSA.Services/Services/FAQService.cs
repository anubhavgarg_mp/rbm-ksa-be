﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.FAQModels;
using RedbullKSA.Entities.API.Models.FAQModels.Mobile;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class FAQService : BaseService, IFAQService
    {
        private readonly IFAQRepository _faqRepository;

        public FAQService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
            _faqRepository = serviceProvider.GetFAQRepository();
        }

        public async Task<FAQDTO> Create(CreateFAQRequest faq)
        {
            if (faq == null)
            {
                throw new KSAException(KSAExceptionCode.General.RequestBodyIsNull);
            }

            var dbFAQ = await _faqRepository.Add(new FAQ()
            {
                QuestionEN = faq.QuestionEN,
                QuestionAR = faq.QuestionAR,
                AnswerEN = faq.AnswerEN,
                AnswerAR = faq.AnswerAR,
                FAQTagId = faq.FAQTagId,
                TopXIndicator = faq.TopXIndicator
            });

            return new FAQDTO(dbFAQ);
        }


        public async Task Update(UpdateFAQRequest faq)
        {
            if (faq == null)
            {
                throw new KSAException(KSAExceptionCode.General.RequestBodyIsNull);
            }

            var dbFAQ = await _faqRepository.Find(faq.Id);

            if (dbFAQ == null)
            {
                throw new KSAException(KSAExceptionCode.FAQ.NotFound);
            }

            dbFAQ.QuestionEN = string.IsNullOrEmpty(faq.QuestionEN) ? dbFAQ.QuestionEN : faq.QuestionEN;
            dbFAQ.QuestionAR = string.IsNullOrEmpty(faq.QuestionAR) ? dbFAQ.QuestionAR : faq.QuestionAR;
            dbFAQ.AnswerEN = string.IsNullOrEmpty(faq.AnswerEN) ? dbFAQ.AnswerEN : faq.AnswerEN;
            dbFAQ.AnswerAR = string.IsNullOrEmpty(faq.AnswerAR) ? dbFAQ.AnswerAR : faq.AnswerAR;
            dbFAQ.FAQTagId = faq.FAQTagId;
            dbFAQ.TopXIndicator = faq.TopXIndicator;
            await _faqRepository.Update(dbFAQ);
        }

        public async Task Delete(int id)
        {
            var faq = await _faqRepository.Find(id);

            if (faq == null)
            {
                throw new KSAException(KSAExceptionCode.FAQ.NotFound);
            }

            await _faqRepository.Delete(faq);
        }

        public async Task<FAQDTO> Get(int id)
        {
            var faq = await _faqRepository.Find(id);

            return new FAQDTO(faq) ?? throw new KSAException(KSAExceptionCode.FAQ.NotFound);
        }

        public async Task<IEnumerable<FAQView>> GetAll()
            => await _faqRepository.GetAll();

        public async Task<IEnumerable<FAQMobile>> GetAll(ServiceLanguage language)
        {
            var faqList = await _faqRepository.GetAll();
            if (faqList == null)
            {
                return new List<FAQMobile>();
            }
            return faqList.Select(f => new FAQMobile(f, language));
        }
    }
}
