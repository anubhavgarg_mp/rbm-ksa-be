﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Database.Models.UtilityServices;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Helpers;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class UtilityHuaweiService : BaseHuaweiService, IUtilityHuaweiService
    {
        public UtilityHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache) { }

        public async Task<OneOffFeeDTO> QueryCalOneOffFee(CalOneOffFeeRequest request)
        {
            if (request == null)
            {
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);
            }

            var response = await Post<CalOneOffFee_Rsp, CalOneOffFeeRspMsg>(
                HuaweiPaths.Utility,
                HuaweiMessageBuilder.CalOneOffFeeMessage(request)
            );
            
            return new OneOffFeeDTO(response.OneOffFeeList.OneOffFee);
        }
    }
}
