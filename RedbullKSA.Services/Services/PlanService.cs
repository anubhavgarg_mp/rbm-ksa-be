﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.API.Models.PlanModels;
using RedbullKSA.Entities.API.Models.PlanModels.Mobile;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts.Plans;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class PlanService : BaseService, IPlanService
    {
        private readonly IPlanRepository _planRepository;
        private readonly IPlanTagRepository _planTagRepository;
        private readonly IOfferingHuaweiService _offeringHuaweiService;

        public PlanService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _planRepository = services.GetPlanRepository();
            _planTagRepository = services.GetPlanTagRepository();
            _offeringHuaweiService = services.GetOfferingHuaweiService();
        }

        public async Task<PlanDTO> Create(CreatePlanRequest plan)
        {
            await ValidatePlan(plan);
            SupplementaryOfferingDTO offering = await ValidateOffering(plan.OfferingId);

            var dbPlan = await _planRepository.Add(
                new Plan()
                {
                    NameEN = plan.NameEN,
                    NameAR = plan.NameAR,
                    OfferingId = plan.OfferingId,
                    PlanTagId = plan.PlanTagId,
                    PlanType = plan.PlanType,
                    Price = offering.OneTimeCost.ConvertToGBPrice(),
                    PriceTextEN = plan.PriceTextEN,
                    PriceTextAR = plan.PriceTextAR,
                    IsActive = plan.IsActive,
                    IsNew = plan.IsNew,
                    NewEndDate = plan.NewEndDate,
                    MeasurementId = plan.MeasurementId,
                    ActivationStartDate = plan.IsActive ? DateTime.UtcNow : null,
                    Comment1EN = plan.Comment1EN,
                    Comment2EN = plan.Comment2EN,
                    Comment1AR = plan.Comment1AR,
                    Comment2AR = plan.Comment2AR,
                    AddedBy = plan.UserId,
                    AddedDate = DateTime.UtcNow
                });

            return new PlanDTO(dbPlan);
        }

        public async Task Update(UpdatePlanRequest plan)
        {
            if (plan == null)
            {
                throw new KSAException(KSAExceptionCode.General.RequestBodyIsNull);
            }

            var dbPlan = await _planRepository.Find(plan.Id);

            dbPlan.NameEN = plan.NameEN;
            dbPlan.NameAR = plan.NameAR;
            dbPlan.PlanType = plan.PlanType;
            dbPlan.PriceTextEN = plan.PriceTextEN;
            dbPlan.PriceTextAR = plan.PriceTextAR;
            dbPlan.IsActive = plan.IsActive;
            dbPlan.IsNew = plan.IsNew;
            dbPlan.NewEndDate = plan.NewEndDate;
            dbPlan.MeasurementId = plan.MeasurementId;
            dbPlan.ActivationStartDate = plan.IsActive ? DateTime.UtcNow : null;
            dbPlan.Comment1EN = plan.Comment1EN;
            dbPlan.Comment2EN = plan.Comment2EN;
            dbPlan.Comment1EN = plan.Comment1AR;
            dbPlan.Comment2EN = plan.Comment2AR;
            dbPlan.ModifiedBy = plan.UserId;
            dbPlan.ModifiedDate = DateTime.UtcNow;

            await _planRepository.Update(dbPlan);
        }

        public async Task<IEnumerable<PlanGridView>> GetAllPlans()
        {
            var allSupplementaryOfferings = await _offeringHuaweiService.QueryAllSupplementaryOfferings();
            var dbPlans = await _planRepository.GetAll();

            var plans = new List<PlanGridView>();
            var dbPlansExist = dbPlans != null && dbPlans.Any();

            foreach (var offering in allSupplementaryOfferings)
            {
                Plan dbPlan = null;
                if (dbPlansExist)
                {
                    dbPlan = dbPlans.FirstOrDefault(x => x.OfferingId == offering.OfferingId);
                }

                plans.Add(new PlanGridView(dbPlan, offering));
            }

            return plans;
        }

        public async Task<PlanGridView> GetPlanDetails(int id)
        {
            if (id == 0)
            {
                throw new KSAException(KSAExceptionCode.Plan.PlanIdNotProvided);
            }

            var plan = await _planRepository.Find(id);
            if (plan == null)
            {
                throw new KSAException(KSAExceptionCode.Plan.PlanNotFound);
            }

            var supplementaryOffering = await _offeringHuaweiService.QuerySupplementaryOffering(plan.OfferingId);

            return new PlanGridView(plan, supplementaryOffering);
        }

        public async Task<IEnumerable<PlansMobile>> GetAvailablePlans(string msisdn, ServiceLanguage language)
        {
            if (string.IsNullOrEmpty(msisdn))
            {
                throw new KSAException(KSAExceptionCode.Client.PhoneInvalid);
            }

            var clientSupplementaryOfferings = await _offeringHuaweiService.QueryAvailableSupplementaryOfferings(msisdn);
            
            var plans = new List<PlansMobile>();
            var planTags = await _planTagRepository.GetAll();
            foreach (var offering in clientSupplementaryOfferings)
            {
                var dbPlan = await _planRepository.Get(offering.OfferingId);
                if (dbPlan != null)
                {
                    var planTag = planTags.FirstOrDefault(p => p.Id == dbPlan.PlanTagId);
                    plans.Add(new PlansMobile(dbPlan, planTag,language));
                }                
            }

            return plans;
        }

        private async Task<SupplementaryOfferingDTO> ValidateOffering(string offeringId)
        {
            var offering = await _offeringHuaweiService.QuerySupplementaryOffering(offeringId);

            if (offering == null)
            {
                throw new KSAException(KSAExceptionCode.SupplementaryOffering.NotFound);
            }

            return offering;
        }

        private async Task ValidatePlan(CreatePlanRequest plan)
        {
            if (plan == null)
            {
                throw new KSAException(KSAExceptionCode.General.RequestBodyIsNull);
            }

            var duplicatedPlan = await _planRepository.Get(plan.OfferingId);

            if (duplicatedPlan != null)
            {
                throw new KSAException(KSAExceptionCode.Plan.PlanExists);
            }
        }
        
    }
}
