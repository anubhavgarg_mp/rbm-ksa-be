﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.PaymentModels.DTO;
using RedbullKSA.Entities.API.Models.PaymentModels.Requests;
using RedbullKSA.Entities.API.Models.PaymentModels.Responses;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class PayfortPaymentService : BasePayfortPaymentService, IPaymentService
    {
        private const string DEF_CREATE_SDK_TOKEN_COMMAND = "SDK_TOKEN";

        public PayfortPaymentService(IServiceProvider serviceProvider, IMemoryCache memoryCache) 
            : base(serviceProvider, memoryCache)
        {
        }

        public async Task<SdkTokenDTO> CreateSdkToken(string deviceId, string language)
        {
            if (string.IsNullOrWhiteSpace(deviceId))
            {
                throw new KSAException(KSAExceptionCode.General.InvalidRequest);
            }

            // building request body
            var createSdkTokenRequestBody = CreateBodyBase<CreateSdkTokenRequest>(language);
            
            //filing specific properties
            createSdkTokenRequestBody.DeviceId = deviceId;
            createSdkTokenRequestBody.ServiceCommand = DEF_CREATE_SDK_TOKEN_COMMAND;

            var signatureToHash = GetSignatureForCreationSdkToken(createSdkTokenRequestBody);
            createSdkTokenRequestBody.HashedSignature = GetHashedString(signatureToHash);

            var response = await Post<CreateSdkTokenRequest, CreateSdkTokenResponse>(createSdkTokenRequestBody);

            if (string.IsNullOrWhiteSpace(response.SdkToken))
            {
                throw new KSAException(KSAExceptionCode.Payment.ResponseUnsuccessful, response.ResponseMessage);
            }

            return new SdkTokenDTO() { SdkToken = response.SdkToken };
        }
    }
}
