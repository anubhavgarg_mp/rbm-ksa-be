﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Database.Models.XMLResponses.BillingServices;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Helpers;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class BillingHuaweiService : BaseHuaweiService, IBillingHuaweiService
    {
        public BillingHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
        }

        public async Task<QueryUnbilledAmountRspMsg> QueryUnbilledAmount(UnbilledAmountRequest request)
        {
            if (request == null)
            {
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);
            }

            return await Post<QueryUnbilledAmount_Rsp, QueryUnbilledAmountRspMsg>(
                HuaweiPaths.Billing,
                HuaweiMessageBuilder.QueryUnbilledAmountMessage(request));
        }
    }
}
