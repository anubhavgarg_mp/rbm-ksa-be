﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wiry.Base32;

namespace RedbullKSA.Services.Services
{
    public class SessionTokenService : BaseService, ISessionTokenService
    {
        private readonly ISessionTokenRepository _sessionTokenRepository;

        public SessionTokenService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _sessionTokenRepository = services.GetSessionTokenRepository();
        }

        public bool IsValidFormat(string tokenString) 
            => tokenString.Count(v => v == '.') == 3 ||
                tokenString.Count(v => v == '.') == 2;

        public bool IsGuestToken(string tokenString)
            => tokenString.Count(v => v == '.') == 2;

        public async Task LogoutUser(int userId, SessionTokenType type, IUnitOfWork uow = null)
        {
            var dbTokens = await GetSessionTokenRepository(uow).GetMany(userId, type);

            foreach (var token in dbTokens)
                await GetSessionTokenRepository(uow).Delete(token);
        }

        public async Task LogoutClient(int clientId, IUnitOfWork uow = null)
        {
            var tokens = await GetSessionTokenRepository(uow).GetClientTokens(clientId);

            foreach (var token in tokens)
                await GetSessionTokenRepository(uow).Delete(token);
        }

        public async Task<SessionToken> GenerateToken(int clientId, string phoneNumber, SessionTokenType sessionTokenType)
        {
            var token = SessionToken.FormToken(sessionTokenType, clientId, phoneNumber, _config.SessionToken.LifetimeMinutes);
            token.Token = GenerateTokenString(sessionTokenType, clientId);

            using (TransactionSession transactionSession = new TransactionSession(_services)) 
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    await LogoutClient(clientId, uow);
                    var newToken = await uow.SessionTokenRepository.Add(token);

                    uow.Commit();

                    return newToken;
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }

        public async Task<SessionToken> GenerateGuestTokenMobile()
        {
            var token = SessionToken.FormToken(SessionTokenType.App, null, null, _config.SessionToken.LifetimeMinutes);
            token.Token = GenerateGuestTokenString(SessionTokenType.App);

            return await _sessionTokenRepository.Add(token);
        }

        public async Task<SessionToken> GenerateGuestTokenWeb()
        {
            var token = SessionToken.FormToken(SessionTokenType.Web, null, null, _config.SessionToken.LifetimeMinutes);
            token.Token = GenerateGuestTokenString(SessionTokenType.Web);

            return await _sessionTokenRepository.Add(token);
        }

        public async Task<SessionToken> GenerateAdminToken(int userId)
        {
            var token = SessionToken.FormAdminToken(userId, _config.SessionToken.LifetimeMinutes);
            token.Token = GenerateTokenString(SessionTokenType.Admin, userId);

            return await _sessionTokenRepository.Add(token);
        }

        public async Task<SessionToken> GenerateApiKeyToken(int userId)
        {
            var existingTokens = await _sessionTokenRepository.GetApiTokens(userId);

            // Remove all existing API keys
            foreach (var existingToken in existingTokens)
                await _sessionTokenRepository.Delete(existingToken);

            var token = PrepareApiKeyToken(userId);

            return await _sessionTokenRepository.Add(token);
        }

        private SessionToken PrepareApiKeyToken(int userId)
        {
            return new SessionToken
            {
                Expires = DateTime.UtcNow.AddMinutes(_config.SessionToken.LifetimeMinutes),
                Started = DateTime.UtcNow,
                Token = GenerateTokenString(SessionTokenType.Api, userId),
                TokenType = SessionTokenType.Api,
                UserId = userId
            };
        }

        private string GenerateTokenString(SessionTokenType type, int userId, string phoneNumber = "")
        {
            byte[] randomBytes = _services.GetRngService().Bytes(_config.SessionToken.ByteCount);

            var tokenString = Base32Encoding.Standard.GetString(randomBytes);

            var typeEncoded = Base32Encoding.Standard.GetString(BitConverter.GetBytes((int)type));

            var userIdEncoded = Base32Encoding.Standard.GetString(BitConverter.GetBytes(userId));

            var phoneNumberEncoded = Base32Encoding.Standard.GetString(Encoding.Unicode.GetBytes(phoneNumber));

            return $"{tokenString}.{typeEncoded}.{userIdEncoded}.{phoneNumberEncoded}";
        }

        private string GenerateGuestTokenString(SessionTokenType type)
        {
            byte[] randomBytes = _services.GetRngService().Bytes(_config.SessionToken.ByteCount);
            var tokenString = Base32Encoding.Standard.GetString(randomBytes);
            var typeEncoded = Base32Encoding.Standard.GetString(BitConverter.GetBytes((int)type));
            var isGuest = Base32Encoding.Standard.GetString(BitConverter.GetBytes(true));

            return $"{tokenString}.{typeEncoded}.{isGuest}";
        }

        public int ParseUserId(string tokenString)
            => BitConverter.ToInt32(Base32Encoding.Standard.ToBytes(tokenString.Split('.')[2]), 0);

        public SessionTokenType ParseUserType(string tokenString)
            => (SessionTokenType)BitConverter.ToInt32(Base32Encoding.Standard.ToBytes(tokenString.Split('.')[1]), 0);

        private ISessionTokenRepository GetSessionTokenRepository(IUnitOfWork uow)
        {
            return uow != null ? uow.SessionTokenRepository : _sessionTokenRepository;
        }
    }
}
