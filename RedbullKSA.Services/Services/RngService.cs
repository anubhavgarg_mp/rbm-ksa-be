﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Security.Cryptography;

namespace RedbullKSA.Services.Services
{
    public class RngService : BaseService, IRngService
    {
        private readonly RNGCryptoServiceProvider _rng;

        public RngService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache) => _rng = services.GetRngCrypto();

        public byte[] Bytes(int size)
        {
            var bytes = new byte[size];

            _rng.GetBytes(bytes);

            return bytes;
        }
    }
}
