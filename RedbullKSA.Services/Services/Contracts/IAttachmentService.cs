﻿using RedbullKSA.Entities.API.Models.AttachmentModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IAttachmentService
    {
        Task<Attachment> CreateAttachment(byte[] image, AttachmentCategory category, int? userId, ServiceLanguage language, IUnitOfWork uow = null);
        Task DeleteDetachedAttachments();

        /// <summary>
        /// Detaches all attachments by a given interest ID.
        /// </summary>
        /// <param name="interestId"></param>
        /// <returns></returns>
        Task DetachInterestAttachments(int interestId, ServiceLanguage serviceLanguage, IUnitOfWork uow = null);

        Task DetachCarouselEntityAttachments(int entityId, ServiceLanguage serviceLanguage, AttachmentCategory category, IUnitOfWork uow = null);
    }
}
