﻿using RedbullKSA.Entities.API.Models.SmsModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface ISmsService
    {
        Task<string> Send(SendSMSRequestModel request);
        Task Send(string destination, string messageBody);
        Task<bool> IsPresent(string msisdn);
    }
}
