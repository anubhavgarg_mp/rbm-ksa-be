﻿using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.Database.Models;
using System.Threading.Tasks;
using RedbullKSA.Entities.API.Models.Auth.Mobile;
using RedbullKSA.Entities.API.Models.Auth.Open;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IAuthService
    {
        /// <summary>
        /// Returns a new session token if the provided login model details are valid.
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        Task<TokenModel> Login(LoginModel loginModel, SessionTokenType sessionTokenType);

        Task<SessionToken> Login(LoginMobileWithPassword loginMobileWithPassword, SessionTokenType sessionTokenType);

        /// <summary>
        /// Returns a new session token if user already exists and verification code is correct, id user is not yet created throws an exception(to continue registration)
        /// </summary>
        /// <param name="registration"></param>
        /// <returns></returns>
        Task<SessionToken> Login(LoginMobile login, SessionTokenType sessionTokenType);

        /// <summary>
        /// Returns a new Api key session token if the provided login model details are valid.
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        Task<SessionToken> Login(OpenLoginModel login);

        /// <summary>
        /// Returns a new client if the provided registration details are correct
        /// </summary>
        /// <param name="registration"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        Task<Client> SignUp(RegistrationMobile registration, ServiceLanguage language);
    }
}