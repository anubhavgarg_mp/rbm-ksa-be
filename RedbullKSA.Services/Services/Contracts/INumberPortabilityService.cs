﻿using RedbullKSA.Entities.API.Models.Portability;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface INumberPortabilityService
    {
        Task<NumberPortingResponse> PortNumber(PortNumberRequest request, ServiceLanguage language);
    }
}
