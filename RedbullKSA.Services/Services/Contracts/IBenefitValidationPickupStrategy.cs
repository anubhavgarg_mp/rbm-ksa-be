﻿using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IBenefitValidationPickupStrategy
    {
        IBenefitValidation PickupStrategy(BenefitType type);
    }
}
