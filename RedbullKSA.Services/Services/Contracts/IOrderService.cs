﻿using RedbullKSA.Entities.API.Models.OrderModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IOrderService
    {
        Task Add(int clientId, SimOrderRequest request);
    }
}
