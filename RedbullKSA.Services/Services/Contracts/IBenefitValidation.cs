﻿using RedbullKSA.Entities.API.Models.BenefitModels;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IBenefitValidation
    {
        BenefitWithAttachmentsDetails ValidateBenefit(BenefitWithAttachmentsDetails benefit);
    }
}
