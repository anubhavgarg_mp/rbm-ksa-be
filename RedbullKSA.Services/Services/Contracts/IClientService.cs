﻿using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using RedbullKSA.Entities.Enums;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IClientService
   {
        /// <summary>
        /// Changes client's password
        /// </summary>
        /// <param name="changePassword"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task ChangePassword(ChangePasswordDTO changePasswordDto, int clientId);

        /// <summary>
        /// Gets profile information
        /// </summary>
        /// <param name="clientId">client id</param>
        /// <returns></returns>
        Task<ClientProfileMobile> GetMobileProfile(int? clientId);
    }
}
