﻿using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models.CustomerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface ICustomerHuaweiService
    {
        public Task<CustomerInformationDTO> QueryCustomerInformation(string customerId = null, string serviceNumber = null);
    }
}
