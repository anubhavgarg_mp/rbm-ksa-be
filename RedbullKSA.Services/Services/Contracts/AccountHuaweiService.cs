﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Database.Models.AccountServices;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Helpers;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public class AccountHuaweiService : BaseHuaweiService, IAccountHuaweiService
    {
        public AccountHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache)
            : base(serviceProvider, memoryCache)
        {
        }

        public async Task<AccountBalanceAndFreeUnitDTO> QueryBalanceAndFreeUnit(QueryBalanceAndFreeUnitRequest request)
        {
            if (request == null || request.AccessInfo == null)
            {
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);
            }

            // validation value of ObjectIdType
            if (request.AccessInfo.ObjectIdType != ObjectIdType.SubscriberId && request.AccessInfo.ObjectIdType != ObjectIdType.ServiceNumber)
            {
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect,
                    $"ObjectIdType accept only {ObjectIdType.SubscriberId} or {ObjectIdType.ServiceNumber} values");
            }

            var response = await Post<QueryBalanceAndFreeUnit_Rsp, QueryBalanceAndFreeUnitRspMsg>(
                HuaweiPaths.Account,
                HuaweiMessageBuilder.QueryAccountBalanceAndFreeUnit(request));

            var accountBalanceAndFreeUnitDTO = new AccountBalanceAndFreeUnitDTO(response);

            return accountBalanceAndFreeUnitDTO;
        }
    }
}
