﻿using RedbullKSA.Entities.API.Models.Auth.Mobile;
using RedbullKSA.Entities.Enums;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IPhoneNumberConfirmationService
    {
        Task SendConfirmationCode(PhoneNumberMobile login, ServiceLanguage language);
        Task ValidateConfirmationCode(string phoneNumber, string confirmationCode);
    }
}
