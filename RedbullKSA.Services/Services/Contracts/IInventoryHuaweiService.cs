﻿using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models.CustomerServices;
using RedbullKSA.Entities.Database.Models.InventoryServices;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IInventoryHuaweiService
    {
        public Task<IEnumerable<long>> QueryAvailableNumbers(string pattern, string numberLevel, int count);
        public Task<int> QuerySIMStatus(string iccid, string puk = null);
        public IEnumerable<AvailableNumberTierDto> AvailableNumberTiers();
        public Task<int> QueryOperateMsisdn(MsisdnOperationType operationType, List<string> msisdns);
    }
}
