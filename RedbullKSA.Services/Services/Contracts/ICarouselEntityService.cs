﻿using RedbullKSA.Entities.API.Models.CarouselModels;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface ICarouselEntityService
    {
        Task<IEnumerable<CarouselListViewEntity>> GetAll();
        Task<CarouselEntityDetailsView> Update(int id, CarouselEntityDTO newDetails, int? modifiedBy);
        Task<CarouselEntityDetailsView> AddEntity(CarouselEntityDTO details, int? addedBy);
        Task<IEnumerable<MobileCarouselEntity>> GetActive(WebParameters parameters, ServiceLanguage language);
        Task<CarouselEntityDetailsView> Get(int id);
    }
}
