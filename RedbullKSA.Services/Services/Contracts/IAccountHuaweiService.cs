﻿using RedbullKSA.Entities.API.Models.HuaweiModels;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IAccountHuaweiService
    {
        Task<AccountBalanceAndFreeUnitDTO> QueryBalanceAndFreeUnit(QueryBalanceAndFreeUnitRequest request);
    }
}
