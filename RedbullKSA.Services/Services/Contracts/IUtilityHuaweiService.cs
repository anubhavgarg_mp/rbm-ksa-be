﻿using RedbullKSA.Entities.API.Models.HuaweiModels;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IUtilityHuaweiService
    {
        /// <summary>
        /// Calculate one off charges for the order, i.e. business charge,
        /// offering subscription fee, MSISDN charge, SIM charge 
        /// </summary>
        /// <param name="request">request</param>
        /// <returns>Calculated one off charges</returns>
        Task<OneOffFeeDTO> QueryCalOneOffFee(CalOneOffFeeRequest request);
    }
}