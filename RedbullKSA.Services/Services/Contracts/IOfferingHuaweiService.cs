﻿using RedbullKSA.Entities.API.Models.HuaweiModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IOfferingHuaweiService
    {
        #region PRIMARY
        Task<PurchasedOfferingDTO> QueryPurchasedPrimaryOffering(string objectId);
        Task<string> ChangePrimaryOffering(ChangeOffering primaryoffering);
        Task<IEnumerable<AvailableOfferingDTO>> QueryAvailablePrimaryOfferings(string objectId);
        #endregion

        #region SUPPLEMENTARY
        Task<IEnumerable<PurchasedOfferingDTO>> QueryPurchasedSupplementaryOfferings(string objectId);

        Task<string> ChangeSupplementaryOffering(ChangeOffering supplementaryOffering);

        Task<IEnumerable<AvailableOfferingDTO>> QueryAvailableSupplementaryOfferings(string objectId);

        Task<IEnumerable<SupplementaryOfferingDTO>> QueryAllSupplementaryOfferings();

        Task<SupplementaryOfferingDTO> QuerySupplementaryOffering(string offeringId);
        #endregion
    }
}
