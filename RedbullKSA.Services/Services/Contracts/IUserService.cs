﻿using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.API.Models.UserModels;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IUserService
    {
        /// <summary>
        /// Sends a password reset token to the provided e-mail.
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        Task RequestPasswordReset(string email);

        /// <summary>
        /// Hashes the password and updates the user entity with the new password hash.
        /// </summary>
        /// <param name="resetPasswordModel"></param>
        /// <returns></returns>
        Task ResetPassword(ResetPasswordModel resetPasswordModel);

        /// <summary>
        /// Returns a boolean status of the password reset token. If status = true, then token is valid.
        /// </summary>
        /// <param name="resetTokenModel"></param>
        /// <returns></returns>
        Task<bool> GetResetTokenStatus(ResetTokenModel resetTokenModel);

        /// <summary>
        /// Changes a user's password if the provided model data is valid.
        /// </summary>
        /// <param name="changePasswordDto"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task ChangePassword(ChangePasswordDTO changePasswordDto, int userId);

        /// <summary>
        /// Returns an administrator user's details with a list of claim types.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<AdministratorDetailView> GetAdministratorDetails(int id);

        /// <summary>
        /// Add a new administrator user and returns the newly added details.
        /// </summary>
        /// <param name="details"></param>
        /// <returns></returns>
        Task<AdministratorDetailView> AddAdministrator(AdministratorDetailView details, int addedBy);

        /// <summary>
        /// Edits administrator user's details and returns the newly updated details.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="details"></param>
        /// <returns></returns>
        Task<AdministratorDetailView> EditAdministratorDetails(int id, AdministratorDetailView details, int modifiedBy);
    }
}
