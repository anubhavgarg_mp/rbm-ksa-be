﻿using RedbullKSA.Entities.API.Models.PaymentModels.DTO;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IPaymentService
    {
        public Task<SdkTokenDTO> CreateSdkToken(string deviceId, string language);
    }
}
