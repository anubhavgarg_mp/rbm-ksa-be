﻿using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models.SubscriberServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface ISubscriberHuaweiService
    {
        Task<ChangeSubInfoRspMsg> ChangeSubscriberInformation(ChangeSubscriberInformation request);

        Task<QueryEntityIdsRspMsg> QueryEntityIds(string serviceNumber);

        Task<QuerySubAllInfoRspMsg> QuerySubscriberAllInfo(QuerySubscriberAllInformationRequest request);

        Task<QuerySubMateDatesRspMsg> QuerySubscriberMateDates(List<ObjectAccessInfo> subscribers);
    }
}
