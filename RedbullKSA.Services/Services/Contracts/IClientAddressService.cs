﻿using RedbullKSA.Entities.API.Models.AddressModels;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IClientAddressService
    {
        /// <summary>
        /// Create client address
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="request">address</param>
        /// <param name="language">language</param>
        /// <returns>Client address</returns>
        Task<ClientAddressDTO> Create(int? clientId, CreateClientAddressRequest address, ServiceLanguage language);

        /// <summary>
        /// Update client address
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="request">address</param>
        /// <param name="language">language</param>
        /// <returns></returns>
        Task Update(int? clientId, UpdateClientAddressRequest request, ServiceLanguage language);

        /// <summary>
        /// Get client address by id
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="id">API address id</param>
        /// <param name="language">language</param>
        /// <returns>Client address</returns>
        Task<ClientAddressDTO> Get(int? clientId, int id, ServiceLanguage language);

        /// <summary>
        /// Get client shipping address
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="language">language</param>
        /// <returns>Shipping address</returns>
        Task<ClientAddressDTO> GetShippingAddress(int? clientId, ServiceLanguage language);

        /// <summary>
        /// Get all client addresses
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="language">language</param>
        /// <returns>List of client address</returns>
        Task<IEnumerable<ClientAddressDTO>> GetAll(int? clientId, ServiceLanguage language);

        /// <summary>
        /// Delete client address
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="id">API address id</param>
        /// <returns></returns>
        Task Delete(int? clientId, int id);
    }
}
