﻿using RedbullKSA.Entities.API.Models.FAQModels;
using RedbullKSA.Entities.API.Models.FAQModels.Mobile;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IFAQService
    {
        /// <summary>
        /// Create FAQ
        /// </summary>
        /// <param name="faq"></param>
        /// <returns></returns>
        Task<FAQDTO> Create(CreateFAQRequest faq);

        /// <summary>
        /// Update FAQ
        /// </summary>
        /// <param name="faq"></param>
        /// <returns></returns>
        Task Update(UpdateFAQRequest faq);

        /// <summary>
        /// Get FAQ by id
        /// </summary>
        /// <param name="id">FAQ id</param>
        /// <returns></returns>
        Task<FAQDTO> Get(int id);

        /// <summary>
        /// Delete FAQ
        /// </summary>
        /// <param name="id">FAQ id</param>
        /// <returns></returns>
        Task Delete(int id);

        /// <summary>
        /// Get list of FAQ
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<FAQView>> GetAll();

        /// <summary>
        /// Get list of FAQ by language
        /// </summary>
        /// <param name="language">language</param>
        /// <returns></returns>
        Task<IEnumerable<FAQMobile>> GetAll(ServiceLanguage language);
    }
}
