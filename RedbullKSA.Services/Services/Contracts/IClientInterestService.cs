﻿using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IClientInterestService
    {
        /// <summary>
        /// Gets mobile clients interests
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task<ClientInterestsMobile> GetClientInterests(int clientId, ServiceLanguage serviceLanguage);

        /// <summary>
        /// Updates mobil clients interests
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientInterests"></param>
        /// <returns></returns>
        Task<ClientInterestsMobile> UpdateClientInterests(int clientId, ClientInterestsMobile clientInterests);
    }
}
