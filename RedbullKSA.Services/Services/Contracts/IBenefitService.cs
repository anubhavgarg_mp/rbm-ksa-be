﻿using System.IO;
using RedbullKSA.Entities.API.Models.BenefitModels;
using System.Threading.Tasks;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.API.WebParams;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IBenefitService
    {
        Task<BenefitWithAttachmentsDetails> Add(BenefitWithAttachmentsDetails details, Stream excel, int userId);

        Task<BenefitWithAttachmentsDetails> Update(int id, BenefitWithAttachmentsDetails details, int userId);

        Task<BenefitWithAttachmentsDetails> GetDetails(int benefitId);

        Task<BenefitOffersViewMobile> GetBenefitOffersMobile(BenefitOfferParameters parameters, ServiceLanguage language, int? clientId);

        Task<BenefitDetailsMobile> GetMobileDetails(int benefitId, ServiceLanguage language);
    }
}
