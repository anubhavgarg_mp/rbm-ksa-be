﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface ISessionTokenService
    {
        /// <summary>
        /// Parses the given token. Extracts and returns the user ID found within the parsed string.
        /// </summary>
        /// <param name="tokenString"></param>
        /// <returns></returns>
        int ParseUserId(string tokenString);

        /// <summary>
        /// Checks and returns a bool value whether the token provided is formed in a valid format.
        /// </summary>
        /// <param name="tokenString"></param>
        /// <returns></returns>
        bool IsValidFormat(string tokenString);

        /// <summary>
        /// Checks and returns a boolean indicating wether token is a guest token
        /// </summary>
        /// <param name="tokenString"></param>
        /// <returns></returns>
        bool IsGuestToken(string tokenString);

        /// <summary>
        /// Parses the given token. Extracts and returns session token's type found within the parsed string.
        /// </summary>
        /// <param name="tokenString"></param>
        /// <returns></returns>
        SessionTokenType ParseUserType(string tokenString);

        /// <summary>
        /// Removes any session tokens that belong to the given user ID and are of the provided session token type.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task LogoutUser(int userId, SessionTokenType type, IUnitOfWork uow = null);

        /// <summary>
        /// Removes single client session token.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task LogoutClient(int clientId, IUnitOfWork uow = null);

        /// <summary>
        /// Generates and returns a session token for mobile applications. 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="notificationToken"></param>
        /// <param name="sessionTokenType"></param>
        /// <returns></returns>
        Task<SessionToken> GenerateToken(int userId, string notificationToken, SessionTokenType sessionTokenType);

        /// <summary>
        /// Generates and returns a session token for the web application of administrator user type.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<SessionToken> GenerateAdminToken(int userId);

        /// <summary>
        /// Generates and returns a session token with an API key to be used for external API communications via webhooks.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<SessionToken> GenerateApiKeyToken(int userId);

        /// <summary>
        /// Generates guest token for mobile application
        /// </summary>
        /// <returns>Guest session token</returns>
        Task<SessionToken> GenerateGuestTokenMobile();

        /// <summary>
        /// Generates guest token for web application
        /// </summary>
        /// <returns>Guest session token</returns>
        Task<SessionToken> GenerateGuestTokenWeb();
    }
}
