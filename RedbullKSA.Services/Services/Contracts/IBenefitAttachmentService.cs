﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using RedbullKSA.Entities.API.Models.AttachmentModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IBenefitAttachmentService
    {
        Task HandleAttachmentsOnUpdate(IEnumerable<AttachmentModel> attachments, int benefitId, ServiceLanguage language,
            IUnitOfWork uow = null);
        Task HandleAttachmentsOnAdd(IEnumerable<AttachmentModel> attachments, int benefitId, ServiceLanguage language,
            IUnitOfWork uow = null);
    }
}
