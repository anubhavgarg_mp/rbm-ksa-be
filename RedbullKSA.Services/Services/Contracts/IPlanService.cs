﻿using RedbullKSA.Entities.API.Models.PlanModels;
using RedbullKSA.Entities.API.Models.PlanModels.Mobile;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IPlanService
    {
        /// <summary>
        /// Gets available plans
        /// </summary>
        /// <param name="msisdn">phone number</param>
        /// <param name="language">language</param>
        /// <returns></returns>
        Task<IEnumerable<PlansMobile>> GetAvailablePlans(string msisdn, ServiceLanguage language);

        /// <summary>
        /// Create plan
        /// </summary>
        /// <param name="plan">plan</param>
        /// <returns></returns>
        Task<PlanDTO> Create(CreatePlanRequest plan);

        /// <summary>
        /// Update plan
        /// </summary>
        /// <param name="plan">plan</param>
        /// <returns></returns>
        Task Update(UpdatePlanRequest plan);

        /// <summary>
        /// Get all plans
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<PlanGridView>> GetAllPlans();

        /// <summary>
        /// Get plan details
        /// </summary>
        /// <param name="id">plan id</param>
        /// <returns></returns>
        Task<PlanGridView> GetPlanDetails(int id);
    }
}
