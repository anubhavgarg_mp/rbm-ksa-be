﻿using RedbullKSA.Entities.API.Models.PlanModels;
using RedbullKSA.Entities.API.Models.PlanModels.Tags;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public interface IPlanTagService
    {
        /// <summary>
        /// Create plan tag
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<PlanTagDTO> Create(CreatePlanTagRequest request);

        /// <summary>
        /// Update plan tag
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task Update(UpdatePlanTagRequest request);

        /// <summary>
        /// Get plan tag
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<PlanTagDTO> Get(int id);

        /// <summary>
        /// Get all plan tags
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<PlanTagDTO>> GetAll();
    }
}
