﻿using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.Database.Models;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IInterestService
    {
        /// <summary>
        /// Adds a new interest entity and returns the newly created details.
        /// </summary>
        /// <param name="details"></param>
        /// <param name="addedBy"></param>
        /// <returns></returns>
        Task<InterestDetailsView> AddInterest(InterestDTO details, int addedBy);

        /// <summary>
        /// Updates and existing interest entity.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="details"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        Task<InterestDetailsView> EditDetails(int id, InterestDTO details, int modifiedBy);

        Task SaveSelected(ClientInterestsRequest request, int clientId);
    }
}
