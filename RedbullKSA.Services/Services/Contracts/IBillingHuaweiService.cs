﻿using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models.XMLResponses.BillingServices;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IBillingHuaweiService
    {
        /// <summary>
        /// Query subscriber's unbilled amount
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<QueryUnbilledAmountRspMsg> QueryUnbilledAmount(UnbilledAmountRequest request);
    }
}
