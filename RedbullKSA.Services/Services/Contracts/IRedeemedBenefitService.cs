﻿using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IRedeemedBenefitService
    {
        Task Add(int benefitId, BenefitType type, Stream streamFile, int? quantityOfUser,
            IUnitOfWork uow = null);
        Task Update(int benefitId, BenefitType type, int? quantityOfUser, IUnitOfWork uow = null);

        Task<RedeemedBenefitViewMobile> Redeem(RedeemBenefitAddMobile redeem, int clientId);
        Task ValidateChangeOfBenefitType(int benefitId, IUnitOfWork uow = null);

        Task PickWinners(int benefitId, int numberOfWinners);
    }
}
