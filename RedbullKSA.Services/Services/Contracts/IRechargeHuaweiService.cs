﻿using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models.RechargeServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IRechargeHuaweiService
    {
        Task<RechargeDTO> QueryRechargeByCash(RechargeModel rechargeModel);
        Task<IEnumerable<RechargeLogEntryDTO>> QueryRechargeLog(RechargeLogModel rechargeLog);
        Task<RechargeByVoucherRspMsg> QueryRechargeByVoucher(RechargeVoucherModel rechargeModel);
    }
}
