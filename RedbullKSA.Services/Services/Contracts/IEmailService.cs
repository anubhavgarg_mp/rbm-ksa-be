﻿using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Email;
using RedbullKSA.Entities.Enums;
using SendGrid;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Contracts
{
    public interface IEmailService
    {
        /// <summary>
        /// Sends HTML content via SMTP
        /// </summary>
        /// <param name="email"></param>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        Task SendHtmlViaSmtp(string email, string subject, string content);

        /// <summary>
        /// Sends RAW content via SMTP
        /// </summary>
        /// <param name="email"></param>
        /// <param name="subject"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        Task SendRawViaSmtp(string email, string subject, string content);
    }
}
