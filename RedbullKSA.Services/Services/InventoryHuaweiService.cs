﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Helpers;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Database.Models.InventoryServices;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RedbullKSA.Services.Helpers;
using RedbullKSA.Entities.API.Models.HuaweiModels;

namespace RedbullKSA.Services.Services
{
    public class InventoryHuaweiService : BaseHuaweiService, IInventoryHuaweiService
    {
        public InventoryHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache) { }

        public IEnumerable<AvailableNumberTierDto> AvailableNumberTiers() 
            => ConversionHelper.ConvertEnumToIntArray<NumberTiers>().Select(s => new AvailableNumberTierDto { 
                Id = s,
                Title = ((NumberTiers)s).ToString()
            });

        public async Task<IEnumerable<long>> QueryAvailableNumbers(string pattern, string numberLevel, int count)
        {
            var response = await Post<QueryAvailableNumber_Rsp, QueryAvailableNumberRspMsg>(
                HuaweiPaths.Inventory, 
                HuaweiMessageBuilder.AvailableNumberMessage(pattern, numberLevel, count)
            );

            return response.AvailableNumberList?.Select(x => x.ServiceNumber) ?? new List<long>();
        }

        public async Task<int> QuerySIMStatus(string iccid, string puk = null)
        {
            var response = await Post<QuerySIMStatus_Rsp, QuerySIMStatusRspMsg>(
                HuaweiPaths.Inventory, 
                HuaweiMessageBuilder.SIMStatusMessage(iccid, puk)
            );

            if (!response.SIMExists)
                throw new KSAException(KSAExceptionCode.Huawei.SIMDoesNotExist);

            return response.SIMDetails.SIMStatus;
        }

        public async Task<int> QueryOperateMsisdn(MsisdnOperationType operationType, List<string> msisdns)
        {
            var response = await Post<OperateMsisdn_pick_Rsp, OperateMsisdnPickRspMsg>(
                HuaweiPaths.Inventory, 
                HuaweiMessageBuilder.OperateMSISDNMessage(operationType, msisdns)
            );

            return response.Result;
        }

    }
}

