﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Configuration;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Extensions;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class AttachmentService : BaseService, IAttachmentService
    {
        private const string DEFAULT_FOLDER = "default";
        private const string INTEREST_FOLDER = "interest";
        private const string CLIENT_FOLDER = "client";
        private const string CAROUSEL_FOLDER = "carousel";

        #region Image ratios

        private static readonly Dictionary<AttachmentCategory, (int width, int height)?> _imageRatios = new Dictionary<AttachmentCategory, (int width, int height)?>
        {
            { AttachmentCategory.InterestImage, (10, 3) },
            { AttachmentCategory.CarouselImage, (10, 3)}
        };

        #endregion

        #region Extensions

        private const string JPEG_EXTENSION = ".jpeg";
        private const string PNG_EXTENSION = ".png";

        #endregion

        private readonly IAttachmentRepository _attachmentRepository;
        private readonly ConfigurationSettings _configurationSettings;

        public AttachmentService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _attachmentRepository = services.GetAttachmentRepository();
            _configurationSettings = services.GetConfigurationSettings().Value;
        }

        public async Task<Attachment> CreateAttachment(byte[] image, AttachmentCategory category, int? userId, ServiceLanguage language,
            IUnitOfWork uow = null)
        {
            ImageFormat extension = null;

            if (IsJPG(image))
                extension = ImageFormat.Jpeg;

            else if (IsPNG(image))
                extension = ImageFormat.Png;

            ValidateImageRatio(image, category);

            if (extension != null)
            {
                var directory = GetDirectory(category, language);
                
                var fileInfo = SaveFile(image, extension, directory);

                var attachment = new Attachment(fileInfo.Name, fileInfo.Guid, extension.ToString(), fileInfo.Url, category, language);
                attachment.UploadedFileType = AttachmentFileType.Image;

                return await GetAttachmentRepository(uow).Add(attachment, userId);
            }

            else
            {
                throw new KSAException(KSAExceptionCode.Upload.FileTypeInvalid);
            }
        }

        public async Task DeleteDetachedAttachments()
        {
            //Gets all unowned attachments.
            var detachedAttachments = await _attachmentRepository.GetDetachedAttachments();

            await RemoveAllFromLocalStorage(detachedAttachments);
        }

        private void RemoveFromLocalStorage(Attachment attachment) {
            var directory = GetDirectory(attachment.Category, attachment.FileLanguage);
            string imageLocation = DirectoryBase() + Path.Combine(DirectoryBase(), _configurationSettings.General.AttachmentsDirectory, directory, attachment.GuidId.ToString()) + "." + attachment.FileType;

            if (!File.Exists(imageLocation))
                throw new KSAException(KSAExceptionCode.General.FileNotFound);
            File.Delete(imageLocation);

        }

        public async Task DetachInterestAttachments(int interestId, ServiceLanguage serviceLanguage, IUnitOfWork uow = null)
        {
            var attachments = await GetAttachmentRepository(uow).GetInterestImages(interestId);

            var detachedAttachment = attachments.Where(x => x.FileLanguage == serviceLanguage).FirstOrDefault();

            detachedAttachment.DetachIds();

            await GetAttachmentRepository(uow).Update(detachedAttachment);
        }

        public async Task DetachCarouselEntityAttachments(int entityId, ServiceLanguage serviceLanguage, AttachmentCategory category, IUnitOfWork uow = null)
        {
            var attachments = await GetAttachmentRepository(uow).GetCarouselImages(entityId, category);

            var detachedAttachment = attachments.Where(x => x.FileLanguage == serviceLanguage).FirstOrDefault();

            detachedAttachment.DetachIds();

            await GetAttachmentRepository(uow).Update(detachedAttachment);
        }

        private async Task RemoveAllFromLocalStorage(IEnumerable<Attachment> attachments)
        {
            foreach (var attachment in attachments)
            {
                RemoveFromLocalStorage(attachment);
            };

            await _attachmentRepository.Delete(attachments);
        }

        private SavedFileInformationModel SaveFile(byte[] imageBytes, ImageFormat extension, string directory)
        {
            using (var ms = new MemoryStream(imageBytes))
            {
                Image image = Image.FromStream(ms);
                Guid fileGuid = Guid.NewGuid();
                string imageLocation = Path.Combine(DirectoryBase(), _configurationSettings.General.AttachmentsDirectory, directory, fileGuid.ToString());
                imageLocation = extension.AppendTo(imageLocation);

                image.Save(DirectoryBase() + imageLocation, extension);
                var fileName = extension.AppendTo(fileGuid.ToString());
                return new SavedFileInformationModel(fileGuid, fileName, FormUrl(directory, fileName));
            }
        }

        private string PickCategoryFolder(AttachmentCategory category)
            => category switch
            {
                AttachmentCategory.InterestImage => INTEREST_FOLDER,
                AttachmentCategory.CarouselImage => CAROUSEL_FOLDER,
                _ => DEFAULT_FOLDER

            };

        private string PickLanguageFolder(ServiceLanguage language)
            => language switch
            {
                ServiceLanguage.AR => EnumExtensions.GetDescription(ServiceLanguage.AR),
                ServiceLanguage.EN => EnumExtensions.GetDescription(ServiceLanguage.EN),
                _ => EnumExtensions.GetDescription(ServiceLanguage.EN)
            };

        private string GetDirectory(AttachmentCategory category, ServiceLanguage language) => 
            Path.Combine(PickLanguageFolder(language), PickCategoryFolder(category));

        private bool IsJPG(byte[] fileData)
        {
            try
            {
                using (var ms = new MemoryStream(fileData))
                {
                    Image image = Image.FromStream(ms);
                    if (ImageFormat.Jpeg.Equals(image.RawFormat))
                        return true;

                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        private bool IsPNG(byte[] fileData)
        {
            try
            {
                using (var ms = new MemoryStream(fileData))
                {
                    Image image = Image.FromStream(ms);
                    if (ImageFormat.Png.Equals(image.RawFormat))
                        return true;

                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        private void ValidateImageRatio(byte[] imageData, AttachmentCategory attachmentCategory)
        {
            using (var ms = new MemoryStream(imageData))
            {
                using (var image = Image.FromStream(ms))
                {
                    // use image.Width and image.Height
                    (int width, int height) ratio = (image.Width / GreatestCommonDivisor(image.Width, image.Height), image.Height / GreatestCommonDivisor(image.Width, image.Height));

                    var ratioRecord = _imageRatios[attachmentCategory];

                    if (ratioRecord.HasValue && (ratioRecord.Value.height != ratio.height || ratioRecord.Value.width != ratio.width))
                        throw new KSAException(KSAExceptionCode.Upload.WrongAttachmentRatio, $" Should be {ratioRecord.Value.width}:{ratioRecord.Value.height}");
                }
            }
        }

        private static int GreatestCommonDivisor(int width, int height)
        {
            int remainder;

            while (height != 0)
            {
                remainder = width % height;
                width = height;
                height = remainder;
            }

            return width;
        }

        private IAttachmentRepository GetAttachmentRepository(IUnitOfWork uow)
        {
            return uow != null ? uow.AttachmentRepository : _attachmentRepository;
        }

        private string FormUrl(string directory, string fileName)
            => $"{_configurationSettings.General.BackendUrl}/{_configurationSettings.General.AttachmentsDirectory.Substring(1)}/{directory.Replace("\\", "/")}/{fileName}";

        private string DirectoryBase()
            => Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
    }
}
