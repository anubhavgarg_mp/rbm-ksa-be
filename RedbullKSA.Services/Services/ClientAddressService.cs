﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.AddressModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class ClientAddressService : BaseService, IClientAddressService
    {
        public readonly IClientAddressRepository _clientAddressRepository;

        public ClientAddressService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
            _clientAddressRepository = serviceProvider.GetClientAddressRepository();
        }

        public async Task<ClientAddressDTO> Create(int? clientId, CreateClientAddressRequest address, ServiceLanguage language)
        {
            var dbAddress = await ValidateAddressRequest(clientId, address, language);
            if (dbAddress != null)
            {
                throw new KSAException(KSAExceptionCode.ClientAddress.AddressNameExists);
            }

            ClientAddress clientAddress;
            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    await ResetShippingAddress((int)clientId, address.IsShippingAddress, uow);

                    clientAddress = await uow.ClientAddressRepository.Add(SetAddress((int)clientId, address, dbAddress, language));

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw new KSAException(KSAExceptionCode.ClientAddress.SaveUnexpectedError);
                }
            }

            return new ClientAddressDTO(clientAddress, language);
        }

        public async Task Update(int? clientId, UpdateClientAddressRequest address, ServiceLanguage language)
        {
            var dbAddress = await ValidateAddressRequest(clientId, address, language);
            if (dbAddress == null)
            {
                dbAddress = await _clientAddressRepository.Get((int)clientId, address.Id);
            }
            else
            {
                var name = language == ServiceLanguage.EN ? dbAddress.NameEN : dbAddress.NameAR;
                if (!string.IsNullOrEmpty(name) && dbAddress.Id != address.Id)
                {
                    throw new KSAException(KSAExceptionCode.ClientAddress.AddressNameExists);
                }
            }

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    await ResetShippingAddress((int)clientId, address.IsShippingAddress, uow);

                    await uow.ClientAddressRepository.Update(SetAddress((int)clientId, address, dbAddress, language, address.Id));

                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw new KSAException(KSAExceptionCode.ClientAddress.SaveUnexpectedError);
                }
            }
        }

        public async Task<ClientAddressDTO> Get(int? clientId, int id, ServiceLanguage language)
        {
            ValidateClient(clientId);

            if (id == 0)
            {
                throw new KSAException(KSAExceptionCode.ClientAddress.AddressIdNotProvided);
            }

            var dbAddress = await _clientAddressRepository.Get((int)clientId, id)
                ?? throw new KSAException(KSAExceptionCode.ClientAddress.NotFound);

            return new ClientAddressDTO(dbAddress, language);                
        }

        public async Task<IEnumerable<ClientAddressDTO>> GetAll(int? clientId, ServiceLanguage language)
        {
            ValidateClient(clientId);

            var clientAddresses = await _clientAddressRepository.GetAll((int)clientId);
            return clientAddresses.Select(x => new ClientAddressDTO(x, language));
        }

        public async Task<ClientAddressDTO> GetShippingAddress(int? clientId, ServiceLanguage language)
        {
            ValidateClient(clientId);

            var dbAddress = await _clientAddressRepository.GetShippingAddress((int)clientId)
                ?? throw new KSAException(KSAExceptionCode.ClientAddress.NotFound);

            return new ClientAddressDTO(dbAddress, language);                
        }

        public async Task Delete(int? clientId, int id)
        {
            ValidateClient(clientId);

            var address = await _clientAddressRepository.Get((int)clientId, id);
            if (address == null)
            {
                throw new KSAException(KSAExceptionCode.ClientAddress.NotFound);
            }

            await _clientAddressRepository.Delete(address);
        }

        private static async Task ResetShippingAddress(int clientId, bool isShippingAddress, UnitOfWork uow)
        {
            if (isShippingAddress)
            {
                var shippingAddress = await uow.ClientAddressRepository.GetShippingAddress(clientId);
                if (shippingAddress != null)
                {
                    await uow.ClientAddressRepository.ResetShippingAddress(clientId, shippingAddress);
                }
            }
        }

        private static void ValidateClient(int? clientId)
        {
            if (clientId == null)
            {
                throw new KSAException(KSAExceptionCode.Client.ClientIdNotProvided);
            }
        }

        private async Task<ClientAddress> ValidateAddressRequest(int? clientId, BaseClientAddressRequest address, ServiceLanguage language)
        {
            ValidateClient(clientId);

            if (address == null)
            {
                throw new KSAException(KSAExceptionCode.General.RequestBodyIsNull);
            }

            return await _clientAddressRepository.Get((int)clientId, address.Name);
        }

        private static ClientAddress SetAddress(
            int clientId,
            BaseClientAddressRequest address,
            ClientAddress dbAddress,
            ServiceLanguage language,
            int addressId = 0)
        {
            return new ClientAddress()
            {
                Id = addressId,
                NameAR = language == ServiceLanguage.AR ? address.Name : dbAddress?.NameAR,
                NameEN = language == ServiceLanguage.EN ? address.Name : dbAddress?.NameEN,
                ClientId = clientId,
                AddressType = address.AddressType,
                City = address.City,
                Province = address.Province,
                Street = address.AddressLine1,
                AddressLine4 = address.AddressLine2,
                PostCode = address.ZipCode,
                IsDefault = address.IsShippingAddress
            };
        }
    }
}
