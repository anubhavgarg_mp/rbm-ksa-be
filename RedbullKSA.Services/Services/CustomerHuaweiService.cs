﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Database.Models.CustomerServices;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Helpers;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class CustomerHuaweiService : BaseHuaweiService, ICustomerHuaweiService
    {
        public CustomerHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache) { }

        public async Task<CustomerInformationDTO> QueryCustomerInformation(string customerId, string serviceNumber)
        {
            string searchText = "";
            CustomerInformationSearchCategory searchCategory;

            if (!string.IsNullOrEmpty(customerId))
            {
                searchText = customerId;
                searchCategory = CustomerInformationSearchCategory.CustomerId;
            }
            else if (!string.IsNullOrEmpty(serviceNumber))
            {
                searchText = serviceNumber;
                searchCategory = CustomerInformationSearchCategory.ServiceNumber;
            }
            else
                throw new KSAException(KSAExceptionCode.Huawei.InvalidCustomerSearchParameters);

            var response = await Post<QueryCustomerInformation_Rsp, QueryCustInfoRspMsg>(
                HuaweiPaths.Customer,
                HuaweiMessageBuilder.CustomerInformationMessage(searchCategory, searchText)
            );

            return new CustomerInformationDTO(response.Customer);
        }
    }
}
