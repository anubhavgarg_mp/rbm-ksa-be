﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Database.Models.SubscriberServices;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Helpers;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class SubscriberHuaweiService : BaseHuaweiService, ISubscriberHuaweiService
    {
        public SubscriberHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
        }

        public async Task<ChangeSubInfoRspMsg> ChangeSubscriberInformation(ChangeSubscriberInformation request)
        {
            if (request == null)
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);

            var response = await Post<ChangeSubscriberInformation_Rsp, ChangeSubInfoRspMsg>(
                HuaweiPaths.Subscriber,
                HuaweiMessageBuilder.QueryChangeSubscriberInformationMessage(request)
            );

            return response;
        }

        public async Task<QueryEntityIdsRspMsg> QueryEntityIds(string serviceNumber)
        {
            if (string.IsNullOrEmpty(serviceNumber))
                throw new KSAException(KSAExceptionCode.Huawei.InvalidServiceNumber);

            var response = await Post<QueryEntityIds_Rsp, QueryEntityIdsRspMsg>(
                HuaweiPaths.Subscriber,
                HuaweiMessageBuilder.QueryEntityIdsMessage(serviceNumber)
            );

            return response;
        }

        public async Task<QuerySubAllInfoRspMsg> QuerySubscriberAllInfo(QuerySubscriberAllInformationRequest request)
        {
            if (request == null)
                throw new KSAException(KSAExceptionCode.Huawei.RequestBodyIsNullOrIncorrect);

            var response = await Post<QuerySubscriberAllInfomation_Rsp, QuerySubAllInfoRspMsg>(
                HuaweiPaths.Subscriber,
                HuaweiMessageBuilder.QuerySubscriberAllInformationMessage(request)
            );

            return response;
        }

        public async Task<QuerySubMateDatesRspMsg> QuerySubscriberMateDates(List<ObjectAccessInfo> subscribers)
        {
            if (subscribers == null || subscribers.Count == 0)
                throw new KSAException(KSAExceptionCode.Huawei.NoSubscriberSearchParameters);

            var response = await Post<QuerySubscriberMateDates_Rsp, QuerySubMateDatesRspMsg>(
                HuaweiPaths.Subscriber,
                HuaweiMessageBuilder.QuerySubscriberMateDatesMessage(subscribers)
            );

            return response;
        }
    }
}
