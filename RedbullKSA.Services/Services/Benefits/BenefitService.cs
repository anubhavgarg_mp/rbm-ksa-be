﻿using Hangfire;
using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.AttachmentModels;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services.Benefits
{
    public class BenefitService : BaseService, IBenefitService
    {
        private readonly IBenefitRepository _benefitRepository;
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly IBenefitValidationPickupStrategy _benefitPickupStrategy;
        private readonly IBenefitAttachmentService _benefitAttachmentService;
        private readonly IRedeemedBenefitService _redeemedBenefitService;
        private readonly IBenefitTagRepository _benefitTagRepository;
        private readonly ITagRepository _tagRepository;

        public BenefitService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _benefitRepository = services.GetBenefitRepository();
            _attachmentRepository = services.GetAttachmentRepository();
            _benefitPickupStrategy = services.GetBenefitValidationStrategy();
            _benefitAttachmentService = services.GetBenefitAttachmentService();
            _redeemedBenefitService = services.GetRedeemedBenefitService();
            _benefitTagRepository = services.GetBenefitTagRepository();
            _tagRepository = services.GetTagRepository();
        }

        public async Task<BenefitWithAttachmentsDetails> Add(BenefitWithAttachmentsDetails details, Stream file, int userId)
        {
            var validationService = _benefitPickupStrategy.PickupStrategy(details.Type);
            details = validationService.ValidateBenefit(details);

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin(IsolationLevel.Serializable);

                try
                {
                    var benefit = await uow.BenefitRepository.Add(Benefit.FromBenefitDetails(details), userId);

                    var attachARTask = _benefitAttachmentService.HandleAttachmentsOnAdd(details.BenefitAR.GetAttachments(), benefit.Id, ServiceLanguage.AR, uow);
                    var attachENTask = _benefitAttachmentService.HandleAttachmentsOnAdd(details.BenefitEN.GetAttachments(), benefit.Id, ServiceLanguage.EN, uow);
                    var benefitUpdateTask = uow.BenefitRepository.Update(benefit);
                    var addRedeemedBenefitsTask = _redeemedBenefitService.Add(benefit.Id, benefit.Type, file, details.AvailableQuantity, uow);
                    var addBenefitTagTask = uow.BenefitTagRepository.AddManyTagsBenefit(benefit.Id, details.BenefitTags);

                    await Task.WhenAll(attachENTask, attachARTask, addRedeemedBenefitsTask, benefitUpdateTask, addBenefitTagTask);

                    uow.Commit();

                    var newBenefit = await GetDetails(benefit.Id);

                    return newBenefit;
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }

        public async Task<BenefitWithAttachmentsDetails> Update(int id, BenefitWithAttachmentsDetails details, int userId)
        {
            var validationService = _benefitPickupStrategy.PickupStrategy(details.Type);
            details = validationService.ValidateBenefit(details);

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin(IsolationLevel.Serializable);

                try
                {
                    var detachImgENTask = _benefitAttachmentService.HandleAttachmentsOnUpdate(details.BenefitEN.GetAttachments(), details.Id.Value, ServiceLanguage.EN, uow);
                    var detachImgARTask = _benefitAttachmentService.HandleAttachmentsOnUpdate(details.BenefitAR.GetAttachments(), details.Id.Value, ServiceLanguage.AR, uow);
                    var deleteBenefitTagsTask = uow.BenefitTagRepository.DeleteAllFromBenefit(id);
                    await Task.WhenAll(detachImgENTask, detachImgARTask, deleteBenefitTagsTask);

                    var benefit = await uow.BenefitRepository.Get(id);
                    if (benefit.Type != details.Type)
                        throw new KSAException(KSAExceptionCode.Benefit.BenefitTypeIsNotAllowedToBeChanged);

                    if (benefit.IsDifferentUpdateType(details.Type)) await _redeemedBenefitService.ValidateChangeOfBenefitType(id, uow);

                    benefit.UpdateProperties(details);

                    var benefitUpdateTask = uow.BenefitRepository.Update(benefit, userId);
                    var redeemedUpdateTask = _redeemedBenefitService.Update(benefit.Id, benefit.Type, details.AvailableQuantity, uow);
                    var updatedBenefitTagsTask = uow.BenefitTagRepository.AddManyTagsBenefit(benefit.Id, details.BenefitTags);

                    await Task.WhenAll(benefitUpdateTask, redeemedUpdateTask, updatedBenefitTagsTask);

                    uow.Commit();

                    return await GetDetails(benefit.Id);
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }

        public async Task<BenefitWithAttachmentsDetails> GetDetails(int benefitId)
        {
            var benefitDetailsTask = _benefitRepository.GetDetails(benefitId);
            var attachmentsTask = _attachmentRepository.GetBenefitImages(benefitId);
            var benefitTagTask = _benefitTagRepository.GetTagsViewByBenefit(benefitId);

            await Task.WhenAll(benefitDetailsTask, attachmentsTask, benefitTagTask);

            var benefit = new BenefitWithAttachmentsDetails(benefitDetailsTask.Result, attachmentsTask.Result.Select(x =>
            {
                return new AttachmentModel(x);
            }),
            benefitTagTask.Result);

            return benefit;
        }

        public async Task<BenefitOffersViewMobile> GetBenefitOffersMobile(BenefitOfferParameters parameters, ServiceLanguage language, int? clientId)
        {
            var benefitTags = await _benefitTagRepository.GetTagsView();
            var tags = await _tagRepository.GetActive();

            var benefitsView = new BenefitOffersViewMobile()
            {
                Tags = tags.OrderBy(tag => tag.OrderPosition)
                    .Select(tag => TagDetailsMobileView.FormTag(tag, language))
            };

            if (clientId.HasValue)
            {
                var benefits = await _benefitRepository.GetBenefitOffersMobile(parameters, language, clientId.Value);

                benefitsView.Benefits = benefits.Select(x => new BenefitOfferMobile(x, language, benefitTags));
                benefitsView.Banners = benefits.Where(b => b.IsFeatured).Select(b => new OfferBannerMobile(b.Id, b.TeaserImage));
            }
            else
            {
                var benefits = await _benefitRepository.GetGuestBenefits(parameters, language);

                benefitsView.Benefits = benefits.Select(x => new BenefitOfferMobile(x, language, benefitTags));
                benefitsView.Banners = benefits.Where(b => b.IsFeatured).Select(b => new OfferBannerMobile(b.Id, b.TeaserImage));
            }

            benefitsView.Categories = benefitsView.Benefits.ToList()
                .Select(b => (short)b.Category)
                .Distinct()
                .Select(v => new BenefitCategoryDetailsMobileView { Id = v, Name = ((BenefitCategory)v).ToString() });

            return benefitsView;
        }

        public async Task<BenefitDetailsMobile> GetMobileDetails(int benefitId, ServiceLanguage language)
        {
            var benefitView = await _benefitRepository.GetMobileDetails(benefitId);

            if (benefitView == null)
                throw new KSAException(KSAExceptionCode.Benefit.BenefitNotFound);

            var benefit = new BenefitDetailsMobile(benefitView, language);

            var attachments = await _attachmentRepository.GetBenefitImages(benefitId, language);

            benefit.AssignAttachments(attachments);

            return benefit;
        }
    }
}
