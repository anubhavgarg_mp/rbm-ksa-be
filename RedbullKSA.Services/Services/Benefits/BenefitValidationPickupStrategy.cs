﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;

namespace RedbullKSA.Services.Services.Benefits
{
    public class BenefitValidationPickupStrategy : BaseService, IBenefitValidationPickupStrategy
    {
        public BenefitValidationPickupStrategy(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
        }

        public IBenefitValidation PickupStrategy(BenefitType type)
        {
            switch (type)
            {
                case BenefitType.Experience:
                    return _services.GetBenefitExperienceValidation();
                case BenefitType.Voucher:
                    return _services.GetBenefitVoucherValidation();
                case BenefitType.Explore:
                    return _services.GetBenefitExploreValidation();
                default:
                    throw new ArgumentException("Picked up the wrong strategy. ");
            }
        }
    }
}
