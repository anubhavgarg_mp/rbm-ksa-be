﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Common.Helpers;
using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class UserService : BaseService, IUserService
    {
        private readonly Dictionary<AdminClaimType, AdminClaimType> ClaimPairs = new Dictionary<AdminClaimType, AdminClaimType>
        {
            { AdminClaimType.AdministratorsView, AdminClaimType.AdministratorsWrite},
            { AdminClaimType.BenefitsView, AdminClaimType.BenefitsWrite},
            { AdminClaimType.InterestsView, AdminClaimType.InterestsWrite},
        };

        private readonly IUserRepository _userRepository;
        private readonly ISessionTokenService _sessionTokenService;
        private readonly IPasswordService _passwordService;
        private readonly IUserClaimRepository _userClaimRepository;

        public UserService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _userRepository = services.GetUserRepository();
            _sessionTokenService = services.GetSessionTokenService();
            _passwordService = services.GetPasswordService();
            _userClaimRepository = services.GetUserClaimRepository();
        }

        public async Task ChangePassword(ChangePasswordDTO changePasswordDto, int userId)
        {
            if (changePasswordDto.OldPassword == changePasswordDto.NewPassword)
                throw new KSAException(KSAExceptionCode.General.PasswordSameAsOld);

            if (changePasswordDto.NewPassword != changePasswordDto.RepeatPassword)
                throw new KSAException(KSAExceptionCode.General.PasswordsDoNotMatch);

            if (!changePasswordDto.NewPassword.PasswordMeetsRequirements())
                throw new KSAException(KSAExceptionCode.General.PasswordDoesNotMeetRequirements);

            var user = await _userRepository.Get(userId);

            if (!_passwordService.ValidatePassword(changePasswordDto.OldPassword, user.Guid, user.Password))
                throw new KSAException(KSAExceptionCode.General.UserPasswordIncorrect);

            user.Password = _passwordService.HashPassword(changePasswordDto.NewPassword, user.Guid);

            await _userRepository.Update(user);
        }

        public async Task<bool> GetResetTokenStatus(ResetTokenModel resetTokenModel)
        {
            if (string.IsNullOrWhiteSpace(resetTokenModel.ResetToken))
                return false;

            var user = await _userRepository.Get(resetTokenModel.ResetToken);

            if (user == null)
                return false;

            if (user.PasswordResetTokenExpiration < DateTime.UtcNow)
                return false;

            else
                return true;
        }

        public async Task RequestPasswordReset(string email)
        {
            email.Trim();

            if (!email.IsValidEmail())
                throw new KSAException(KSAExceptionCode.User.EmailInvalid);

            var user = await _userRepository.Get(email, true);

            if (user == null)
                return;

            var hasPasswordResetToken = !string.IsNullOrWhiteSpace(user.PasswordResetToken);
            var passwordResetTokenExpired = user.PasswordResetTokenExpiration.HasValue && user.PasswordResetTokenExpiration.Value < DateTime.UtcNow;

            if (!hasPasswordResetToken || passwordResetTokenExpired)
            {
                var resetToken = Guid.NewGuid().ToString();
                user.PasswordResetToken = resetToken;
                user.PasswordResetTokenExpiration = DateTime.UtcNow.AddHours(1);

                await _userRepository.Update(user);
            }
        }

        public async Task ResetPassword(ResetPasswordModel resetPasswordModel)
        {
            var user = await _userRepository.Get(resetPasswordModel.ResetToken) ?? throw new KSAException(KSAExceptionCode.User.NotFound);

            if (resetPasswordModel.Password != resetPasswordModel.RepeatPassword)
                throw new KSAException(KSAExceptionCode.General.PasswordsDoNotMatch);

            if (!resetPasswordModel.Password.PasswordMeetsRequirements())
                throw new KSAException(KSAExceptionCode.General.PasswordDoesNotMeetRequirements);

            user.PasswordResetToken = null;
            user.PasswordResetTokenExpiration = null;
            user.Password = _passwordService.HashPassword(resetPasswordModel.Password, user.Guid);

            await _userRepository.Update(user);
        }

        public async Task<AdministratorDetailView> GetAdministratorDetails(int id)
        {
            var userTask = _userRepository.Get(id);
            var claimsTask = _userClaimRepository.Get(id);

            await Task.WhenAll(userTask, claimsTask);

            return AdministratorDetailView.FromUserAndClaims(userTask.Result, claimsTask.Result);
        }

        public async Task<AdministratorDetailView> EditAdministratorDetails(int id, AdministratorDetailView details, int modifiedBy)
        {
            details.CleanDetails();
            ValidateAdministratorDetails(details);

            var user = await _userRepository.Get(id);

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    // If status changed to Inactive - logout the user.
                    if (user.Status == ActiveStatus.Active && details.Status == ActiveStatus.Inactive)
                        await _sessionTokenService.LogoutUser(details.Id, SessionTokenType.Admin, uow);

                    user.ExtractAdministratorDetails(details);
                    await uow.UserRepository.Update(user, modifiedBy);

                    var editClaimModel = new EditClaimModel
                    {
                        UserId = id,
                        Claims = details.Permissions
                    };
                    var newClaims = await UpdateUserClaims(editClaimModel, modifiedBy, uow);

                    uow.Commit();

                    return AdministratorDetailView.FromUserAndClaims(await uow.UserRepository.Get(id), newClaims);
                }
                catch (Exception)
                {
                    uow.Dispose();
                    throw;
                }

            }
        }

        private void ValidateAdministratorDetails(AdministratorDetailView details)
        {
            if (!details.PhoneNumber.IsValidPhoneNumber())
                throw new KSAException(KSAExceptionCode.User.PhoneInvalid);

            if (!details.Email.IsValidEmail())
                throw new KSAException(KSAExceptionCode.User.EmailInvalid);

            CheckAdministratorPermissionTypes(details.Permissions);
        }

        private void CheckAdministratorPermissionTypes(IEnumerable<AdminClaimType> Permissions)
        {
            foreach (var permission in Permissions)
            {
                var claimPair = ClaimPairs.Where(writeClaim => writeClaim.Value == permission);

                if (claimPair.Any() && !Permissions.Any(viewClaim => viewClaim == claimPair.First().Key))
                    throw new KSAException(KSAExceptionCode.User.ClaimInvalid);
            }
        }

        public async Task<AdministratorDetailView> AddAdministrator(AdministratorDetailView details, int addedBy)
        {
            details.CleanDetails();
            details.Status = ActiveStatus.Active;
            ValidateAdministratorDetails(details);

            var existingEmail = await _userRepository.Get(details.Email, true);

            if (existingEmail != null)
                throw new KSAException(KSAExceptionCode.User.EmailExists);

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    var userDetails = User.FromAdministratorDetails(details);
                    var newUser = await uow.UserRepository.Add(userDetails, addedBy);
                    var repositoryTasks = new List<Task>();
                    var newClaims = new List<UserClaim>();

                    foreach (var item in details.Permissions)
                    {
                        var newClaim = new UserClaim(newUser.Id, item, bool.TrueString);
                        repositoryTasks.Add(uow.UserClaimRepository.Add(newClaim));
                        newClaims.Add(newClaim);
                    }

                    await Task.WhenAll(repositoryTasks);

                    uow.Commit();

                    await RequestPasswordReset(newUser.Email);

                    return AdministratorDetailView.FromUserAndClaims(newUser, newClaims);
                }
                catch (Exception)
                {
                    uow.Dispose();
                    throw;
                }
            }
        }

        public async Task<IEnumerable<UserClaim>> UpdateUserClaims(EditClaimModel model, int modifiedBy,
            IUnitOfWork uow = null)
        {
            var userClaims = await GetUserClaimRepository(uow).Get(model.UserId, false);
            var oldTypes = userClaims.Where(x => ConversionHelper.InterpretBool(x.Value)).Select(oc => oc.ClaimType);
            var hasCommon = oldTypes.Intersect(model.Claims);

            var inactiveClaims = oldTypes.Except(hasCommon);
            var activeClaims = model.Claims.Except(hasCommon);

            var repositoryTasks = new List<Task>();

            ///All "deselected" user claims are set to inactive.
            foreach (var item in inactiveClaims)
            {
                var userClaim = userClaims.FirstOrDefault(x => x.ClaimType == item);
                userClaim.Value = bool.FalseString;

                repositoryTasks.Add(GetUserClaimRepository(uow).Update(userClaim, modifiedBy));
            }

            ///All newly "selected" user claims are set to active or created if not existent before.
            foreach (var item in activeClaims)
            {
                var userClaim = userClaims.FirstOrDefault(x => x.ClaimType == item);

                if (userClaim == null)
                {
                    userClaim = UserClaim.NewActiveClaim(model.UserId, item);
                    repositoryTasks.Add(GetUserClaimRepository(uow).Add(userClaim, modifiedBy));
                }

                else
                {
                    userClaim.Value = bool.TrueString;
                    repositoryTasks.Add(GetUserClaimRepository(uow).Update(userClaim, modifiedBy));
                }
            }

            await Task.WhenAll(repositoryTasks);

            return await GetUserClaimRepository(uow).Get(model.UserId);
        }

        private IUserClaimRepository GetUserClaimRepository(IUnitOfWork uow)
        {
            return uow != null ? uow.UserClaimRepository : _userClaimRepository;
        }
    }
}
