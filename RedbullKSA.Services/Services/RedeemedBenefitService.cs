﻿using Microsoft.Extensions.Caching.Memory;
using OfficeOpenXml;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Email;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class RedeemedBenefitService : BaseService, IRedeemedBenefitService
    {
        private readonly IRedeemedBenefitRepository _benefitRedeemedRepository;
        private readonly IBenefitRepository _benefitRepository;
        private readonly IAttachmentRepository _attachmentRepository;
        private readonly IRandomGeneratorService _randomGeneratorService;

        public RedeemedBenefitService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _benefitRedeemedRepository = services.GetBenefitRedeemedRepository();
            _benefitRepository = services.GetBenefitRepository();
            _attachmentRepository = services.GetAttachmentRepository();
            _randomGeneratorService = services.GetRandomGeneratorService();
        }

        public async Task Add(int benefitId, BenefitType type, Stream streamFile, int? quantityOfUser,
            IUnitOfWork uow = null)
        {
            var redeemedBenefits = new List<RedeemedBenefit>();

            if (type == BenefitType.Voucher)
                redeemedBenefits = ImportFromExcel(streamFile, benefitId);

            else if (type == BenefitType.Experience || type == BenefitType.Tangible)
                redeemedBenefits = GenereateRedeemedBenefits(benefitId, quantityOfUser);

            if (redeemedBenefits.Count() > 0)
                await GetRedeemedBenefitRepository(uow).AddMany(redeemedBenefits);
        }

        public async Task Update(int benefitId, BenefitType type, int? quantityOfUser = 0, IUnitOfWork uow = null)
        {
            if (type == BenefitType.Explore || type == BenefitType.Voucher)
                return;

            // get count of free and occupied redeemedBenefit
            // compare counts with a new value, if their sum is the same, return from method
            // if not equal then update with delete or update
            var counts = await GetRedeemedBenefitRepository(uow).GetCountsOfStatus(benefitId);

            var freeRedemeedCount = GetCountOfStatus(counts, RedeemedBenefitType.Available);
            var occupiedRedemeedCount = GetCountOfStatus(counts, RedeemedBenefitType.Winner);

            int sumOfCounts = freeRedemeedCount + occupiedRedemeedCount;

            if (sumOfCounts == 0) 
                return;

            if (sumOfCounts == quantityOfUser.Value)
                return;

            if (sumOfCounts > quantityOfUser.Value)
            {
                var howManyDelete = sumOfCounts - quantityOfUser.Value;
                await GetRedeemedBenefitRepository(uow).DeleteNumberOfRedeemedBenefit(howManyDelete, benefitId);
            }

            else
            {
                var howManyAdd = quantityOfUser.Value - sumOfCounts;
                GenereateRedeemedBenefits(benefitId, howManyAdd);

                var redeemedBenefits = GenereateRedeemedBenefits(benefitId, quantityOfUser);

                if (redeemedBenefits.Count() > 0)
                    await GetRedeemedBenefitRepository(uow).AddMany(redeemedBenefits); // no transaction needed here, because there is already a tranasction outer this method
            }
        }

        public async Task<RedeemedBenefitViewMobile> Redeem(RedeemBenefitAddMobile redeemDto, int clientId)
        {
            RedeemedBenefit redeemedBenefit;
            var benefitTask = _benefitRepository.Get(redeemDto.BenefitId);
            var redeemedBenefitTask = _benefitRedeemedRepository.GetSingleAvailableReedemedBenefit(redeemDto.BenefitId);

            await Task.WhenAll(benefitTask, redeemedBenefitTask);

            var benefit = await benefitTask ?? throw new KSAException(KSAExceptionCode.Benefit.BenefitNotFound);
            benefit.ValidateForRedeemBenefit();
            redeemDto.ValidateDeliveryAddressByType(benefit.Type);

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin(IsolationLevel.Serializable);

                try
                {
                    if (benefit.Type == BenefitType.Explore)
                        return new RedeemedBenefitViewMobile(string.Empty, string.Empty, benefit.Type);

                    if (benefit.IsRandomSelection())
                        redeemedBenefit = await GenerateForParticipant(clientId, benefit.Id, redeemDto.DeliveryAddress, uow);

                    else
                    {
                        redeemedBenefit = await redeemedBenefitTask ?? throw new KSAException(KSAExceptionCode.Benefit.BenefitIsNotAvailableAnymore);

                        if (benefit.IsLimitedToOneUser.HasValue && benefit.IsLimitedToOneUser == true)
                            await ValidateUserRedeemBenefit(clientId, benefit.Id, uow);

                        redeemedBenefit.ToWinner(clientId, redeemDto.DeliveryAddress);
                        await uow.RedeemedBenefitRepository.Update(redeemedBenefit);

                        if (benefit.Type == BenefitType.Experience && await uow.RedeemedBenefitRepository.GetFirstAlreadyDelivered(benefit.Id, clientId) != null)
                            await uow.RedeemedBenefitRepository.UpdateDeliveryState(redeemedBenefit.Id, RedeemedBenefitDeliveryState.EmailSent);
                    }

                    uow.Commit();

                    return new RedeemedBenefitViewMobile(redeemedBenefit.PromoCode, string.Empty, benefit.Type);
                }
                catch (Exception ex)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }

        public async Task PickWinners(int benefitId, int numberOfWinners)
        {
            if (numberOfWinners < 1)
                throw new KSAException(KSAExceptionCode.Benefit.InvalidWinnerCount);

            if (await _benefitRedeemedRepository.GetSingleWonReedemedBenefit(benefitId) != null)
                throw new KSAException(KSAExceptionCode.Benefit.WinnerAlreadySelected);

            var redeemedBenefits = await _benefitRedeemedRepository.GetParticipating(benefitId);
            var winnerIds = await GetWinnerIds(redeemedBenefits, numberOfWinners);

            var winnerBenefits = await UpdateWinnerRedeemedBenefits(redeemedBenefits, winnerIds);

            if (winnerBenefits == null) return;
        }

        private async Task<IEnumerable<RedeemedBenefitEmailView>> UpdateWinnerRedeemedBenefits(IEnumerable<RedeemedBenefit> redeemedBenefits, IEnumerable<int> winnerIds)
        {
            var winningBenefits = redeemedBenefits.Where(benefit => winnerIds.Any(winner => winner == benefit.ClientId));
            var updateTasks = new List<Task>();
            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    foreach (var benefit in winningBenefits)
                    {
                        benefit.Status = RedeemedBenefitType.Winner;
                        updateTasks.Add(uow.RedeemedBenefitRepository.Update(benefit));
                    }

                    await Task.WhenAll(updateTasks);

                    uow.Commit();

                    return await uow.RedeemedBenefitRepository.GetEmailView(redeemedBenefits.First().BenefitId);
                }

                catch (Exception ex)
                {
                    uow.Rollback();
                    throw ex;
                }
            }
        }

        private async Task<IEnumerable<int>> GetWinnerIds(IEnumerable<RedeemedBenefit> redeemedBenefits, int winnerCount)
        {
            if (redeemedBenefits == null)
                throw new KSAException(KSAExceptionCode.Benefit.NoRedeemedBenefits);

            var participants = redeemedBenefits.Select(benefit => benefit.ClientId.Value).ToList();
            // We do -1 here because this will be used against a zero-based index collection.
            var possibleMaxValue = participants.Count() - 1;

            if (possibleMaxValue < winnerCount - 1)
                throw new KSAException(KSAExceptionCode.Benefit.NotEnoughParticipants);

            var randomSelection = await _randomGeneratorService.RandomIntegers(winnerCount, possibleMaxValue);
            return GetIdsAtIndexes(participants, randomSelection);
        }

        /// <summary>
        /// Goes through a List<int> and returns a filtered out list with the provided indexes to be returned.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<int> GetIdsAtIndexes(List<int> list, List<int> indexes)
        {
            foreach (var item in indexes)
                yield return list.ElementAt(item);
        }

        private async Task<RedeemedBenefit> GenerateForParticipant(int clientId, int benefitId, string deliveryAddress,
            IUnitOfWork uow = null)
        {
            await ValidateUserRedeemBenefit(clientId, benefitId, uow);

            return await GetRedeemedBenefitRepository(uow).Add(RedeemedBenefit.New(clientId, benefitId, deliveryAddress));
        }

        private async Task ValidateUserRedeemBenefit(int userId, int benefitId, IUnitOfWork uow = null)
        {
            var redeemedBenefit = await GetRedeemedBenefitRepository(uow).GetUserRedeemedBenefit(userId, benefitId);

            if (redeemedBenefit != null)
                throw new KSAException(KSAExceptionCode.Benefit.RedeemedBenefitAlreadyOccupied);
        }

        private List<RedeemedBenefit> ImportFromExcel(Stream stream, int benefitId)
        {
            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets[0];

                return ParseWorksheet(ref workSheet, benefitId);
            }
        }

        private List<RedeemedBenefit> GenereateRedeemedBenefits(int benefitId, int? selectedUserCount)
        {
            var redemeedBenefits = new List<RedeemedBenefit>();

            if (!selectedUserCount.HasValue) // so if if random winners selection was made, than selectedUserCount is null
                return redemeedBenefits;

            for (int i = 0; i < selectedUserCount; i++)
                redemeedBenefits.Add(new RedeemedBenefit(benefitId, string.Empty));

            return redemeedBenefits;
        }

        private List<RedeemedBenefit> ParseWorksheet(ref ExcelWorksheet workSheet, int benefitId)
        {
            var startRow = workSheet.Dimension.Start.Row + 1;
            var endRow = workSheet.Dimension.End.Row;

            var redeemedBenefits = new List<RedeemedBenefit>();

            for (int row = startRow; row <= endRow; row++)
            {
                //Dimension.End.Row sometimes takes the wrong end row, which is higher than the last row with any data in it
                if (workSheet.Cells[row, 1].Value == null)
                    break;

                redeemedBenefits.Add(new RedeemedBenefit(benefitId, workSheet.Cells[row, 1].Value.ToString().Trim()));
            }

            return redeemedBenefits;
        }

        private int GetCountOfStatus(IEnumerable<RedeemedBenefitStatusView> statuses, RedeemedBenefitType status)
        {
            var count = statuses.FirstOrDefault(x => x.Status == status)?.Count;

            if (count.HasValue)
                return count.Value;

            return 0;
        }

        public async Task ValidateChangeOfBenefitType(int benefitId, IUnitOfWork uow = null)
        {
            var redeemedBenefit = await GetRedeemedBenefitRepository(uow).GetSingleAvailableReedemedBenefit(benefitId);

            if (redeemedBenefit != null)
                throw new KSAException(KSAExceptionCode.Benefit.RedeemedBenefitAlreadyExists);
        }

        private IRedeemedBenefitRepository GetRedeemedBenefitRepository(IUnitOfWork uow)
        {
            return uow != null ? uow.RedeemedBenefitRepository : _benefitRedeemedRepository;
        }

        private IAttachmentRepository GetAttachmentRepository(IUnitOfWork uow)
        {
            return uow != null ? uow.AttachmentRepository : _attachmentRepository;
        }
    }
}
