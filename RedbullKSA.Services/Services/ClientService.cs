﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.Auth;
using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class ClientService : BaseService, IClientService
    {
        private readonly IPasswordService _passwordService;
        private readonly IClientRepository _clientRepository;

        public ClientService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _passwordService = services.GetPasswordService();
            _clientRepository = services.GetClientRepository();
        }

        public async Task ChangePassword(ChangePasswordDTO changePasswordDto, int clientId)
        {
            if (changePasswordDto.OldPassword == changePasswordDto.NewPassword)
                throw new KSAException(KSAExceptionCode.General.PasswordSameAsOld);

            if (changePasswordDto.NewPassword != changePasswordDto.RepeatPassword)
                throw new KSAException(KSAExceptionCode.General.PasswordsDoNotMatch);

            if (!changePasswordDto.NewPassword.PasswordMeetsRequirements())
                throw new KSAException(KSAExceptionCode.General.PasswordDoesNotMeetRequirements);

            var client = await _clientRepository.Get(clientId);

            if (!_passwordService.ValidatePassword(changePasswordDto.OldPassword, client.Guid, client.Password))
                throw new KSAException(KSAExceptionCode.General.UserPasswordIncorrect);

            client.Password = _passwordService.HashPassword(changePasswordDto.NewPassword, client.Guid);

            await _clientRepository.Update(client);
        }

       
        public async Task<ClientProfileMobile> GetMobileProfile(int? clientId)
        {
            if (clientId == null)
            {
                throw new KSAException(KSAExceptionCode.General.RequestBodyIsNull);
            }

            var client = await _clientRepository.Get((int)clientId);
            return new ClientProfileMobile(client) ?? throw new KSAException(KSAExceptionCode.Client.ClientDoesNotExist);
        }
        
    }
}
