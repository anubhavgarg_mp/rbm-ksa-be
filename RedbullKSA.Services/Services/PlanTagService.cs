﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.PlanModels;
using RedbullKSA.Entities.API.Models.PlanModels.Tags;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Repositories.Contracts.Plans;
using RedbullKSA.Services.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class PlanTagService : BaseService, IPlanTagService
    {
        private readonly IPlanTagRepository _planTagRepository;

        public PlanTagService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
            _planTagRepository = serviceProvider.GetPlanTagRepository();
        }

        public async Task<PlanTagDTO> Create(CreatePlanTagRequest tag)
        {
            var planTag = await _planTagRepository.Add(
                new PlanTag
                {
                    NameEN = tag.NameEN,
                    NameAR = tag.NameAR
                });

            return new PlanTagDTO(planTag);
        }

        public async Task Update(UpdatePlanTagRequest tag)
        {
            if (tag.Id == 0)
            {
                throw new KSAException(KSAExceptionCode.Plan.TagIdNotProvided);
            }

            var dbTag = await _planTagRepository.Find(tag.Id);

            if (dbTag == null)
            {
                throw new KSAException(KSAExceptionCode.Plan.TagNotFound);
            }

            dbTag.NameEN = string.IsNullOrEmpty(tag.NameEN) ? dbTag.NameEN : tag.NameEN;
            dbTag.NameAR = string.IsNullOrEmpty(tag.NameAR) ? dbTag.NameAR : tag.NameAR;
            await _planTagRepository.Update(dbTag);
        }

        public async Task<PlanTagDTO> Get(int id)
        {
            if (id == 0)
            {
                throw new KSAException(KSAExceptionCode.Plan.TagIdNotProvided);
            }

            var tag = await _planTagRepository.Find(id)
                ?? throw new KSAException(KSAExceptionCode.Plan.TagNotFound);

            return new PlanTagDTO(tag);
        }

        public async Task<IEnumerable<PlanTagDTO>> GetAll()
        {
            var tags = await _planTagRepository.GetAll();
            return tags.Select(t => new PlanTagDTO(t));
        }
    }
}
