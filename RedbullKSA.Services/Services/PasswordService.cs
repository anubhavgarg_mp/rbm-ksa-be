﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace RedbullKSA.Services.Services
{
    public class PasswordService : BaseService, IPasswordService
    {

        public PasswordService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache) { }

        public string HashPassword(string password, Guid guid)
        {
            byte[] salt = guid.ToByteArray();

            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

            byte[] saltedValue = passwordBytes.Concat(salt).ToArray();

            byte[] hashedValue = new SHA256Managed().ComputeHash(saltedValue);

            return $"{Convert.ToBase64String(hashedValue)}";
        }

        public bool ValidatePassword(string password, Guid guid, string correctHash)
        {
            var salt = guid.ToByteArray();

            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            var saltedValue = passwordBytes.Concat(salt).ToArray();
            var hash = Convert.FromBase64String(correctHash);

            var testHash = new SHA256Managed().ComputeHash(saltedValue);

            return SlowEquals(hash, testHash);
        }

        private static bool SlowEquals(byte[] a, byte[] b)
        {
            var diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
            {
                diff |= (uint)(a[i] ^ b[i]);
            }
            return diff == 0;
        }
    }
}
