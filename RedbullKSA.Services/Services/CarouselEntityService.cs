﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.CarouselModels;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class CarouselEntityService : BaseHuaweiService, ICarouselEntityService
    {
        private readonly ICarouselEntityRepository _carouselEntityRepository;
        private readonly IAttachmentService _attachmentService;
        private readonly IAttachmentRepository _attachmentRepository;

        public CarouselEntityService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
            _carouselEntityRepository = serviceProvider.GetCarouselEntityRepository();
            _attachmentService = serviceProvider.GetAttachmentService();
            _attachmentRepository = serviceProvider.GetAttachmentRepository();
        }

        public async Task<CarouselEntityDetailsView> AddEntity(CarouselEntityDTO details, int? addedBy)
        {
            if (!details.ArabicImageId.HasValue || !details.EnglishImageId.HasValue)
                throw new KSAException(KSAExceptionCode.CarouselEntity.CarouselPhotoMissing);

            var entity = new CarouselEntity(details);
            var englishAttachment = await _attachmentRepository.Get(details.EnglishImageId.Value);
            var arabicAttachment = await _attachmentRepository.Get(details.ArabicImageId.Value);

            if (englishAttachment == null || arabicAttachment == null)
                throw new KSAException(KSAExceptionCode.CarouselEntity.InvalidAttachmentId);

            entity.ImageUrlEn = englishAttachment.FileUrl;
            entity.ImageUrlAr = arabicAttachment.FileUrl;

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    var addedCarouselEntity = await uow.CarouselEntityRepository.Add(entity, addedBy);
                    englishAttachment.CarouselEntityId = addedCarouselEntity.Id;
                    arabicAttachment.CarouselEntityId = addedCarouselEntity.Id;

                    var updateEnglish = uow.AttachmentRepository.Update(englishAttachment);
                    var updateArabic = uow.AttachmentRepository.Update(arabicAttachment);

                    await Task.WhenAll(updateEnglish, updateArabic);

                    uow.Commit();

                    return new CarouselEntityDetailsView(addedCarouselEntity);
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }
            
        public async Task<IEnumerable<CarouselListViewEntity>> GetAll()
        {
            var entities = await _carouselEntityRepository.GetAll();

            return entities?.Select(x => new CarouselListViewEntity(x)) ?? new List<CarouselListViewEntity>();
        }

        public async Task<IEnumerable<MobileCarouselEntity>> GetActive(WebParameters parameters, ServiceLanguage language)
        {
            var entities = await _carouselEntityRepository.GetActive(parameters);

            return entities?.Select(x => new MobileCarouselEntity(x, language)) ?? new List<MobileCarouselEntity>();
        }

        public async Task<CarouselEntityDetailsView> Update(int id, CarouselEntityDTO newDetails, int? modifiedBy)
        {
            var foundEntity = await _carouselEntityRepository.Get(id);

            if (foundEntity == null)
                throw new KSAException(KSAExceptionCode.CarouselEntity.CarouselEntityNotFound);

            var entity = new CarouselEntity(newDetails, foundEntity.Id);

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    var updateTasks = new List<Task>();

                    if (newDetails.ArabicImageId.HasValue)
                    {
                        if (await _attachmentRepository.Get(newDetails.ArabicImageId.Value) == null)
                            throw new KSAException(KSAExceptionCode.CarouselEntity.InvalidAttachmentId);

                        await _attachmentService.DetachCarouselEntityAttachments(entity.Id, ServiceLanguage.AR, AttachmentCategory.CarouselImage, uow);
                        var arabicAttachment = await uow.AttachmentRepository.Get(newDetails.ArabicImageId.Value);

                        arabicAttachment.FileLanguage = ServiceLanguage.AR;
                        arabicAttachment.CarouselEntityId = entity.Id;
                        entity.ImageUrlAr = arabicAttachment.FileUrl;

                        updateTasks.Add(uow.AttachmentRepository.Update(arabicAttachment));
                    }

                    if (newDetails.EnglishImageId.HasValue)
                    {
                        if (await _attachmentRepository.Get(newDetails.EnglishImageId.Value) == null)
                            throw new KSAException(KSAExceptionCode.CarouselEntity.InvalidAttachmentId);

                        await _attachmentService.DetachCarouselEntityAttachments(entity.Id, ServiceLanguage.EN, AttachmentCategory.CarouselImage, uow);
                        var englishAttachment = await uow.AttachmentRepository.Get(newDetails.EnglishImageId.Value);

                        englishAttachment.FileLanguage = ServiceLanguage.EN;
                        englishAttachment.CarouselEntityId = entity.Id;
                        entity.ImageUrlEn = englishAttachment.FileUrl;

                        updateTasks.Add(uow.AttachmentRepository.Update(englishAttachment));
                    }

                    updateTasks.Add(uow.CarouselEntityRepository.Update(entity, modifiedBy));

                    await Task.WhenAll(updateTasks);

                    uow.Commit();

                    return new CarouselEntityDetailsView(entity);

                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }
            }
        }

        public async Task<CarouselEntityDetailsView> Get(int id)
        {
            var entity = await _carouselEntityRepository.Get(id);

            if (entity == null)
                throw new KSAException(KSAExceptionCode.CarouselEntity.CarouselEntityNotFound);

            return new CarouselEntityDetailsView(entity);

        }
    }
}