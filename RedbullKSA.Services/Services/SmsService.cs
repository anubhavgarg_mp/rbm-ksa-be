﻿using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.PDU;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RedbullKSA.Common.Configuration;
using RedbullKSA.Common.Enums;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.SmsModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class SmsService : ISmsService
    {
        private readonly ConfigurationSettings _config;
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ILogger<SmsService> _logger;
        private readonly SmppClient _client;

        public SmsService() { }

        public SmsService(IServiceScopeFactory scopeFactory, ILogger<SmsService> logger, IOptions<ConfigurationSettings> config)
        {
            _config = config.Value;
            _scopeFactory = scopeFactory;
            _logger = logger;

            _client = new SmppClient();
            _client.evDeliverSm += OnDeliverSmTracking;
        }

        public async Task<string> Send(SendSMSRequestModel request)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var smsRepository = scope.ServiceProvider.GetSmsPhoneNumberRepository();

                try
                {
                    if (await _client.ConnectAsync(_config.Smpp.IP, _config.Smpp.Port))
                    {
                        BindResp bindResp = await _client.BindAsync(_config.Smpp.SystemId, _config.Smpp.Password);

                        if (bindResp.Header.Status == CommandStatus.ESME_ROK)
                        {
                            var sentSmsRecord = await smsRepository.Add(new SmsPhoneNumber
                            {
                                SerialNumber = request.SerialNumber,
                                Timestamp = DateTime.Now,
                                PhoneNumber = request.LaunchContactNumber,
                                Status = SentSmsStatus.Created
                            });

                            _logger.LogTrace("Created sms record");

                            var list = SMS.ForSubmit()
                                    .From(_config.Smpp.SourceNumber)
                                    .To(request.LaunchContactNumber)
                                    .Coding(DataCodings.UCS2)
                                    .DeliveryReceipt()
                                    .Text(request.Message)
                                    .Create(_client);

                            foreach (SubmitSm sm in list)
                            {
                                sm.Header.Sequence = _client.SequenceGenerator.NextSequenceNumber();
                            }

                            var submitResp = await _client.SubmitAsync(list);

                            _logger.LogTrace("Submitted SMS");

                            if (submitResp.All(x => x.Header.Status == CommandStatus.ESME_ROK))
                            {
                                await smsRepository.SaveMessagePartIds(sentSmsRecord.Id, submitResp.Select(s => s.MessageId).ToList());
                                _logger.LogTrace("Saved message parts");
                                await smsRepository.UpdateMessageStatus(sentSmsRecord.Id, SentSmsStatus.SentToServer);
                                _logger.LogTrace("Updated message status");

                                return $"Message sent. Record ID:  {sentSmsRecord.Id}";
                            }
                            else
                            {
                                return "Message sending failed.";
                            }
                        }
                        else
                        {
                            await _client.DisconnectAsync();
                            throw new KSAException(KSAExceptionCode.SMS.BindFailure);
                        }

                    }
                    else
                        throw new KSAException(KSAExceptionCode.SMS.ConnectionFailure);
                }
                // Pass all KSAExceptions thrown from inside, capture all others and wrap as KSAException
                catch (Exception ex) when (ex.GetType() != typeof(KSAException))
                {
                    throw new KSAException(KSAExceptionCode.SMS.UnexpectedError, ex.Message);
                }
                finally
                {
                    if (_client.Status == ConnectionStatus.Open || _client.Status == ConnectionStatus.Bound)
                        await _client.DisconnectAsync();
                }

            }
        }

        public async Task Send(string destination, string messageBody)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                var smsMessageRepository = scope.ServiceProvider.GetSmsMessageRepository();

                try
                {
                    if (await _client.ConnectAsync(_config.Smpp.IP, _config.Smpp.Port))
                    {
                        BindResp bindResp = await _client.BindAsync(_config.Smpp.SystemId, _config.Smpp.Password);

                        if (bindResp.Header.Status == CommandStatus.ESME_ROK)
                        {
                            var submitResp = await _client.SubmitAsync(
                                SMS.ForSubmit()
                                    .From(_config.Smpp.SourceNumber)
                                    .To(destination)
                                    .Coding(DataCodings.UCS2)
                                    .Text(messageBody)
                                );

                            if (submitResp.All(x => x.Header.Status == CommandStatus.ESME_ROK))
                            {
                                var sentSmsRecord = await smsMessageRepository.Add(new SmsMessage
                                {
                                    MessageBody = messageBody,
                                    Timestamp = DateTime.Now,
                                    Destination = destination
                                });
                            }
                            else
                            {
                                throw new KSAException(KSAExceptionCode.SMS.MessageSendingFailed);
                            }
                        }
                        else
                        {
                            await _client.DisconnectAsync();
                            throw new KSAException(KSAExceptionCode.SMS.BindFailure);
                        }
                    }
                    else
                        throw new KSAException(KSAExceptionCode.SMS.ConnectionFailure);
                }
                // Pass all KSAExceptions thrown from inside, capture all others and wrap as KSAException
                catch (Exception ex) when (ex.GetType() != typeof(KSAException))
                {
                    throw new KSAException(KSAExceptionCode.SMS.UnexpectedError, ex.Message);
                }
                finally
                {
                    if (_client.Status == ConnectionStatus.Open || _client.Status == ConnectionStatus.Bound)
                        await _client.DisconnectAsync();
                }
            }
        }

        public async Task<bool> IsPresent(string msisdn)
        {
            using var scope = _scopeFactory.CreateScope();

            return await scope.ServiceProvider.GetSmsPhoneNumberRepository().IsPresent(msisdn);
        }

        private async void OnDeliverSmTracking(object o, DeliverSm deliverSm)
        {
            if (deliverSm.MessageType == MessageTypes.SMSCDeliveryReceipt)
            {
                using var scope = _scopeFactory.CreateScope();
                var smsRepository = scope.ServiceProvider.GetSmsPhoneNumberRepository();
                var errorRepository = scope.ServiceProvider.GetErrorLogRepository();

                string messageId = deliverSm.Receipt.MessageId;
                MessageState deliveryStatus = deliverSm.Receipt.State;
                _logger.LogTrace("Got receipt for message id {id}; Status {status}", messageId, deliveryStatus);

                try
                {
                    var allMessageParts = await smsRepository.UpdateMessagePartStatus(messageId, MapFromInetlabStatus(deliveryStatus));
                    _logger.LogTrace("Updated message part status");

                    // If all message parts were delivered, it means the whole message was delivered
                    if (allMessageParts.All(x => x.Status == SmppMessageState.Delivered))
                        await smsRepository.UpdateMessageStatus(allMessageParts.First().WholeMessageId, SentSmsStatus.Delivered);

                } catch (Exception ex)
                {
                    _logger.LogError(ex, "Exception during delivery receipt processing: ");
                    // Some values are set 
                    await errorRepository.Add(new ErrorLogEntry
                    {
                        Device = SessionTokenType.Api,
                        UserName = "None",
                        RequestUrl = "None",
                        RequestParameters = "None",
                        ExceptionType = ExceptionType.System,
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    });
                }

            }
        }

        private SmppMessageState MapFromInetlabStatus(MessageState status)
            => status switch
            {
                MessageState.None => SmppMessageState.None,
                MessageState.Enroute => SmppMessageState.Enroute,
                MessageState.Delivered => SmppMessageState.Delivered,
                MessageState.Expired => SmppMessageState.Expired,
                MessageState.Deleted => SmppMessageState.Deleted,
                MessageState.Undeliverable => SmppMessageState.Undeliverable,
                MessageState.Accepted => SmppMessageState.Accepted,
                MessageState.Unknown => SmppMessageState.Unknown,
                MessageState.Rejected => SmppMessageState.Rejected,
                _ => SmppMessageState.None
            };
    }
}
