﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Comparers;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class ClientInterestService : BaseService, IClientInterestService
    {
        private readonly IClientInterestRepository _clientInterestRepository;

        public ClientInterestService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _clientInterestRepository = services.GetClientInterestRepository();
        }

        public async Task<ClientInterestsMobile> GetClientInterests(int clientId, ServiceLanguage serviceLanguage)
        {
            var clientInterests = await _clientInterestRepository.GetClientInterestsView(clientId);
            return ClientInterestsMobile.FromInterestList(clientInterests, serviceLanguage);
        }

        public async Task<ClientInterestsMobile> UpdateClientInterests(int clientId, ClientInterestsMobile clientInterests)
        {
            var currentClientInterests = await _clientInterestRepository.GetClientsInterests(clientId);

            var updatedClientInterests = ClientInterest.FromMobileClientInterests(clientInterests.Interests, clientId);

            var newClientInterests = updatedClientInterests.Except(currentClientInterests, new ClientInterestEqualityComparer());
            var deletedClientInterests = currentClientInterests.Except(updatedClientInterests, new ClientInterestEqualityComparer());

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    var deleteClientInterests = uow.ClientInterestRepository.Delete(deletedClientInterests);
                    var addClientInterests = uow.ClientInterestRepository.AddMany(newClientInterests);

                    await Task.WhenAll(deleteClientInterests, addClientInterests);
                    uow.Commit();
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }
            }

            return clientInterests;
        }
    }
}
