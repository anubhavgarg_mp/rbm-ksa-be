﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class InterestService : BaseService, IInterestService
    {
        private readonly IInterestRepository _interestRepository;
        private readonly IClientInterestRepository _clientInterestRepository;
        private readonly IAttachmentService _attachmentService;
        private readonly IAttachmentRepository _attachmentRepository;

        public InterestService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
            _interestRepository = services.GetInterestRepository();
            _clientInterestRepository = services.GetClientInterestRepository();
            _attachmentService = services.GetAttachmentService();
            _attachmentRepository = services.GetAttachmentRepository();
        }

        public async Task<InterestDetailsView> AddInterest(InterestDTO details, int addedBy)
        {
            if (string.IsNullOrWhiteSpace(details.Name.English) || string.IsNullOrWhiteSpace(details.Name.Arabic))
                throw new KSAException(KSAExceptionCode.Interest.InterestNameMissing);

            if (await _interestRepository.Get(details.Name.English, details.Name.Arabic) != null)
                throw new KSAException(KSAExceptionCode.Interest.InterestNameExists);

            if (!details.ArabicImageId.HasValue || !details.EnglishImageId.HasValue)
                throw new KSAException(KSAExceptionCode.Interest.InterestPhotoMissing);

            var interest = Interest.FromEditModel(details);
            var englishAttachmentTask = _attachmentRepository.Get(details.EnglishImageId.Value);
            var arabicAttachmentTask = _attachmentRepository.Get(details.ArabicImageId.Value);

            await Task.WhenAll(englishAttachmentTask, arabicAttachmentTask);
            var englishAttachment = englishAttachmentTask.Result;
            var arabicAttachment = arabicAttachmentTask.Result;

            interest.ImageEN = englishAttachment.FileUrl;
            interest.ImageAR = arabicAttachment.FileUrl;

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    var addedInterest = await uow.InterestRepository.Add(interest, addedBy);
                    englishAttachment.InterestId = addedInterest.Id;
                    arabicAttachment.InterestId = addedInterest.Id;

                    var updateEnglish = uow.AttachmentRepository.Update(englishAttachment);
                    var updateArabic = uow.AttachmentRepository.Update(arabicAttachment);

                    await Task.WhenAll(updateEnglish, updateArabic);

                    uow.Commit();

                    return InterestDetailsView.FromInterest(addedInterest);
                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }

            }
        }

        public async Task<InterestDetailsView> EditDetails(int id, InterestDTO details, int modifiedBy)
        {
            var interest = await _interestRepository.Get(id);
            interest.ExtractDetails(details);

            using (TransactionSession transactionSession = new TransactionSession(_services))
            {
                UnitOfWork uow = transactionSession.UnitOfWork;
                uow.Begin();

                try
                {
                    var updateTasks = new List<Task>();

                    if (details.ArabicImageId.HasValue)
                    {
                        var deattachTask = _attachmentService.DetachInterestAttachments(interest.Id, ServiceLanguage.AR, uow);
                        var attachmentTask = uow.AttachmentRepository.Get(details.ArabicImageId.Value);

                        await Task.WhenAll(deattachTask, attachmentTask);

                        var arabicAttachment = attachmentTask.Result;
                        arabicAttachment.FileLanguage = ServiceLanguage.AR;
                        arabicAttachment.InterestId = interest.Id;
                        interest.ImageAR = arabicAttachment.FileUrl;

                        updateTasks.Add(uow.AttachmentRepository.Update(arabicAttachment));
                    }

                    if (details.EnglishImageId.HasValue)
                    {
                        var deattachTask = _attachmentService.DetachInterestAttachments(interest.Id, ServiceLanguage.EN, uow);
                        var attachmentTask = uow.AttachmentRepository.Get(details.EnglishImageId.Value);

                        await Task.WhenAll(deattachTask, attachmentTask);

                        var englishAttachment = attachmentTask.Result;
                        englishAttachment.FileLanguage = ServiceLanguage.EN;
                        englishAttachment.InterestId = interest.Id;
                        interest.ImageEN = englishAttachment.FileUrl;

                        updateTasks.Add(uow.AttachmentRepository.Update(englishAttachment));
                    }

                    updateTasks.Add(uow.InterestRepository.Update(interest, modifiedBy));

                    await Task.WhenAll(updateTasks);

                    uow.Commit();

                    return InterestDetailsView.FromInterest(interest);

                }
                catch (Exception)
                {
                    uow.Rollback();
                    throw;
                }

            }
        }

        public async Task SaveSelected(ClientInterestsRequest request, int clientId)
        {
            if (request.Interests.Count <= 0)
                throw new KSAException(KSAExceptionCode.Interest.NoneSelected);

            await _clientInterestRepository.DeleteByClient(clientId);

            await _clientInterestRepository.AddMany(request.Interests.Select(i =>
                new ClientInterest {
                    ClientId = clientId,
                    InterestId = i
                }));
        }
    }
}
