﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Entities.API.Models.OrderModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Enums.Address;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class OrderService : BaseService, IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IClientAddressRepository _clientAddressRepository;

        public OrderService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
            _orderRepository = serviceProvider.GetOrderRepository();
            _clientAddressRepository = serviceProvider.GetClientAddressRepository();
        }

        public async Task Add(int clientId, SimOrderRequest request)
        {
            int? clientAddressId = null;

            if (request.DeliveryType == SimPickupType.HomeDelivery)
            {
                var address = await _clientAddressRepository.Add(new ClientAddress 
                { 
                    ClientId = clientId,
                    AddressType = AddressType.Delivery,
                    City = request.DeliveryAddress.City,
                    Street = request.DeliveryAddress.Street,
                    AddressLine4 = request.DeliveryAddress.Address,
                    Province = request.DeliveryAddress.District,
                    PostCode = request.DeliveryAddress.ZipCode,
                    IsDefault = false
                });

                clientAddressId = address.Id;
            }

            await _orderRepository.Add(new OngoingOrder
            {
                DeliveryType = request.DeliveryType,
                NumberTier = request.NumberTier,
                SimType = request.SimType,
                MSISDN = request.MSISDN,
                ClientId = clientId,
                ClientAddressId = clientAddressId
            });
        }
    }
}
