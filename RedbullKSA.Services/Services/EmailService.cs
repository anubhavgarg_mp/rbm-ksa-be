﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Entities.API.Models.BenefitModels.Email;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Services
{
    public class EmailService : BaseService, IEmailService
    {

        public EmailService(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache) { }

        public async Task SendRawViaSmtp(string email, string subject, string content)
        {
            using (var client = new SmtpClient(_config.Email.SmtpHost, _config.Email.SmtpPort))
            {
                client.Credentials = new NetworkCredential(_config.Email.SmtpUsername, _config.Email.SmtpPassword);
                client.EnableSsl = true;

                var message = BaseMessage(new MailAddress(email), subject, content, false);

                await client.SendMailAsync(message);
            }
        }

        public async Task SendHtmlViaSmtp(string email, string subject, string content)
        {
            using (var client = new SmtpClient(_config.Email.SmtpHost, _config.Email.SmtpPort))
            {
                client.Credentials = new NetworkCredential(_config.Email.SmtpUsername, _config.Email.SmtpPassword);
                client.EnableSsl = true;

                var message = BaseMessage(new MailAddress(email), subject, content);

                await client.SendMailAsync(message);
            }
        }

        private MailMessage BaseMessage(MailAddress to, string subject, string content, bool isContentHtml = true)
        {
            return new MailMessage(new MailAddress(_config.Email.SenderAddress, _config.Email.DisplayUsername), to)
            {
                Subject = subject,
                IsBodyHtml = isContentHtml,
                Body = content
            };
        }
    }
}
