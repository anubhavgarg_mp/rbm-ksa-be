﻿using RedbullKSA.Common.Comparers;
using RedbullKSA.Entities.API.Models.BenefitModels;
using System.Collections.Generic;

namespace RedbullKSA.Services.Comparers
{
    public static class BenefitNotificationComparer
    {
        public static readonly IEqualityComparer<BenefitPushNotificationView> ClientIdComparer = new CustomEqualityComparer<BenefitPushNotificationView>(a => new { a.ClientId });
    }
}