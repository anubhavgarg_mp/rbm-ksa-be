﻿using RedbullKSA.Common.Comparers;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using System.Collections.Generic;

namespace RedbullKSA.Services.Comparers
{
    public class OfferingComparer
    {
        public static readonly IEqualityComparer<SupplementaryOfferingDTO> OfferingIdComparer = new CustomEqualityComparer<SupplementaryOfferingDTO>(a => new { a.OfferingId });
    }
}
