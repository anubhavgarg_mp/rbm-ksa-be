﻿using RedbullKSA.Entities.Database.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Services.Comparers
{
    public class ClientInterestEqualityComparer : IEqualityComparer<ClientInterest>
    {
        public bool Equals(ClientInterest x, ClientInterest y) => x.InterestId == y.InterestId;

        public int GetHashCode(ClientInterest obj) => base.GetHashCode();
    }
}
