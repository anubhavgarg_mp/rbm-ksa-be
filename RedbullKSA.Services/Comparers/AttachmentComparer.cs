﻿using RedbullKSA.Common.Comparers;
using RedbullKSA.Entities.API.Models.AttachmentModels;
using RedbullKSA.Entities.Database.Models;
using System.Collections.Generic;

namespace RedbullKSA.Services.Comparers
{
    public static class AttachmentComparer
    {
        public static readonly IEqualityComparer<AttachmentModel> Id = new CustomEqualityComparer<AttachmentModel>(a => new { a.Id });
    }
}
