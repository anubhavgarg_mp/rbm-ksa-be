﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using RedbullKSA.Common.Configuration;
using RedbullKSA.Entities.API.Models.EmailModels;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts.Plans;
using RedbullKSA.Repositories.Repositories.Plans;
using RedbullKSA.Services.Exporting;
using RedbullKSA.Services.Exporting.Contracts;
using RedbullKSA.Services.Services;
using RedbullKSA.Services.Services.Benefits;
using RedbullKSA.Services.Services.Contracts;
using RedbullKSA.Services.Strategies.Email;
using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using System.Security.Cryptography;

namespace RedbullKSA.Services.Base
{
    public static class ServiceExtensions
    {
        #region ACCESS_EXTENSIONS

        // Services
        public static IUserService GetUserService(this IServiceProvider services) => services.GetRequiredService<IUserService>();
        public static ISessionTokenService GetSessionTokenService(this IServiceProvider services) => services.GetRequiredService<ISessionTokenService>();
        public static IRngService GetRngService(this IServiceProvider services) => services.GetRequiredService<IRngService>();
        public static IAuthService GetAuthService(this IServiceProvider services) => services.GetRequiredService<IAuthService>();
        public static IPasswordService GetPasswordService(this IServiceProvider services) => services.GetRequiredService<IPasswordService>();
        public static IClientService GetClientService(this IServiceProvider services) => services.GetRequiredService<IClientService>();
        public static IInterestService GetInterestService(this IServiceProvider services) => services.GetRequiredService<IInterestService>();
        public static IPlanService GetPlanService(this IServiceProvider services) => services.GetRequiredService<IPlanService>();
        public static IPlanTagService GetPlanTagService(this IServiceProvider services) => services.GetRequiredService<IPlanTagService>();
        public static IAttachmentService GetAttachmentService(this IServiceProvider services) => services.GetRequiredService<IAttachmentService>();
        public static IExcelExportService GetExcelExportService(this IServiceProvider services) => services.GetRequiredService<IExcelExportService>();
        public static IPhoneNumberConfirmationService GetPhoneNumberConfirmationService(this IServiceProvider services) => services.GetRequiredService<IPhoneNumberConfirmationService>();
        public static IClientInterestService GetClientInterestService(this IServiceProvider services) => services.GetRequiredService<IClientInterestService>();
        public static IBenefitService GetBenefitService(this IServiceProvider services) => services.GetRequiredService<IBenefitService>();
        public static IBenefitValidationPickupStrategy GetBenefitValidationStrategy(this IServiceProvider services) => services.GetRequiredService<IBenefitValidationPickupStrategy>();
        public static IBenefitAttachmentService GetBenefitAttachmentService(this IServiceProvider services) => services.GetRequiredService<IBenefitAttachmentService>();
        public static IRedeemedBenefitService GetRedeemedBenefitService(this IServiceProvider services) => services.GetRequiredService<IRedeemedBenefitService>();
        public static IEmailService GetEmailService(this IServiceProvider services) => services.GetRequiredService<IEmailService>();
        
        #region HUAWEI SERVICES

        public static IInventoryHuaweiService GetInventoryHuaweiService(this IServiceProvider services) => services.GetRequiredService<IInventoryHuaweiService>();
        public static IRechargeHuaweiService GetRechargeHuaweiService(this IServiceProvider services) => services.GetRequiredService<IRechargeHuaweiService>();
        public static IOfferingHuaweiService GetOfferingHuaweiService(this IServiceProvider services) => services.GetRequiredService<IOfferingHuaweiService>();
        public static ICustomerHuaweiService GetCustomerHuaweiService(this IServiceProvider services) => services.GetRequiredService<ICustomerHuaweiService>();
        public static ISubscriberHuaweiService GetSubscriberHuaweiService(this IServiceProvider services) => services.GetRequiredService<ISubscriberHuaweiService>();
        public static IUtilityHuaweiService GetUtilityHuaweiService(this IServiceProvider services) => services.GetRequiredService<IUtilityHuaweiService>();
        public static IBillingHuaweiService GetBillingHuaweiService(this IServiceProvider services) => services.GetRequiredService<IBillingHuaweiService>();
        public static IAccountHuaweiService GetAccountHuaweiService(this IServiceProvider services) => services.GetRequiredService<IAccountHuaweiService>();

        #endregion

        public static ISmsService GetSmsService(this IServiceProvider services) => services.GetRequiredService<ISmsService>();
        public static ICarouselEntityService GetCarouselEntityService(this IServiceProvider services) => services.GetRequiredService<ICarouselEntityService>();
        public static IOrderService GetOrderService(this IServiceProvider services) => services.GetRequiredService<IOrderService>();
        public static IPaymentService GetPaymentService(this IServiceProvider services) => services.GetRequiredService<IPaymentService>();
        public static IClientAddressService GetClientAddressService(this IServiceProvider services) => services.GetRequiredService<IClientAddressService>();
        public static IFAQService GetFAQService(this IServiceProvider services) => services.GetRequiredService<IFAQService>();
        public static INumberPortabilityService GetNumberPortabilityService(this IServiceProvider services) => services.GetRequiredService<INumberPortabilityService>();

        // Repositories
        public static IUserRepository GetUserRepository(this IServiceProvider services) => services.GetRequiredService<IUserRepository>();
        public static ISessionTokenRepository GetSessionTokenRepository(this IServiceProvider services) => services.GetRequiredService<ISessionTokenRepository>();
        public static IUserClaimRepository GetUserClaimRepository(this IServiceProvider services) => services.GetRequiredService<IUserClaimRepository>();
        public static IClientRepository GetClientRepository(this IServiceProvider services) => services.GetRequiredService<IClientRepository>();
        public static IClientMssisdnRepository GetClientMsisdnRepository(this IServiceProvider services) => services.GetRequiredService<IClientMssisdnRepository>();
        public static IInterestRepository GetInterestRepository(this IServiceProvider services) => services.GetRequiredService<IInterestRepository>();
        public static IClientAddressRepository GetClientAddressRepository(this IServiceProvider services) => services.GetRequiredService<IClientAddressRepository>();
        public static IClientInterestRepository GetClientInterestRepository(this IServiceProvider services) => services.GetRequiredService<IClientInterestRepository>();
        public static IPlanRepository GetPlanRepository(this IServiceProvider services) => services.GetRequiredService<IPlanRepository>();
        public static IPlanTagRepository GetPlanTagRepository(this IServiceProvider services) => services.GetRequiredService<IPlanTagRepository>();
        public static IAttachmentRepository GetAttachmentRepository(this IServiceProvider services) => services.GetRequiredService<IAttachmentRepository>();
        public static INotificationRepository GetNotificationRepository(this IServiceProvider services) => services.GetRequiredService<INotificationRepository>();
        public static IPhoneNumberConfirmationRepository GetPhoneNumberConfirmationRepository(this IServiceProvider services) => services.GetRequiredService<IPhoneNumberConfirmationRepository>();
        public static IBenefitRepository GetBenefitRepository(this IServiceProvider services) => services.GetRequiredService<IBenefitRepository>();
        public static IRedeemedBenefitRepository GetBenefitRedeemedRepository(this IServiceProvider services) => services.GetRequiredService<IRedeemedBenefitRepository>();
        public static ITagRepository GetTagRepository(this IServiceProvider services) => services.GetRequiredService<ITagRepository>();
        public static IBenefitTagRepository GetBenefitTagRepository(this IServiceProvider services) => services.GetRequiredService<IBenefitTagRepository>();
        public static ISmsPhoneNumberRepository GetSmsPhoneNumberRepository(this IServiceProvider services) => services.GetRequiredService<ISmsPhoneNumberRepository>();
        public static ICarouselEntityRepository GetCarouselEntityRepository(this IServiceProvider services) => services.GetRequiredService<ICarouselEntityRepository>();
        public static IOrderRepository GetOrderRepository(this IServiceProvider services) => services.GetRequiredService<IOrderRepository>();
        public static ISmsMessageRepository GetSmsMessageRepository(this IServiceProvider services) => services.GetRequiredService<ISmsMessageRepository>();
        public static IErrorLogEntryRepository GetErrorLogRepository(this IServiceProvider services) => services.GetRequiredService<IErrorLogEntryRepository>();
        public static IFAQRepository GetFAQRepository(this IServiceProvider services) => services.GetRequiredService<IFAQRepository>();
        public static INumberPortabilityRepository GetNumberPortabilityRepository(this IServiceProvider services) => services.GetRequiredService<INumberPortabilityRepository>();

        //strategies
        public static BenefitExperienceValidationStrategy GetBenefitExperienceValidation(this IServiceProvider services) => services.GetRequiredService<BenefitExperienceValidationStrategy>();
        public static BenefitVoucherValidationStrategy GetBenefitVoucherValidation(this IServiceProvider services) => services.GetRequiredService<BenefitVoucherValidationStrategy>();
        public static BenefitExploreValidationStrategy GetBenefitExploreValidation(this IServiceProvider services) => services.GetRequiredService<BenefitExploreValidationStrategy>();

        public static IEmailSendingStrategy<PortingNumberEmailData> GetNumberPortingEmailStrategy(this IServiceProvider services) => services.GetRequiredService<NumberPortingEmailStrategy>();

        // Other
        public static ConfigurationSettings GetConfig(this IServiceProvider services) => services.GetRequiredService<ConfigurationSettings>();
        public static RNGCryptoServiceProvider GetRngCrypto(this IServiceProvider service) => service.GetRequiredService<RNGCryptoServiceProvider>();
        public static IRandomGeneratorService GetRandomGeneratorService(this IServiceProvider services) => services.GetRequiredService<IRandomGeneratorService>();
        public static IOptions<ConfigurationSettings> GetConfigurationSettings(this IServiceProvider services) => services.GetRequiredService<IOptions<ConfigurationSettings>>();

        #endregion ACCESS_EXTENSIONS

        #region HTTP

        // All HttpClient configurations go here, more info: 
        // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/http-requests?view=aspnetcore-6.0
        public static IServiceCollection AddNamedHttpClients(this IServiceCollection services, IConfigurationRoot config)
        {
            services.AddHttpClient("Huawei", cfg =>
            {
                cfg.BaseAddress = new Uri(config.GetValue<string>("Huawei:Url"));
                cfg.Timeout = TimeSpan.FromSeconds(60);
                
            }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; }
            });

            return services;
        }

        #endregion HTTP

        #region MISCELLANEOUS

        public static IServiceCollection AddSmppLicense(this IServiceCollection services)
        {
            var rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var license = Path.Combine(rootPath, "Inetlab.SMPP.license");

            using var reader = new FileStream(license, FileMode.Open);
            Inetlab.SMPP.LicenseManager.SetLicense(reader);

            return services;
        }

        #endregion MISCELLANEOUS

        // Called on API startup. DI configuration.
        public static IServiceCollection ConfigureServices(this IServiceCollection services, IConfigurationRoot config)
        {
            // Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<ISessionTokenService, SessionTokenService>();
            services.AddScoped<IRngService, RngService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IPasswordService, PasswordService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IInterestService, InterestService>();
            services.AddScoped<IPlanService, PlanService>();
            services.AddScoped<IPlanTagService, PlanTagService>();
            services.AddScoped<IExcelExportService, ExcelExportService>();
            services.AddScoped<IAttachmentService, AttachmentService>();
            services.AddScoped<IPhoneNumberConfirmationService, PhoneNumberConfirmationService>();
            services.AddScoped<IClientAddressService, ClientAddressService>();
            services.AddScoped<IClientInterestService, ClientInterestService>();
            services.AddScoped<IBenefitService, BenefitService>();
            services.AddScoped<IBenefitValidationPickupStrategy, BenefitValidationPickupStrategy>();
            services.AddScoped<IBenefitAttachmentService, BenefitAttachmentService>();
            services.AddScoped<IRedeemedBenefitService, RedeemedBenefitService>();
            services.AddScoped<IFAQService, FAQService>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IInventoryHuaweiService, InventoryHuaweiService>();
            services.AddScoped<ICustomerHuaweiService, CustomerHuaweiService>();
            services.AddScoped<IRechargeHuaweiService, RechargeHuaweiService>();
            services.AddScoped<IOfferingHuaweiService, OfferingHuaweiService>();
            services.AddScoped<ISubscriberHuaweiService, SubscriberHuaweiService>();
            services.AddScoped<IUtilityHuaweiService, UtilityHuaweiService>();
            services.AddScoped<IBillingHuaweiService, BillingHuaweiService>();
            services.AddScoped<ISmsService, SmsService>();
            services.AddScoped<IAccountHuaweiService, AccountHuaweiService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPaymentService, PayfortPaymentService>();
            services.AddScoped<INumberPortabilityService, NumberPortabilityService>();
            services.AddScoped<ICarouselEntityService, CarouselEntityService>();

            //// Repositories
            services.AddTransient<IDataStore, DataStore>();

            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ISessionTokenRepository, SessionTokenRepository>();
            services.AddScoped<IUserClaimRepository, UserClaimRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IClientMssisdnRepository, ClientMsisdnRepository>();
            services.AddScoped<IClientAddressRepository, ClientAddressRepository>();
            services.AddScoped<IInterestRepository, InterestRepository>();
            services.AddScoped<IClientInterestRepository, ClientInterestRepository>();
            services.AddScoped<INotificationRepository, NotificationRepository>();
            services.AddScoped<IPlanRepository, PlanRepository>();
            services.AddScoped<IPlanTagRepository, PlanTagRepository>();
            services.AddScoped<IPhoneNumberConfirmationRepository, PhoneNumberConfirmationRepository>();
            services.AddScoped<IAttachmentRepository, AttachmentRepository>();
            services.AddScoped<IBenefitRepository, BenefitRepository>();
            services.AddScoped<IRedeemedBenefitRepository, RedeemedBenefitRepository>();
            services.AddScoped<IFAQRepository, FAQRepository>();
            services.AddScoped<ITagRepository, TagRepository>();
            services.AddScoped<IBenefitTagRepository, BenefitTagRepository>();
            services.AddScoped<ISmsPhoneNumberRepository, SmsPhoneNumberRepository>();
            services.AddScoped<ICarouselEntityRepository, CarouselEntityRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IErrorLogEntryRepository, ErrorLogEntryRepository>();
            services.AddScoped<ISystemLogEntryRepository, SystemLogEntryRepository>();
            services.AddScoped<ISmsMessageRepository, SmsMessageRepository>();
            services.AddScoped<INumberPortabilityRepository, NumberPortabilityRepository>();

            //strategies
            services.AddScoped<IEmailSendingStrategy<PortingNumberEmailData>, NumberPortingEmailStrategy>();

            // Other
            services.AddScoped<IRandomGeneratorService, RandomGeneratorService>();
            services.AddSingleton<RNGCryptoServiceProvider>();
            services.AddSingleton<ISmsService, SmsService>();

            return services;
        }
    }
}
