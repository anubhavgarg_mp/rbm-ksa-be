﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Entities.API.Models.PaymentModels.Requests;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using RestSharp;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.PaymentModels.Base;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Common.Extensions;
using System.Net.Http;
using static System.Net.Mime.MediaTypeNames;
using Microsoft.Extensions.DependencyInjection;

namespace RedbullKSA.Services.Base
{
    public abstract class BasePayfortPaymentService : BaseService
    {
        private readonly string DEF_LANGUAGE = ServiceLanguage.AR.GetDescription();
        private readonly IHttpClientFactory _httpClientFactory;


        public BasePayfortPaymentService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache)
        {
            _httpClientFactory = serviceProvider.GetRequiredService<IHttpClientFactory>();
        }

        protected async Task<P> Post<T, P>(T requestBody, string queryParams = "")
        {
            try
            {
                using var client = _httpClientFactory.CreateClient("PayFort");
                var requestContent = new StringContent(JsonConvert.SerializeObject(requestBody), Encoding.UTF8, Application.Json);

                var urlPath = _config.PayFortConfig.ApiUrl + queryParams;
                using var response = await client.PostAsync(urlPath, requestContent);

                if (!response.IsSuccessStatusCode)
                {
                    throw new KSAException(KSAExceptionCode.Payment.ResponseUnsuccessful, response.ReasonPhrase);
                }

                var responseContent = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<P>(responseContent);
            }
            catch (Exception ex)
            {
                throw new KSAException(KSAExceptionCode.Payment.RequestFailed, ex.Message);
            }

        }

        protected virtual T CreateBodyBase<T>(string language = null) where  T : PayFortRequestBase, new()
        {
            T body = new T()
            {
                AccessCode = _config.PayFortConfig.AccessCode,
                MerchantIdentifier = _config.PayFortConfig.MerchantIdentifier,
                Language = language ?? DEF_LANGUAGE,
            };

            return body;
        }

        protected string GetSignatureForCreationSdkToken(CreateSdkTokenRequest request)
        {
            var createTokenSignature = $@"{_config.PayFortConfig.SHARequestPhrase}
                access_code={request.AccessCode}
                device_id={request.DeviceId}
                language={request.Language}
                merchant_identifier={request.MerchantIdentifier}
                service_command={request.ServiceCommand}
                {_config.PayFortConfig.SHARequestPhrase}";

            // removing characters of new line and empty spaces
            return Regex.Replace(createTokenSignature, @"\t|\n|\r| ", "");
        }

        protected static string GetHashedString(string value)
        {

            var crypt = new SHA256Managed();
            var hash = new StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(value));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }

        protected RestClient GetRestClient()
        {
            return new RestClient(_config.PayFortConfig.ApiUrl);
        }
    }
}
