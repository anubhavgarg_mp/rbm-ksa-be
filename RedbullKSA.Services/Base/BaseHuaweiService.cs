﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RedbullKSA.Common.Helpers;
using System.Net.Http;
using RedbullKSA.Entities.Constants;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using RedbullKSA.Services.Services.Contracts;
using RedbullKSA.Common.Exceptions;
using System.Net.Sockets;

namespace RedbullKSA.Services.Base
{
    public abstract class BaseHuaweiService : BaseService
    {
        public const string successCode = "0000";
        private readonly IHttpClientFactory _httpClientFactory;
        private IRandomGeneratorService _rng;

        public BaseHuaweiService(IServiceProvider serviceProvider, IMemoryCache memoryCache) : base(serviceProvider, memoryCache) {
            _rng = serviceProvider.GetRequiredService<IRandomGeneratorService>();
            _httpClientFactory = serviceProvider.GetRequiredService<IHttpClientFactory>();
        }

        public async Task<P> Post<T, P>(string urlPath, string xmlMessage, string timeType = "2") where T : class where P : BaseResponseMessage
        {
            string xmlRequest = ConstructEnvelopeBody(xmlMessage.Replace("[REQUESTHEADER]", await ConstructRequestHeader(timeType)));
            try { 

                using (var client = _httpClientFactory.CreateClient("Huawei")) {
                    var request = new StringContent(xmlRequest, Encoding.UTF8, "text/xml");
                    using (var response = await client.PostAsync(urlPath, request))
                    {
                        var soapResponse = await response.Content.ReadAsStringAsync();

                        XmlDocument xmlResponse = new();
                        xmlResponse.LoadXml(soapResponse);

                        var restResponse = ConvertSoapToRest<T, P>(xmlResponse);

                        if (restResponse.RspHeader.ReturnCode != successCode)
                            throw new KSAException(KSAExceptionCode.Huawei.ResponseFailure, restResponse.RspHeader.ReturnMsg);

                        return restResponse;
                    }
                }
            }
            catch (Exception ex) when (ex.InnerException is SocketException)
            {
                throw new KSAException(KSAExceptionCode.Huawei.APIUnreachable);
            }

        }

        public virtual P ConvertSoapToRest<T, P>(XmlDocument xmlDoc) where T : class
        {
            return ConversionHelper.ConvertSoapToRest<T, P>(xmlDoc);
        }

        public virtual string ConstructEnvelopeBody(string xmlMessage) => String.Format(
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" >
                <soapenv:Header/>
                <soapenv:Body>
                    {0}
                </soapenv:Body>
            </soapenv:Envelope>", xmlMessage);

        protected virtual async Task<string> ConstructRequestHeader(string timeType) => String.Format(
            @"<com:ReqHeader>
                <com:Version>{0}</com:Version>
                <com:TransactionId>{1}</com:TransactionId>
                <com:Channel>{2}</com:Channel>
                <com:PartnerId>{3}</com:PartnerId>
                <com:ReqTime>{4}</com:ReqTime>
                <com:TimeFormat>
                    <com:TimeType>{5}</com:TimeType>
                </com:TimeFormat>
                <com:AccessUser>{6}</com:AccessUser>
                <com:AccessPassword>{7}</com:AccessPassword>
            </com:ReqHeader>", _config.Huawei.Version, await GenerateTransactionId(), _config.Huawei.Channel, _config.Huawei.PartnerId,
            DateTime.Now.ToString("yyyyMMddHHmmss"), timeType, _config.Huawei.AccessUser, _config.Huawei.AccessPassword);

        private async Task<string> GenerateTransactionId() {
            return $"{DateTime.Now.ToString("yyyyMMddHHmmss")}{(int)(await _rng.Range(0.0, 1.0)*8000)+1000}";
        }

    }
}
