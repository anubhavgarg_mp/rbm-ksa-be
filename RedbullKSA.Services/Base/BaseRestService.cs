﻿using Microsoft.ApplicationInsights;
using Microsoft.Extensions.Caching.Memory;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Base
{
    public abstract class BaseRestService : BaseService
    {

        public string BaseUrl { get; set; }
        
        public string ApiKey { get; set; }

        public BaseRestService(IServiceProvider services, IMemoryCache memoryCache, TelemetryClient telemetry) : base(services, memoryCache)
        {
        }

        public RestClient BaseClient()
        {
            var client = new RestClient(BaseUrl);
            client.AddHandler("application/json", () => RestSharp.Serializers.Newtonsoft.Json.NewtonsoftJsonSerializer.Default);

            return client;
        }

        public RestRequest BaseRequest(string url, Method method)
        {
            var request = new RestRequest(url, method)
            {
                RequestFormat = DataFormat.Json,
                JsonSerializer = new RestSharp.Serializers.Newtonsoft.Json.NewtonsoftJsonSerializer()
            };

            return request;
        }

        public async Task<IRestResponse<T>> ExecuteTaskAsync<T>(IRestRequest request)
        {
            var startTime = DateTime.UtcNow;
            var watch = System.Diagnostics.Stopwatch.StartNew();

            var response = await BaseClient().ExecuteTaskAsync<T>(request);

            watch.Stop();

            return response;
        }

        public void ExecuteTask(IRestRequest request)
        {
            var startTime = DateTime.UtcNow;
            var watch = System.Diagnostics.Stopwatch.StartNew();

            var response = BaseClient().Execute(request);

            watch.Stop();
        }
    }
}
