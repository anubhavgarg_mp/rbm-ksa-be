﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Common.Configuration;
using System;
using Microsoft.Extensions.Caching.Memory;

namespace RedbullKSA.Services.Base
{
    public abstract class BaseService
    {
        protected readonly IServiceProvider _services;
        protected readonly ILogger _logger;
        protected readonly ConfigurationSettings _config;
        protected readonly IMemoryCache _memoryCache;

        public BaseService(IServiceProvider serviceProvider, IMemoryCache memoryCache)
        {
            _services = serviceProvider;
            _logger = _services.GetService<ILoggerFactory>().CreateLogger(this.GetType());
            _config = serviceProvider.GetService<ConfigurationSettings>();
            _memoryCache = memoryCache;
        }
    }
}
