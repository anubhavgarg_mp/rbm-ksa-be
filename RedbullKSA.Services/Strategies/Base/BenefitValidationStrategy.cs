﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;

namespace RedbullKSA.Services.Strategies.Base
{
    public abstract class BenefitValidationStrategy : BaseService, IBenefitValidation
    {
        protected BenefitValidationStrategy(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache) { }

        public BenefitWithAttachmentsDetails ValidateBenefit(BenefitWithAttachmentsDetails benefit)
        {
            ValidateDetails(benefit);
            Clear(benefit); //abstract
            Validate(benefit); //abstract

            return benefit;
        }

        public abstract BenefitWithAttachmentsDetails Validate(BenefitWithAttachmentsDetails benefit);
        public abstract BenefitWithAttachmentsDetails Clear(BenefitWithAttachmentsDetails benefit);

        protected virtual void ValidateDetails(BenefitWithAttachmentsDetails details)
        {
            ValidateModel(details.BenefitEN, ServiceLanguage.EN);
            ValidateModel(details.BenefitAR, ServiceLanguage.AR);
            ValidateEmail(details.Email);

            if (details.StartDate <= DateTime.MinValue)
                throw new KSAException(KSAExceptionCode.Benefit.StartDateIsInvalid);

            if (details.StartDate > details.EventDate || details.StartDate > details.EndDate) // if start date is a head of event 
                throw new KSAException(KSAExceptionCode.Benefit.StartDateIsInvalid);

            if (!details.IsIndefinite && !details.EndDate.HasValue)
                throw new KSAException(KSAExceptionCode.Benefit.EndDateIsInvalid);
        }

        private void ValidateModel(BenefitModel model, ServiceLanguage language)
        {
            if (string.IsNullOrWhiteSpace(model.Name))
                throw new KSAException(KSAExceptionCode.Benefit.NameIsInvalid, $"({language.GetDescription()})");

            if (string.IsNullOrWhiteSpace(model.Description) || model.Description.StripHTMLTags().Length > 120)
                throw new KSAException(KSAExceptionCode.Benefit.DescriptionIsInvalid, $"({language.GetDescription()})");

            if (string.IsNullOrWhiteSpace(model.DescriptionTeaser) || model.DescriptionTeaser.Length > 150)
                throw new KSAException(KSAExceptionCode.Benefit.DescriptionTeaserIsInvalid, $"({language.GetDescription()})");

            if (string.IsNullOrWhiteSpace(model.SecondaryDescription))
                throw new KSAException(KSAExceptionCode.Benefit.SecondaryDescriptionIsInvalid, $"({language.GetDescription()})");

            if (string.IsNullOrWhiteSpace(model.Location))
                throw new KSAException(KSAExceptionCode.Benefit.LocationIsInvalid, $"({language.GetDescription()})");
        }

        private void ValidateEmail(BenefitEmailView email)
        {
            if (string.IsNullOrWhiteSpace(email.IntroEN) || string.IsNullOrWhiteSpace(email.IntroAR))
                throw new KSAException(KSAExceptionCode.Benefit.EmailIntroIsInvalid);

            if (string.IsNullOrWhiteSpace(email.InstructionTitleEN)
                || string.IsNullOrWhiteSpace(email.InstructionTitleAR)
                || string.IsNullOrWhiteSpace(email.InstructionDescriptionEN)
                || string.IsNullOrWhiteSpace(email.InstructionDescriptionAR))
                throw new KSAException(KSAExceptionCode.Benefit.EmailIntroIsInvalid);
        }
    }
}
