﻿using RedbullKSA.Entities.API.Models.EmailModels;
using RedbullKSA.Services.Base;
using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Strategies.Email
{
    public class NumberPortingEmailStrategy : IEmailSendingStrategy<PortingNumberEmailData>
    {
        private readonly IEmailService _emailService;

        private const string SHARED_MAILBOX = "test@testovich.com";
        private const string SUBJECT = "Request to port number has been made";

        public NumberPortingEmailStrategy(IServiceProvider serviceProvider)
        {
            _emailService = serviceProvider.GetEmailService();
        }

        public async Task Send(PortingNumberEmailData emailData)
        {
            // TODO: When template is known, fill it with data and send email

        }
    }
}
