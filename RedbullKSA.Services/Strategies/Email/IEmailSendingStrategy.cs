﻿using RedbullKSA.Services.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Services.Strategies.Email
{
    public interface IEmailSendingStrategy<T>
    {
        Task Send(T emailData);
    }
}
