﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Services.Strategies.Base;
using System;

namespace RedbullKSA.Services.Services.Benefits
{
    public class BenefitExploreValidationStrategy : BenefitValidationStrategy
    {
        public bool IsAppUrlsEN { get; private set; }
        public bool IsAppUrlsAR { get; private set; }

        public BenefitExploreValidationStrategy(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache) { }

        public override BenefitWithAttachmentsDetails Clear(BenefitWithAttachmentsDetails benefit)
        {
            benefit.SkuCode = null;
            benefit.AvailableQuantity = null;
            benefit.NotificationType = null;
            benefit.DistributionType = null;
            benefit.IsLimitedToOneUser = null;
            benefit.EndDate = benefit.IsIndefinite ? null : benefit.EndDate;

            if (IsAppUrlsEN)
            {
                benefit.BenefitEN.ButtonUrl = null;
            }
            else
            {
                benefit.BenefitEN.AppleStoreUrl = null;
                benefit.BenefitEN.GooglePlayUrl = null;
            }

            if (IsAppUrlsAR)
            {
                benefit.BenefitAR.ButtonUrl = null;
            }
            else
            {
                benefit.BenefitAR.AppleStoreUrl = null;
                benefit.BenefitAR.GooglePlayUrl = null;
            }

            return benefit;
        }

        public override BenefitWithAttachmentsDetails Validate(BenefitWithAttachmentsDetails benefit) => benefit;

        protected override void ValidateDetails(BenefitWithAttachmentsDetails details)
        {
            if (string.IsNullOrEmpty(details.BenefitEN.ButtonUrl)) // if button is null, then it is app urls and they must be not null
            {
                if (string.IsNullOrEmpty(details.BenefitEN.AppleStoreUrl) || string.IsNullOrEmpty(details.BenefitEN.GooglePlayUrl))
                    throw new KSAException(KSAExceptionCode.Benefit.AppUrlIsInvalid);

                IsAppUrlsEN = true;
            }
            else // otherwise non of app urls must have values
            {
                if (!string.IsNullOrEmpty(details.BenefitEN.AppleStoreUrl) || !string.IsNullOrEmpty(details.BenefitEN.GooglePlayUrl))
                    throw new KSAException(KSAExceptionCode.Benefit.ButtonUrlIsInvalid);

                IsAppUrlsEN = false;
            }


            if (string.IsNullOrEmpty(details.BenefitAR.ButtonUrl)) // if button is null, then it is app urls and they must be not null
            {
                if (string.IsNullOrEmpty(details.BenefitAR.AppleStoreUrl) || string.IsNullOrEmpty(details.BenefitAR.GooglePlayUrl))
                    throw new KSAException(KSAExceptionCode.Benefit.AppUrlIsInvalid);

                IsAppUrlsAR = true;
            }
            else // otherwise non of app urls must have values
            {
                if (!string.IsNullOrEmpty(details.BenefitAR.AppleStoreUrl) || !string.IsNullOrEmpty(details.BenefitAR.GooglePlayUrl))
                    throw new KSAException(KSAExceptionCode.Benefit.ButtonUrlIsInvalid);

                IsAppUrlsAR = false;
            }

            base.ValidateDetails(details);
        }
    }
}
