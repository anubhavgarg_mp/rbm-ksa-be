﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Services.Strategies.Base;
using System;

namespace RedbullKSA.Services.Services.Benefits
{
    public class BenefitVoucherValidationStrategy : BenefitValidationStrategy
    {
        public BenefitVoucherValidationStrategy(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache) { }

        public override BenefitWithAttachmentsDetails Clear(BenefitWithAttachmentsDetails benefit)
        {
            benefit.AvailableQuantity = null;
            benefit.BenefitEN.AppleStoreUrl = null;
            benefit.BenefitAR.AppleStoreUrl = null;
            benefit.BenefitEN.GooglePlayUrl = null;
            benefit.BenefitAR.GooglePlayUrl = null;
            benefit.EndDate = benefit.IsIndefinite ? null : benefit.EndDate;

            return benefit;
        }

        public override BenefitWithAttachmentsDetails Validate(BenefitWithAttachmentsDetails benefit)
        {
            return benefit;
        }
    }
}
