﻿using Microsoft.Extensions.Caching.Memory;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Services.Strategies.Base;
using System;

namespace RedbullKSA.Services.Services.Benefits
{
    public class BenefitExperienceValidationStrategy : BenefitValidationStrategy
    {
        public BenefitExperienceValidationStrategy(IServiceProvider services, IMemoryCache memoryCache) : base(services, memoryCache)
        {
        }

        public override BenefitWithAttachmentsDetails Clear(BenefitWithAttachmentsDetails benefit)
        {
            benefit.BenefitEN.ButtonUrl = null;
            benefit.BenefitAR.ButtonUrl = null;
            benefit.BenefitEN.AppleStoreUrl = null;
            benefit.BenefitAR.AppleStoreUrl = null;
            benefit.BenefitEN.GooglePlayUrl = null;
            benefit.BenefitAR.GooglePlayUrl = null;
            benefit.IsLimitedToOneUser = null;
            benefit.EndDate = benefit.IsIndefinite ? null : benefit.EndDate;
            benefit.AvailableQuantity = benefit.LotteryIsRandom ? null : benefit.AvailableQuantity;
            return benefit;
        }

        public override BenefitWithAttachmentsDetails Validate(BenefitWithAttachmentsDetails benefit)
        {
            if (!benefit.LotteryIsRandom && benefit.AvailableQuantity.HasValue && benefit.AvailableQuantity.Value <= 0)
                throw new KSAException(KSAExceptionCode.Benefit.AvailableQtyIsInvalid);

            if (string.IsNullOrEmpty(benefit.SkuCode))
                throw new KSAException(KSAExceptionCode.Benefit.SkuCodeIsInvalid);

            return benefit;
        }
    }
}
