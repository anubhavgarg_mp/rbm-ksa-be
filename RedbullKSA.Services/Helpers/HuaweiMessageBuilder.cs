﻿using RedbullKSA.Common.Helpers;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.API.Models.HuaweiModels.Common;
using RedbullKSA.Entities.Constants;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RedbullKSA.Services.Helpers
{
    /// <summary>
    /// Helper class for Huawei message construction
    /// </summary>
    public static class HuaweiMessageBuilder
    {
        private const string XML_COMMON = "http://www.huawei.com/bss/soaif/interface/common/";
        private const string XML_INVENTORY_SERVICE = "http://www.huawei.com/bss/soaif/interface/InventoryService/";
        private const string XML_CUSTOMER_SERVICE = "http://www.huawei.com/bss/soaif/interface/CustomerService/";
        private const string XML_SUBSCRIBER_SERVICE = "http://www.huawei.com/bss/soaif/interface/SubscriberService/";
        private const string XML_UTILITY_SERIVCE = "http://www.huawei.com/bss/soaif/interface/UtilityService/";
        private const string XML_OFFERING_SERVICE = "http://www.huawei.com/bss/soaif/interface/OfferingService/";
        private const string XML_RECHARGE_SERVICE = "http://www.huawei.com/bss/soaif/interface/RechargeService/";
        private const string XML_BILLING_SERVICE = "http://www.huawei.com/bss/soaif/interface/BillingService/";
        private const string XML_ACCOUNT_SERVICE = "http://www.huawei.com/bss/soaif/interface/AccountService/";

        #region BILLING

        public static string QueryUnbilledAmountMessage(UnbilledAmountRequest request)
        {
            string message =
            $@"<bil:QueryUnbilledAmountReqMsg xmlns:bil=""{XML_BILLING_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                [ACCESSINFO]
            </bil:QueryUnbilledAmountReqMsg>";

            string additionalFields = request.PaymentType != null ? $@"<bil:PaymentType>{(byte)request.PaymentType}</bil:PaymentType>" : null;

            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Billing, request.AccessInfo.ObjectId, request.AccessInfo.ObjectIdType, additionalFields);
        }

        #endregion

        #region INVENTORY

        public static string AvailableNumberMessage(string pattern, string numberLevel, int count, string itemCode = "MSISDN")
            => $@"<inv:QueryAvailableNumberReqMsg xmlns:inv=""{XML_INVENTORY_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                <inv:ItemCode>{itemCode}</inv:ItemCode>
                <inv:Pattern>{pattern}</inv:Pattern>
                <inv:NumberLevel>{numberLevel}</inv:NumberLevel>
                <inv:ResCnt>{count}</inv:ResCnt>
             </inv:QueryAvailableNumberReqMsg>";

        public static string SIMStatusMessage(string iccid, string puk)
            => $@"<inv:QuerySIMStatusReqMsg xmlns:inv=""{XML_INVENTORY_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                <inv:ICCID>{iccid}</inv:ICCID>
                { (!string.IsNullOrEmpty(puk) ?
                GetAdditionalPropertyTags("inv", new List<SimpleProperty> { new SimpleProperty { Code = "PUK", Value = puk } }) : "") }
            </inv:QuerySIMStatusReqMsg>";

        public static string OperateMSISDNMessage(MsisdnOperationType operationType, List<string> msisdns)
            => $@"<inv:OperateMsisdnReqMsg xmlns:inv=""{XML_INVENTORY_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                <inv:ResType>10</inv:ResType>
                <inv:OperType>{(short)operationType}</inv:OperType>
                <inv:ResCodeList>
                    {string.Join("\n", msisdns.Select(m => $"<inv:ResCode>{m}</inv:ResCode>"))}
                </inv:ResCodeList>
          </inv:OperateMsisdnReqMsg>";

        #endregion INVENTORY

        #region CUSTOMER

        public static string CustomerInformationMessage(CustomerInformationSearchCategory searchCategory, string searchText)
        {
            var tagParameter = searchCategory switch
            {
                CustomerInformationSearchCategory.CustomerId => "CustId",
                CustomerInformationSearchCategory.ServiceNumber => "ServiceNumber",
                _ => "CustId"
            };

            return $@"<cus:QueryCustInfoReqMsg xmlns:cus=""{XML_CUSTOMER_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                <cus:{tagParameter}>{searchText}</cus:{tagParameter}>
                <cus:ReturnConsentFlag>0</cus:ReturnConsentFlag>
                 <cus:IsSubscriberOrAccountIncluded>2</cus:IsSubscriberOrAccountIncluded>
             </cus:QueryCustInfoReqMsg>";
        }

        #endregion CUSTOMER

        #region SUBSCRIBER

        public static string QueryEntityIdsMessage(string serviceNumber)
            => $@"<sub:QueryEntityIdsReqMsg xmlns:sub=""{XML_SUBSCRIBER_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                <sub:ServiceNum>{serviceNumber}</sub:ServiceNum>
             </sub:QueryEntityIdsReqMsg>";

        public static string QueryChangeSubscriberInformationMessage(ChangeSubscriberInformation request)
            => $@"<sub:ChangeSubInfoReqMsg xmlns:sub=""{XML_SUBSCRIBER_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                <sub:Subscriber>
                    { (string.IsNullOrEmpty(request.Language) ? "" : $"<sub:Language>{request.Language}</sub:Language>") }
                    { (string.IsNullOrEmpty(request.WrittenLanguage) ? "" : $"<sub:WrittenLanguage>{request.WrittenLanguage}</sub:WrittenLanguage>") }
                    <sub:IMEI>{request.IMEI}</sub:IMEI>
                    {GetAdditionalPropertyTags("sub", request.AdditionalProperties)}
                </sub:Subscriber>
             </sub:ChangeSubInfoReqMsg>";

        public static string QuerySubscriberMateDatesMessage(List<ObjectAccessInfo> subscribers)
            => $@"<sub:QuerySubMateDatesReqMsg xmlns:sub=""{XML_SUBSCRIBER_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                { string.Join("\n", 
                        subscribers.Select(s => 
                            $"<sub:AccessInfo>\n<com:ObjectIdType>{(byte)s.ObjectIdType}</com:ObjectIdType>\n<com:ObjectId>{s.ObjectId}</com:ObjectId></sub:AccessInfo>")) 
                }
             </sub:QuerySubMateDatesReqMsg>";

        public static string QuerySubscriberAllInformationMessage(QuerySubscriberAllInformationRequest request)
        {
            string message =
            $@"<sub:QuerySubAllInfoReqMsg xmlns:sub=""{XML_SUBSCRIBER_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                [ACCESSINFO]
                <sub:IncludeOfferFlag>{(request.IncludeOfferFlag ? 1 : 0)}</sub:IncludeOfferFlag>
             </sub:QuerySubAllInfoReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Subscriber, request.AccessInfo.ObjectId, request.AccessInfo.ObjectIdType);
        }

        #endregion SUBSCRUBER

        #region UTILITY

        public static string CalOneOffFeeMessage(CalOneOffFeeRequest request)
            => $@"<util:CalOneOffFeeReqMsg xmlns:util=""{XML_UTILITY_SERIVCE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                <util:BusinessCode>{request.BusinessCode}</util:BusinessCode>
                <util:PrimaryOfferingId>
                  <com:OfferingId>{request.PrimaryOfferingIdDTO.OfferingId}</com:OfferingId>
                  {(request.PrimaryOfferingIdDTO.PurchaseSeq == null ? string.Empty : "<com:PurchaseSeq></com:PurchaseSeq>")}
                 </util:PrimaryOfferingId>
                 <util:SuppOfferingId>
                    <com:OfferingId>{request.SupplementaryOfferingIdDTO.OfferingId}</com:OfferingId>
                    {(request.SupplementaryOfferingIdDTO.PurchaseSeq == null ? string.Empty : "<com:PurchaseSeq></com:PurchaseSeq>")}
                 </util:SuppOfferingId>
                 <util:ServiceNum>{request.ServiceNumber}</util:ServiceNum>
                 <util:ICCIDItemCode>{request.ICCIDItemCode}</util:ICCIDItemCode>
                 {GetAdditionalPropertyTags("util", request.AdditionalProperties)}
            </util:CalOneOffFeeReqMsg>";
        #endregion UTILITY

        #region OFFERING
        public static string PurchasedPrimaryOfferingMessage(string objectId)
        {
            string message =
                $@"<off:QueryPurchasedPrimaryOfferingReqMsg xmlns:off=""{XML_OFFERING_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    [ACCESSINFO]
                </off:QueryPurchasedPrimaryOfferingReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Offering, objectId);
        }

        public static string ChangePrimaryOfferingMessage(string objectId, string oldOfferId, long purchaseSeq, string newOfferId, string mode = "0")
        {
            string message =
                $@"<off:ChangePrimaryOfferingReqMsg xmlns:off=""{XML_OFFERING_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    [ACCESSINFO]
                    <off:OldPrimaryOffering>
                        <com:OfferingId>{oldOfferId}</com:OfferingId>
                        <com:PurchaseSeq>{purchaseSeq}</com:PurchaseSeq>
                    </off:OldPrimaryOffering>
                    <off:NewPrimaryOffering>
                        <com:OfferingId>
                            <com:OfferingId>{newOfferId}</com:OfferingId>
                        </com:OfferingId>
                        <off:EffectiveMode>
                            <com:Mode>{0}</com:Mode>
                        </off:EffectiveMode>
                    </off:NewPrimaryOffering>
                </off:ChangePrimaryOfferingReqMsg>";

            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Offering, objectId);
        }

        public static string AvailablePrimaryOfferingsMessage(string objectId)
        {
            string message =
                $@"<off:QueryAvailablePrimaryOfferingReqMsg xmlns:off=""{XML_OFFERING_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    [ACCESSINFO]
                </off:QueryAvailablePrimaryOfferingReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Offering, objectId);
        }

        public static string PurchasedSupplementaryOfferingMessage(string objectId)
        {
            string message =
                $@"<off:QueryPurchasedSupplementaryOfferingReqMsg xmlns:off=""{XML_OFFERING_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    [ACCESSINFO]
                </off:QueryPurchasedSupplementaryOfferingReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Offering, objectId);
        }

        public static string ChangeSupplementaryOfferingMessage(string objectId, string newOfferId, string effectiveMode = "0", string activeMode = "A")
        {
            string message =
                $@"<off:ChangeSupplementaryOfferingReqMsg xmlns:off=""{XML_OFFERING_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    [ACCESSINFO]
                     <off:AddOffering>
                        <com:OfferingId>
                            <com:OfferingId>{newOfferId}</com:OfferingId>
                        </com:OfferingId>
                        <off:EffectiveMode>
                            <com:Mode>{effectiveMode}</com:Mode>
                        </off:EffectiveMode>
                        <off:ActiveMode>
                            <com:Mode>{activeMode}</com:Mode>
                        </off:ActiveMode>
                    </off:AddOffering>
                </off:ChangeSupplementaryOfferingReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Offering, objectId);
        }

        public static string AvailableSupplementaryOfferingsMessage(string objectId)
        {
            string message =
                $@"<off:QueryAvailableSupplementaryOfferingReqMsg xmlns:off=""{XML_OFFERING_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    [ACCESSINFO]
                </off:QueryAvailableSupplementaryOfferingReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Offering, objectId);
        }

        public static string SupplementaryOfferingsMessage(object primaryOfferingId)
        {
            string message =
                $@"<off:QueryAvailableSupplementaryOfferingReqMsg xmlns:off=""{XML_OFFERING_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    <off:OfferingId>
                        <com:OfferingId>{primaryOfferingId}</com:OfferingId>
                    </off:OfferingId>
                </off:QueryAvailableSupplementaryOfferingReqMsg>";
            return message;
        }
        #endregion

        #region RECHARGE
        public static string RechargeByCashMessage(RechargeModel rechargeModel)
        {
            string message =
                $@"<rec:RechargeByCashReqMsg xmlns:rec=""{XML_RECHARGE_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    [ACCESSINFO]
                    <rec:Amount>{rechargeModel.Amount}</rec:Amount>
                    <rec:Currency>{rechargeModel.Currency}</rec:Currency>
                </rec:RechargeByCashReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Recharge, rechargeModel.ObjectId);
        }

        public static string RechargeByVoucherMessage(RechargeVoucherModel rechargeModel)
        {
            string message =
                $@"<rec:RechargeByVoucherReqMsg xmlns:rec=""{XML_RECHARGE_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                    [REQUESTHEADER]
                    [ACCESSINFO]
                    <rec:RechargePIN>{rechargeModel.PIN}</rec:RechargePIN>
                </rec:RechargeByVoucherReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Recharge, rechargeModel.ObjectId);
        }

        public static string QueryRechargeLog(RechargeLogModel rechargeLog)
        {
            string message =
                $@"<rec:QueryRechargeLogReqMsg xmlns:rec=""{XML_RECHARGE_SERVICE}"" xmlns:com=""{XML_COMMON}"">
            [REQUESTHEADER]
                    [ACCESSINFO]
                    <rec:TimePeriod>
                        <com:StartTime>{ConversionHelper.DateTimeToHuaweiDate(rechargeLog.TimePeriod.StartTime)}</com:StartTime>
                        <com:EndTime>{ConversionHelper.DateTimeToHuaweiDate(rechargeLog.TimePeriod.EndTime)}</com:EndTime>
                    </rec:TimePeriod>
                    <rec:PagingInfo>
                        <com:TotalRowNum>{rechargeLog.PagingInfo.TotalRowNumber}</com:TotalRowNum>
                        <com:BeginRowNum>{rechargeLog.PagingInfo.BeginRowNumber}</com:BeginRowNum>
                        <com:FetchRowNum>{rechargeLog.PagingInfo.FetchRowNumber}</com:FetchRowNum>
                    </rec:PagingInfo>
                </rec:QueryRechargeLogReqMsg>";
            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Recharge, rechargeLog.ObjectId);

        }
        #endregion

        #region ACCOUNT
        public static string QueryAccountBalanceAndFreeUnit(QueryBalanceAndFreeUnitRequest request)
        {
            var message = $@"<acc:QueryBalanceAndFreeUnitReqMsg xmlns:acc=""{XML_ACCOUNT_SERVICE}"" xmlns:com=""{XML_COMMON}"">
                [REQUESTHEADER]
                [ACCESSINFO]
             </acc:QueryBalanceAndFreeUnitReqMsg>";

            return ConstructMessageWithAccessInfo(message, HuaweiNamespaceContractions.Account, request.AccessInfo.ObjectId, request.AccessInfo.ObjectIdType);
        }
        #endregion

        #region HELPERS
        private static string ConstructMessageWithAccessInfo(string message, string namespaceContraction, string objectId, ObjectIdType objectIdType = ObjectIdType.ServiceNumber, string additionalFields = null)
        {
            var replacement = string.Empty;
            if (!string.IsNullOrEmpty(objectId))
            {
                replacement = ConstructAccessInfo(namespaceContraction, objectId, objectIdType, additionalFields);
            }

            return message.Replace("[ACCESSINFO]", replacement);
        }

        private static string ConstructAccessInfo(string namespaceContraction, string objectId, ObjectIdType objectIdType = ObjectIdType.ServiceNumber, string additionalFields = null) => String.Format(
            $@"<{namespaceContraction}:AccessInfo>
                <com:ObjectIdType>{(byte)objectIdType}</com:ObjectIdType>
                <com:ObjectId>{objectId}</com:ObjectId>
                {(string.IsNullOrEmpty(additionalFields) ? "" : additionalFields)}
            </{namespaceContraction}:AccessInfo>");

        private static string GetAdditionalPropertyTags(string prefix, IEnumerable<SimpleProperty> additionalProperties)
        {
            return additionalProperties.Any()
            ? string.Join("\n",
                additionalProperties.Select(p =>
                $"<{prefix}:AdditionalProperty>\n<com:Code>{p.Code}</com:Code>\n<com:Value>{p.Value}</com:Value>\n</{prefix}:AdditionalProperty>"
            ))
            : "";
        }
        #endregion
    }
}
