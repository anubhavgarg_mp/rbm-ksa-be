﻿using Microsoft.Extensions.Logging;
using Moq;
using RedbullKSA.Repositories.Base.Contracts;
using System;

namespace RedbullKSA.Tests.MockHelper
{
    public static class MockDependencyServiceHelper
    {
        public static Mock<IServiceProvider> BaseServiceMockedProvider(this Mock<IServiceProvider> serviceProvider)
        {
            var logger = new Mock<ILoggerFactory>();
            var logging = new Mock<ILogger>();

            serviceProvider.Setup(x => x.GetService(typeof(ILoggerFactory))).Returns(logger.Object);

            logger.Setup(x => x.CreateLogger(It.IsAny<string>())).Returns(logging.Object);
            serviceProvider.Setup(x => x.GetService(typeof(ILogger))).Returns(logging.Object);

            return serviceProvider;
        }
    }
}
