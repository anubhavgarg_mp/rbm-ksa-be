﻿using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Data;
using System.Data.SqlClient;

namespace RedbullKSA.Repositories.Base
{
    public class UnitOfWork : IUnitOfWork
    {
        private IServiceProvider _services;

        #region Private Repositories

        private IAttachmentRepository attachmentRepository;
        private ICarouselEntityRepository carouselEntityRepository;
        private IBenefitRepository benefitRepository;
        private IRedeemedBenefitRepository redeemedBenefitRepository;
        private IClientAddressRepository _clientAddressRepository;
        private IClientInterestRepository clientInterestRepository;
        private IInterestRepository interestRepository;
        private ISessionTokenRepository sessionTokenRepository;
        private IUserRepository userRepository;
        private IUserClaimRepository userClaimRepository;
        private IBenefitTagRepository benefitTagRepository;

        #endregion

        #region Public Repositories

        public IAttachmentRepository AttachmentRepository => attachmentRepository ?? new AttachmentRepository(_services, Transaction);

        public IBenefitRepository BenefitRepository => benefitRepository ?? new BenefitRepository(_services, Transaction);

        public IRedeemedBenefitRepository RedeemedBenefitRepository => redeemedBenefitRepository ?? new RedeemedBenefitRepository(_services, Transaction);

        public IClientAddressRepository ClientAddressRepository => _clientAddressRepository ?? new ClientAddressRepository(_services, Transaction);

        public IClientInterestRepository ClientInterestRepository => clientInterestRepository ?? new ClientInterestRepository(_services, Transaction);

        public IInterestRepository InterestRepository => interestRepository ?? new InterestRepository(_services, Transaction);

        public ISessionTokenRepository SessionTokenRepository => sessionTokenRepository ?? new SessionTokenRepository(_services, Transaction);

        public IUserRepository UserRepository => userRepository ?? new UserRepository(_services, Transaction);

        public IUserClaimRepository UserClaimRepository => userClaimRepository ?? new UserClaimRepository(_services, Transaction);

        public IBenefitTagRepository BenefitTagRepository => benefitTagRepository ?? new BenefitTagRepository(_services, Transaction);

        public ICarouselEntityRepository CarouselEntityRepository => carouselEntityRepository ?? new CarouselEntityRepository(_services, Transaction);

        #endregion

        public SqlTransaction Transaction { get; set; }
        public SqlConnection Connection { get; set; }

        public UnitOfWork(IServiceProvider services, SqlConnection connection)
        {
            _services = services;
            Connection = connection;
        }

        public void Begin(IsolationLevel level = IsolationLevel.ReadCommitted)
        {
            Transaction = Connection.BeginTransaction(level);
        }

        public void Commit()
        {
            Transaction.Commit();
            Dispose();
        }

        public void Rollback()
        {
            Transaction.Rollback();
            Dispose();
        }

        public void Dispose()
        {
            if (Transaction != null)
                Transaction.Dispose();
            Transaction = null;
        }
    }
}
