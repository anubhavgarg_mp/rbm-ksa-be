﻿using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Common.Configuration;
using System;
using System.Data.SqlClient;

namespace RedbullKSA.Repositories.Base
{
    public class TransactionSession : IDisposable
    {
        private readonly IServiceProvider _services;

        public SqlConnection Connection { get; set; }
        public UnitOfWork UnitOfWork { get; set; }

        public TransactionSession(IServiceProvider services)
        {
            _services = services;
            Connection = new SqlConnection(_services.GetRequiredService<ConfigurationSettings>().ConnectionStrings.DefaultConnection);
            Connection.Open();
            UnitOfWork = new UnitOfWork(_services, Connection);
        }

        public void Dispose()
        {
            UnitOfWork.Dispose();
            Connection.Dispose();
        }
    }
}
