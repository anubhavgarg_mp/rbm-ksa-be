﻿using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Repositories.Base.Filtering;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Base.Contracts
{
    public interface IDataStore
    {
        void ResetFilters();

        Task<IEnumerable<T>> Get<T>(int? commandTimeout = null);
        Task<IEnumerable<T>> GetDistinct<T>(string distinctColumn);
        Task<T> FirstOrNull<T>();
        Task<T> Aggregate<T>(IEnumerable<AggregateColumn> columns);

        Task<T> Add<T>(T newItem, params object[] otherColumnValues) where T : class, new();
        Task AddMany<T>(IEnumerable<T> newItems, int? commandTimeout = null) where T : class, new();

        Task BulkAdd(DataTable data);

        Task Update(object updatedItem);

        Task Delete<T>(object key);
        Task Delete<T>(IEnumerable<object> key);

        Task<IEnumerable<T>> Query<T>(string sql, dynamic parameters = null, int? commandTimeout = null);
        Task Execute(string sql, dynamic parameters, int? commandTimeout = null);

        IDataStore As<T>(string aliasedTableName = null);
        IDataStore WithFunctionParameters(params object[] parameters);
        IDataStore Alias(string tableAlias);
        IDataStore Filtered(FilteringParameter parameter);
        IDataStore Filtered(string propertyName, object value, ComparisonOperator operation = ComparisonOperator.Equals, LogicalOperator logicalOperator = LogicalOperator.AND);
        IDataStore Filtered(WebParameters webParameters, bool isFull = true);

        IDataStore Paged(int? pageNumber, int? pageSize);
        IDataStore Paged(WebParameters webParameters);
        IDataStore Paged(PagingParameters webParameters);

        IDataStore Sorted(string propertyName, string direction);
        IDataStore Sorted(string propertyName, ListSortDirection direction = ListSortDirection.Ascending);
        IDataStore Sorted(WebParameters webParameters);
    }
}
