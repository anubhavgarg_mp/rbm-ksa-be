﻿using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Data;

namespace RedbullKSA.Repositories.Base.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        #region Repositories

        IAttachmentRepository AttachmentRepository { get; }
        IBenefitRepository BenefitRepository { get; }
        IRedeemedBenefitRepository RedeemedBenefitRepository { get; }
        IClientInterestRepository ClientInterestRepository { get; }
        IInterestRepository InterestRepository { get; }
        ISessionTokenRepository SessionTokenRepository { get; }
        IUserRepository UserRepository { get; }
        IUserClaimRepository UserClaimRepository { get; }
        IBenefitTagRepository BenefitTagRepository { get; }

        #endregion
        void Begin(IsolationLevel level = IsolationLevel.ReadCommitted);
        void Commit();
        void Rollback();
    }
}
