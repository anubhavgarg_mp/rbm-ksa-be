﻿using Dapper;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RedbullKSA.Repositories.Base.Filtering
{
    public class FilteringMultipleValuesParameter : FilteringParameter
    {
        public FilteringMultipleValuesParameter(string propertyName, string parameterName, object value, ComparisonType compareType, ComparisonOperator comparisonOperator, LogicalOperator logicalOperator) : base(
            comparisonOperator, propertyName, parameterName, value, logicalOperator, compareType)
        { }

        public override string ToClause(string tableName = null)
        {
            var table = tableName != null ? tableName + "." : string.Empty;

            var arrayOfValues = (IEnumerable<string>)Value;
            StringBuilder stringBuilder = new StringBuilder();

            for (int i = 0; i < arrayOfValues.Count(); i++)
            {
                stringBuilder.Append($" {table}{ColumnName.Bracketed()} LIKE ");

                if (i == arrayOfValues.Count() - 1)
                    stringBuilder.Append($" '%{arrayOfValues.ElementAt(i)}%'");

                else
                    stringBuilder.Append($" '%{arrayOfValues.ElementAt(i)}%' {LogicalOperator.ToString()} ");
            }

            return stringBuilder.ToString();
        }

        public override void AddToParameters(DynamicParameters parameters)
        {
            if (Value == null)
                return;

            parameters.Add(ParameterizedName, Value);
        }
    }
}