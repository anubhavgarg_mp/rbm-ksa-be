﻿using RedbullKSA.Entities.API.Models.NotificationModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class NotificationRepository : BaseRepository<Notification>, INotificationRepository
    {
        private readonly IDbTransaction _transaction;

        public NotificationRepository(IServiceProvider serviceProvider) : base(serviceProvider) { }

        public NotificationRepository(IServiceProvider serviceProvider, IDbTransaction transaction) : this(serviceProvider)
        {
            _transaction = transaction;
        }

        public async Task AddMany(IEnumerable<Notification> notifications) => await Store().AddMany(notifications);

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<Notification>();
            }
        }

        #endregion

    }
}