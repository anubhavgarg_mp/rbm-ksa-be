﻿using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.Models.ClientModels;
using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class InterestRepository : BaseRepository<Interest>, IInterestRepository
    {
        private readonly IDbTransaction _transaction;

        public InterestRepository(IServiceProvider services) : base(services) { }

        public InterestRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task<Interest> Add(Interest entity, int? addedBy)
        {
            if (addedBy.HasValue)
                entity.AddedBy = addedBy;

            entity.AddedDate = DateTime.UtcNow;

            return await Store().Add(entity);
        }

        public async Task Update(Interest entity, int? modifiedBy)
        {
            if (modifiedBy.HasValue)
                entity.ModifiedBy = modifiedBy;

            entity.ModifiedDate = DateTime.UtcNow;

            await Store().Update(entity);
        }

        public async Task<Interest> Get(int id) => await Store().Filtered(nameof(Interest.Id), id).FirstOrNull<Interest>();

        public async Task<IEnumerable<Interest>> Get(WebParameters parameters) =>
            await Store()
            .Filtered(parameters)
            .Filtered(nameof(Interest.Status), ActiveStatus.Active)
            .Get<Interest>();

        public async Task<Interest> Get(string englishName, string arabicName) => await Store()
                .Filtered(nameof(Interest.EnglishName), englishName, ComparisonOperator.Contains, LogicalOperator.OR)
                .Filtered(nameof(Interest.ArabicName), arabicName, ComparisonOperator.Contains, LogicalOperator.OR)
                .FirstOrNull<Interest>();

        public async Task<IEnumerable<InterestGridView>> GetGridView(InterestParameters parameters) => await GridStore().Filtered(parameters).Get<InterestGridView>();

        public async Task<InterestTotals> GetInterestTotals(InterestParameters parameters)
        {
            var aggregateColumns = new List<AggregateColumn>
            {
                new AggregateColumn(AggregateType.COUNT, nameof(InterestGridView.Id), nameof(InterestTotals.Count))
            };

            var totals = await GridStore().Filtered(parameters).Aggregate<InterestTotals>(aggregateColumns);
            totals.PageCount = (int)Math.Ceiling(totals.Count / (decimal)parameters.PerPage);
            return totals;
        }

        public async Task<IEnumerable<ClientInterestsView>> GetClientInterests(int clientId) 
            => await ClientInterestStore().Filtered(nameof(ClientInterestsView.ClientId), clientId).Get<ClientInterestsView>();

        public async Task<IEnumerable<ClientInterestsView>> GetInterestClients(int interestId) 
            => await ClientInterestStore().Filtered(nameof(ClientInterestsView.Id), interestId).Get<ClientInterestsView>();

        private IDataStore ClientInterestStore() => _services.GetService<IDataStore>().As<ClientInterestsView>();

        private IDataStore GridStore() => _services.GetService<IDataStore>().As<InterestGridView>();

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<Interest>();
            }
        }

        #endregion

    }
}
