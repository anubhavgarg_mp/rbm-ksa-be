﻿using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class CarouselEntityRepository : BaseRepository<CarouselEntity>, ICarouselEntityRepository
    {
        private readonly IDbTransaction _transaction;

        public CarouselEntityRepository(IServiceProvider services) : base(services)
        {
        }

        public CarouselEntityRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<CarouselEntity>> GetAll() => await Store().Get<CarouselEntity>();

        public async Task<IEnumerable<CarouselEntity>> GetActive(WebParameters parameters) => 
            await Store().Filtered(nameof(CarouselEntity.Status), ActiveStatus.Active).Filtered(parameters).Get<CarouselEntity>();

        public async Task<CarouselEntity> Get(int id) => await Store().Filtered(nameof(CarouselEntity.Id), id).FirstOrNull<CarouselEntity>();

        public async Task Update(CarouselEntity entity, int? modifiedBy)
        {
            if (modifiedBy.HasValue)
                entity.ModifiedBy = modifiedBy;

            entity.ModifiedDate = DateTime.UtcNow;

            await Store().Update(entity);
        }

        public async Task<CarouselEntity> Add(CarouselEntity entity, int? addedBy)
        {
            if (addedBy.HasValue)
                entity.AddedBy = addedBy;

            entity.AddedDate = DateTime.UtcNow;

            return await Store().Add(entity);
        }
    }
}
