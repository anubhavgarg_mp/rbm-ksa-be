﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;

namespace RedbullKSA.Repositories.Repositories
{
    public class ClientMsisdnRepository : BaseRepository<ClientMsisdn>, IClientMssisdnRepository
    {
        public ClientMsisdnRepository(IServiceProvider services) : base(services)
        {
        }
    }
}
