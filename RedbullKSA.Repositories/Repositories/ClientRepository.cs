﻿using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class ClientRepository : BaseRepository<Client>, IClientRepository
    {

        public ClientRepository(IServiceProvider services) : base(services)
        {
        }

        public async Task<Client> Get(int id) => await Store().Filtered(nameof(Client.Id), id).Filtered(nameof(Client.IsActive), true).FirstOrNull<Client>();

        public async Task<Client> GetByPhoneNumber(string phoneNumber) => await Store().Filtered(nameof(Client.PhoneNumber), phoneNumber).FirstOrNull<Client>();

        public async Task<Client> GetByEmail(string email) => await Store().Filtered(nameof(Client.Email), email).Filtered(nameof(Client.IsActive), true).FirstOrNull<Client>();

    }
}
