﻿using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.Models.FAQModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class FAQRepository : BaseRepository<FAQ>, IFAQRepository
    {
        public FAQRepository(IServiceProvider services) : base(services)
        {
        }

        public async Task<IEnumerable<FAQView>> GetAll()
            => await _services.GetService<IDataStore>().As<FAQView>().Get<FAQView>();
    }
}
