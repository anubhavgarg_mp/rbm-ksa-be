﻿using RedbullKSA.Entities.API.Models.ClientModels;
using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IInterestRepository : IBaseRepository<Interest>
    {
        Task<Interest> Add(Interest entity, int? addedBy);
        Task Update(Interest entity, int? modifiedBy);

        /// <summary>
        /// Returns a list of interests filtered according to the provided parameters.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<IEnumerable<InterestGridView>> GetGridView(InterestParameters parameters);

        /// <summary>
        /// Returns a single interest entity by interest ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Interest> Get(int id);

        /// <summary>
        /// Returns all interests filtered according to the provided paramters.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<IEnumerable<Interest>> Get(WebParameters parameters);

        /// <summary>
        /// Returns if found any interest with one of the provided names.
        /// </summary>
        /// <param name="englishName"></param>
        /// <param name="arabicName"></param>
        /// <returns></returns>
        Task<Interest> Get(string englishName, string arabicName);

        /// <summary>
        /// Returns interest totals
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<InterestTotals> GetInterestTotals(InterestParameters parameters);

        /// <summary>
        /// Returns client interests
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task<IEnumerable<ClientInterestsView>> GetClientInterests(int clientId);

        /// <summary>
        /// Returns interest clients
        /// </summary>
        /// <param name="interestId"></param>
        /// <returns></returns>
        Task<IEnumerable<ClientInterestsView>> GetInterestClients(int interestId);
    }
}
