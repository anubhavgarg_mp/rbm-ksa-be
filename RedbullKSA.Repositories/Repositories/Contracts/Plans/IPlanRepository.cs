﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts.Plans
{
    public interface IPlanRepository : IBaseRepository<Plan>
    {
        Task<Plan> Get(string offeringId);
    }
}
