﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;

namespace RedbullKSA.Repositories.Repositories.Contracts.Plans
{
    public interface IPlanTagRepository : IBaseRepository<PlanTag>
    {
    }
}
