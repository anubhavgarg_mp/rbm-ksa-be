﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IAttachmentRepository : IBaseRepository<Attachment>
    {
        /// <summary>
        /// Returns a single attachment by a given attachment ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Attachment> Get(int id);

        /// <summary>
        /// Returns multiple attachments by IDs
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<IEnumerable<Attachment>> Get(IEnumerable<int> ids);

        /// <summary>
        /// Returns a single attachment by a given attachment GUID.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        Task<Attachment> Get(Guid guid);

        /// <summary>
        /// Adds new attachment to database.
        /// </summary>
        /// <param name="englishName"></param>
        /// <param name="arabicName"></param>
        /// <returns></returns>
        Task<Attachment> Add(Attachment entity, int? addedBy);

        /// <summary>
        /// Returns all attachments which are detached.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Attachment>> GetDetachedAttachments();

        /// <summary>
        /// Get all atachments by a given interest ID.
        /// </summary>
        /// <param name="interestId"></param>
        /// <returns></returns>
        Task<IEnumerable<Attachment>> GetInterestImages(int interestId);

        /// <summary>
        /// Get all attachments by a given benefit ID
        /// </summary>
        /// <param name="benefitId"></param>
        /// <returns></returns>
        Task<IEnumerable<Attachment>> GetBenefitImages(int benefitId);

        /// <summary>
        /// Gets the header image attachment of a given benefit ID
        /// </summary>
        /// <param name="benefitId"></param>
        /// <returns></returns>
        Task<Attachment> GetBenefitHeader(int benefitId, ServiceLanguage language);

        /// <summary>
        /// Get all attachments by a given benefit ID and option category or language
        /// </summary>
        /// <param name="benefitId"></param>
        /// <param name="category"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        Task<IEnumerable<Attachment>> GetBenefitImages(int benefitId, AttachmentCategory category, ServiceLanguage language);

        /// <summary>
        /// Get all attachments by a given benefit ID and language
        /// </summary>
        /// <param name="benefitId"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        Task<IEnumerable<Attachment>> GetBenefitImages(int benefitId, ServiceLanguage language);

        Task<IEnumerable<Attachment>> GetCarouselImages(int entityId, AttachmentCategory category);
    }
}
