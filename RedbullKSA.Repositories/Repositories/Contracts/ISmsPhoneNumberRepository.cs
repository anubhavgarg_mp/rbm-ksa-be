﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface ISmsPhoneNumberRepository : IBaseRepository<SmsPhoneNumber>
    {
        Task SaveMessagePartIds(int messageid, List<string> partIds);

        Task UpdateMessageStatus(int messageId, SentSmsStatus status);

        Task<IEnumerable<OutgoingSmsPart>> UpdateMessagePartStatus(string messageId, SmppMessageState deliveryStatus);

        Task<bool> IsPresent(string msisdn);
    }
}
