﻿using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.NotificationModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface INotificationRepository : IBaseRepository<Notification>
    {
        Task AddMany(IEnumerable<Notification> notifications);
    }
}
