﻿using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IBenefitRepository : IBaseRepository<Benefit>
    {
        /// <summary>
        /// Gets a list of benefit grid with submitted parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<IEnumerable<BenefitGridView>> GetGridView(BenefitParameters parameters);

        /// <summary>
        /// Gets benefit grid totals based on submitted parameters
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<BenefitTotals> GetBenefitTotals(BenefitParameters parameters);

        /// <summary>
        /// Gets a benefit details by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BenefitDetailsView> GetDetails(int id);

        /// <summary>
        /// Gets a benefit by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Benefit> Get(int id);

        /// <summary>
        /// Gets benefit redeemed details by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<BenefitRedeemedDetails> GetBenefitRedeemedDetails(int id);

        /// <summary>
        /// Gets redeemed benefits for mobile user
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<IEnumerable<RedeemedBenefitRecordMobile>> GetRedeemedBenefitsMobile(int id, ExtendedWebParameters parameters);

        /// <summary>
        /// Gets benefit offers for mobile user
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        Task<IEnumerable<BenefitOfferMobileView>> GetBenefitOffersMobile(BenefitOfferParameters parameters, ServiceLanguage language, int clientId);

        /// <summary>
        /// Gets benefits that are marked as to be shown to guest users
        /// </summary>
        /// <param name="parameters"></param>
        /// <param name="language"></param>
        /// <returns></returns>
        Task<IEnumerable<BenefitGuestOfferMobileView>> GetGuestBenefits(BenefitOfferParameters parameters, ServiceLanguage language);

        /// <summary>
        /// Gets a benefit with available redeem quantity by benefit Id
        /// </summary>
        /// <param name="benefitId"></param>
        /// <returns></returns>
        Task<BenefitDetailsMobileView> GetMobileDetails(int benefitId);

        /// <summary>
        /// Adds benefit
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="addedBy"></param>
        /// <returns></returns>
        Task<Benefit> Add(Benefit entity, int? addedBy);

        /// <summary>
        /// Updates benefit
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="addedBy"></param>
        /// <returns></returns>
        Task Update(Benefit entity, int? modifiedBy);

        /// <summary>
        /// Returns a list of BenefitPushNotificationView objects filtered by a benefit Id.
        /// </summary>
        /// <param name="benefitId"></param>
        /// <returns></returns>
        Task<IEnumerable<BenefitPushNotificationView>> GetBenefitNotificationView(int benefitId);
    }
}
