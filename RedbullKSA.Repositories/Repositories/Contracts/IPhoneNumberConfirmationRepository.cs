﻿using System.Threading.Tasks;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IPhoneNumberConfirmationRepository : IBaseRepository<PhoneNumberConfirmation>
    {
        /// <summary>
        /// Gets a last PhoneNumberConfirmation entity by phone number
        /// </summary>
        /// <param name="phoneNumber"></param>
        /// <returns></returns>
        Task<PhoneNumberConfirmation> Get(string phoneNumber);
    }
}
