﻿using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IClientInterestRepository : IBaseRepository<ClientInterest>
    {
        /// <summary>
        /// Adds many client interests
        /// </summary>
        /// <param name="clientInterests"></param>
        /// <returns></returns>
        Task AddMany(IEnumerable<ClientInterest> clientInterests);

        /// <summary>
        /// Gets user interests
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<IEnumerable<ClientInterest>> GetClientsInterests(int userId);

        /// <summary>
        /// Gets users interests view
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<IEnumerable<ClientIntrestViewMobile>> GetClientInterestsView(int userId);

        /// <summary>
        /// Deletes client's interests selections
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task DeleteByClient(int clientId);
    }
}
