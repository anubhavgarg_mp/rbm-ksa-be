﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IErrorLogEntryRepository : IBaseRepository<ErrorLogEntry>
    {
    }
}
