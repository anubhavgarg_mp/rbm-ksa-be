﻿using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Email;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IRedeemedBenefitRepository : IBaseRepository<RedeemedBenefit>
    {
        /// <summary>
        /// Gets all redeemed benefits with the provided Id that have the status - participating.
        /// </summary>
        /// <param name="benefitId"></param>
        /// <returns></returns>
        Task<IEnumerable<RedeemedBenefit>> GetParticipating(int benefitId);

        /// <summary>
        /// Adds many redeemed benefits to the repository
        /// </summary>
        /// <param name="newStores"></param>
        /// <returns></returns>
        Task AddMany(IEnumerable<RedeemedBenefit> redeemedBenefits);

        /// <summary>
        /// Gets counts of occupied and free redeemed benefits
        /// </summary>
        /// <param name="newStores"></param>
        /// <returns></returns>
        Task<IEnumerable<RedeemedBenefitStatusView>> GetCountsOfStatus(int benefitId);

        /// <summary>
        /// Deletes top first free redeemed benefits by benefitId
        /// </summary>
        /// <param name="newStores"></param>
        /// <returns></returns>
        Task DeleteNumberOfRedeemedBenefit(int count, int benefitId);

        /// <summary>
        /// Gets single available redeemed benefit if exists
        /// </summary>
        /// <param name="newStores"></param>
        /// <returns></returns>
        Task<RedeemedBenefit> GetSingleAvailableReedemedBenefit(int benefitId);

        /// <summary>
        /// Gets single available redeemed benefit with status - "Winner" if exists
        /// </summary>
        /// <param name="newStores"></param>
        /// <returns></returns>
        Task<RedeemedBenefit> GetSingleWonReedemedBenefit(int benefitId);

        /// <summary>
        /// Gets user redeemed benefit
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="benefitId"></param>
        /// <returns></returns>
        Task<RedeemedBenefit> GetUserRedeemedBenefit(int userId, int benefitId);

        /// <summary>
        /// Gets all benefits that are eligible for choosing winners.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<BenefitEligibleSelectView>> GetEligible();


        /// <summary>
        /// Gets all benefits that are eligible for choosing winners.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<RedeemedBenefitEmailView>> GetEmailView(int benefitId);

        /// <summary>
        /// Gets benefit for the specific benefit and its winner.
        /// </summary>
        /// <returns></returns>
        Task<RedeemedBenefitEmailView> GetEmailView(int benefitId, int clientId);

        /// <summary>
        /// Updates a single redeemed benefit delivery state with a provided value.
        /// </summary>
        /// <param name="redeemedBenefitId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        Task UpdateDeliveryState(int redeemedBenefitId, RedeemedBenefitDeliveryState state);

        /// <summary>
        /// Returns the first found entity of a redeemed benefit belonging to a specific client that has already been delivered via email.
        /// </summary>
        /// <param name="benefitId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task<RedeemedBenefit> GetFirstAlreadyDelivered(int benefitId, int clientId);
    }
}
