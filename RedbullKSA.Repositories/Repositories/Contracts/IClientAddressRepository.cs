﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IClientAddressRepository : IBaseRepository<ClientAddress>
    {
        /// <summary>
        /// Get client address by address name 
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="name">address name</param>
        /// <returns></returns>
        Task<ClientAddress> Get(int clientId, string name);

        /// <summary>
        /// Get client shipping address
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <returns></returns>
        Task<ClientAddress> GetShippingAddress(int clientId);

        /// <summary>
        /// Reset shipping address
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="address">client address</param>
        /// <returns></returns>
        Task ResetShippingAddress(int clientId, ClientAddress address);

        /// <summary>
        /// Get client address by address id
        /// </summary>
        /// <param name="clientId">API client id</param>
        /// <param name="id">address id</param>
        /// <returns></returns>
        Task<ClientAddress> Get(int clientId, int id);

        /// <summary>
        /// Get list of client addresses
        /// </summary>
        /// <param name="clientId">API client Id</param>
        /// <returns></returns>
        Task<IEnumerable<ClientAddress>> GetAll(int clientId);
    }
}
