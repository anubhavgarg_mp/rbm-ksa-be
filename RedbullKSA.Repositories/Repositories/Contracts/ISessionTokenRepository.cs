﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface ISessionTokenRepository : IBaseRepository<SessionToken>
    {
        /// <summary>
        /// Returns the last session token of a given user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<SessionToken> GetLast(int userId);

        /// <summary>
        /// Returns a session token, checked against user ownership.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="token"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<SessionToken> Get(int userId, string token, SessionTokenType type);

        /// <summary>
        /// Returns a session token
        /// </summary>
        /// <param name="token"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<SessionToken> Get(string token, SessionTokenType type);

        /// <summary>
        /// Gets client token
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task<SessionToken> GetClientToken(int clientId);

        /// <summary>
        /// Gets client tokens
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        Task<IEnumerable<SessionToken>> GetClientTokens(int clientId);

        /// <summary>
        /// Gets api key tokens
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<IEnumerable<SessionToken>> GetApiTokens(int userId);

        /// <summary>
        /// Returns all session tokens of a user according to the provided type.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        Task<IEnumerable<SessionToken>> GetMany(int userId, SessionTokenType type);

        /// <summary>
        /// Returns all Session tokens based on give client ids list
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<IEnumerable<SessionToken>> GetClientsWithTokens(IEnumerable<int> ids);
    }
}
