﻿using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IClientRepository : IBaseRepository<Client>
    {
        /// <summary>
        /// Returns a single client user by a given client ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Client> Get(int id);

        /// <summary>
        /// Returns a single client user by a given phone number.
        /// Only for registration.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Client> GetByPhoneNumber(string phoneNumber);

        /// <summary>
        /// Returns a single client user by a given email.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Client> GetByEmail(string email);
    }
}
