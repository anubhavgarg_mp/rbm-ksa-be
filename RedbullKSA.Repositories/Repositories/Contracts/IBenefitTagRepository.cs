﻿using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IBenefitTagRepository : IBaseRepository<BenefitTag>
    {
        Task AddManyTagsBenefit(int benefitId, IEnumerable<BenefitTagModel> BenefitTags);
        Task<IEnumerable<BenefitTagView>> GetTagsViewByBenefit(int benefitId);
        Task<IEnumerable<BenefitTagView>> GetTagsView();
        Task DeleteAllFromBenefit(int benefitId);
    }
}
