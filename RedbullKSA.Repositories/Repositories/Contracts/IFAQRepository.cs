﻿using RedbullKSA.Entities.API.Models.FAQModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IFAQRepository : IBaseRepository<FAQ>
    {
        Task<IEnumerable<FAQView>> GetAll();
    }
}
