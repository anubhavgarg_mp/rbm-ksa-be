﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IOrderRepository : IBaseRepository<OngoingOrder>
    {
    }
}
