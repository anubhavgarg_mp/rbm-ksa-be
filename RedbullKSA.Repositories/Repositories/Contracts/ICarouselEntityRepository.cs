﻿using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface ICarouselEntityRepository : IBaseRepository<CarouselEntity>
    {
        Task<IEnumerable<CarouselEntity>> GetAll();
        Task<CarouselEntity> Get(int id);
        Task Update(CarouselEntity entity, int? modifiedBy);
        Task<CarouselEntity> Add(CarouselEntity entity, int? addedBy);
        Task<IEnumerable<CarouselEntity>> GetActive(WebParameters parameters);
    }
}
