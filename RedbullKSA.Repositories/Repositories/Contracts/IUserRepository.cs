﻿using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface IUserRepository : IBaseRepository<User>
    {
        /// <summary>
        /// Returns a list of administrator users as a grid view list filtered according to the provided parameters.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<IEnumerable<AdministratorGridView>> GetAdministratorGridView(AdministratorParameters parameters);

        /// <summary>
        /// Returns administrator totals filtered according to the provided parameters.
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        Task<AdministratorTotals> GetAdministratorTotals(AdministratorParameters parameters);

        /// <summary>
        /// Returns a single user by a given e-mail address. Optionally can return only active users.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="onlyActive"></param>
        /// <returns></returns>
        Task<User> Get(string emailAddress, bool onlyActive);

        /// <summary>
        /// Returns a single user by a given reset token. 
        /// </summary>
        /// <param name="resetToken"></param>
        /// <returns></returns>
        Task<User> Get(string resetToken);

        /// <summary>
        /// Returns a single user by a given user ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<User> Get(int id);

        /// <summary>
        /// Updates a single user's entity with the provided details. 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>
        Task Update(User entity, int? modifiedBy);

        /// <summary>
        /// Adds a new user entity to the repository. Returns the newly created user entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="addedBy"></param>
        /// <returns></returns>
        Task<User> Add(User entity, int? addedBy);
    }
}
