﻿using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Contracts
{
    public interface ITagRepository : IBaseRepository<Tag>
    {

        /// <summary>
        /// Returns all Tag entities.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IEnumerable<Tag>> Get();

        /// <summary>
        /// Returns all active Tag entities.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IEnumerable<Tag>> GetActive();

        /// <summary>
        /// Returns a single Tag entity by Tag ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Tag> Get(int id);

        /// <summary>
        /// Returns if found any Tag with one of the provided names.
        /// </summary>
        /// <param name="englishName"></param>
        /// <param name="arabicName"></param>
        /// <returns></returns>
        Task<Tag> Get(string englishName, string arabicName);
    }
}
