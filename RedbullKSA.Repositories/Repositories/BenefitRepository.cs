﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class BenefitRepository : BaseRepository<Benefit>, IBenefitRepository
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IDbTransaction _transaction;

        #region Cache keys
        private const string BENEFIT_KEY = "benefit:benefit#";
        private const string BENEFIT_MOBILE_KEY = "benefit:benefit-mobile#";
        private const string BENEFIT_DETAILS_KEY = "benefit:benefit-details-view#";
        private const string BENEFIT_MOBILE_OFFERS_KEY = "benefit:benefit-mobile-offer#";
        private const string BENEFIT_MOBILE_GUEST_OFFERS_KEY = "benefit:benefit-guest-offers";
        #endregion

        public BenefitRepository(IServiceProvider services) : base(services)
        {
            _memoryCache = services.GetRequiredService<IMemoryCache>();
        }

        public BenefitRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<BenefitGridView>> GetGridView(BenefitParameters parameters) => await BenefitGridViewStore(parameters).Get<BenefitGridView>();

        public async Task<BenefitTotals> GetBenefitTotals(BenefitParameters parameters)
        {
            var aggregateColumns = new List<AggregateColumn>
            {
                new AggregateColumn(AggregateType.COUNT, nameof(BenefitGridView.Id), nameof(BenefitTotals.Count))
            };

            var totals = await BenefitGridViewStore(parameters).Aggregate<BenefitTotals>(aggregateColumns);
            totals.PageCount = (int)Math.Ceiling(totals.Count / (decimal)parameters.PerPage);

            return totals;
        }

        public async Task<BenefitDetailsMobileView> GetMobileDetails(int benefitId)
        {
            var key = BENEFIT_MOBILE_KEY + benefitId.ToString();
            return await _memoryCache.GetOrCreateAsync(key, async entry =>
            {
                entry.SetSlidingExpiration(TimeSpan.FromSeconds(20));
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(300);

                return await BenefitMobileViewStore(benefitId).FirstOrNull<BenefitDetailsMobileView>();
            });
        }

        public async Task<BenefitDetailsView> GetDetails(int id)
        {
            var key = BENEFIT_DETAILS_KEY + id.ToString();
            return await _memoryCache.GetOrCreateAsync(key, async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(300);
                entry.SetSlidingExpiration(TimeSpan.FromSeconds(20));

                return await BenefitDetailsStore(id).FirstOrNull<BenefitDetailsView>();
            });
        }

        public async Task<Benefit> Get(int id)
        {
            var key = BENEFIT_KEY + id.ToString();
            return await _memoryCache.GetOrCreateAsync(key, async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(60);
                entry.SetSlidingExpiration(TimeSpan.FromSeconds(10));

                return await Store().Filtered(nameof(Benefit.Id), id).FirstOrNull<Benefit>();
            });
        }

        public async Task<BenefitRedeemedDetails> GetBenefitRedeemedDetails(int id) => await _services.GetService<IDataStore>()
            .Filtered(nameof(BenefitRedeemedDetails.Id), id)
            .As<BenefitRedeemedDetails>()
            .FirstOrNull<BenefitRedeemedDetails>();

        public async Task<IEnumerable<RedeemedBenefitRecordMobile>> GetRedeemedBenefitsMobile(int clientId, ExtendedWebParameters parameters)
        {
            return await RedeemedBenefitMobileStore(parameters)
                .Filtered(nameof(RedeemedBenefitRecordMobile.ClientId), clientId)
                .Get<RedeemedBenefitRecordMobile>();
        }

        public async Task<Benefit> Add(Benefit entity, int? addedBy)
        {
            if (addedBy.HasValue)
                entity.AddedBy = addedBy.Value;

            entity.AddedDate = DateTime.UtcNow;

            return await Store().Add(entity);
        }

        public async Task Update(Benefit entity, int? modifiedBy)
        {
            var key = BENEFIT_KEY + entity.Id.ToString();
            _memoryCache.Set(key, entity, TimeSpan.FromSeconds(10));
            _memoryCache.Remove(BENEFIT_DETAILS_KEY + entity.Id.ToString());
            _memoryCache.Remove(BENEFIT_MOBILE_KEY + entity.Id.ToString());

            if (modifiedBy.HasValue)
                entity.ModifiedBy = modifiedBy.Value;

            entity.ModifiedDate = DateTime.UtcNow;

            await Store().Update(entity);
        }

        public async Task<IEnumerable<BenefitOfferMobileView>> GetBenefitOffersMobile(BenefitOfferParameters parameters, ServiceLanguage language, int clientId)
        {
            var key = BENEFIT_MOBILE_OFFERS_KEY + clientId.ToString();
            return await _memoryCache.GetOrCreateAsync(key, async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(300);
                entry.SetSlidingExpiration(TimeSpan.FromSeconds(60));

                return await BenefitOfferViewStore(parameters, language, clientId)
                    .Filtered(nameof(BenefitOfferMobileView.EndDate), DateTime.UtcNow, ComparisonOperator.GreaterThanOrEquals, LogicalOperator.OR)
                    .Filtered(nameof(BenefitOfferMobileView.EndDate), null, ComparisonOperator.Equals, LogicalOperator.OR)
                    .Get<BenefitOfferMobileView>();
            });
        }

        public async Task<IEnumerable<BenefitGuestOfferMobileView>> GetGuestBenefits(BenefitOfferParameters parameters, ServiceLanguage language)
        {
            var key = BENEFIT_MOBILE_GUEST_OFFERS_KEY;
            return await _memoryCache.GetOrCreateAsync(key, async entry =>
            {
                entry.SetAbsoluteExpiration(TimeSpan.FromSeconds(300));
                entry.SetSlidingExpiration(TimeSpan.FromSeconds(30));

                return await BenefitGuestOfferViewStore(parameters, language)
                    .Filtered(nameof(BenefitGuestOfferMobileView.IsShownToGuests), true)
                    .Filtered(nameof(BenefitGuestOfferMobileView.EndDate), DateTime.UtcNow, ComparisonOperator.GreaterThanOrEquals, LogicalOperator.OR)
                    .Filtered(nameof(BenefitGuestOfferMobileView.EndDate), null, ComparisonOperator.Equals, LogicalOperator.OR)
                    .Get<BenefitGuestOfferMobileView>();
            });
        }

        public async Task<IEnumerable<BenefitPushNotificationView>> GetBenefitNotificationView(int benefitId)
        {
            var store = _services.GetService<IDataStore>().As<BenefitPushNotificationView>();

            return await store
                .Filtered(nameof(BenefitPushNotificationView.BenefitId), benefitId)
                .Get<BenefitPushNotificationView>();
        }

        private IDataStore BenefitGridViewStore(BenefitParameters parameters) => _services.GetService<IDataStore>()
            .Filtered(parameters)
            .As<BenefitGridView>();

        private IDataStore BenefitGuestOfferViewStore(BenefitOfferParameters parameters, ServiceLanguage language) =>
            _services
                .GetService<IDataStore>()
                .Filtered(parameters)
                .As<BenefitGuestOfferMobileView>()
                .WithFunctionParameters((int)language)
                .Sorted(nameof(BenefitGuestOfferMobileView.EndDate), ListSortDirection.Descending);

        private IDataStore BenefitOfferViewStore(BenefitOfferParameters parameters, ServiceLanguage language, int? clientId) =>
            _services
                .GetService<IDataStore>()
                .Filtered(parameters)
                .As<BenefitOfferMobileView>()
                .WithFunctionParameters((int)language, clientId ?? null)
                .Sorted(nameof(BenefitOfferMobileView.EndDate), ListSortDirection.Descending);

        private IDataStore BenefitDetailsStore(int id) => _services.GetService<IDataStore>()
            .Filtered(nameof(BenefitDetailsView.Id), id)
            .As<BenefitDetailsView>();

        private IDataStore BenefitMobileViewStore(int id) => _services.GetService<IDataStore>()
            .Filtered(nameof(BenefitDetailsMobileView.Id), id)
            .As<BenefitDetailsMobileView>();

        private IDataStore RedeemedBenefitMobileStore(ExtendedWebParameters parameters)
        {
            var store = _services.GetService<IDataStore>()
                .As<RedeemedBenefitRecordMobile>()
                .Filtered(parameters);

            if (parameters.DateFrom.HasValue)
                store = store.Filtered(nameof(RedeemedBenefitRecordMobile.RedeemedDate), parameters.DateFrom.FromUnixTimeStamp(), ComparisonOperator.GreaterThanOrEquals);

            if (parameters.DateTo.HasValue)
                store = store.Filtered(nameof(RedeemedBenefitRecordMobile.RedeemedDate), parameters.DateTo.FromUnixTimeStamp(), ComparisonOperator.LessThanOrEquals);

            return store;
        }

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<Benefit>();
            }
        }

        #endregion
    }
}
