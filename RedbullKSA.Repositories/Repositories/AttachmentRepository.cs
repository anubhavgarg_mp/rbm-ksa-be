﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class AttachmentRepository : BaseRepository<Attachment>, IAttachmentRepository
    {
        private readonly IDbTransaction _transaction;

        public AttachmentRepository(IServiceProvider services) : base(services)
        {
        }

        public AttachmentRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task<Attachment> Add(Attachment entity, int? addedBy)
        {
            if (addedBy.HasValue)
                entity.AddedBy = addedBy;

            entity.AddedDate = DateTime.UtcNow;

            return await Store().Add(entity);
        }

        public async Task<Attachment> Get(int id) => await Store().Filtered(nameof(Attachment.Id), id).FirstOrNull<Attachment>();

        public async Task<IEnumerable<Attachment>> Get(IEnumerable<int> ids) 
            => await Store().Filtered(nameof(Attachment.Id), ids, ComparisonOperator.In).Get<Attachment>();

        public async Task<Attachment> Get(Guid guid) 
            => await Store().Filtered(nameof(Attachment.GuidId), guid).FirstOrNull<Attachment>();

        public async Task<IEnumerable<Attachment>> GetDetachedAttachments()
        {
            var store = Store()
                .Filtered(nameof(Attachment.BenefitId), null)
                .Filtered(nameof(Attachment.InterestId), null)
                .Filtered(nameof(Attachment.ClientId), null)
                .Filtered(nameof(Attachment.CarouselEntityId), null);

            return await store.Get<Attachment>();
        }

        public async Task<IEnumerable<Attachment>> GetInterestImages(int interestId) =>
            await Store()
            .Filtered(nameof(Attachment.InterestId), interestId)
            .Filtered(nameof(Attachment.Category), AttachmentCategory.InterestImage)
            .Get<Attachment>();

        public async Task<IEnumerable<Attachment>> GetCarouselImages(int entityId, AttachmentCategory category) =>
            await Store()
            .Filtered(nameof(Attachment.CarouselEntityId), entityId)
            .Get<Attachment>();

        public async Task<IEnumerable<Attachment>> GetBenefitImages(int benefitId, AttachmentCategory category, ServiceLanguage language)
        {
            var store = Store()
                .Filtered(nameof(Attachment.BenefitId), benefitId)
                .Filtered(nameof(Attachment.Category), category)
                .Filtered(nameof(Attachment.FileLanguage), language);

            return await store.Get<Attachment>();
        }

        public async Task<Attachment> GetBenefitHeader(int benefitId, ServiceLanguage language)
            => await Store()
            .Filtered(nameof(Attachment.BenefitId), benefitId)
            .Filtered(nameof(Attachment.Category), AttachmentCategory.BenefitHeader)
            .Filtered(nameof(Attachment.FileLanguage), language)
            .FirstOrNull<Attachment>();

        public async Task<IEnumerable<Attachment>> GetBenefitImages(int benefitId, ServiceLanguage language)
        {
            var store = Store()
                .Filtered(nameof(Attachment.BenefitId), benefitId)
                .Filtered(nameof(Attachment.FileLanguage), language);

            return await store.Get<Attachment>();
        }

        public async Task<IEnumerable<Attachment>> GetBenefitImages(int benefitId)
        {
            var store = Store()
                .Filtered(nameof(Attachment.BenefitId), benefitId);

            return await store.Get<Attachment>();
        }

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<Attachment>();
            }
        }

        #endregion

    }
}
