﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using System.Data;
using System.Linq;

namespace RedbullKSA.Repositories.Repositories
{
    public class ClientInterestRepository : BaseRepository<ClientInterest>, IClientInterestRepository
    {
        private readonly IDbTransaction _transaction;
        public ClientInterestRepository(IServiceProvider services) : base(services) { }

        public ClientInterestRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task AddMany(IEnumerable<ClientInterest> clientInterests) 
            => await Store().AddMany(clientInterests);

        public async Task<IEnumerable<ClientInterest>> GetClientsInterests(int userId) 
            => await Store().Filtered(nameof(ClientInterest.ClientId), userId).Get<ClientInterest>();

        public async Task<IEnumerable<ClientIntrestViewMobile>> GetClientInterestsView(int userId) 
            => await ClienInterestsStore().Filtered(nameof(ClientInterest.ClientId), userId).Get<ClientIntrestViewMobile>();

        public async Task DeleteByClient(int clientId)
        {
            var toDelete = await Store().Filtered(nameof(ClientInterest.ClientId), clientId).Get<ClientInterest>();

            await Store().Delete<ClientInterest>(toDelete.Select(d => (object)d.Id));
        }

        private IDataStore ClienInterestsStore() => _services.GetService<IDataStore>().As<ClientIntrestViewMobile>();

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<ClientInterest>();
            }
        }

        #endregion
    }
}
