﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class PhoneNumberConfirmationRepository : BaseRepository<PhoneNumberConfirmation>, IPhoneNumberConfirmationRepository
    {
        public PhoneNumberConfirmationRepository(IServiceProvider services) : base(services)
        {
        }

        public async Task<PhoneNumberConfirmation> Get(string phoneNumber)
        {
            return await Store()
                .Filtered(nameof(PhoneNumberConfirmation.PhoneNumber), phoneNumber)
                .Sorted(nameof(PhoneNumberConfirmation.AddedDate), ListSortDirection.Descending)
                .FirstOrNull<PhoneNumberConfirmation>();
        }

        public override async Task<PhoneNumberConfirmation> Add(PhoneNumberConfirmation entity)
        {
            entity.AddedDate = DateTime.UtcNow;

            return await Store().Add(entity);
        }
    }
}
