﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class SmsMessageRepository : BaseRepository<SmsMessage>, ISmsMessageRepository
    {
        public SmsMessageRepository(IServiceProvider services) : base(services)
        {
        }
    }
}
