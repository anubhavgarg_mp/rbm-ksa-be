﻿using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        private readonly IDbTransaction _transaction;

        public UserRepository(IServiceProvider services) : base(services) { }

        public UserRepository(IServiceProvider services, IDbTransaction transaction) : this(services) 
        {
            _transaction = transaction;
        }


        public async Task<User> Add(User entity, int? addedBy)
        {
            if (addedBy.HasValue)
                entity.AddedBy = addedBy;

        entity.AddedDate = DateTime.UtcNow;

            return await Store().Add(entity);
        }

        public async Task Update(User entity, int? modifiedBy)
        {
            if (modifiedBy.HasValue)
                entity.ModifiedBy = modifiedBy;

            entity.ModifiedDate = DateTime.UtcNow;

            await Store().Update(entity);
        }

        public async Task<User> Get(string emailAddress, bool isActive = true)
        {
            var store = Store()
                .Filtered(nameof(User.Email), emailAddress);

            if (isActive)
                store = store.Filtered(nameof(User.Status), ActiveStatus.Active);

            return await store.FirstOrNull<User>();
        }
        
        public async Task<User> Get(int id) => await Store().Filtered(nameof(User.Id), id).FirstOrNull<User>();

        public async Task<User> Get(string resetToken) => await Store().Filtered(nameof(User.PasswordResetToken), resetToken).FirstOrNull<User>();

        public async Task<IEnumerable<AdministratorGridView>> GetAdministratorGridView(AdministratorParameters parameters) 
            => await AdministratorGridStore(parameters)
            .Get<AdministratorGridView>();

        public async Task<AdministratorTotals> GetAdministratorTotals(AdministratorParameters parameters)
        {
            var aggregateColumns = new List<AggregateColumn>
            {
                new AggregateColumn(AggregateType.COUNT, nameof(AdministratorGridView.Id), nameof(AdministratorTotals.Count))
            };

            var totals = await AdministratorGridStore(parameters).Aggregate<AdministratorTotals>(aggregateColumns);
            totals.PageCount = (int)Math.Ceiling(totals.Count / (decimal)parameters.PerPage);
            return totals;
        }

        private IDataStore AdministratorGridStore(AdministratorParameters parameters) => _services.GetService<IDataStore>().As<AdministratorGridView>().Filtered(parameters);

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<User>();
            }
        }

        #endregion
    }
}
