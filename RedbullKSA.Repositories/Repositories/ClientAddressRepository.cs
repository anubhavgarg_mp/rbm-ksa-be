﻿using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class ClientAddressRepository : BaseRepository<ClientAddress>, IClientAddressRepository
    {
        private readonly IDbTransaction _transaction;

        public ClientAddressRepository(IServiceProvider services) : base(services)
        {
        }

        public ClientAddressRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<ClientAddress>> GetAll(int clientId)
        {
            return await GetClientAddress(clientId).Get<ClientAddress>();
        }

        public async Task<ClientAddress> Get(int clientId, string name)
        {
            return await GetClientAddress(clientId)
                .Filtered(nameof(ClientAddress.NameAR), name, ComparisonOperator.Equals, LogicalOperator.OR)
                .Filtered(nameof(ClientAddress.NameEN), name, ComparisonOperator.Equals, LogicalOperator.OR)
                .FirstOrNull<ClientAddress>();
        }

        public async Task<ClientAddress> Get(int clientId, int id)
        {
            return await GetClientAddress(clientId).Filtered(nameof(ClientAddress.Id), id).FirstOrNull<ClientAddress>();
        }

        public async Task<ClientAddress> GetShippingAddress(int clientId)
        {
            return await GetClientAddress(clientId).Filtered(nameof(ClientAddress.IsDefault), true).FirstOrNull<ClientAddress>();
        }

        public async Task ResetShippingAddress(int clientId, ClientAddress address)
        {
            address.IsDefault = false;
            await Store().Update(address);
        }


        private IDataStore GetClientAddress(int clientId)
        {
            return Store().Filtered(nameof(ClientAddress.ClientId), clientId);
        }

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<ClientAddress>();
            }
        }

        #endregion
    }
}
