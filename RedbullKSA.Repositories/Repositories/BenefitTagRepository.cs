﻿using Dapper;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class BenefitTagRepository : BaseRepository<BenefitTag>, IBenefitTagRepository
    {
        private readonly IDbTransaction _transaction;

        public BenefitTagRepository(IServiceProvider services) : base(services) { }

        public BenefitTagRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task AddManyTagsBenefit(int benefitId, IEnumerable<BenefitTagModel> benefitTags)
        {
            var tagIds = benefitTags.Select(tag => tag.TagId);

            if (tagIds.Any())
                await Store().AddMany(tagIds.Select(tagId => new BenefitTag(benefitId, tagId)));
        }

        public Task<IEnumerable<BenefitTagView>> GetTagsViewByBenefit(int benefitId)
        {
            return BenefitTagViewStore().Filtered(nameof(BenefitTagView.BenefitId), benefitId).Get<BenefitTagView>();
        }

        public Task<IEnumerable<BenefitTagView>> GetTagsView()
        {
            return BenefitTagViewStore().Get<BenefitTagView>();
        }

        public Task DeleteAllFromBenefit(int benefitId)
        {
            DynamicParameters parameter = new DynamicParameters();
            parameter.Add("@benefitId", benefitId);

            return _services.GetService<IDataStore>().Execute(
                        $@" DELETE FROM [{nameof(BenefitTag)}] 
                            WHERE {nameof(BenefitTag)}.{nameof(BenefitTag.BenefitId)} = @benefitId", parameter);
        }

        #region private methods

        private IDataStore BenefitTagViewStore() =>
            _services.GetService<IDataStore>()
            .As<BenefitTagView>();

        #endregion

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<BenefitTag>();
            }
        }

        #endregion
    }
}
