﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class SessionTokenRepository : BaseRepository<SessionToken>, ISessionTokenRepository
    {
        private readonly IDbTransaction _transaction;

        public SessionTokenRepository(IServiceProvider services) : base(services) { }

        public SessionTokenRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<SessionToken>> GetMany(int userId, SessionTokenType type)
        {
            return await Store()
                .Filtered(nameof(SessionToken.UserId), userId)
                .Filtered(nameof(SessionToken.TokenType), type)
                .Get<SessionToken>();
        }

        public async Task<SessionToken> GetClientToken(int clientId) =>
            await Store()
            .Filtered(nameof(SessionToken.ClientId), clientId)
            .Filtered(nameof(SessionToken.TokenType), SessionTokenType.App)
            .FirstOrNull<SessionToken>();

        public async Task<IEnumerable<SessionToken>> GetClientsWithTokens(IEnumerable<int> ids)
            => await Store().Filtered(nameof(SessionToken.ClientId), ids, Entities.API.WebParams.ComparisonOperator.In).Get<SessionToken>();

        public async Task<IEnumerable<SessionToken>> GetClientTokens(int clientId) =>
            await Store()
            .Filtered(nameof(SessionToken.ClientId), clientId)
            .Filtered(nameof(SessionToken.TokenType), SessionTokenType.App)
            .Get<SessionToken>();

        public async Task<SessionToken> Get(int userId, string token, SessionTokenType type)
        {
            var store = Store()
                .Filtered(nameof(SessionToken.TokenType), type)
                .Filtered(nameof(SessionToken.Token), token);

            if (type == SessionTokenType.Admin)
                store = store.Filtered(nameof(SessionToken.UserId), userId);

            else if (type == SessionTokenType.App)
                store = store.Filtered(nameof(SessionToken.ClientId), userId);

            return await store.FirstOrNull<SessionToken>();
        }

        public async Task<SessionToken> Get(string token, SessionTokenType type)
            => await Store()
                .Filtered(nameof(SessionToken.TokenType), type)
                .Filtered(nameof(SessionToken.Token), token)
                .FirstOrNull<SessionToken>();

        public async Task<IEnumerable<SessionToken>> GetApiTokens(int userId) =>
            await Store()
            .Filtered(nameof(SessionToken.UserId), userId)
            .Filtered(nameof(SessionToken.TokenType), SessionTokenType.Api)
            .Get<SessionToken>();

        public async Task<SessionToken> GetLast(int userId)
        {
            return await Store()
                .Filtered(nameof(SessionToken.UserId), userId)
                .Sorted(nameof(SessionToken.Id), "DESC")
                .FirstOrNull<SessionToken>();
        }

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<SessionToken>();
            }
        }

        #endregion
    }
}
