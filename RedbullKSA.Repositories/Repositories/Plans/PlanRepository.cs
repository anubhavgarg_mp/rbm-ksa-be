﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts.Plans;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Plans
{
    public class PlanRepository : BaseRepository<Plan>, IPlanRepository
    {

        public PlanRepository(IServiceProvider services) : base(services)
        {
        }

        public async Task<Plan> Get(string offeringId)
        {
            return await Store().Filtered(nameof(Plan.OfferingId), offeringId).FirstOrNull<Plan>();
        }
    }
}
