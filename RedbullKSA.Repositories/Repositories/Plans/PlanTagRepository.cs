﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts.Plans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories.Plans
{
    public class PlanTagRepository : BaseRepository<PlanTag>, IPlanTagRepository
    {
        public PlanTagRepository(IServiceProvider services) : base(services)
        {
        }
    }
}
