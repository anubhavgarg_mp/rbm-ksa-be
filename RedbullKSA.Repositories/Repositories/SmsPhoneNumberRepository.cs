﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class SmsPhoneNumberRepository : BaseRepository<SmsPhoneNumber>, ISmsPhoneNumberRepository
    {
        public SmsPhoneNumberRepository(IServiceProvider services) : base(services)
        {
        }

        public async Task SaveMessagePartIds(int messageId, List<string> partIds)
            => await MessagePartsStore()
                .AddMany(partIds.Select(s => new OutgoingSmsPart
                {
                    WholeMessageId = messageId,
                    MessageId = s,
                    SequenceNumber = null,
                    Status = SmppMessageState.None
                }));

        public async Task UpdateMessageStatus(int messageId, SentSmsStatus status)
        {
            var message = await Store().Filtered(nameof(SmsPhoneNumber.Id), messageId).FirstOrNull<SmsPhoneNumber>();

            if (message == null)
                throw new Exception($"Message not found. ID {messageId}");

            message.Status = status;

            await Store().Update(message);
        }
        
        public async Task<IEnumerable<OutgoingSmsPart>> UpdateMessagePartStatus(string messageId, SmppMessageState deliveryStatus)
        {
            var messagePart = await MessagePartsStore().Filtered(nameof(OutgoingSmsPart.MessageId), messageId).FirstOrNull<OutgoingSmsPart>();

            if (messagePart == null)
                throw new Exception($"Message part not found. ID {messageId}");

            messagePart.Status = deliveryStatus;

            await MessagePartsStore().Update(messagePart);

            return await MessagePartsStore().Filtered(nameof(OutgoingSmsPart.WholeMessageId), messagePart.WholeMessageId).Get<OutgoingSmsPart>();
        }

        public async Task<bool> IsPresent(string msisdn)
        {
            return await Store()
                .Filtered(nameof(SmsPhoneNumber.PhoneNumber), msisdn)
                .FirstOrNull<SmsPhoneNumber>() != null;   
        }
        
        private IDataStore MessagePartsStore() => Store().As<OutgoingSmsPart>();

    }
}
