﻿using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class UserClaimRepository : BaseRepository<UserClaim>, IUserClaimRepository
    {
        private readonly IDbTransaction _transaction;

        public UserClaimRepository(IServiceProvider services) : base(services) { }

        public UserClaimRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task<UserClaim> Add(UserClaim entity, int? addedBy)
        {
            if (addedBy.HasValue)
                entity.AddedBy = addedBy;

            entity.AddedDate = DateTime.UtcNow;

            return await Store().Add(entity);
        }

        public async Task Update(UserClaim entity, int? modifiedBy)
        {
            if (modifiedBy.HasValue)
                entity.ModifiedBy = modifiedBy;

            entity.ModifiedDate = DateTime.UtcNow;

            await Store().Update(entity);
        }

        /// <summary>
        /// Gets all user claims of a single user. By default filters out only active claims.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserClaim>> Get(int userId, bool onlyActive = true)
        {
            var store = Store()
                .Filtered(nameof(UserClaim.UserId), userId);

            if (onlyActive)
                store.Filtered(nameof(UserClaim.Value), bool.TrueString, ComparisonOperator.Equals);

            return await store.Get<UserClaim>();
        }

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<UserClaim>();
            }
        }

        #endregion
    }
}
