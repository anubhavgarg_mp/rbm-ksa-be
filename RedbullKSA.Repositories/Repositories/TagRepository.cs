﻿using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class TagRepository : BaseRepository<Tag>, ITagRepository
    {
        private readonly IDbTransaction _transaction;

        public TagRepository(IServiceProvider services) : base(services) { }

        public TagRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task<IEnumerable<Tag>> Get() => await Store().Get<Tag>();

        public Task<IEnumerable<Tag>> GetActive() => Store().Filtered(nameof(Tag.Status), ActiveStatus.Active).Get<Tag>();

        public async Task<Tag> Get(int id) => await Store().Filtered(nameof(Tag.Id), id).FirstOrNull<Tag>();

        public async Task<Tag> Get(string englishName, string arabicName) => await Store()
            .Filtered(nameof(Tag.NameEN), englishName, ComparisonOperator.Contains, LogicalOperator.OR)
            .Filtered(nameof(Tag.NameAR), arabicName, ComparisonOperator.Contains, LogicalOperator.OR)
            .FirstOrNull<Tag>();

        #region private methods

        private IDataStore TagGridStore(TagParameters parameters) => _services.GetService<IDataStore>().As<Tag>().Filtered(parameters);

        #endregion

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<Tag>();
            }
        }

        #endregion
    }
}
