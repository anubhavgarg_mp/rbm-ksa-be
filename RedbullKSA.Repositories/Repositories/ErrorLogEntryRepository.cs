﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;

namespace RedbullKSA.Repositories.Repositories
{
    public class ErrorLogEntryRepository : BaseRepository<ErrorLogEntry>, IErrorLogEntryRepository
    {
        public ErrorLogEntryRepository(IServiceProvider services) : base(services)
        {
        }
    }
}
