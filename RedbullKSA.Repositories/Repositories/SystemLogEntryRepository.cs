﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;

namespace RedbullKSA.Repositories.Repositories
{
    public class SystemLogEntryRepository : BaseRepository<SystemLogEntry>, ISystemLogEntryRepository
    {
        public SystemLogEntryRepository(IServiceProvider services) : base(services)
        {
        }
    }
}
