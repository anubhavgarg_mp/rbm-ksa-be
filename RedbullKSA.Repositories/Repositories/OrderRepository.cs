﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class OrderRepository : BaseRepository<OngoingOrder>, IOrderRepository
    {
        public OrderRepository(IServiceProvider services) : base(services)
        {
        }
    }
}
