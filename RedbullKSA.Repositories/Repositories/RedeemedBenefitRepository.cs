﻿using Dapper;
using Microsoft.Extensions.DependencyInjection;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Email;
using RedbullKSA.Entities.API.WebParams;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Repositories.Base;
using RedbullKSA.Repositories.Base.Contracts;
using RedbullKSA.Repositories.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RedbullKSA.Repositories.Repositories
{
    public class RedeemedBenefitRepository : BaseRepository<RedeemedBenefit>, IRedeemedBenefitRepository
    {
        private readonly IDbTransaction _transaction;

        public RedeemedBenefitRepository(IServiceProvider services) : base(services) { }

        public RedeemedBenefitRepository(IServiceProvider services, IDbTransaction transaction) : this(services)
        {
            _transaction = transaction;
        }

        public async Task AddMany(IEnumerable<RedeemedBenefit> redeemedBenefits) => await Store().AddMany(redeemedBenefits);

        public async override Task<RedeemedBenefit> Add(RedeemedBenefit entity)
        {
            entity.AddedDate = DateTime.UtcNow;

            return await Store().Add(entity);
        }
        public async Task<IEnumerable<RedeemedBenefitStatusView>> GetCountsOfStatus(int benefitId) => await _services.GetService<IDataStore>()
            .Filtered(nameof(RedeemedBenefitStatusView.BenefitId), benefitId)
            .As<RedeemedBenefitStatusView>()
            .Get<RedeemedBenefitStatusView>();

        public async Task DeleteNumberOfRedeemedBenefit(int deleteCount, int benefitIdComparer)
        {
            var redeemedStatus = (int)RedeemedBenefitType.Available;

            DynamicParameters parameter = new DynamicParameters();

            parameter.Add("@deleteCount", deleteCount);
            parameter.Add("@benefitIdComparer", benefitIdComparer);
            parameter.Add("@redeemedStatus", redeemedStatus);

            await _services.GetService<IDataStore>().Execute
            ($@"DELETE TOP(@deleteCount) 
                FROM [{nameof(RedeemedBenefit)}] 
                WHERE {nameof(RedeemedBenefit)}.{nameof(RedeemedBenefit.BenefitId)} = @benefitIdComparer 
                    AND {nameof(RedeemedBenefit)}.{nameof(RedeemedBenefit.Status)} = @redeemedStatus",
                parameter);
        }

        public async Task<IEnumerable<BenefitEligibleSelectView>> GetEligible() => await _services.GetService<IDataStore>()
                .As<BenefitEligibleSelectView>()
                .Filtered(nameof(BenefitEligibleSelectView.RegisteredUsers), 0, ComparisonOperator.GreaterThan)
                .Filtered(nameof(BenefitEligibleSelectView.Status), BenefitStatus.Active)
                .Get<BenefitEligibleSelectView>();

        public async Task<RedeemedBenefit> GetSingleAvailableReedemedBenefit(int benefitId) => await Store()
            .Filtered(nameof(RedeemedBenefit.BenefitId), benefitId)
            .Filtered(nameof(RedeemedBenefit.Status), RedeemedBenefitType.Available)
            .FirstOrNull<RedeemedBenefit>();

        public async Task<RedeemedBenefit> GetSingleWonReedemedBenefit(int benefitId) => await Store()
            .Filtered(nameof(RedeemedBenefit.BenefitId), benefitId)
            .Filtered(nameof(RedeemedBenefit.Status), RedeemedBenefitType.Winner)
            .FirstOrNull<RedeemedBenefit>();

        public async Task<RedeemedBenefit> GetUserRedeemedBenefit(int userId, int benefitId) => await Store()
            .Filtered(nameof(RedeemedBenefit.BenefitId), benefitId)
            .Filtered(nameof(RedeemedBenefit.ClientId), userId)
            .Filtered(nameof(RedeemedBenefit.Status), RedeemedBenefitType.Winner)
            .FirstOrNull<RedeemedBenefit>();

        public async Task<IEnumerable<RedeemedBenefit>> GetParticipating(int benefitId) => await Store()
            .Filtered(nameof(RedeemedBenefit.BenefitId), benefitId)
            .Filtered(nameof(RedeemedBenefit.Status), RedeemedBenefitType.Participant)
            .Get<RedeemedBenefit>();

        public async Task<IEnumerable<RedeemedBenefitEmailView>> GetEmailView(int benefitId) => await _services.GetService<IDataStore>()
            .As<RedeemedBenefitEmailView>()
            .Filtered(nameof(RedeemedBenefitEmailView.BenefitId), benefitId)
            .Filtered(nameof(RedeemedBenefitEmailView.Status), RedeemedBenefitType.Winner)
            .Get<RedeemedBenefitEmailView>();

        public async Task<RedeemedBenefitEmailView> GetEmailView(int benefitId, int clientId) => await _services.GetService<IDataStore>()
            .As<RedeemedBenefitEmailView>()
            .Filtered(nameof(RedeemedBenefitEmailView.BenefitId), benefitId)
            .Filtered(nameof(RedeemedBenefitEmailView.ClientId), clientId)
            .Filtered(nameof(RedeemedBenefitEmailView.Status), RedeemedBenefitType.Winner)
            .FirstOrNull<RedeemedBenefitEmailView>();

        public async Task<RedeemedBenefit> GetFirstAlreadyDelivered(int benefitId, int clientId)
        {
            DynamicParameters parameters = new DynamicParameters();

            parameters.Add("@benefitId", benefitId);
            parameters.Add("@clientId", clientId);
            parameters.Add("@deliveryState", RedeemedBenefitDeliveryState.EmailSent);

            var results = await _services.GetService<IDataStore>().Query<RedeemedBenefit>
                ($@"SELECT TOP (1) * FROM [{nameof(RedeemedBenefit)}]
                    WHERE {nameof(RedeemedBenefit.BenefitId)} = @benefitId 
                        AND {nameof(RedeemedBenefit.ClientId)} = @clientId 
                        AND {nameof(RedeemedBenefit.DeliveryState)} = @deliveryState",
                parameters);

            return results.FirstOrDefault();
        }

        public async Task UpdateDeliveryState(int redeemedBenefitId, RedeemedBenefitDeliveryState state)
        {
            DynamicParameters parameter = new DynamicParameters();

            parameter.Add("@redeemedBenefitId", redeemedBenefitId);
            parameter.Add("@redeemedDeliveryState", (int)state);
            parameter.Add("@redeemedDeliveryDate", DateTime.UtcNow);

            await _services.GetService<IDataStore>().Execute
            ($@" UPDATE [{nameof(RedeemedBenefit)}]
                    SET {nameof(RedeemedBenefit.DeliveryState)} = @redeemedDeliveryState,
                        {nameof(RedeemedBenefit.DeliveryDate)} = @redeemedDeliveryDate
                    WHERE {nameof(RedeemedBenefit)}.{nameof(RedeemedBenefit.Id)} = @redeemedBenefitId",
                parameter);
        }

        #region overrides

        public override IDataStore Store()
        {
            if (_transaction == null)
            {
                return base.Store();
            }
            else
            {
                return new DataStore(_services, _transaction).As<RedeemedBenefit>();
            }
        }

        #endregion
    }
}
