﻿namespace RedbullKSA.Entities.Converters
{
    using global::RedbullKSA.Common.Extensions;
    using Newtonsoft.Json;
    using System;

    namespace RedbullKSA.Entities.Converters
    {
        public class DateTimeJsonConverter : JsonConverter<DateTime>
        {
            public override DateTime ReadJson(JsonReader reader, Type objectType, DateTime existingValue, bool hasExistingValue, JsonSerializer serializer)
            {
                return ((long)reader.Value).FromUnixTimeStamp();
            }

            public override void WriteJson(JsonWriter writer, DateTime value, JsonSerializer serializer)
            {
                writer.WriteValue(value.ToUnixTimeStamp());
            }
        }
    }

}
