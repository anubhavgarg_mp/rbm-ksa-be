﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.Attributes
{
    public class TransferUnitDescriptionAttribute : Attribute
    {
        public string Type { get; set; }

        public TransferUnitDescriptionAttribute(string type) => Type = type;
    }
}
