﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Extensions;
using RedbullKSA.Entities.Migrations.Helpers;

namespace RedBullRuby.Entities.Migrations
{
    [Migration(201902191656)]
    public class M201902191656_PhoneNumberConfirmation_Table : BaseMigration
    {
        public override void Up()
        {
            Create.Table(nameof(PhoneNumberConfirmation))
                .WithIdColumn()
                .WithAddedByColumn(nameof(Client), nameof(Client.Id))
                .WithAddedDateColumn()
                .WithColumn(nameof(PhoneNumberConfirmation.PhoneNumber)).AsMaxString().NotNullable()
                .WithColumn(nameof(PhoneNumberConfirmation.ConfirmationCode)).AsMaxString().NotNullable()
                .WithColumn(nameof(PhoneNumberConfirmation.RetryCount)).AsInt32().NotNullable().WithDefaultValue(3);
        }

        public override void Down()
        {
            Delete.Table(nameof(PhoneNumberConfirmation));
        }
    }
}
