﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202112011725)]
    public class M202112011725_UpdateClientAddress : Migration
    {
        public override void Up()
        {
            Alter.Table(nameof(ClientAddress))
                .AddColumn(nameof(ClientAddress.AddressLine4))
                .AsString(128)
                .Nullable();

            Alter.Table(nameof(ClientAddress))
                .AddColumn(nameof(ClientAddress.NameAR))
                .AsString(200)
                .Nullable();

            Alter.Table(nameof(ClientAddress))
                .AddColumn(nameof(ClientAddress.NameEN))
                .AsString(200)
                .Nullable();

            Create.Index("IDX_ClientAddress_ClientId")
                .OnTable(nameof(ClientAddress))
                .OnColumn(nameof(ClientAddress.ClientId));
        }

        public override void Down()
        {
            Delete.Column(nameof(ClientAddress.AddressLine4)).FromTable(nameof(ClientAddress));
            Delete.Column(nameof(ClientAddress.NameAR)).FromTable(nameof(ClientAddress));
            Delete.Column(nameof(ClientAddress.NameEN)).FromTable(nameof(ClientAddress));

            Delete.Index("IDX_ClientAddress_ClientId").OnTable(nameof(ClientAddress));
        }
    }
}
