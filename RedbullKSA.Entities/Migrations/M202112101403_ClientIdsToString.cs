﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202112101403)]
    public class M202112101403_ClientIdsToString : BaseMigration
    {
        public override void Up()
        {
            Delete.Column(nameof(Client.NationalityId)).FromTable(nameof(Client));
            Delete.Column(nameof(Client.BorderId)).FromTable(nameof(Client));

            Alter.Table(nameof(Client))
                .AddColumn(nameof(Client.NationalityId)).AsString().Nullable()
                .AddColumn(nameof(Client.BorderId)).AsString().Nullable();
        }

        public override void Down()
        {
        }
    }
}
