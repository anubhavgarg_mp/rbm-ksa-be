﻿using FluentMigrator;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.Database.SqlViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202112031106)]
    public class M202112031106_AddMissingBenefitsView : Migration
    {
        public override void Up()
        {
            Execute.Sql(BenefitSql.BenefitGuestOfferSqlFunction(nameof(BenefitGuestOfferMobileView)));
        }

        public override void Down()
        {
            Execute.Sql($"IF Object_ID('dbo.BenefitGuestOfferMobileView', N'IF') IS NOT NULL DROP FUNCTION dbo.BenefitGuestOfferMobileView");
        }
    }
}
