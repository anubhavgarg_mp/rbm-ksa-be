﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Migrations.Helpers;
using System;

namespace RedBullRuby.Entities.Migrations
{
    [Migration(201903051501)]
    public class M201903051501_ClientLanguage : BaseMigration
    {
        public override void Up()
        {
            Alter.Table(nameof(Client))
                .AddColumn(nameof(Client.Language))
                .AsInt32()
                .NotNullable()
                .SetExistingRowsTo((int)ServiceLanguage.EN);
        }

        public override void Down()
        {
            Delete.Column(nameof(Client.Language)).FromTable(nameof(Client));
        }
    }
}
