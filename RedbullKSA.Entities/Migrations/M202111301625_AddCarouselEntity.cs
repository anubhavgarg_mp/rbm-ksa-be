﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Extensions;
using RedbullKSA.Entities.Migrations.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111301625)]
    public class M202111301625_AddCarouselEntity : BaseMigration
    {
        public override void Up()
        {
            Create.Table(nameof(CarouselEntity))
                .WithIdColumn()
                .WithAddedByColumn(nameof(User), nameof(User.Id))
                .WithAddedDateColumn()
                .WithModifiedByColumn(nameof(User), nameof(User.Id))
                .WithModifiedDateColumn()
                .WithColumn(nameof(CarouselEntity.DescriptionAr)).AsString().Nullable()
                .WithColumn(nameof(CarouselEntity.DescriptionEn)).AsString().Nullable()
                .WithColumn(nameof(CarouselEntity.TitleAR)).AsString().Nullable()
                .WithColumn(nameof(CarouselEntity.TitleEN)).AsString().Nullable()
                .WithColumn(nameof(CarouselEntity.ImageUrlEn)).AsString().Nullable()
                .WithColumn(nameof(CarouselEntity.ImageUrlAr)).AsString().Nullable()
                .WithColumn(nameof(CarouselEntity.Status)).AsByte().NotNullable();

            Alter.Table(nameof(Attachment))
                .AddColumn(nameof(Attachment.CarouselEntityId)).AsInt32().Nullable();
        }

        public override void Down()
        {
            Delete.Table(nameof(CarouselEntity));
            Delete.Column(nameof(Attachment.CarouselEntityId)).FromTable(nameof(Attachment));
        }
    }
}
