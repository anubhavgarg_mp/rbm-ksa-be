﻿using FluentMigrator;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.Database.SqlViews;
using RedbullKSA.Entities.Migrations.Helpers;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202112070941)]
    public class M202112070941_AddAdministratorGridView : Migration
    {
        public override void Up()
        {
            Execute.Sql(SqlViewHelper.RecreateView(nameof(AdministratorGridView), UserSql.AdministratorGridViewSql));
        }

        public override void Down()
        {
            Execute.Sql(SqlViewHelper.DropView(nameof(AdministratorGridView)));
        }
    }
}