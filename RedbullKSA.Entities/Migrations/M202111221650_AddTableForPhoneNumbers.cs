﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111221650)]
    public class M202111221650_AddTableForPhoneNumbers : Migration
    {
        public override void Up()
        {
            Create.Table(nameof(SmsPhoneNumber))
                .WithIdColumn()
                .WithColumn(nameof(SmsPhoneNumber.SerialNumber)).AsString(1024).NotNullable()
                .WithColumn(nameof(SmsPhoneNumber.Timestamp)).AsDateTime2().NotNullable()
                .WithColumn(nameof(SmsPhoneNumber.PhoneNumber)).AsString(64).NotNullable();
        }

        public override void Down()
        {
            Delete.Table(nameof(SmsPhoneNumber));
        }
    }
}
