﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Migrations.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202112141544)]
    public class M202112141544_AddOutgoingSmsPartsTable : Migration
    {
        public override void Up()
        {
            Alter.Table(nameof(SmsPhoneNumber))
                .AddColumn(nameof(SmsPhoneNumber.Status)).AsByte().Nullable();

            Create.Table(nameof(OutgoingSmsPart))
                .WithIdColumn()
                .WithColumn(nameof(OutgoingSmsPart.WholeMessageId)).AsInt32().NotNullable().ForeignKey(nameof(SmsPhoneNumber), nameof(SmsPhoneNumber.Id))
                .WithColumn(nameof(OutgoingSmsPart.MessageId)).AsString(255).NotNullable().Indexed()
                .WithColumn(nameof(OutgoingSmsPart.SequenceNumber)).AsString(255).Nullable()
                .WithColumn(nameof(OutgoingSmsPart.Status)).AsInt16().NotNullable();

        }

        public override void Down()
        {
            Delete.Column(nameof(SmsPhoneNumber.Status)).FromTable(nameof(SmsPhoneNumber));
            Delete.Table(nameof(OutgoingSmsPart));
        }

    }
}
