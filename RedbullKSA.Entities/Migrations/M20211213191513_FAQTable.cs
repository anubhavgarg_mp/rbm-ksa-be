﻿using FluentMigrator;
using RedbullKSA.Entities.API.Models.FAQModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Database.SqlViews;
using RedbullKSA.Entities.Migrations.Extensions;
using RedbullKSA.Entities.Migrations.Helpers;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(20211213191513)]
    public class M20211213191513_FAQTable : BaseMigration
    {
        public override void Up()
        {
            Create.Table(nameof(FAQ))
                .WithIdColumn()
                .WithColumn(nameof(FAQ.FAQTagId)).AsInt32().Nullable()
                .WithColumn(nameof(FAQ.QuestionEN)).AsString(500).Nullable()
                .WithColumn(nameof(FAQ.QuestionAR)).AsString(500).Nullable()
                .WithColumn(nameof(FAQ.AnswerEN)).AsMaxString().Nullable()
                .WithColumn(nameof(FAQ.AnswerAR)).AsMaxString().Nullable()
                .WithColumn(nameof(FAQ.TopXIndicator)).AsBoolean().NotNullable();

            Create.Table(nameof(FAQTag))
                .WithIdColumn()
                .WithColumn(nameof(FAQTag.NameEN)).AsString(150).NotNullable()
                .WithColumn(nameof(FAQTag.NameAR)).AsString(150).NotNullable();

            Create.ForeignKey("FK_FAQTag_FAQ")
                .FromTable(nameof(FAQ)).ForeignColumn(nameof(FAQ.FAQTagId))
                .ToTable(nameof(FAQTag)).PrimaryColumn(nameof(FAQTag.Id));

            Execute.Sql(SqlViewHelper.RecreateView(nameof(FAQView), FAQSql.FAQGridViewSql));
        }

        public override void Down()
        {
            Delete.ForeignKey("FK_FAQTag_FAQ");
            Execute.Sql(SqlViewHelper.DropView(nameof(FAQView)));
            Delete.Table(nameof(FAQ));
        }
    }
}
