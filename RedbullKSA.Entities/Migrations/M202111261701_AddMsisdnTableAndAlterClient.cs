﻿using FluentMigrator;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Database.SqlViews;
using RedbullKSA.Entities.Migrations.Extensions;
using RedbullKSA.Entities.Migrations.Helpers;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111261701)]
    public class M202111261701_AddMsisdnTableAndAlterClient : BaseMigration
    {
        public override void Up()
        {
            Alter.Table(nameof(Client))
                .AddColumn(nameof(Client.HuaweiCustomerId))
                .AsString(64)
                .Nullable();

            Create.Table(nameof(ClientMsisdn))
                .WithIdColumn()
                .WithAddedByColumn(nameof(Client), nameof(Client.Id))
                .WithAddedDateColumn()
                .WithModifiedByColumn(nameof(Client), nameof(Client.Id))
                .WithModifiedDateColumn()
                .WithColumn(nameof(ClientMsisdn.ClientId)).AsInt32().NotNullable()
                .WithColumn(nameof(ClientMsisdn.SubscriberStatus)).AsByte().NotNullable()
                .WithColumn(nameof(ClientMsisdn.PhoneNumber)).AsString(64).NotNullable();

            Create.Table(nameof(ClientAddress))
                .WithIdColumn()
                .WithColumn(nameof(ClientAddress.HuaweiId)).AsString(64).Nullable()
                .WithColumn(nameof(ClientAddress.ClientId)).AsInt32().NotNullable()
                .WithColumn(nameof(ClientAddress.IsDefault)).AsBoolean().NotNullable()
                .WithColumn(nameof(ClientAddress.AddressType)).AsByte().Nullable()
                .WithColumn(nameof(ClientAddress.Province)).AsString(128).Nullable()
                .WithColumn(nameof(ClientAddress.City)).AsString(128).Nullable()
                .WithColumn(nameof(ClientAddress.Street)).AsString(128).Nullable()
                .WithColumn(nameof(ClientAddress.PostCode)).AsString(16).Nullable()
                .WithColumn(nameof(ClientAddress.PhoneNumber1)).AsString(60).Nullable()
                .WithColumn(nameof(ClientAddress.PhoneNumber2)).AsString(60).Nullable()
                .WithColumn(nameof(ClientAddress.Email1)).AsString(128).Nullable()
                .WithColumn(nameof(ClientAddress.Email2)).AsString(128).Nullable()
                .WithColumn(nameof(ClientAddress.SmsNumber)).AsString(20).Nullable();

            Create.Index("IDX_ClientMsisdn_PhoneNumber")
                .OnTable(nameof(ClientMsisdn))
                .OnColumn(nameof(ClientMsisdn.PhoneNumber));

            Create.ForeignKey("FK_ClientMsisdn_Client")
                .FromTable(nameof(ClientMsisdn)).ForeignColumn(nameof(ClientMsisdn.ClientId))
                .ToTable(nameof(Client)).PrimaryColumn(nameof(Client.Id));

            Create.ForeignKey("FK_ClientAddress_Client")
                .FromTable(nameof(ClientAddress)).ForeignColumn(nameof(ClientAddress.ClientId))
                .ToTable(nameof(Client)).PrimaryColumn(nameof(Client.Id));

            Execute.Sql(SqlViewHelper.RecreateView(nameof(BenefitRedeemedDetails), BenefitSql.RedeemedBenefitDetailsSql));
        }

        public override void Down()
        {
            Delete.Column(nameof(Client.HuaweiCustomerId)).FromTable(nameof(Client));
            Delete.Table(nameof(ClientMsisdn));
            Delete.Table(nameof(ClientAddress));

            Delete.Index("IDX_ClientMsisdn_PhoneNumber").OnTable(nameof(ClientMsisdn));

            Delete.ForeignKey("FK_ClientMsisdn_Client");
            Delete.ForeignKey("FK_ClientAddress_Client");

            Execute.Sql(SqlViewHelper.DropView(nameof(BenefitRedeemedDetails)));
        }
    }
}
