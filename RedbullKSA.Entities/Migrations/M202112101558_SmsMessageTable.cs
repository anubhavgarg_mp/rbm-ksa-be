﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Extensions;
using RedbullKSA.Entities.Migrations.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202112101558)]
    public class M202112101558_SmsMessageTable : BaseMigration
    {
        public override void Up()
        {
            Create.Table(nameof(SmsMessage))
                .WithIdColumn()
                .WithColumn(nameof(SmsMessage.MessageBody)).AsString(1024).NotNullable()
                .WithColumn(nameof(SmsMessage.Timestamp)).AsDate().NotNullable()
                .WithColumn(nameof(SmsMessage.Destination)).AsString().NotNullable()
                .WithColumn(nameof(SmsMessage.Type)).AsByte().NotNullable();
        }

        public override void Down()
        {
            Delete.Table(nameof(SmsMessage));
        }
    }
}
