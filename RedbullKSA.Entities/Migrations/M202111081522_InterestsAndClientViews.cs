﻿using FluentMigrator;
using RedbullKSA.Entities.API.Models.ClientModels;
using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Database.SqlViews;
using RedbullKSA.Entities.Migrations.Extensions;
using RedbullKSA.Entities.Migrations.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111081522)]
    public class M202111081522_InterestsAndClientViews : Migration
    {
        public override void Up()
        {
            Create.Table(nameof(Interest))
               .WithIdColumn()
               .WithAddedByColumn(nameof(User), nameof(User.Id))
               .WithAddedDateColumn()
               .WithModifiedByColumn(nameof(User), nameof(User.Id))
               .WithModifiedDateColumn()
               .WithColumn(nameof(Interest.EnglishName)).AsString(255).NotNullable()
               .WithColumn(nameof(Interest.ArabicName)).AsString(255).NotNullable()
               .WithColumn(nameof(Interest.ImageEN)).AsString(255).NotNullable()
               .WithColumn(nameof(Interest.ImageAR)).AsString(255).NotNullable()
               .WithColumn(nameof(Interest.Status)).AsByte().NotNullable();

            Create.Table(nameof(ClientInterest))
                .WithIdColumn()
                .WithAddedByColumn(nameof(User), nameof(User.Id))
                .WithAddedDateColumn()
                .WithModifiedByColumn(nameof(User), nameof(User.Id))
                .WithModifiedDateColumn()
                .WithColumn(nameof(ClientInterest.ClientId)).AsInt32().NotNullable()
                .WithColumn(nameof(ClientInterest.InterestId)).AsInt32().NotNullable();

            Create.ForeignKey("FK_ClientInterest_Client")
                .FromTable(nameof(ClientInterest)).ForeignColumn(nameof(ClientInterest.ClientId))
                .ToTable(nameof(Client)).PrimaryColumn(nameof(Client.Id));

            Create.ForeignKey("FK_ClientInterest_Interest")
                .FromTable(nameof(ClientInterest)).ForeignColumn(nameof(ClientInterest.InterestId))
                .ToTable(nameof(Interest)).PrimaryColumn(nameof(Interest.Id));

            Create.Table(nameof(Attachment))
                .WithIdColumn()
                .WithAddedByColumn(nameof(User), nameof(User.Id))
                .WithAddedDateColumn()
                .WithColumn(nameof(Attachment.GuidId)).AsGuid().NotNullable()
                .WithColumn(nameof(Attachment.FileName)).AsString(255).NotNullable()
                .WithColumn(nameof(Attachment.FileType)).AsString(255).NotNullable()
                .WithColumn(nameof(Attachment.FileUrl)).AsString(255).NotNullable()
                .WithColumn(nameof(Attachment.UploadedFileType)).AsByte().NotNullable()
                .WithColumn(nameof(Attachment.Category)).AsInt16().NotNullable()
                .WithColumn(nameof(Attachment.FileLanguage)).AsByte().NotNullable()
                .WithColumn(nameof(Attachment.InterestId)).AsInt32().Nullable()
                .WithColumn(nameof(Attachment.ClientId)).AsInt32().Nullable()
                .WithColumn(nameof(Attachment.BenefitId)).AsInt32().Nullable();

            Execute.Sql(SqlViewHelper.RecreateView(nameof(InterestGridView), InterestSql.InterestGridViewSql));
            Execute.Sql(SqlViewHelper.RecreateView(nameof(ClientInterestsView), InterestSql.ClientInterestsViewSql));
        }

        public override void Down()
        {
            Execute.Sql(SqlViewHelper.DropView(nameof(ClientInterestsView)));
            Execute.Sql(SqlViewHelper.DropView(nameof(InterestGridView)));

            Delete.Table(nameof(Attachment));

            Delete.ForeignKey("FK_ClientInterest_Interest");
            Delete.ForeignKey("FK_ClientInterest_Client");

            Delete.Table(nameof(ClientInterest));
            Delete.Table(nameof(Interest));
        }

    }
}
