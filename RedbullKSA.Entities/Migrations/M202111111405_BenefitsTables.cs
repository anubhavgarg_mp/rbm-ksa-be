﻿using FluentMigrator;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Database.SqlViews;
using RedbullKSA.Entities.Migrations.Extensions;
using RedbullKSA.Entities.Migrations.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111111405)]
    public class M202111111405_BenefitsTables : Migration
    {
        public override void Up()
        {
            Create.Table(nameof(Benefit))
                .WithIdColumn()
                .WithAddedByColumn(nameof(User), nameof(User.Id))
                .WithAddedDateColumn()
                .WithModifiedByColumn(nameof(User), nameof(User.Id))
                .WithModifiedDateColumn()
                .WithColumn(nameof(Benefit.Type)).AsByte().NotNullable()
                .WithColumn(nameof(Benefit.Category)).AsInt16().NotNullable()
                .WithColumn(nameof(Benefit.NotificationType)).AsInt16().Nullable()
                .WithColumn(nameof(Benefit.DistributionType)).AsByte().Nullable()
                .WithColumn(nameof(Benefit.StartDate)).AsDateTime2().NotNullable()
                .WithColumn(nameof(Benefit.EndDate)).AsDateTime2().Nullable()
                .WithColumn(nameof(Benefit.EventDate)).AsDateTime2().Nullable()
                .WithColumn(nameof(Benefit.Status)).AsByte().NotNullable()
                .WithColumn(nameof(Benefit.AvailableQuantity)).AsInt32().Nullable()
                .WithColumn(nameof(Benefit.NameAR)).AsString(2048).NotNullable()
                .WithColumn(nameof(Benefit.DescriptionTeaserAR)).AsMaxString().Nullable()
                .WithColumn(nameof(Benefit.DescriptionAR)).AsMaxString().Nullable()
                .WithColumn(nameof(Benefit.SecondaryDescriptionAR)).AsMaxString().Nullable()
                .WithColumn(nameof(Benefit.NameEN)).AsString(2048).NotNullable()
                .WithColumn(nameof(Benefit.DescriptionTeaserEN)).AsMaxString().Nullable()
                .WithColumn(nameof(Benefit.DescriptionEN)).AsMaxString().Nullable()
                .WithColumn(nameof(Benefit.SecondaryDescriptionEN)).AsMaxString().Nullable()
                .WithColumn(nameof(Benefit.LocationAR)).AsString(2048).Nullable()
                .WithColumn(nameof(Benefit.LocationEN)).AsString(2048).Nullable()
                .WithColumn(nameof(Benefit.SkuCode)).AsString(2048).Nullable()
                .WithColumn(nameof(Benefit.IsLimitedToOneUser)).AsBoolean().Nullable()
                .WithColumn(nameof(Benefit.IsShownToGuests)).AsBoolean().Nullable()
                .WithColumn(nameof(Benefit.IsFeatured)).AsBoolean().NotNullable()
                .WithColumn(nameof(Benefit.ButtonUrlAR)).AsString(2048).Nullable()
                .WithColumn(nameof(Benefit.ButtonUrlEN)).AsString(2048).Nullable()
                .WithColumn(nameof(Benefit.AppleStoreUrlAR)).AsString(2048).Nullable()
                .WithColumn(nameof(Benefit.AppleStoreUrlEN)).AsString(2048).Nullable()
                .WithColumn(nameof(Benefit.GooglePlayUrlAR)).AsString(2048).Nullable()
                .WithColumn(nameof(Benefit.GooglePlayUrlEN)).AsString(2048).Nullable();

            Create.Table(nameof(Tag))
                .WithIdColumn()
                .WithAddedByColumn(nameof(User), nameof(User.Id))
                .WithAddedDateColumn()
                .WithModifiedByColumn(nameof(User), nameof(User.Id))
                .WithModifiedDateColumn()
                .WithColumn(nameof(Tag.NameEN)).AsString(255).NotNullable()
                .WithColumn(nameof(Tag.NameAR)).AsString(255).NotNullable()
                .WithColumn(nameof(Tag.OrderPosition)).AsInt32().NotNullable()
                .WithColumn(nameof(Tag.Status)).AsByte().NotNullable();

            Create.Table(nameof(BenefitTag))
                .WithIdColumn()
                .WithAddedByColumn(nameof(User), nameof(User.Id))
                .WithAddedDateColumn()
                .WithModifiedByColumn(nameof(User), nameof(User.Id))
                .WithModifiedDateColumn()
                .WithColumn(nameof(BenefitTag.BenefitId)).AsInt32().NotNullable().ForeignKey(nameof(Benefit), nameof(Benefit.Id))
                .WithColumn(nameof(BenefitTag.TagId)).AsInt32().NotNullable().ForeignKey(nameof(Tag), nameof(Tag.Id));

            Create.Table(nameof(RedeemedBenefit))
                .WithIdColumn()
                .WithAddedByColumn(nameof(User), nameof(User.Id))
                .WithAddedDateColumn()
                .WithColumn(nameof(RedeemedBenefit.ClientId)).AsInt32().Nullable().ForeignKey(nameof(Client), nameof(Client.Id))
                .WithColumn(nameof(RedeemedBenefit.PromoCode)).AsString(255).Nullable()
                .WithColumn(nameof(RedeemedBenefit.BenefitId)).AsInt32().NotNullable().ForeignKey(nameof(Benefit), nameof(Benefit.Id))
                .WithColumn(nameof(RedeemedBenefit.DeliveryAddress)).AsString(2048).Nullable()
                .WithColumn(nameof(RedeemedBenefit.DeliveryDate)).AsDateTime2().Nullable()
                .WithColumn(nameof(RedeemedBenefit.DeliveryState)).AsByte().Nullable()
                .WithColumn(nameof(RedeemedBenefit.Status)).AsByte().NotNullable();

            Execute.Sql(SqlViewHelper.RecreateView(nameof(BenefitGridView), BenefitSql.BenefitGridViewSql));
            Execute.Sql(SqlViewHelper.RecreateView(nameof(BenefitDetailsView), BenefitSql.BenefitDetailsSql));
            Execute.Sql(SqlViewHelper.RecreateView(nameof(RedeemedBenefitRecordMobile), BenefitSql.RedeemedBenefitHistoryMobileSql));
            Execute.Sql(SqlViewHelper.RecreateView(nameof(BenefitTagView), BenefitTagSql.BenefitTagViewSql));
            Execute.Sql(BenefitSql.BenefitOfferSqlFunction(nameof(BenefitOfferMobileView)));
            Execute.Sql(SqlViewHelper.RecreateView(nameof(BenefitDetailsMobileView), BenefitSql.BenefitDetailsMobileSql));
            Execute.Sql(SqlViewHelper.RecreateView(nameof(BenefitPushNotificationView), BenefitSql.PushBenefitNotificationViewSql));
            
        }

        public override void Down()
        {
            Execute.Sql(SqlViewHelper.DropView(nameof(BenefitPushNotificationView)));
            Execute.Sql(SqlViewHelper.DropView(nameof(BenefitDetailsMobileView)));
            Execute.Sql($"IF Object_ID('dbo.BenefitOfferMobileView', N'IF') IS NOT NULL DROP FUNCTION dbo.BenefitOfferMobileView");
            Execute.Sql(SqlViewHelper.DropView(nameof(BenefitTagView)));
            Execute.Sql(SqlViewHelper.DropView(nameof(RedeemedBenefitRecordMobile)));
            Execute.Sql(SqlViewHelper.DropView(nameof(BenefitDetailsView)));
            Execute.Sql(SqlViewHelper.DropView(nameof(BenefitGridView)));

            Delete.Table(nameof(RedeemedBenefit));
            Delete.Table(nameof(BenefitTag));
            Delete.Table(nameof(Tag));
            Delete.Table(nameof(Benefit));
        }
    }
}
