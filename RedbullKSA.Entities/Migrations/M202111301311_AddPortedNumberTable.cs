﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111301311)]
    public class M202111301311_AddPortedNumberTable : Migration
    {
        public override void Up()
        {
            Create.Table(nameof(PortedNumber))
                .WithIdColumn()
                .WithColumn(nameof(PortedNumber.ClientId)).AsInt32().NotNullable().ForeignKey(nameof(Client), nameof(Client.Id))
                .WithColumn(nameof(PortedNumber.MSISDN)).AsString(64).NotNullable()
                .WithColumn(nameof(PortedNumber.CurrentOperator)).AsString(64).Nullable()
                .WithColumn(nameof(PortedNumber.RequestedAt)).AsDateTime2().NotNullable()
                .WithColumn(nameof(PortedNumber.Status)).AsByte().NotNullable();
        }

        public override void Down()
        {
            Delete.Table(nameof(PortedNumber));
        }
    }
}
