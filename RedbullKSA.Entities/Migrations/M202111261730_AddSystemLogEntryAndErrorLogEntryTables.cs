﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Extensions;
using RedbullKSA.Entities.Migrations.Helpers;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111261730)]
    public class M202111261730_AddSystemLogEntryAndErrorLogEntryTables : BaseMigration
    {
        public override void Up()
        {
            Create.Table(nameof(SystemLogEntry))
                .WithIdColumn()
                .WithAddedByColumn(nameof(SystemLogEntry), nameof(SystemLogEntry.Id))
                .WithAddedDateColumn()
                .WithColumn(nameof(SystemLogEntry.Device)).AsString().Nullable()
                .WithColumn(nameof(SystemLogEntry.UserName)).AsString().Nullable()
                .WithColumn(nameof(SystemLogEntry.RequestUrl)).AsString(2048).NotNullable()
                .WithColumn(nameof(SystemLogEntry.RequestParameters)).AsMaxString().Nullable();

            Create.Table(nameof(ErrorLogEntry))
                .WithIdColumn()
                .WithAddedByColumn(nameof(ErrorLogEntry), nameof(ErrorLogEntry.Id))
                .WithAddedDateColumn()
                .WithColumn(nameof(ErrorLogEntry.Device)).AsString().Nullable()
                .WithColumn(nameof(ErrorLogEntry.UserName)).AsString().Nullable()
                .WithColumn(nameof(ErrorLogEntry.RequestUrl)).AsString(2048).NotNullable()
                .WithColumn(nameof(ErrorLogEntry.RequestParameters)).AsMaxString().Nullable()
                .WithColumn(nameof(ErrorLogEntry.KSAExceptionCode)).AsInt32().Nullable()
                .WithColumn(nameof(ErrorLogEntry.KSAExceptionType)).AsString(128).Nullable()
                .WithColumn(nameof(ErrorLogEntry.ExceptionType)).AsInt16().NotNullable()
                .WithColumn(nameof(ErrorLogEntry.ExceptionMessage)).AsString()
                .WithColumn(nameof(ErrorLogEntry.StackTrace)).AsMaxString().Nullable();
        }

        public override void Down()
        {
            Delete.Table(nameof(SystemLogEntry));
            Delete.Table(nameof(ErrorLogEntry));
        }
    }
}
