﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111091331)]
    public class M202111091331_AddGuid : BaseMigration
    {
        public override void Up()
        {
            Alter.Table(nameof(Client))
                .AddColumn(nameof(Client.Guid))
                .AsGuid()
                .NotNullable()
                .SetExistingRowsTo(Guid.NewGuid());

            Alter.Table(nameof(User))
                .AddColumn(nameof(User.Guid))
                .AsGuid()
                .NotNullable()
                .SetExistingRowsTo(Guid.NewGuid());
        }

        public override void Down()
        {
            Delete.Column(nameof(Client.Guid)).FromTable(nameof(Client));
            Delete.Column(nameof(User.Guid)).FromTable(nameof(User));
        }
    }
}
