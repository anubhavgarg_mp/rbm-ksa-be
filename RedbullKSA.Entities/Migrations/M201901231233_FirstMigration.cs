﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Helpers;
using RedbullKSA.Entities.Migrations.Extensions;
using System;

[Migration(201901231233)]
public class M201901231233_FirstMigration : BaseMigration
{

    public override void Up()
    {
        Create.Table(nameof(User))
            .WithIdColumn()
            .WithModifiedByColumn(nameof(User), nameof(User.Id))
            .WithModifiedDateColumn()
            .WithAddedByColumn(nameof(User), nameof(User.Id))
            .WithAddedDateColumn()
            .WithColumn(nameof(User.FirstName)).AsMaxString().Nullable()
            .WithColumn(nameof(User.LastName)).AsMaxString().Nullable()
            .WithColumn(nameof(User.Status)).AsInt32().NotNullable()
            .WithColumn(nameof(User.PhoneNumber)).AsMaxString().NotNullable()
            .WithColumn(nameof(User.Email)).AsMaxString().Nullable()
            .WithColumn(nameof(User.PasswordResetToken)).AsMaxString().Nullable()
            .WithColumn(nameof(User.PasswordResetTokenExpiration)).AsDateTime().Nullable()
            .WithColumn(nameof(User.EmailConfirmed)).AsBoolean().Nullable()
            .WithColumn(nameof(User.Password)).AsMaxString().Nullable()
            .WithColumn(nameof(User.IsApiUser)).AsBoolean().NotNullable();

        Create.Table(nameof(UserClaim))
            .WithIdColumn()
            .WithModifiedByColumn(nameof(User), nameof(User.Id))
            .WithModifiedDateColumn()
            .WithAddedByColumn(nameof(User), nameof(User.Id))
            .WithAddedDateColumn()
            .WithColumn(nameof(UserClaim.UserId)).AsInt32().NotNullable().ForeignKey(nameof(User), nameof(User.Id))
            .WithColumn(nameof(UserClaim.Value)).AsMaxString().Nullable()
            .WithColumn(nameof(UserClaim.ClaimType)).AsInt32().NotNullable();

        Create.Table(nameof(Client))
            .WithIdColumn()
            .WithColumn(nameof(Client.FirstName)).AsMaxString().Nullable()
            .WithColumn(nameof(Client.LastName)).AsMaxString().Nullable()
            .WithColumn(nameof(Client.NationalityId)).AsInt32().Nullable()
            .WithColumn(nameof(Client.BorderId)).AsInt32().Nullable()
            .WithColumn(nameof(Client.Email)).AsString(128).NotNullable()
            .WithColumn(nameof(Client.PhoneNumber)).AsString(64).NotNullable()
            .WithColumn(nameof(Client.BirthDate)).AsDateTime().Nullable()
            .WithColumn(nameof(Client.Gender)).AsInt32().Nullable()
            .WithColumn(nameof(Client.RegistrationDate)).AsDateTime().NotNullable()
            .WithColumn(nameof(Client.Password)).AsMaxString().NotNullable()
            .WithColumn(nameof(Client.IsActive)).AsBoolean().NotNullable().WithDefaultValue(true);

        Create.Table(nameof(SessionToken))
           .WithIdColumn()
           .WithColumn(nameof(SessionToken.PhoneNumber)).AsString().Nullable()
           .WithColumn(nameof(SessionToken.HuaweiNotificationToken)).AsString().Nullable()
           .WithColumn(nameof(SessionToken.Token)).AsMaxString().NotNullable()
           .WithColumn(nameof(SessionToken.Started)).AsDateTime().NotNullable()
           .WithColumn(nameof(SessionToken.Expires)).AsDateTime().NotNullable()
           .WithColumn(nameof(SessionToken.TokenType)).AsInt32().NotNullable()
           .WithColumn(nameof(SessionToken.NotificationToken)).AsMaxString().Nullable()
           .WithColumn(nameof(SessionToken.UserId)).AsInt32().Nullable().ForeignKey(nameof(User), nameof(User.Id))
           .WithColumn(nameof(SessionToken.ClientId)).AsInt32().Nullable().ForeignKey(nameof(Client), nameof(Client.Id));

    }
    public override void Down()
    {
    }
}
