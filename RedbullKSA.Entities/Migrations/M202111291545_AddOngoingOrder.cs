﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111291545)]
    public class M202111291545_AddOngoingOrder : Migration
    {
        public override void Up()
        {
            Create.Table(nameof(OngoingOrder))
                .WithIdColumn()
                .WithColumn(nameof(OngoingOrder.DeliveryType)).AsInt16().NotNullable()
                .WithColumn(nameof(OngoingOrder.NumberTier)).AsInt16().NotNullable()
                .WithColumn(nameof(OngoingOrder.SimType)).AsInt16().NotNullable()
                .WithColumn(nameof(OngoingOrder.MSISDN)).AsString(64).NotNullable()
                .WithColumn(nameof(OngoingOrder.ClientId)).AsInt32().NotNullable().ForeignKey(nameof(Client), nameof(Client.Id))
                .WithColumn(nameof(OngoingOrder.ClientAddressId)).AsInt32().Nullable().ForeignKey(nameof(ClientAddress), nameof(ClientAddress.Id));
        }

        public override void Down()
        {
            Delete.Table(nameof(OngoingOrder));
        }
    }
}
