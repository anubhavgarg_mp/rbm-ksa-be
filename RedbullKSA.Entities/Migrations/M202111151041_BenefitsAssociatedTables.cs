﻿using FluentMigrator;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Email;
using RedbullKSA.Entities.Database.SqlViews;
using RedbullKSA.Entities.Migrations.Helpers;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202111151041)]
    public class M202111151041_BenefitsAssociatedTables : Migration
    {
        public override void Up()
        {
            Execute.Sql(SqlViewHelper.RecreateView(nameof(RedeemedBenefitEmailView), BenefitSql.RedeemedBenefitEmailViewSql));
            Execute.Sql(SqlViewHelper.RecreateView(nameof(BenefitEligibleSelectView), BenefitSql.EligibleBenefitViewSql));
        }

        public override void Down()
        {
            Execute.Sql(SqlViewHelper.DropView(nameof(BenefitEligibleSelectView)));
            Execute.Sql(SqlViewHelper.DropView(nameof(RedeemedBenefitEmailView)));
        }
    }
}
