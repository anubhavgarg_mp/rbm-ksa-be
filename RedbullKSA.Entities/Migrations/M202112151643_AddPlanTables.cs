﻿using FluentMigrator;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Migrations.Extensions;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202112151643)]
    public class M202112151643_AddPlanTables : Migration
    {
        public override void Up()
        {
            Create.Table(nameof(Measurement))
                .WithColumn(nameof(Measurement.Id)).AsByte().PrimaryKey().NotNullable()
                .WithColumn(nameof(Measurement.ShortNameAR)).AsString(20).Nullable()
                .WithColumn(nameof(Measurement.ShortNameEN)).AsString(20).Nullable()
                .WithColumn(nameof(Measurement.NameAR)).AsString(50).Nullable()
                .WithColumn(nameof(Measurement.NameEN)).AsString(50).Nullable();

            Create.Table(nameof(PlanTag))
                .WithIdColumn()                
                .WithColumn(nameof(PlanTag.NameEN)).AsString(250).Nullable()
                .WithColumn(nameof(PlanTag.NameAR)).AsString(250).Nullable();

            Create.Table(nameof(Plan))
                .WithIdColumn()
                .WithAddedByColumn(nameof(User), nameof(User.Id))
                .WithAddedDateColumn()
                .WithModifiedByColumn(nameof(User), nameof(User.Id))
                .WithModifiedDateColumn()
                .WithColumn(nameof(Plan.PlanTagId)).AsInt32().NotNullable()
                .WithColumn(nameof(Plan.OfferingId)).AsString(10).NotNullable()
                .WithColumn(nameof(Plan.NameAR)).AsString(200).Nullable()
                .WithColumn(nameof(Plan.NameEN)).AsString(200).Nullable()
                .WithColumn(nameof(Plan.PlanType)).AsByte().NotNullable()
                .WithColumn(nameof(Plan.Price)).AsDecimal().NotNullable()
                .WithColumn(nameof(Plan.PriceTextAR)).AsString(150).Nullable()
                .WithColumn(nameof(Plan.PriceTextEN)).AsString(150).Nullable()
                .WithColumn(nameof(Plan.MeasurementId)).AsByte().NotNullable()
                .WithColumn(nameof(Plan.IsNew)).AsBoolean().NotNullable()
                .WithColumn(nameof(Plan.NewEndDate)).AsDateTime().Nullable()
                .WithColumn(nameof(Plan.IsActive)).AsBoolean().NotNullable()
                .WithColumn(nameof(Plan.ActivationStartDate)).AsDateTime().Nullable()
                .WithColumn(nameof(Plan.Comment1EN)).AsString(500).Nullable()
                .WithColumn(nameof(Plan.Comment2EN)).AsString(500).Nullable()
                .WithColumn(nameof(Plan.Comment1AR)).AsString(500).Nullable()
                .WithColumn(nameof(Plan.Comment2AR)).AsString(500).Nullable();

            Create.ForeignKey("FK_Plan_PlanTag")
                .FromTable(nameof(Plan)).ForeignColumn(nameof(Plan.PlanTagId))
                .ToTable(nameof(PlanTag)).PrimaryColumn(nameof(PlanTag.Id));

            Create.ForeignKey("FK_Plan_Measurement")
                .FromTable(nameof(Plan)).ForeignColumn(nameof(Plan.MeasurementId))
                .ToTable(nameof(Measurement)).PrimaryColumn(nameof(Measurement.Id));

            Insert.IntoTable(nameof(Measurement)).Row(new { Id = 1, NameEN = "GIGACOIN", NameAR = "جيجاكوين", ShortNameEN = "GB", ShortNameAR = "غيغابايت"});
        }

        public override void Down()
        {
            Delete.Table(nameof(Measurement));
            Delete.Table(nameof(PlanTag));
            Delete.Table(nameof(Plan));
        }
    }
}
