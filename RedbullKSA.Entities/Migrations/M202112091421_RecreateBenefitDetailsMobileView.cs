﻿using FluentMigrator;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.Database.SqlViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Migrations
{
    [Migration(202112091421)]
    public class M202112091421_RecreateBenefitDetailsMobileView : Migration
    {
        public override void Up()
        {
            Execute.Sql(BenefitSql.BenefitOfferSqlFunction(nameof(BenefitOfferMobileView)));
            Execute.Sql(BenefitSql.BenefitGuestOfferSqlFunction(nameof(BenefitGuestOfferMobileView)));
        }

        public override void Down()
        {
            throw new NotImplementedException();
        }

    }
}
