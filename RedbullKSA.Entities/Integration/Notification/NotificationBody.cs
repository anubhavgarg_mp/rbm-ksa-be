﻿using Newtonsoft.Json;
using RedbullKSA.Entities.API.Models.NotificationModels;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.Integration.Notification
{
    public class NotificationBody
    {
        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        public NotificationBody(string title, string body)
        {
            Title = title;
            Body = body;
        }

        public NotificationBody(PushNotificationModel model, ServiceLanguage language)
        {
            (Title, Body) = language switch
            {
                ServiceLanguage.AR => (model.Arabic.Title, model.Arabic.Body),
                _ => (model.English.Title, model.English.Body)
            };
        }
    }
}
