﻿using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;

namespace RedbullKSA.Entities.Integration.Notification
{
    public class BasePushNotification
    {
        public string To { get; set; }

        public int ClientId { get; set; }

        public NotificationBody Notification { get; set; }

        public IReadOnlyDictionary<string, string> Data { get; set; }

        public NotificationPurpose Type { get; set; }

        public BasePushNotification() { }

        public static BasePushNotification NewBenefitNotification(NotificationBody notification, IReadOnlyDictionary<string, string> data)
        {
            return new BasePushNotification()
            {
                Notification = notification,
                Data = data,
                Type = NotificationPurpose.Benefit
            };
        }
    }
}
