﻿using RedbullKSA.Entities.API.Models.CarouselModels;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Database.Models
{
    public class CarouselEntity : IEntity, IAdded, IModified
    {
        public int Id { get; set; }
        public string TitleEN { get; set; }
        public string TitleAR { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionAr { get; set; }
        public string ImageUrlEn { get; set; }
        public string ImageUrlAr { get; set; }
        public ActiveStatus Status { get; set; }
        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public CarouselEntity(CarouselEntityDTO details)
        {
            if (details == null)
                return;

            TitleEN = details.Title.English;
            TitleAR = details.Title.Arabic;
            DescriptionEn = details.Description.English;
            DescriptionAr = details.Description.Arabic;
            Status = details.Status;
        }

        public CarouselEntity(CarouselEntityDTO details, int id)
        {
            if (details == null)
                return;

            Id = id;
            TitleEN = details.Title?.English;
            TitleAR = details.Title?.Arabic;
            DescriptionEn = details.Description?.English;
            DescriptionAr = details.Description?.Arabic;
            Status = details.Status;
        }

        public CarouselEntity() { }
    }
}
