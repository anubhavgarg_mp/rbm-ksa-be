﻿using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class Tag : IEntity, IModified, IAdded
    {
        [Key]
        public int Id { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }

        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public string NameEN { get; set; }
        public string NameAR { get; set; }

        public int OrderPosition { get; set; }

        public ActiveStatus Status { get; set; }

        public void ExtractDetails(TagDTO details)
        {
            NameEN = details.Name.English;
            NameAR = details.Name.Arabic;
            Status = details.Status;
        }

        public static Tag FromEditModel(TagDTO details, int orderPosition)
        {
            return new Tag()
            {
                NameEN = details.Name.English,
                NameAR = details.Name.Arabic,
                Status = details.Status,
                OrderPosition = orderPosition
            };
        }
    }
}
