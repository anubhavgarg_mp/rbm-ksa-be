﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Database.Models
{
    public class SmsMessage : IEntity
    {
        [Key]
        public int Id { get; set; }
        public string Destination { get; set; }
        public DateTime Timestamp { get; set; }
        public string MessageBody { get; set; }
        public SmsType Type { get; set; }
    }
}
