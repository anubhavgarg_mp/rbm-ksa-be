﻿using RedbullKSA.Entities.Database.Base;

namespace RedbullKSA.Entities.Database.Models
{
    public class PlanTag : IEntity
    {
        public int Id { get; set; }

        public string NameEN { get; set; }

        public string NameAR { get; set; }
    }
}
