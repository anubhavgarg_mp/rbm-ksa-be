﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RedbullKSA.Entities.API.Models.Auth.Mobile;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;

namespace RedbullKSA.Entities.Database.Models
{
    public class Client : IEntity
    {
        public int Id { get; set; }

        public Guid Guid { get; set; }

        public string NationalityId { get; set; }

        public string BorderId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        /// <summary>
        /// Phone number from registration form
        /// </summary>
        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        [JsonProperty(PropertyName = "birthDate")]
        [JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTime? BirthDate { get; set; }

        public DateTime RegistrationDate { get; set; }

        public Gender? Gender { get; set; }

        public ServiceLanguage Language { get; set; }

        public string HuaweiCustomerId { get; set; }

        public bool IsActive { get; set; }

        public static Client FromRegistrationInputs(RegistrationMobile registration, Guid guid, ServiceLanguage language, string hashedPassword)
        {
            return new Client
            {
                NationalityId = registration.NationalityId,
                BorderId = registration.BorderId,
                PhoneNumber = registration.PhoneNumber,
                Email = registration.Email,
                RegistrationDate = DateTime.UtcNow,
                IsActive = true,
                Password = hashedPassword,
                Language = language,
                Guid = guid
            };
        }
    }
}
