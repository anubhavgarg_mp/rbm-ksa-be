﻿using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class User : IEntity, IAdded, IModified
    {
        [Key]
        public int Id { get; set; }

        public Guid Guid { get; set; }
        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }

        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string PasswordResetToken { get; set; }

        public DateTime? PasswordResetTokenExpiration { get; set; }

        public ActiveStatus Status { get; set; }

        public bool? EmailConfirmed { get; set; }

        public bool IsApiUser { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public static User FromAdministratorDetails(AdministratorDetailView details)
        {
            return new User
            {
                FirstName = details.FirstName,
                LastName = details.LastName,
                PhoneNumber = details.PhoneNumber,
                Email = details.Email,
                Status = details.Status,
                IsApiUser = details.IsApiUser
            };
        }

        public void ExtractAdministratorDetails(AdministratorDetailView details)
        {
            FirstName = details.FirstName;
            LastName = details.LastName;
            PhoneNumber = details.PhoneNumber;
            Email = details.Email;
            Status = details.Status;
            IsApiUser = details.IsApiUser;
        }
    }
}
