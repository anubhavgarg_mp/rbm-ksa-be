﻿using RedbullKSA.Entities.Database.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class BenefitTag : IEntity, IModified, IAdded
    {
        [Key]
        public int Id { get; set; }

        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }

        public int BenefitId { get; set; }
        public int TagId { get; set; }

        public BenefitTag() { }

        public BenefitTag(int benefitId, int tagId)
        {
            BenefitId = benefitId;
            TagId = tagId;
        }
    }
}
