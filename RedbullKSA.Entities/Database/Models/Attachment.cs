﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RedbullKSA.Entities.Database.Models
{
    public class Attachment : IAdded, IEntity
    {
        [Key]
        public int Id { get; set; }

        public Guid GuidId { get; set; }

        public string FileName { get; set; }

        public string FileType { get; set; }

        public string FileUrl { get; set; }

        public AttachmentFileType UploadedFileType { get; set; }

        public AttachmentCategory Category { get; set; }

        public ServiceLanguage FileLanguage { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? BenefitId { get; set; }

        public int? InterestId { get; set; }

        public int? ClientId { get; set; }
        public int? CarouselEntityId { get; set; }

        public Attachment() { }

        public Attachment(string fileName, Guid guid, string extension, string fileUrl, AttachmentCategory category, ServiceLanguage language = ServiceLanguage.EN)
        {
            FileName = fileName;
            GuidId = guid;
            FileType = extension;
            FileUrl = fileUrl;
            Category = category;
            FileLanguage = language;
        }

        public void DetachIds()
        {
            InterestId = null;
            BenefitId = null;
            ClientId = null;
            CarouselEntityId = null;
        }

        public bool IsAttached() => InterestId.HasValue || BenefitId.HasValue || ClientId.HasValue;
    }
}
