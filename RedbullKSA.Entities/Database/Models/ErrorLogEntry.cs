﻿using Newtonsoft.Json;
using RedbullKSA.Common.Enums;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class ErrorLogEntry : IEntity, IAdded
    {
        [Key]
        public int Id { get; set; }

        public SessionTokenType? Device { get; set; }

        public string UserName { get; set; }

        [Required]
        public string RequestUrl { get; set; }

        [JsonProperty]
        public string RequestParameters { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; } = DateTime.Now;

        public int? KSAExceptionCode { get; set; }

        public string KSAExceptionType { get; set; }

        [Required]
        public ExceptionType ExceptionType { get; set; }

        [Required]
        public string ExceptionMessage { get; set; }
        
        public string StackTrace { get; set; }
    }
}
