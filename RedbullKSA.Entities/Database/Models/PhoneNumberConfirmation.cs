﻿using RedbullKSA.Entities.Database.Base;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class PhoneNumberConfirmation : IEntity, IAdded
    {
        [Key]
        public int Id { get; set; }

        public int? AddedBy { get; set; }

        public DateTime? AddedDate { get; set; }

        public string PhoneNumber { get; set; }

        public string ConfirmationCode { get; set; }
        public int RetryCount { get; set; }

        public PhoneNumberConfirmation(string phoneNumber, string confirmationCode)
        {
            PhoneNumber = phoneNumber;
            ConfirmationCode = confirmationCode;
            RetryCount = 3;
        }

        public PhoneNumberConfirmation() { }
    }
}
