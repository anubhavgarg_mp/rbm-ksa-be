﻿using Newtonsoft.Json;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class SystemLogEntry : IEntity, IAdded
    {
        [Key]
        public int Id { get; set; }

        public SessionTokenType? Device { get; set; }
        
        public string UserName { get; set; }
        
        [Required]
        public string RequestUrl { get; set; }

        [JsonProperty]
        public string RequestParameters { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; } = DateTime.Now;
    }
}
