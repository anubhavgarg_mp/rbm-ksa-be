﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class RedeemedBenefit : IEntity, IAdded
    {
        [Key]
        public int Id { get; set; }

        public int? ClientId { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }

        public string PromoCode { get; set; }

        public int BenefitId { get; set; }

        public string DeliveryAddress { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public RedeemedBenefitDeliveryState? DeliveryState { get; set; }

        public RedeemedBenefitType Status { get; set; }

        public RedeemedBenefit(int benefitId, string voucher)
        {
            BenefitId = benefitId;
            PromoCode = voucher;
            Status = RedeemedBenefitType.Available;
        }

        public RedeemedBenefit() { }

        public void ToWinner(int clientId, string deliveryAddress)
        {
            ClientId = clientId;
            DeliveryAddress = deliveryAddress;
            Status = RedeemedBenefitType.Winner;
            AddedDate = DateTime.UtcNow;
        }

        public static RedeemedBenefit New(int clientId, int benefitId, string deliveryAddress)
        {
            return new RedeemedBenefit
            {
                ClientId = clientId,
                BenefitId = benefitId,
                Status = RedeemedBenefitType.Participant,
                DeliveryAddress = deliveryAddress
            };
        }
    }
}
