﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums.Address;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class ClientAddress : IEntity
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Arabic name
        /// </summary>
        public string NameAR { get; set; }

        /// <summary>
        /// English name
        /// </summary>
        public string NameEN { get; set; }

        public string HuaweiId { get; set; }

        public int ClientId { get; set; }

        /// <summary>
        /// 1 - default, 0 - not
        /// </summary>
        public bool IsDefault { get; set; }

        public AddressType? AddressType { get; set; }

        /// <summary>
        /// Address1
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Address2
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Address3
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Address4
        /// </summary>
        public string AddressLine4 { get; set; } 

        public string PostCode { get; set; }

        public string PhoneNumber1 { get; set; }

        public string PhoneNumber2 { get; set; }

        public string Email1 { get; set; }

        public string Email2 { get; set; }

        public string SmsNumber { get; set; }
    }
}
