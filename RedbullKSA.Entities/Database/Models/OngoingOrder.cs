﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Database.Models
{
    public class OngoingOrder : IEntity
    {
        [Key]
        public int Id { get; set; }
        public SimPickupType DeliveryType { get; set; }
        public NumberTiers NumberTier { get; set; }
        public SimType SimType { get; set; }
        public string MSISDN { get; set; }
        public int ClientId { get; set; }
        public int? ClientAddressId { get; set; }
    }
}
