﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Enums.Plan;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class Plan : IEntity, IAdded, IModified
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Huawei offering Id of Supplementary available offering
        /// </summary>
        public string OfferingId { get; set; }

        /// <summary>
        /// English plan name
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Arabic plan name
        /// </summary>
        public string NameAR { get; set; }

        /// <summary>
        /// Plan type
        /// </summary>
        public PlanType PlanType { get; set; }

        /// <summary>
        /// New - 1
        /// </summary>
        public bool IsNew { get; set; }

        /// <summary>
        /// Before this date a plan is new
        /// </summary>
        public DateTime? NewEndDate { get; set; }

        /// <summary>
        /// Price in GB
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// English price text
        /// </summary>
        public string PriceTextEN { get; set; }

        /// <summary>
        /// Arabic price text
        /// </summary>
        public string PriceTextAR { get; set; }

        /// <summary>
        /// Measurement of plan
        /// </summary>
        public MeasurementType MeasurementId { get; set; }

        /// <summary>
        /// Active/Inactive
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Activation start date
        /// </summary>
        public DateTime? ActivationStartDate { get; set; }

        /// <summary>
        /// Plan tag
        /// </summary>
        public int PlanTagId { get; set; }

        /// <summary>
        /// Comment 1
        /// </summary>
        public string Comment1EN { get; set; }

        /// <summary>
        /// Comment 2
        /// </summary>
        public string Comment2EN { get; set; }

        /// <summary>
        /// Arabic Comment 1
        /// </summary>
        public string Comment1AR { get; set; }

        /// <summary>
        /// Arabic Comment 2
        /// </summary>
        public string Comment2AR { get; set; }

        public int? AddedBy { get; set; }

        public DateTime? AddedDate { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}
