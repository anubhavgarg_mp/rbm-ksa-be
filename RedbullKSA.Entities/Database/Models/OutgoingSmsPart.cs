﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Database.Models
{
    public class OutgoingSmsPart : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int WholeMessageId { get; set; }

        public string MessageId { get; set; }

        public string SequenceNumber { get; set; }

        public SmppMessageState Status { get; set; }
    }
}
