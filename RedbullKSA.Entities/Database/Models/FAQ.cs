﻿using RedbullKSA.Entities.Database.Base;

namespace RedbullKSA.Entities.Database.Models
{
    public class FAQ : IEntity
    {
        public int Id { get; set; }

        public int? FAQTagId { get; set; }

        public string QuestionEN { get; set; }

        public string QuestionAR { get; set; }

        public string AnswerEN { get; set; }

        public string AnswerAR { get; set; }

        public bool TopXIndicator { get; set; }
    }
}
