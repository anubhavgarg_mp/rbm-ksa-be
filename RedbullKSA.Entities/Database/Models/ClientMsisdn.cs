﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class ClientMsisdn : IEntity, IAdded, IModified
    {
        [Key]
        public int Id { get; set; }

        public int ClientId { get; set; }

        /// <summary>
        /// Red Bull Service number/MSISDN
        /// </summary>
        public string PhoneNumber { get; set; }

        public SubscriberStatus SubscriberStatus { get; set; }

        public int? AddedBy { get; set; }

        public DateTime? AddedDate { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }
    }
}
