﻿using Newtonsoft.Json;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.NotificationModels;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Integration.Notification;
using System;
using FirebaseAdmin.Messaging;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RedbullKSA.Entities.API.Base;

namespace RedbullKSA.Entities.Database.Models
{
    public class Notification : IEntity, IAdded, IModified
    {
        [Key]
        public int Id { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public string MessageId { get; set; }

        public int ClientId { get; set; }

        public int? BenefitId { get; set; }
        public int? AddonId { get; set; }
        public int? PlanId { get; set; }
        public int? OfferId { get; set; }
        public int? TelcoOfferId { get; set; }
        public string PromoCode { get; set; }
        public string DeeplinkUrl { get; set; }

        public NotificationStatus Status { get; set; }

        public NotificationPurpose Purpose { get; set; }

        public DateTime Created { get; set; }

        //Firebase fields
        public string Title { get; set; }
        public string Body { get; set; }
        public string Data { get; set; }
        public string Token { get; set; }

        //Transfers
        public string TransferRecipient { get; set; }
        public string TransferSender { get; set; }
        public decimal? TransferAmount { get; set; }
        public TransferUnit? TransferUnit { get; set; }

        public string ImageUrl { get; set; }

        public string BenefitName { get; set; }

        public string TelcoOfferName { get; set; }

        public bool IsSeen { get; set; }

        public string MinAndroidVersion { get; set; }

        public string MinIOSVersion { get; set; }

        public Notification() { }

        public static Notification FromResponse(SendResponse response, BasePushNotification message)
        {
            return new Notification()
            {
                MessageId = response.IsSuccess ? response.MessageId : string.Empty,
                Status = response.IsSuccess ? NotificationStatus.Success : NotificationStatus.Failure,
                Title = message.Notification.Title,
                Body = message.Notification.Body,
                Data = message.Data != null ? JsonConvert.SerializeObject(message.Data).ToString() : string.Empty,
                Purpose = message.Type,
                Token = message.To,
                ClientId = message.ClientId
            };
        }

        public static Notification FromResponse(string response, BasePushNotification message)
        {
            return new Notification()
            {
                MessageId = response,
                Status = response.Length > 0 ? NotificationStatus.Success : NotificationStatus.Failure,
                Title = message.Notification.Title,
                Body = message.Notification.Body,
                Data = message.Data != null ? JsonConvert.SerializeObject(message.Data).ToString() : string.Empty,
                Purpose = message.Type,
                Token = message.To,
                ClientId = message.ClientId
            };
        }

        public void TryExtractBenefitData(BasePushNotification data)
        {
            if (data.Data.TryGetValue("benefitId", out string benefitId))
            {
                BenefitId = int.Parse(benefitId);
                BenefitName = data.Data.GetValueOrDefault("benefitName");
            }
        }
    }
}
