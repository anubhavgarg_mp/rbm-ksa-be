﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Database.Models
{
    /// <summary>
    /// Phone number and information for sent SMS
    /// </summary>
    public class SmsPhoneNumber : IEntity
    {
        [Key]
        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public DateTime Timestamp { get; set; }
        public string PhoneNumber { get; set; }
        public SentSmsStatus? Status { get; set; }
    }
}
