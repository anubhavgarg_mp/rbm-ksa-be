﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;
using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.UtilityServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class CalOneOffFee_Rsp
    {
        public CalOneOffFee_RspBody Body;
    }

    public class CalOneOffFee_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.UtilityService)]
        public CalOneOffFeeRspMsg CalOneOffFeeRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.UtilityService, IsNullable = false)]
    public class CalOneOffFeeRspMsg : BaseResponseMessage
    {
        public CalOneOffFeeRspMsgOneOffFeeList OneOffFeeList;
    }

    public class CalOneOffFeeRspMsgOneOffFeeList
    {
        public CalOneOffFeeRspMsgOneOffFeeListOneOffFee OneOffFee;
    }

    public class CalOneOffFeeRspMsgOneOffFeeListOneOffFee
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string FeeItemCode;

        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string FeeType;

        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = true)]
        public string FeeName;

        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string TotalAmt;

        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string OriginalAmt;

        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string DiscountAmt;

        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public TaxInfo TaxInfo;

        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = true)]
        public OfferingIdModel OfferingId;

        [XmlElement("AdditionalProperty", Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public SimpleProperty[] AdditionalProperty;
    }
}
