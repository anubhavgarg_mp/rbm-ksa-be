﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.RechargeServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryRechargeLog_Rsp
    {
        public QueryRechargeLog_RspBody Body;
    }

    public class QueryRechargeLog_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.RechargeService)]
        public QueryRechargeLogRspMsg QueryRechargeLogRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.RechargeService, IsNullable = false)]
    public class QueryRechargeLogRspMsg : BaseResponseMessage
    {
        public QueryRechargeLogRspMsgPagingInfo PagingInfo;
        [XmlElementAttribute("RechargeLog")]
        public QueryRechargeLogRspMsgRechargeLog[] RechargeLog;
    }

    public class QueryRechargeLogRspMsgPagingInfo
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int TotalRowNum;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int BeginRowNum;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int FetchRowNum;
    }

    public class QueryRechargeLogRspMsgRechargeLog
    {
        public string SerialNum;
        public string RechargeTime;
        public decimal ActualAmt;
        public int Currency;
        public int RechargeChannel;
        public int IsRollBack;
    }
}
