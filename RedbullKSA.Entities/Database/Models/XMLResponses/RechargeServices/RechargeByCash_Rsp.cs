﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.RechargeServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class RechargeByCash_Rsp
    {
        public RechargeByCash_RspBody Body;
    }

    public class RechargeByCash_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.RechargeService)]
        public RechargeByCashRspMsg RechargeByCashRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.RechargeService, IsNullable = false)]
    public class RechargeByCashRspMsg : BaseResponseMessage
    {
        public string SerialNum;
        public RechargeByCashRspMsgBalanceInfo BalanceInfo;
    }

    public class RechargeByCashRspMsgBalanceInfo
    {
        public string BalanceType;
        public string BalanceTypeName;
        public double OldBalanceAmt;
        public double NewBalanceAmt;
        public int Currency;
    }
}
