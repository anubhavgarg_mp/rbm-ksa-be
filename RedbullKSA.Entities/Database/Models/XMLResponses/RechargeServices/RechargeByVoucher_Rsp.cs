﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.RechargeServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class RechargeByVoucher_Rsp
    {
        public RechargeByVoucher_RspBody Body;
    }

    public class RechargeByVoucher_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.RechargeService)]
        public RechargeByVoucherRspMsg RechargeByVoucherRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.RechargeService, IsNullable = false)]
    public class RechargeByVoucherRspMsg : BaseResponseMessage
    {
        public string SerialNum;
        public RechargeByVoucherRspMsgBalanceInfo[] BalanceInfo;
        public RechargeByVoucherRspMsgRechargeBonus rechargeBonusField;
        public RechargeByVoucherRspMsgLifeCycleChangeInfo lifeCycleChangeInfoField;
    }

    public class RechargeByVoucherRspMsgBalanceInfo
    {
        public string BalanceType;
        public string BalanceTypeName;
        public int OldBalanceAmt;
        public int NewBalanceAmt;
        public int Currency;
    }

    public class RechargeByVoucherRspMsgRechargeBonus
    {
        public RechargeByVoucherRspMsgRechargeBonusFreeUnit freeUnitField;
    }

    public class RechargeByVoucherRspMsgRechargeBonusFreeUnit
    {
        public long freeUnitIdField;
        public string freeUnitTypeField;
        public string freeUnitTypeNameField;
        public int measureUnitField;
        public long bonusAmtField;
        public long effectiveTimeField;
        public long expireTimeField;
    }

    public class RechargeByVoucherRspMsgLifeCycleChangeInfo
    {
        [XmlElementAttribute("OldLifeCycleStatus")]
        public RechargeByVoucherRspMsgLifeCycleChangeInfoOldLifeCycleStatus[] oldLifeCycleStatusField;
        [XmlElementAttribute("NewLifeCycleStatus")]
        public RechargeByVoucherRspMsgLifeCycleChangeInfoNewLifeCycleStatus[] newLifeCycleStatusField;
        public int addValidityField;
    }

    public class RechargeByVoucherRspMsgLifeCycleChangeInfoOldLifeCycleStatus
    {
        public string statusNameField;
        public int statusIndexField;
        public long statusExpireTimeField;
    }
    public partial class RechargeByVoucherRspMsgLifeCycleChangeInfoNewLifeCycleStatus
    {
        public string statusNameField;
        public int statusIndexField;
        public long statusExpireTimeField;
    }
}
