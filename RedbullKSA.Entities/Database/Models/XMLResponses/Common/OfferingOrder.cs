﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SoapToRest.XMLResponses.Common
{
    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class OrderItemResponses
    {
        public OrderItemResponsesOrderItemResponse OrderItemResponse;
    }

    public class OrderItemResponsesOrderItemResponse
    {
        public string OrderType;
        public int OrderId;
        public long CustId;
        public decimal CustCode;
        public long AccountId;
        public decimal AcctCode;
    }
}
