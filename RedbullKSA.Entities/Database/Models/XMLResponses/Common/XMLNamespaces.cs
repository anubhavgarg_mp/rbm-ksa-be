﻿

namespace RedbullKSA.Entities.Database.Models.XMLResponses.Common
{
    public static class XMLNamespaces
    {
        public const string Envelope = "http://schemas.xmlsoap.org/soap/envelope/";
        public const string Huawei = "http://www.huawei.com/bss/soaif/interface/";
        public const string HuaweiCbs = "http://www.huawei.com/bme/cbsinterface/arservices";
        public const string CbsCommon = "http://www.huawei.com/bme/cbsinterface/cbscommon";
        public const string ArCommon = "http://cbs.huawei.com/ar/wsservice/arcommon";
    }

    public static class XMLNamespacePaths
    {
        public const string Common = "common/";
        public const string AccountService = "AccountService/";
        public const string ArService = "arservices/";
        public const string BillingService = "BillingService/";
        public const string CustomerService = "CustomerService/";
        public const string InventoryService = "InventoryService/";
        public const string OfferingService = "OfferingService/";
        public const string OrderService = "OrderService/";
        public const string RechargeService = "RechargeService/";
        public const string SubscriberService = "SubscriberService/";
        public const string UtilityService = "UtilityService/";
    }
}
