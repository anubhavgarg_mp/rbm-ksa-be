﻿using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.XMLResponses.Common
{
    [XmlRoot(Namespace = "", IsNullable = false)]
    public class ResultHeader
    {
        [XmlElement(Namespace = XMLNamespaces.CbsCommon)]
        public decimal Version;
        [XmlElement(Namespace = XMLNamespaces.CbsCommon)]
        public int ResultCode;
        [XmlElement(Namespace = XMLNamespaces.CbsCommon)]
        public int MsgLanguageCode;
        [XmlElement(Namespace = XMLNamespaces.CbsCommon)]
        public string ResultDesc;
    }
}
