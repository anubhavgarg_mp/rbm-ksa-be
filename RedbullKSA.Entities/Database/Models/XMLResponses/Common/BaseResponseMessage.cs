﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.XMLResponses.Common
{
    public abstract class BaseResponseMessage
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public RspHeader RspHeader;
    }
}
