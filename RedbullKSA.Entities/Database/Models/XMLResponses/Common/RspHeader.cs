﻿using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.XMLResponses.Common
{
    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class RspHeader
    {
        public decimal Version;
        public string ReturnCode;
        public string ReturnMsg;
        public long RspTime;
    }
}
