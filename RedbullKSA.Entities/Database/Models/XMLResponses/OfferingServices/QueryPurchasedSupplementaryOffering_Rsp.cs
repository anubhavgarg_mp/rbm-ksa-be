﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.OfferingServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryPurchasedSupplementaryOffering_Rsp
    {
        public QueryPurchasedSupplementaryOffering_RspBody Body;
    }

    public class QueryPurchasedSupplementaryOffering_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService)]
        public QueryPurchasedSupplementaryOfferingRspMsg QueryPurchasedSupplementaryOfferingRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService, IsNullable = false)]
    public class QueryPurchasedSupplementaryOfferingRspMsg : BaseResponseMessage
    {
        [XmlElement("SupplementaryOffering")]
        public QueryPurchasedSupplementaryOfferingRspMsgSupplementaryOffering[] SupplementaryOffering;
    }

    public class QueryPurchasedSupplementaryOfferingRspMsgSupplementaryOffering
    {
        public QueryPurchasedSupplementaryOfferingRspMsgSupplementaryOfferingOfferingId OfferingId;
        public string OfferingName;
        public string EffectiveDate;
        public string ExpireDate;
    }

    public class QueryPurchasedSupplementaryOfferingRspMsgSupplementaryOfferingOfferingId
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string OfferingId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long PurchaseSeq;
    }
}
