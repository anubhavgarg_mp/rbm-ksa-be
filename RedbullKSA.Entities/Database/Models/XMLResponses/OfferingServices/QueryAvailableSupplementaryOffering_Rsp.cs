﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.OfferingServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryAvailableSupplementaryOffering_Rsp
    {
        public QueryAvailableSupplementaryOffering_RspBody Body;
    }

    public class QueryAvailableSupplementaryOffering_RspBody
{
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService)]
        public QueryAvailableSupplementaryOfferingRspMsg QueryAvailableSupplementaryOfferingRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService, IsNullable = false)]
    public class QueryAvailableSupplementaryOfferingRspMsg : BaseResponseMessage
    {
        [XmlElement("SupplementaryOffering")]
        public SupplementaryOffering[] SupplementaryOffering;
    }

    public class SupplementaryOffering
    {
        public QueryAvailableSupplementaryOfferingRspMsgSupplementaryOfferingOfferingId OfferingId;
        public string OfferingCode;
        public string OfferingName;
        public string OfferingintName;
        public int OfferingStatus;
        public int NetworkType;
        public int OfferingType;
        public int OwnerType;
        public int PaymentType;
        [XmlIgnore()]
        public bool PaymentTypeSpecified;
        public string EffectiveDate;
        public string ExpireDate;
        public decimal MonthlyCost;
        public decimal OneTimeCost;
        //TODO: ADD contract info if needed (to get duration)
        [XmlElement("AdditionalProperty")]
        public QueryAvailableSupplementaryOfferingRspMsgSupplementaryOfferingAdditionalProperty[] AdditionalProperty;
    }

    public class QueryAvailableSupplementaryOfferingRspMsgSupplementaryOfferingOfferingId
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string OfferingId;
    }

    public class QueryAvailableSupplementaryOfferingRspMsgSupplementaryOfferingAdditionalProperty
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string Code;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string Value;
    }
}
