﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.OfferingServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryAvailablePrimaryOffering_Rsp
    {
        public QueryAvailablePrimaryOffering_RspBody Body;
    }

    public class QueryAvailablePrimaryOffering_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService)]
        public QueryAvailablePrimaryOfferingRspMsg QueryAvailablePrimaryOfferingRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService, IsNullable = false)]
    public class QueryAvailablePrimaryOfferingRspMsg : BaseResponseMessage
    {
        [XmlElement("PrimaryOffering")]
        public PrimaryOffering[] PrimaryOffering;
    }

    public class PrimaryOffering
    {
        public QueryAvailablePrimaryOfferingRspMsgPrimaryOfferingOfferingId OfferingId;
        public string OfferingCode;
        public string OfferingName;
        public string OfferingShortName;
        public int OfferingStatus;
        public int NetworkType;
        public int OfferingType;
        public int OwnerType;
        public int PaymentType;
        public string EffectiveDate;
        public string ExpireDate;
        public decimal MonthlyCost;
        public decimal OneTimeCost;
        public PrimaryOfferingContract Contract;
        [XmlElement("AdditionalProperty")]
        public QueryAvailablePrimaryOfferingRspMsgPrimaryOfferingAdditionalProperty[] AdditionalProperty;
    }

    public class QueryAvailablePrimaryOfferingRspMsgPrimaryOfferingOfferingId
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string OfferingId;
    }
   
    public class PrimaryOfferingContract
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string AgreementId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string DurationUnit;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int DurationValue;
    }

    public class QueryAvailablePrimaryOfferingRspMsgPrimaryOfferingAdditionalProperty
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string Code;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string Value;
    }
}
