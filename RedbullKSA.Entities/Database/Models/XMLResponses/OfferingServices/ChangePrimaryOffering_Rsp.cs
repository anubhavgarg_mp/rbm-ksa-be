﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using SoapToRest.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.OfferingServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class ChangePrimaryOffering_Rsp
    {
        public ChangePrimaryOffering_RspBody Body;
    }

    public class ChangePrimaryOffering_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService)]
        public ChangePrimaryOfferingRspMsg ChangePrimaryOfferingRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService, IsNullable = false)]
    public class ChangePrimaryOfferingRspMsg : BaseResponseMessage
    {
        public ChangePrimaryOfferingRspMsgSubmitOrderResponse SubmitOrderResponse;
    }

    public class ChangePrimaryOfferingRspMsgSubmitOrderResponse
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string OrderId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public OrderItemResponses OrderItemResponses;
    }

    [XmlRootAttribute(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class OrderItemResponses
    {
        public OrderItemResponsesOrderItemResponse OrderItemResponse;
    }

    public class OrderItemResponsesOrderItemResponse
    {
        public string OrderType;
        public int OrderId;
        public long CustId;
        public string CustCode;
        public long AccountId;
        public string AcctCode;
    }

}
