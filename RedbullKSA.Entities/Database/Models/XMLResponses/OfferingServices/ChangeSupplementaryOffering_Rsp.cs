﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using SoapToRest.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.OfferingServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class ChangeSupplementaryOffering_Rsp
    {
        public ChangeSupplementaryOffering_RspBody Body;
    }

    public class ChangeSupplementaryOffering_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService)]
        public ChangeSupplementaryOfferingRspMsg ChangeSupplementaryOfferingRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService, IsNullable = false)]
    public class ChangeSupplementaryOfferingRspMsg : BaseResponseMessage
    {
        public ChangeSupplementaryOfferingRspMsgSubmitOrderResponse SubmitOrderResponse;
    }

    public class ChangeSupplementaryOfferingRspMsgSubmitOrderResponse
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string OrderId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public OrderItemResponses OrderItemResponses;
    }
}
