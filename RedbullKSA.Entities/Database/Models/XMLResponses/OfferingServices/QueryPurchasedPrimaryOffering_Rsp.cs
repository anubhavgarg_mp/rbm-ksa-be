﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.OfferingServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryPurchasedPrimaryOffering_Rsp
    {
        public QueryPurchasedPrimaryOffering_RspBody Body;
    }

    public class QueryPurchasedPrimaryOffering_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService)]
        public QueryPurchasedPrimaryOfferingRspMsg QueryPurchasedPrimaryOfferingRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OfferingService, IsNullable = false)]
    public class QueryPurchasedPrimaryOfferingRspMsg : BaseResponseMessage
    {
        public QueryPurchasedPrimaryOfferingRspMsgPrimaryOffering PrimaryOffering;
    }

    public class QueryPurchasedPrimaryOfferingRspMsgPrimaryOffering
    {
        public QueryPurchasedPrimaryOfferingRspMsgPrimaryOfferingOfferingId OfferingId;
        public string OfferingName;
        public string EffectiveDate;
        public string ExpireDate;
    }

    public class QueryPurchasedPrimaryOfferingRspMsgPrimaryOfferingOfferingId
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string OfferingId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long PurchaseSeq;
    }
}

