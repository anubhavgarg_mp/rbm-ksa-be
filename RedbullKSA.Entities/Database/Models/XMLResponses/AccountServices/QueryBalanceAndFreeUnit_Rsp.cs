﻿
using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.AccountServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryBalanceAndFreeUnit_Rsp
    {
        public QueryBalanceAndFreeUnit_RspBody Body;
    }

    public class QueryBalanceAndFreeUnit_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.AccountService)]
        public QueryBalanceAndFreeUnitRspMsg QueryBalanceAndFreeUnitRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.AccountService, IsNullable = false)]
    public class QueryBalanceAndFreeUnitRspMsg : BaseResponseMessage
    {
        public QueryBalanceAndFreeUnitRspMsgAccount Account;
        [XmlElement("FreeUnit")]
        public QueryBalanceAndFreeUnitRspMsgFreeUnit[] FreeUnits;
    }

    public class QueryBalanceAndFreeUnitRspMsgAccount
    {
        public long AcctId;
        public QueryBalanceAndFreeUnitRspMsgAccountAcctBalance AcctBalance;
        public QueryBalanceAndFreeUnitRspMsgAccountAcctCreditAmt AcctCreditAmt;
        public QueryBalanceAndFreeUnitRspMsgAccountOutstandingAmt OutstandingAmt;
    }

    public class QueryBalanceAndFreeUnitRspMsgAccountAcctBalance
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string BalanceType;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string BalanceTypeName;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public decimal TotalAmount;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int IsDeposit;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int IsRefund;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int Currency;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public BalanceDetail BalanceDetail;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class BalanceDetail
    {
        public long BalanceInstanceId;
        public decimal Amount;
        public long EffectiveTime;
        public long ExpireTime;
    }

    public class QueryBalanceAndFreeUnitRspMsgAccountAcctCreditAmt
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string CreditLimitType;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string CreditLimitTypeName;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public decimal TotalCreditAmt;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public decimal TotalUsageAmt;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public decimal TotalRemainAmt;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public CreditAmtInfo CreditAmtInfo;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class CreditAmtInfo
    {
        public string LimitClass;
        public decimal Amount;
        public long EffectiveTime;
        public long ExpireTime;
    }

    public class QueryBalanceAndFreeUnitRspMsgFreeUnit
    {
        public string TypeId;
        public string TypeName;
        public string MeasureUnit;
        public decimal TotalAmt;
        public decimal UnusedAmt;
        public QueryBalanceAndFreeUnitRspMsgFreeUnitDetail Detail;
    }

    public class QueryBalanceAndFreeUnitRspMsgFreeUnitDetail
    {
        public long UnitId;
        public decimal InitialAmt;
        public decimal UnusedAmt;
        public long EffectiveTime;
        public long ExpiredDate;
        public string IsRollOver;
    }

    public class QueryBalanceAndFreeUnitRspMsgAccountOutstandingAmt
    {
        public string BillCycleId { get; set;  }
        public TimePeriod BillCyclePeriod;
        public long DueDate;
        public OutStandingDetails OutStandingDetails;

    }

    public class TimePeriod
    {
        public long StartTime;
        public long EndTime;
    }

    public class OutStandingDetails
    {
        public decimal OutstandingAmt;
        public string Currency;
    }
}
