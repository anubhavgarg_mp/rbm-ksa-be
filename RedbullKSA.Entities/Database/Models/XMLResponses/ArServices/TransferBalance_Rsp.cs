﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.ArServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class TransferBalance_Rsp
    {
        public TransferBalance_RspBody Body;
    }

    public class TransferBalance_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.HuaweiCbs)]
        public TransferBalanceResultMsg TransferBalanceResultMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.HuaweiCbs, IsNullable = false)]
    public class TransferBalanceResultMsg
    {
        [XmlElement(Namespace = "")]
        public ResultHeader ResultHeader;
        [XmlElement(Namespace = "")]
        public TransferBalanceResult TransferBalanceResult;
    }

    [XmlRoot(Namespace = "", IsNullable = false)]
    public class TransferBalanceResult
    {
        [XmlElement(Namespace = XMLNamespaces.HuaweiCbs)]
        public Transferor Transferor;
        [XmlElement(Namespace = XMLNamespaces.HuaweiCbs)]
        public Transferee Transferee;
    }

    [XmlRoot(Namespace = XMLNamespaces.HuaweiCbs, IsNullable = false)]
    public class Transferor
    {
        public TransferorBalanceChgInfo BalanceChgInfo;
        public int HandlingChargeAmt;
        public TransferorLifeCycleChgInfo LifeCycleChgInfo;
    }

    [XmlType(AnonymousType = true, Namespace = XMLNamespaces.HuaweiCbs)]
    public class TransferorBalanceChgInfo
    {
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public string BalanceType;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public long BalanceID;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public string BalanceTypeName;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public int OldBalanceAmt;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public int NewBalanceAmt;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public int CurrencyID;
    }

    public class TransferorLifeCycleChgInfo
    {
        [XmlElement("OldLifeCycleStatus")]
        public TransferorLifeCycleChgInfoOldLifeCycleStatus[] OldLifeCycleStatus;
        [XmlElement("NewLifeCycleStatus")]
        public TransferorLifeCycleChgInfoNewLifeCycleStatus[] NewLifeCycleStatus;
        public int ChgValidity;
    }

    public class TransferorLifeCycleChgInfoOldLifeCycleStatus
    {
        public string StatusName;
        public long StatusExpireTime;
        public int StatusIndex;
    }

    public class TransferorLifeCycleChgInfoNewLifeCycleStatus
    {
        public string StatusName;
        public long StatusExpireTime;
        public int StatusIndex;
    }

    [XmlRoot(Namespace = XMLNamespaces.HuaweiCbs, IsNullable = false)]
    public class Transferee
    {
        public TransfereeBalanceChgInfo BalanceChgInfo;
        public int HandlingChargeAmt;
        public TransfereeLifeCycleChgInfo LifeCycleChgInfo;
    }

    public class TransfereeBalanceChgInfo
    {
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public string BalanceType;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public long BalanceID;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public string BalanceTypeName;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public int OldBalanceAmt;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public int NewBalanceAmt;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public int CurrencyID;
    }

    public class TransfereeLifeCycleChgInfo
    {
        [XmlElement("OldLifeCycleStatus")]
        public TransfereeLifeCycleChgInfoOldLifeCycleStatus[] OldLifeCycleStatus;
        [XmlElement("NewLifeCycleStatus")]
        public TransfereeLifeCycleChgInfoNewLifeCycleStatus[] NewLifeCycleStatus;
        public int ChgValidity;
    }

    public class TransfereeLifeCycleChgInfoOldLifeCycleStatus
    {
        public string StatusName;
        public long StatusExpireTime;
        public int StatusIndex;
    }

    public class TransfereeLifeCycleChgInfoNewLifeCycleStatus
    {
        public string StatusName;
        public long StatusExpireTime;
        public int StatusIndex;
    }
}

