﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.ArServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryTransferLog_Rsp
    {
        public QueryTransferLog_RspBody Body;
    }

    public class QueryTransferLog_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.HuaweiCbs)]
        public QueryTransferLogResultMsg QueryTransferLogResultMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.HuaweiCbs, IsNullable = false)]
    public class QueryTransferLogResultMsg
    {
        [XmlElement(Namespace = "")]
        public ResultHeader ResultHeader;
        [XmlElement(Namespace = "")]
        public QueryTransferLogResult QueryTransferLogResult;
    }

    [XmlRoot(Namespace = "", IsNullable = false)]
    public class QueryTransferLogResult
    {
        [XmlElement(Namespace = XMLNamespaces.HuaweiCbs)]
        public TransferInfo TransferInfo;
        [XmlElement(Namespace = XMLNamespaces.HuaweiCbs)]
        public int TotalRowNum;
        [XmlElement(Namespace = XMLNamespaces.HuaweiCbs)]
        public int BeginRowNum;
        [XmlElement(Namespace = XMLNamespaces.HuaweiCbs)]
        public int FetchRowNum;
    }

    [XmlRoot(Namespace = XMLNamespaces.HuaweiCbs, IsNullable = false)]
    public class TransferInfo
    {
        public long TradeTime;
        public long AcctKey;
        public long SubKey;
        public int PrimaryIdentity;
        public string TransferChannelID;
        public int TransID;
        public long ExtTransID;
        public int TransferAmount;
        public int CurrencyID;
        public int ResultCode;
        [XmlElement("AdditionalProperty")]
        public TransferInfoAdditionalProperty[] AdditionalProperty;
    }

    public class TransferInfoAdditionalProperty
    {
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public string Code;
        [XmlElement(Namespace = XMLNamespaces.ArCommon)]
        public string Value;
    }
}
