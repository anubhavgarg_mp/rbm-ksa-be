﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.SubscriberServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QuerySubscriberMateDates_Rsp
    {
        public QuerySubscriberMateDates_RspBody Body;
    }

    public class QuerySubscriberMateDates_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.SubscriberService)]
        public QuerySubMateDatesRspMsg QuerySubMateDatesRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.SubscriberService, IsNullable = false)]
    public class QuerySubMateDatesRspMsg : BaseResponseMessage
    {
        public QuerySubMateDatesRspMsgSubscriberMateDates SubscriberMateDates;
    }

    public class QuerySubMateDatesRspMsgSubscriberMateDates
    {
        public QuerySubMateDatesRspMsgSubscriberMateDatesSubscriberMateDate SubscriberMateDate;
    }

    public class QuerySubMateDatesRspMsgSubscriberMateDatesSubscriberMateDate
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long SubscriberId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string Status;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int BEID;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string SupplementFlag;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int ServiceNumber;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long ICCID;
    }
}
