﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.SubscriberServices
{

    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryEntityIds_Rsp
    {
        public QueryEntityIds_RspBody Body;
    }

    public class QueryEntityIds_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.SubscriberService)]
        public QueryEntityIdsRspMsg QueryEntityIdsRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.SubscriberService, IsNullable = false)]
    public class QueryEntityIdsRspMsg : BaseResponseMessage
    {
        [Required]
        [XmlElement("EntityId")]
        public QueryEntityIdsRspMsgEntityId[] EntityId;
    }

    public class QueryEntityIdsRspMsgEntityId
    {
        [Required]
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int ObjectIdType;

        [Required]
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long ObjectId;
    }
}
