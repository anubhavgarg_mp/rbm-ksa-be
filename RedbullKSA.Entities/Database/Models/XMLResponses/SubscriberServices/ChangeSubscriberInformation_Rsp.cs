﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.SubscriberServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class ChangeSubscriberInformation_Rsp
    {
        public ChangeSubscriberInformation_RspBody Body;
    }

    public class ChangeSubscriberInformation_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.SubscriberService)]
        public ChangeSubInfoRspMsg ChangeSubInfoRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.SubscriberService, IsNullable = false)]
    public class ChangeSubInfoRspMsg : BaseResponseMessage
    {
        public ChangeSubInfoRspMsgSubmitOrderResponse SubmitOrderResponse;
    }

    public class ChangeSubInfoRspMsgSubmitOrderResponse
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int OrderId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public OrderItemResponses OrderItemResponses;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class OrderItemResponses
    {
        public OrderItemResponsesOrderItemResponse OrderItemResponse;
    }

    public class OrderItemResponsesOrderItemResponse
    {
        public string OrderType;
        public int OrderId;
        public long CustId;
        public decimal CustCode;
        public long AccountId;
        public decimal AcctCode;
    }
}
