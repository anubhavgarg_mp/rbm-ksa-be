﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.SubscriberServices
{

    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QuerySubscriberAllInfomation_Rsp
    {
        public QuerySubscriberAllInfomation_RspBody Body;
    }

    public class QuerySubscriberAllInfomation_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.SubscriberService)]
        public QuerySubAllInfoRspMsg QuerySubAllInfoRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.SubscriberService, IsNullable = false)]
    public class QuerySubAllInfoRspMsg : BaseResponseMessage
    {
        public QuerySubAllInfoRspMsgSubscriber Subscriber;
    }

    public class QuerySubAllInfoRspMsgSubscriber
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long SubId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long CustId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long AcctId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public decimal CustCode;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int PartnerId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int BrandId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int SubType;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int ServiceNum;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long ICCID;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string PIN1;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string PIN2;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string PUK1;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string PUK2;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long IMSI;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int Language;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int WrittenLanguage;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int NetworkType;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int SubStatus;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int SubStatusReason;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long EffectiveDate;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long ExpireDate;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public PrimaryOffering PrimaryOffering;
        [XmlArray(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        [XmlArrayItem("SupplementaryOffering", IsNullable = false)]
        public SupplementaryOfferingListSupplementaryOffering[] SupplementaryOfferingList;
        [XmlElement("AdditionalProperty", Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public AdditionalProperty[] AdditionalProperty;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class PrimaryOffering
    {
        public PrimaryOfferingOfferingId OfferingId;
        public string OfferingName;
        public long EffectiveDate;
        public long ExpireDate;
        public string OfferStatus;
        [XmlArrayItem("ProductInfo", IsNullable = false)]
        public PrimaryOfferingProductInfo[] ProductList;
        public string GroupMemberFlag;
        public PrimaryOfferingAdditionalProperty AdditionalProperty;
    }

    public class PrimaryOfferingOfferingId
    {
        public int OfferingId;
        public long PurchaseSeq;
    }

    public class PrimaryOfferingProductInfo
    {
        public int ProductId;
        public string ProductName;
        public string Status;

        [XmlElement("AdditionalProperty")]
        public PrimaryOfferingProductInfoAdditionalProperty[] AdditionalProperty;
    }

    public class PrimaryOfferingProductInfoAdditionalProperty
    {
        public int Code;
        public string Value;
    }

    public class PrimaryOfferingAdditionalProperty
    {
        public string Code;
        public int Value;
    }

    public class SupplementaryOfferingListSupplementaryOffering
    {
        public SupplementaryOfferingListSupplementaryOfferingOfferingId OfferingId;
        public string OfferingName;
        public long EffectiveDate;
        public long ExpireDate;
        public string OfferStatus;
        public string GroupMemberFlag;
        public SupplementaryOfferingListSupplementaryOfferingAdditionalProperty AdditionalProperty;
    }

    public class SupplementaryOfferingListSupplementaryOfferingOfferingId
    {
        public int OfferingId;
        public long PurchaseSeq;
    }

    public class SupplementaryOfferingListSupplementaryOfferingAdditionalProperty
    {
        public string Code;
        public int Value;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class AdditionalProperty
    {
        public string Code;
        public string Value;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class SupplementaryOfferingList
    {
        [XmlElement("SupplementaryOffering")]
        public SupplementaryOfferingListSupplementaryOffering[] SupplementaryOffering;
    }
}
