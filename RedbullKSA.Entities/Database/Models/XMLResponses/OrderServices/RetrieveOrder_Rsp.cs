﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.OrderServices
{

    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class RetrieveOrder_Rsp
    {
        public RetrieveOrder_RspBody Body;
    }

    public class RetrieveOrder_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OrderService)]
        public RetrieveOrderRspMsg RetrieveOrderRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OrderService, IsNullable = false)]
    public class RetrieveOrderRspMsg : BaseResponseMessage
    {
        //TODO: Add fields if missing
    }
}
