﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.OrderServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class CreateSaleOrder_Rsp
    {
        public CreateSaleOrder_RspBody Body;
    }

    public class CreateSaleOrder_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OrderService)]
        public CreateSaleOrderRspMsg CreateSaleOrderRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.OrderService, IsNullable = false)]
    public class CreateSaleOrderRspMsg : BaseResponseMessage
    {
        public long CustomerId;
        public decimal CustCode;
        public int OrderId;
    }
}
