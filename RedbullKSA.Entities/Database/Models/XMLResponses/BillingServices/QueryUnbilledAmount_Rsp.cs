﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;
using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.XMLResponses.BillingServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryUnbilledAmount_Rsp
    {
        public QueryUnbilledAmount_RspBody Body;
    }

    public class QueryUnbilledAmount_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService)]
        public QueryUnbilledAmountRspMsg QueryUnbilledAmountRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = false)]
    public class QueryUnbilledAmountRspMsg : BaseResponseMessage
    {
        public QueryUnbilledAmount[] UnbilledAmount;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = true)]
    public class QueryUnbilledAmount
    {
        public string CustId;
        public string ServiceNum;
        public string BillCycleId;
        public string Category;
        public UnbilledInfo UnbilledInfo;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = false)]
    public class UnbilledInfo
    {
        public ChargeFeeInfo RecurringCharge;
        public OneTimeCharge OneTimeCharge;
        public UsageCharge UsageCharge;
        public BillMediumCharge BillMediumCharge;
        public ChargeFeeInfo MininumConsumption;
        public ChargeFeeInfo Discount;
        public Tax Tax;
        public SimpleProperty[] AdditionalProperty;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = false)]
    public class RecurringCharge
    {
        public OfferingIdModel OfferingId;
        public decimal ChargeAmt;
        public string Currency;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = false)]
    public class OneTimeCharge
    {
        public string ChargeType;
        public DateTime? ChargeTime;
        public decimal ChargeAmt;
        public string Currency;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = false)]
    public class UsageCharge
    {
        public string ServiceType;
        public decimal ChargeAmt;
        public string Currency;
        public VolumeInfo VolumeInfo;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = true)]
    public class VolumeInfo
    {
        public string ActualVolume;
        public string RatingVolume;
        public string FreeVolume;
        public string MeasureUnit;
        public string DiscountVolume;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = false)]
    public class BillMediumCharge
    {
        public ChargeFeeInfo ChargeInfo;

        [XmlElement("Type")]
        public string CategoryName;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = false)]
    public class Tax
    {
        public string TaxCode;
        public decimal TaxAmt;
        public string Currency;
    }
}
