﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;

namespace RedbullKSA.Entities.Database.Models.XMLResponses.BillingServices
{
    public class ChargeFeeInfo
    {
        public string ChargeType;

        public string PayType;

        public string Currency;

        public decimal Amount;

        public SimpleProperty[] AdditionalProperty;
    }
}
