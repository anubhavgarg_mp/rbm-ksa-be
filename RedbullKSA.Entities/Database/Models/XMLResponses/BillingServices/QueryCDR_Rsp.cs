﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.BillingServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryCDR_Rsp
    {
        public QueryCDR_RspBody Body;
    }

    public class QueryCDR_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService)]
        public QueryCDRRspMsg QueryCDRRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.BillingService, IsNullable = false)]
    public class QueryCDRRspMsg : BaseResponseMessage
    {
        public QueryCDRRspMsgPagingInfo PagingInfo;
        [XmlElement("CDRInfo")]
        public QueryCDRRspMsgCDRInfo[] CDRInfo;
    }

    public class QueryCDRRspMsgPagingInfo
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int TotalRowNum;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int BeginRowNum;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int FetchRowNum;
    }

    public class QueryCDRRspMsgCDRInfo
    {
        public long SerialNum;
        public int Category;
        public string ServiceType;
        public string OtherServiceNum;
        public string CallType;
        public string RoamType;
        public QueryCDRRspMsgCDRInfoTimePeriod TimePeriod;
        public QueryCDRRspMsgCDRInfoAreaInfo AreaInfo;
        public QueryCDRRspMsgCDRInfoVolumeInfo VolumeInfo;
        public QueryCDRRspMsgCDRInfoTotalChargeAmt TotalChargeAmt;
        public QueryCDRRspMsgCDRInfoChargeDetail ChargeDetail;
        [XmlElement("AdditionalProperty")]
        public QueryCDRRspMsgCDRInfoAdditionalProperty[] AdditionalProperty;
    }

    public class QueryCDRRspMsgCDRInfoTimePeriod
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long StartTime;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long EndTime;
    }

    public class QueryCDRRspMsgCDRInfoAreaInfo
    {
        public int CallingHomeCountryCode;
        public int CallingHomeAreaCode;
        public int CallingRoamCountryCode;
        public int CallingRoamAreaCode;
        public int CalledHomeCountryCode;
        public bool CalledHomeCountryCodeSpecified;
        public int CalledgHomeAreaCode;
        public bool CalledgHomeAreaCodeSpecified;
        public int CalledRoamCountryCode;
        public bool CalledRoamCountryCodeSpecified;
        public int CalledRoamAreaCode;
        public bool CalledRoamAreaCodeSpecified;
    }

    public class QueryCDRRspMsgCDRInfoVolumeInfo
    {
        public int ActualVolume;
        public int RatingVolume;
        public int FreeVolume;
        public int MeasureUnit;
    }

    public class QueryCDRRspMsgCDRInfoTotalChargeAmt
    {
        public int Amount;
        public int WaiveAmt;
        public int Currency;
    }

    public class QueryCDRRspMsgCDRInfoChargeDetail
    {
        public string ChargeCode;
        public string ChargeCodeName;
        public int ChargeAmt;
        public int Currency;
        public QueryCDRRspMsgCDRInfoChargeDetailOfferingId OfferingId;
    }

    public class QueryCDRRspMsgCDRInfoChargeDetailOfferingId
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int OfferingId;
    }

    public class QueryCDRRspMsgCDRInfoAdditionalProperty
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string Code;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int Value;
    }

}
