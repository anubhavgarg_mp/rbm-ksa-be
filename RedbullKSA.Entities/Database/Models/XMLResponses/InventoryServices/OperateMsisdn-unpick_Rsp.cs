﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.InventoryServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class OperateMsisdn_unpick_Rsp
    {
        public OperateMsisdn_unpick_RspBody Body;
    }

    public class OperateMsisdn_unpick_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.InventoryService, ElementName = "OperateMsisdnRspMsg")]
        public OperateMsisdnUnpickRspMsg OperateMsisdnRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.InventoryService, IsNullable = false, ElementName = "OperateMsisdnRspMsg")]
    public class OperateMsisdnUnpickRspMsg : BaseResponseMessage
    {
        public int Result;
    }
}
