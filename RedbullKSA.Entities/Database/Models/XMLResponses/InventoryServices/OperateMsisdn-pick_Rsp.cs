﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.InventoryServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class OperateMsisdn_pick_Rsp
    {
        public OperateMsisdn_pick_RspBody Body;
    }

    public class OperateMsisdn_pick_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.InventoryService, ElementName = "OperateMsisdnRspMsg")]
        public OperateMsisdnPickRspMsg OperateMsisdnRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.InventoryService, IsNullable = false, ElementName = "OperateMsisdnRspMsg")]
    public class OperateMsisdnPickRspMsg : BaseResponseMessage
    {
        public int Result;
    }
}
