﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.InventoryServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QuerySIMStatus_Rsp
    {
        public QuerySIMStatus_RspBody Body;
    }

    public class QuerySIMStatus_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.InventoryService)]
        public QuerySIMStatusRspMsg QuerySIMStatusRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.InventoryService, IsNullable = false)]
    public class QuerySIMStatusRspMsg : BaseResponseMessage
    {
        public bool SIMExists;
        public QuerySIMStatusRspMsgSIMDetails SIMDetails;
    }

    public class QuerySIMStatusRspMsgSIMDetails
    {
        public long ICCID;
        public long IMSI;
        public string ItemCode;
        public long ResDeptId;
        public int PayMode;
        public int TeleType;
        public int SIMStatus;
    }
}
