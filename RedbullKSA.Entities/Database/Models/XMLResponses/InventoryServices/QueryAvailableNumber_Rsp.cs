﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.InventoryServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryAvailableNumber_Rsp
    {
        public QueryAvailableNumber_RspBody Body;
    }
        
    public class QueryAvailableNumber_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.InventoryService)]
        public QueryAvailableNumberRspMsg QueryAvailableNumberRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.InventoryService, IsNullable = false)]
    public class QueryAvailableNumberRspMsg : BaseResponseMessage
    {
        public int ResultCnt;
        [XmlArrayItem("AvailableNumber", IsNullable = false)]
        public QueryAvailableNumberRspMsgAvailableNumber[] AvailableNumberList;
    }

    public class QueryAvailableNumberRspMsgAvailableNumber
    {
        public long ServiceNumber;
        public string ItemCode;
        public long ResDeptId;
        public int PayMode;
        public int TeleType;
        public int Level;
    }
}
