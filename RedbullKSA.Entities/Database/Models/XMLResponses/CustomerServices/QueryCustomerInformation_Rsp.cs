﻿using RedbullKSA.Entities.Database.Models.XMLResponses.Common;
using System.Xml.Serialization;

namespace RedbullKSA.Entities.Database.Models.CustomerServices
{
    [XmlRoot(Namespace = XMLNamespaces.Envelope, IsNullable = false, ElementName = "Envelope")]
    public class QueryCustomerInformation_Rsp
    {
        public QueryCustomerInformation_RspBody Body;
    }

    public class QueryCustomerInformation_RspBody
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.CustomerService)]
        public QueryCustInfoRspMsg QueryCustInfoRspMsg;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.CustomerService, IsNullable = false)]
    public class QueryCustInfoRspMsg : BaseResponseMessage
    {
        public QueryCustInfoRspMsgCustomer Customer;
        public QueryCustInfoRspMsgAccount Account;
    }

    public class QueryCustInfoRspMsgCustomer
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long CustId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int CustLevel;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long DefaultAcctId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int Title;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public Name Name;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int Nationality;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public Certificate Certificate;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int Birthday;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public Address Address;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int Status;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public bool IsLastGdprVersion;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public AdditionalProperty AdditionalProperty;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class Name
    {
        public string LastName;
    }
     
    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class Certificate
    {
        public int IdType;
        public string IdNum;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class Address
    {
        public int AddressClass;
        public int AddressId;
        [XmlIgnore()]
        public bool AddressIdSpecified;
        public int AddressType;
        public int Address1;
        public int Address2;
        public int Address3;
        [XmlIgnore()]
        public bool Address3Specified;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class AdditionalProperty
    {
        public string Code;
        public int Value;
    }

    public class QueryCustInfoRspMsgAccount
    {
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public long AcctId;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public decimal AcctCode;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public string AcctStatus;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int PaymentType;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public Name Name;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int BillCycleType;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int BillLanguage;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public Address Address;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int InitialBalance;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public int AcctPayMethod;
        [XmlElement(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common)]
        public PaymentChannel PaymentChannel;
    }

    [XmlRoot(Namespace = XMLNamespaces.Huawei + XMLNamespacePaths.Common, IsNullable = false)]
    public class PaymentChannel
    {
        public int PaymentId;
        public string AcctType;
    }
}
