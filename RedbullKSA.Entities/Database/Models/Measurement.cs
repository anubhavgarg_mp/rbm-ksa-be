﻿using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.Database.Models
{
    public class Measurement
    {
        public MeasurementType Id { get; set; }

        public string ShortNameEN { get; set; }

        public string ShortNameAR { get; set; }

        public string NameEN { get; set; }

        public string NameAR { get; set; }
    }
}
