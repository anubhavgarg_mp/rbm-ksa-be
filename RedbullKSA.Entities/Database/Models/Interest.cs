﻿using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Models
{
    public class Interest : IEntity, IAdded, IModified
    {
        [Key]
        public int Id { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }

        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public string EnglishName { get; set; }

        public string ArabicName { get; set; }

        public string ImageEN { get; set; }

        public string ImageAR { get; set; }

        public ActiveStatus Status { get; set; }

        public void ExtractDetails(InterestDTO details)
        {
            EnglishName = details.Name.English;
            ArabicName = details.Name.Arabic;
            Status = details.Status;
        }

        public static Interest FromEditModel(InterestDTO details)
        {
            return new Interest()
            {
                EnglishName = details.Name.English,
                ArabicName = details.Name.Arabic,
                Status = details.Status
            };
        }
    }
}
