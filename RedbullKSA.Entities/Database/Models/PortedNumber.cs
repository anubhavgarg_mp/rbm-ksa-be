﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Database.Models
{
    public class PortedNumber : IEntity
    {
        [Key]
        public int Id { get; set; }

        public int ClientId { get; set; }
        public string MSISDN { get; set; }
        public string CurrentOperator { get; set; }
        public DateTime RequestedAt { get; set; }
        public NumberPortingStatus Status { get; set; }
    }
}
