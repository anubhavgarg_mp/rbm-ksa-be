﻿namespace RedbullKSA.Entities.Database.Models
{
    public class GeographyPoint
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public GeographyPoint(decimal lat, decimal lng)
        {
            Latitude = lat;
            Longitude = lng;
        }

        public GeographyPoint() { }
    }
}
