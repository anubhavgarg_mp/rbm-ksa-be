﻿using RedbullKSA.Entities.API.Models.ClientModels.Mobile;
using RedbullKSA.Entities.Database.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RedbullKSA.Entities.Database.Models
{
    public class ClientInterest : IEntity, IAdded, IModified
    {
        [Key]
        public int Id { get; set; }

        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }

        public int ClientId { get; set; }

        public int InterestId { get; set; }

        public ClientInterest() { }

        public static IEnumerable<ClientInterest> FromMobileClientInterests(IEnumerable<ClientInterestMobile> clientInterests, int clientId)
        {
            return clientInterests.Select(x => ClientInterest.FromMobileUserInterest(x, clientId));
        }

        private static ClientInterest FromMobileUserInterest(ClientInterestMobile userInterest, int clientId) =>
            new ClientInterest() { ClientId = clientId, InterestId = userInterest.Id };
    }
}
