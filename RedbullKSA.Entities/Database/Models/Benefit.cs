﻿using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;

namespace RedbullKSA.Entities.Database.Models
{
    public class Benefit : IEntity, IAdded, IModified
    {
        [Key]
        public int Id { get; set; }

        public int? AddedBy { get; set; }
        public DateTime? AddedDate { get; set; }

        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public BenefitType Type { get; set; }
        public BenefitCategory Category { get; set; }
        public NotificationMethod? NotificationType { get; set; }

        public DistributionMethod? DistributionType { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? EventDate { get; set; }

        public BenefitStatus Status { get; set; }

        public int? AvailableQuantity { get; set; }

        public string NameAR { get; set; }
        public string DescriptionTeaserAR { get; set; }
        public string DescriptionAR { get; set; }
        public string SecondaryDescriptionAR { get; set; }

        public string NameEN { get; set; }
        public string DescriptionTeaserEN { get; set; }
        public string DescriptionEN { get; set; }
        public string SecondaryDescriptionEN { get; set; }

        public string LocationAR { get; set; }
        public string LocationEN { get; set; }

        public string SkuCode { get; set; }

        //Voucher
        public bool? IsLimitedToOneUser { get; set; }

        //Segments
        public bool? IsShownToGuests { get; set; }

        //Explore type properties
        public string ButtonUrlEN { get; set; }
        public string ButtonUrlAR { get; set; }
        public string AppleStoreUrlEN { get; set; }
        public string AppleStoreUrlAR { get; set; }
        public string GooglePlayUrlEN { get; set; }
        public string GooglePlayUrlAR { get; set; }

        public bool IsFeatured { get; set; }

        public void UpdateProperties(BenefitWithAttachmentsDetails details)
        {
            Type = details.Type;
            Category = details.Category;
            StartDate = details.StartDate;
            EndDate = details.EndDate;
            EventDate = details.EventDate;
            Status = details.Status;
            AvailableQuantity = details.AvailableQuantity;
            NotificationType = details.NotificationType;

            NameAR = details.BenefitAR.Name;
            NameEN = details.BenefitEN.Name;

            DescriptionTeaserAR = details.BenefitAR.DescriptionTeaser;
            DescriptionTeaserEN = details.BenefitEN.DescriptionTeaser;

            DescriptionAR = details.BenefitAR.Description;
            DescriptionEN = details.BenefitEN.Description;

            SecondaryDescriptionAR = details.BenefitAR.SecondaryDescription;
            SecondaryDescriptionEN = details.BenefitEN.SecondaryDescription;

            LocationAR = details.BenefitAR.Location;
            LocationEN = details.BenefitEN.Location;

            SkuCode = details.SkuCode;
            IsLimitedToOneUser = details.IsLimitedToOneUser;
            IsShownToGuests = details.IsShownToGuests;

            ButtonUrlEN = details.BenefitEN.ButtonUrl;
            ButtonUrlAR = details.BenefitAR.ButtonUrl;

            AppleStoreUrlEN = details.BenefitEN.AppleStoreUrl;
            AppleStoreUrlAR = details.BenefitAR.AppleStoreUrl;

            GooglePlayUrlEN = details.BenefitEN.GooglePlayUrl;
            GooglePlayUrlAR = details.BenefitAR.GooglePlayUrl;

            IsFeatured = details.IsFeatured;
        }

        public static Benefit FromBenefitDetails(BenefitWithAttachmentsDetails details)
        {
            return new Benefit
            {
                Type = details.Type,
                Category = details.Category,
                DistributionType = GetDistributionType(details.Type),
                StartDate = details.StartDate,
                NotificationType = details.NotificationType,
                EndDate = details.EndDate,
                EventDate = details.EventDate,
                Status = details.Status,
                AvailableQuantity = details.AvailableQuantity,

                NameAR = details.BenefitAR.Name,
                NameEN = details.BenefitEN.Name,

                DescriptionTeaserAR = details.BenefitAR.DescriptionTeaser,
                DescriptionTeaserEN = details.BenefitEN.DescriptionTeaser,

                DescriptionAR = details.BenefitAR.Description,
                DescriptionEN = details.BenefitEN.Description,

                SecondaryDescriptionAR = details.BenefitAR.SecondaryDescription,
                SecondaryDescriptionEN = details.BenefitEN.SecondaryDescription,

                LocationAR = details.BenefitAR.Location,
                LocationEN = details.BenefitEN.Location,

                SkuCode = details.SkuCode,
                IsLimitedToOneUser = details.IsLimitedToOneUser,
                IsShownToGuests = details.IsShownToGuests,

                ButtonUrlEN = details.BenefitEN.ButtonUrl,
                ButtonUrlAR = details.BenefitAR.ButtonUrl,

                AppleStoreUrlEN = details.BenefitEN.AppleStoreUrl,
                AppleStoreUrlAR = details.BenefitAR.AppleStoreUrl,

                GooglePlayUrlEN = details.BenefitEN.GooglePlayUrl,
                GooglePlayUrlAR = details.BenefitAR.GooglePlayUrl,

                IsFeatured = details.IsFeatured
            };
        }

        public void ValidateForRedeemBenefit()
        {
            if (Type == BenefitType.Explore) // not mandatory for this type
                return;

            if (StartDate > DateTime.UtcNow || EndDate < DateTime.UtcNow || Status == BenefitStatus.Inactive)
                throw new KSAException(KSAExceptionCode.Benefit.OldBenefitOrInactive);
        }

        public bool IsDifferentUpdateType(BenefitType updateType) => Type != updateType;

        public bool IsRandomSelection()
        {
            if (Type == BenefitType.Voucher || Type == BenefitType.Explore)
                return false;

            if (AvailableQuantity.HasValue)
                return false;

            return true;
        }

        private static DistributionMethod? GetDistributionType(BenefitType type)
        {
            if (type == BenefitType.Experience)
                return Enums.DistributionMethod.MembershipId;

            if (type == BenefitType.Tangible)
                return Enums.DistributionMethod.Post;

            if (type == BenefitType.Voucher)
                return Enums.DistributionMethod.PromoCode;

            if (type == BenefitType.Explore)
                return null;

            throw new ArgumentException("Wrong benefit type passed. ");
        }

    }
}
