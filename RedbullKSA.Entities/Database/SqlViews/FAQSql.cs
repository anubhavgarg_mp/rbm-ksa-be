﻿using RedbullKSA.Entities.API.Models.FAQModels;
using RedbullKSA.Entities.Database.Models;

namespace RedbullKSA.Entities.Database.SqlViews
{
    public class FAQSql
    {
        private const string FAQ_PREFIX = "dbo_faq";
        private const string FAQ_TAG = "dbo_faqTag";

        public static string FAQGridViewSql = $@"
SELECT 
    {FAQ_PREFIX}.{nameof(FAQ.Id)},
    {FAQ_PREFIX}.{nameof(FAQ.QuestionEN)},
    {FAQ_PREFIX}.{nameof(FAQ.QuestionAR)},
    {FAQ_PREFIX}.{nameof(FAQ.AnswerEN)},
    {FAQ_PREFIX}.{nameof(FAQ.AnswerAR)},
    {FAQ_PREFIX}.{nameof(FAQ.TopXIndicator)},
    {FAQ_PREFIX}.{nameof(FAQ.FAQTagId)} AS {nameof(FAQView.FAQTagId)},
    {FAQ_TAG}.{nameof(FAQTag.NameEN)} AS {nameof(FAQView.FAQTagNameEN)},
    {FAQ_TAG}.{nameof(FAQTag.NameAR)} AS {nameof(FAQView.FAQTagNameAR)}
FROM
    [{nameof(FAQ)}] {FAQ_PREFIX}
    LEFT JOIN [{nameof(FAQTag)}] {FAQ_TAG}
    ON {FAQ_PREFIX}.{nameof(FAQ.FAQTagId)} = {FAQ_TAG}.{nameof(FAQTag.Id)}
";
    }
}
