﻿using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.Database.Models;

namespace RedbullKSA.Entities.Database.SqlViews
{
    public static class BenefitTagSql
    {
        private const string BENEFIT_TAG = "dbo_benefit_tag";
        private const string TAG = "dbo_tag";

        public static string BenefitTagViewSql = $@"
SELECT
    {BENEFIT_TAG}.{nameof(BenefitTag.Id)} AS {nameof(BenefitTagView.Id)},
    {BENEFIT_TAG}.{nameof(BenefitTag.TagId)} AS {nameof(BenefitTagView.TagId)},
    {BENEFIT_TAG}.{nameof(BenefitTag.BenefitId)} AS {nameof(BenefitTagView.BenefitId)},
    {TAG}.{nameof(Tag.NameEN)} AS {nameof(BenefitTagView.TagNameEN)},
    {TAG}.{nameof(Tag.NameAR)} AS {nameof(BenefitTagView.TagNameAR)},
    {TAG}.{nameof(Tag.OrderPosition)} AS {nameof(BenefitTagView.OrderPosition)}
FROM
    {nameof(BenefitTag)} {BENEFIT_TAG}
LEFT JOIN [{nameof(Tag)}] {TAG} ON {TAG}.{nameof(Tag.Id)} = {BENEFIT_TAG}.{nameof(BenefitTag.TagId)};";
    }
}
