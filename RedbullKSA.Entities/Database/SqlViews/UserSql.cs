﻿using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.Database.Models;

namespace RedbullKSA.Entities.Database.SqlViews
{
    public static class UserSql
    {
        private const string USER = "dbo_user";

        public static string AdministratorGridViewSql = $@"
SELECT
    {USER}.{nameof(User.Id)},
    {USER}.{nameof(User.PhoneNumber)},
    {USER}.{nameof(User.Email)},
    CONCAT({USER}.{nameof(User.FirstName)} + ' ', {USER}.{nameof(User.LastName)}) AS {nameof(AdministratorGridView.FullName)},
    {USER}.{nameof(User.Status)},
    {USER}.{nameof(User.IsApiUser)}
FROM
    [{nameof(User)}] {USER}
";
    }
}
