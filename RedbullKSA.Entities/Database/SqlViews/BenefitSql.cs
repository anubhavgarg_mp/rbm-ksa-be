﻿using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.Models.BenefitModels.Email;
using RedbullKSA.Entities.API.Models.BenefitModels.Mobile;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.Database.SqlViews
{
    public class BenefitSql
    {
        private const string BENEFIT = "benefit";
        private const string SESSION_TOKEN = "session_token";
        private const string ATTACHMENT = "attachment";
        private const string REDEEMED = "redeemed_benefit";
        private const string REDEEMED_2 = "redeemed_benefit_2";
        private const string CLIENT = "client";
        private const string THUMBNAIL = "thumbnail";
        private const string BENEFIT_TAG = "benefit_tag";
        private const string TAG = "tag";
        private const string MSISDN = "clientMsisdn";

        public static string BenefitGridViewSql = $@"
SELECT
    {BENEFIT}.{nameof(Benefit.Id)} AS {nameof(BenefitGridView.Id)},
    {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(BenefitGridView.Type)},
    {BENEFIT}.{nameof(Benefit.Category)} AS {nameof(BenefitGridView.Category)},
    {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitGridView.Name)},
    {BENEFIT}.{nameof(Benefit.StartDate)} AS {nameof(BenefitGridView.StartDate)},
    {BENEFIT}.{nameof(Benefit.EndDate)} AS {nameof(BenefitGridView.EndDate)},
    {REDEEMED}.{nameof(BenefitGridView.AvailableQuantity)} AS {nameof(BenefitGridView.AvailableQuantity)},
    {BENEFIT}.{nameof(Benefit.DistributionType)} AS {nameof(BenefitGridView.DistributionType)},
    {BENEFIT}.{nameof(Benefit.Status)} AS {nameof(BenefitGridView.Status)},
    {BENEFIT}.{nameof(Benefit.IsFeatured)} AS {nameof(BenefitGridView.IsFeatured)},
    STRING_AGG({TAG}.{nameof(Tag.NameEN)}, ', ') AS {nameof(BenefitGridView.EnglishTagNames)}
FROM
    [{nameof(Benefit)}] {BENEFIT}
LEFT JOIN (SELECT COUNT({nameof(RedeemedBenefit.Id)}) AS {nameof(BenefitGridView.AvailableQuantity)}, {nameof(RedeemedBenefit.BenefitId)}
    FROM [{nameof(RedeemedBenefit)}]
    WHERE {nameof(RedeemedBenefit.Status)} = {(int)RedeemedBenefitType.Available}
    GROUP BY {nameof(RedeemedBenefit.BenefitId)}
) {REDEEMED}
  ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
LEFT JOIN [{nameof(BenefitTag)}] {BENEFIT_TAG}
    ON {BENEFIT_TAG}.{nameof(BenefitTag.BenefitId)} = {BENEFIT}.{nameof(Benefit.Id)}
LEFT JOIN [{nameof(Tag)}] {TAG}
    ON {TAG}.{nameof(Tag.Id)} = {BENEFIT_TAG}.{nameof(BenefitTag.TagId)}
GROUP BY
    {BENEFIT}.{nameof(Benefit.Id)},
    {BENEFIT}.{nameof(Benefit.Type)},
    {BENEFIT}.{nameof(Benefit.Category)},
    {BENEFIT}.{nameof(Benefit.NameEN)},
    {BENEFIT}.{nameof(Benefit.StartDate)},
    {BENEFIT}.{nameof(Benefit.EndDate)},
    {REDEEMED}.{nameof(BenefitGridView.AvailableQuantity)},
    {BENEFIT}.{nameof(Benefit.DistributionType)},
    {BENEFIT}.{nameof(Benefit.Status)},
    {BENEFIT}.{nameof(Benefit.IsFeatured)}
    
";
        
        public static string BenefitDetailsSql = $@"
SELECT
    {BENEFIT}.{nameof(Benefit.Id)} AS {nameof(BenefitDetailsView.Id)},
    {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(BenefitDetailsView.Type)},
    {BENEFIT}.{nameof(Benefit.Category)} AS {nameof(BenefitDetailsView.Category)},
    {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitDetailsView.NameEN)},
    {BENEFIT}.{nameof(Benefit.NameAR)} AS {nameof(BenefitDetailsView.NameAR)},
    {BENEFIT}.{nameof(Benefit.DescriptionTeaserEN)} AS {nameof(BenefitDetailsView.DescriptionTeaserEN)},
    {BENEFIT}.{nameof(Benefit.DescriptionTeaserAR)} AS {nameof(BenefitDetailsView.DescriptionTeaserAR)},
    {BENEFIT}.{nameof(Benefit.DescriptionEN)} AS {nameof(BenefitDetailsView.DescriptionEN)},
    {BENEFIT}.{nameof(Benefit.DescriptionAR)} AS {nameof(BenefitDetailsView.DescriptionAR)},
    {BENEFIT}.{nameof(Benefit.SecondaryDescriptionEN)} AS {nameof(BenefitDetailsView.SecondaryDescriptionEN)},
    {BENEFIT}.{nameof(Benefit.SecondaryDescriptionAR)} AS {nameof(BenefitDetailsView.SecondaryDescriptionAR)},
    {BENEFIT}.{nameof(Benefit.LocationEN)} AS {nameof(BenefitDetailsView.LocationEN)},
    {BENEFIT}.{nameof(Benefit.LocationAR)} AS {nameof(BenefitDetailsView.LocationAR)},
    {BENEFIT}.{nameof(Benefit.StartDate)} AS {nameof(BenefitDetailsView.StartDate)},
    {BENEFIT}.{nameof(Benefit.EndDate)} AS {nameof(BenefitDetailsView.EndDate)},
    {BENEFIT}.{nameof(Benefit.EventDate)} AS {nameof(BenefitDetailsView.EventDate)},
    {BENEFIT}.{nameof(Benefit.AvailableQuantity)} AS {nameof(BenefitDetailsView.AvailableQuantity)},
    {BENEFIT}.{nameof(Benefit.DistributionType)} AS {nameof(BenefitDetailsView.DistributionType)},
    {BENEFIT}.{nameof(Benefit.Status)} AS {nameof(BenefitDetailsView.Status)},
    {BENEFIT}.{nameof(Benefit.SkuCode)} AS {nameof(BenefitDetailsView.SkuCode)},
    {BENEFIT}.{nameof(Benefit.IsLimitedToOneUser)} AS {nameof(BenefitDetailsView.IsLimitedToOneUser)},
    {BENEFIT}.{nameof(Benefit.IsShownToGuests)} AS {nameof(BenefitDetailsView.IsShownToGuests)},
    {BENEFIT}.{nameof(Benefit.ButtonUrlEN)} AS {nameof(BenefitDetailsView.ButtonUrlEN)},
    {BENEFIT}.{nameof(Benefit.ButtonUrlAR)} AS {nameof(BenefitDetailsView.ButtonUrlAR)},
    {BENEFIT}.{nameof(Benefit.AppleStoreUrlEN)} AS {nameof(BenefitDetailsView.AppleStoreUrlEN)},
    {BENEFIT}.{nameof(Benefit.AppleStoreUrlAR)} AS {nameof(BenefitDetailsView.AppleStoreUrlAR)},
    {BENEFIT}.{nameof(Benefit.GooglePlayUrlEN)} AS {nameof(BenefitDetailsView.GooglePlayUrlEN)},
    {BENEFIT}.{nameof(Benefit.GooglePlayUrlAR)} AS {nameof(BenefitDetailsView.GooglePlayUrlAR)},
    {BENEFIT}.{nameof(Benefit.NotificationType)} AS {nameof(BenefitDetailsView.NotificationType)},
    {BENEFIT}.{nameof(Benefit.IsFeatured)} AS {nameof(BenefitDetailsView.IsFeatured)}
    
FROM
    [{nameof(Benefit)}] {BENEFIT}
;
";

        public static string RedeemedBenefitGridViewSql = $@"
SELECT
    {REDEEMED}.{nameof(RedeemedBenefit.Id)} AS {nameof(BenefitStatisticGrid.Id)},
    {REDEEMED}.{nameof(RedeemedBenefit.AddedDate)} AS {nameof(BenefitStatisticGrid.AddedDate)},
    {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(BenefitStatisticGrid.Type)},
    {BENEFIT}.{nameof(Benefit.Category)} AS {nameof(BenefitStatisticGrid.Category)},
    {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitStatisticGrid.BenefitName)},
    {CLIENT}.{nameof(Client.Id)} AS {nameof(BenefitStatisticGrid.ClientId)},
    {BENEFIT}.{nameof(Client.Id)} AS {nameof(BenefitStatisticGrid.BenefitId)},
    {CLIENT}.{nameof(BenefitStatisticGrid.ClientFullName)},
    {BENEFIT}.{nameof(Benefit.SkuCode)} AS {nameof(BenefitStatisticGrid.SkuCode)},
    {REDEEMED}.{nameof(RedeemedBenefit.PromoCode)} AS {nameof(BenefitStatisticGrid.VoucherCode)},
    {BENEFIT}.{nameof(Benefit.DistributionType)} AS {nameof(BenefitStatisticGrid.DistributionType)},
    {nameof(BenefitStatisticGrid.IsLottery)} =
    CASE 
        WHEN {BENEFIT}.{nameof(Benefit.AvailableQuantity)} IS NOT NULL THEN 1
        ELSE 0
    END
FROM
    [{nameof(RedeemedBenefit)}] {REDEEMED}
LEFT JOIN 
(SELECT {nameof(Benefit.Id)}, {nameof(Benefit.NameEN)}, {nameof(Benefit.SkuCode)}, {nameof(Benefit.Category)}, {nameof(Benefit.Type)}, {nameof(Benefit.DistributionType)}, {nameof(Benefit.AvailableQuantity)} 
FROM [{nameof(Benefit)}]) {BENEFIT}
    ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
LEFT JOIN (SELECT {nameof(Client.Id)}, CONCAT({nameof(Client.FirstName)}, ' ', {nameof(Client.LastName)}) AS {nameof(BenefitStatisticGrid.ClientFullName)}
FROM [{nameof(Client)}]) {CLIENT}
    ON {CLIENT}.{nameof(Client.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.ClientId)}
WHERE {REDEEMED}.{nameof(RedeemedBenefit.Status)} = {(int)RedeemedBenefitType.Participant}
OR {REDEEMED}.{nameof(RedeemedBenefit.Status)} = {(int)RedeemedBenefitType.Winner}
";


        public static string RedeemedBenefitDetailsSql = $@"
SELECT
    {REDEEMED}.{nameof(RedeemedBenefit.Id)} AS {nameof(BenefitRedeemedDetails.Id)},
    {REDEEMED}.{nameof(RedeemedBenefit.AddedDate)} AS {nameof(BenefitRedeemedDetails.AddedDate)},
    {REDEEMED}.{nameof(RedeemedBenefit.DeliveryAddress)} AS {nameof(BenefitRedeemedDetails.DeliveryAddress)},
    {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(BenefitRedeemedDetails.Type)},
    {BENEFIT}.{nameof(Benefit.Category)} AS {nameof(BenefitRedeemedDetails.Category)},
    {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitRedeemedDetails.BenefitName)},
    {CLIENT}.{nameof(BenefitRedeemedDetails.ClientFullName)},
    {CLIENT}.{nameof(ClientMsisdn.PhoneNumber)} AS {nameof(BenefitRedeemedDetails.PhoneNumber)},
    {CLIENT}.{nameof(Client.Email)} AS {nameof(BenefitRedeemedDetails.Email)},
    {BENEFIT}.{nameof(Benefit.SkuCode)} AS {nameof(BenefitRedeemedDetails.SkuCode)},
    {REDEEMED}.{nameof(RedeemedBenefit.PromoCode)} AS {nameof(BenefitRedeemedDetails.VoucherCode)},
    {BENEFIT}.{nameof(Benefit.DistributionType)} AS {nameof(BenefitRedeemedDetails.DistributionType)}
FROM
    [{nameof(RedeemedBenefit)}] {REDEEMED}
LEFT JOIN
(SELECT {nameof(Benefit.Id)}, {nameof(Benefit.NameEN)}, {nameof(Benefit.SkuCode)}, {nameof(Benefit.Category)}, {nameof(Benefit.Type)}, {nameof(Benefit.DistributionType)}, {nameof(Benefit.AvailableQuantity)} 
FROM [{nameof(Benefit)}]) {BENEFIT}
    ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
LEFT JOIN (SELECT {nameof(Client)}.{nameof(Client.Id)}, CONCAT({nameof(Client.FirstName)}, ' ', {nameof(Client.LastName)}) AS {nameof(BenefitRedeemedDetails.ClientFullName)}, {MSISDN}.{nameof(ClientMsisdn.PhoneNumber)}, {nameof(Client.Email)}
FROM [{nameof(Client)}]
LEFT JOIN [{nameof(ClientMsisdn)}] {MSISDN}
    ON {nameof(Client)}.{nameof(Client.Id)} = {MSISDN}.{nameof(ClientMsisdn.ClientId)}) {CLIENT}
    ON {CLIENT}.{nameof(Client.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.ClientId)}
";

        public static string BenefitOfferSqlFunction(string functionName) => $@"
IF Object_ID('dbo.{functionName}', N'IF') IS NOT NULL
   DROP FUNCTION {functionName}

GO

CREATE FUNCTION {functionName} (@language int, @clientId int)
RETURNS TABLE  
AS  
RETURN  
(   
    SELECT
    {BENEFIT}.{nameof(Benefit.Id)} AS {nameof(BenefitOfferMobileView.Id)},
    {BENEFIT}.{nameof(Benefit.EndDate)} AS {nameof(BenefitOfferMobileView.EndDate)},
    {BENEFIT}.{nameof(Benefit.AppleStoreUrlEN)} AS {nameof(BenefitOfferMobileView.AppStoreUrlEN)},
    {BENEFIT}.{nameof(Benefit.AppleStoreUrlAR)} AS {nameof(BenefitOfferMobileView.AppStoreUrlAR)},
    {BENEFIT}.{nameof(Benefit.GooglePlayUrlEN)} AS {nameof(BenefitOfferMobileView.GooglePlayUrlEN)},
    {BENEFIT}.{nameof(Benefit.GooglePlayUrlAR)} AS {nameof(BenefitOfferMobileView.GooglePlayUrlAR)},
    {BENEFIT}.{nameof(Benefit.ButtonUrlEN)} AS {nameof(BenefitOfferMobileView.BrowserUrlEN)},
    {BENEFIT}.{nameof(Benefit.ButtonUrlAR)} AS {nameof(BenefitOfferMobileView.BrowserUrlAR)},
    {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(BenefitOfferMobileView.Type)},
    {BENEFIT}.{nameof(Benefit.NameAR)} AS {nameof(BenefitOfferMobileView.NameAR)},
    {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitOfferMobileView.NameEN)},
    {BENEFIT}.{nameof(Benefit.DescriptionTeaserAR)} AS {nameof(BenefitOfferMobileView.DescriptionTeaserAR)},
    {BENEFIT}.{nameof(Benefit.DescriptionTeaserEN)} AS {nameof(BenefitOfferMobileView.DescriptionTeaserEN)},
    {BENEFIT}.{nameof(Benefit.DescriptionAR)} AS {nameof(BenefitOfferMobileView.DescriptionAR)},
    {BENEFIT}.{nameof(Benefit.DescriptionEN)} AS {nameof(BenefitOfferMobileView.DescriptionEN)},
    {BENEFIT}.{nameof(Benefit.Category)} as {nameof(BenefitOfferMobileView.Category)},
    {BENEFIT}.{nameof(Benefit.IsShownToGuests)} AS {nameof(BenefitOfferMobileView.IsShownToGuests)},
    {BENEFIT}.{nameof(Benefit.IsFeatured)} AS {nameof(BenefitOfferMobileView.IsFeatured)},
    {REDEEMED}.{nameof(BenefitOfferMobileView.QuantityRemaining)},
        CASE 
            WHEN {ATTACHMENT}.{nameof(Attachment.UploadedFileType)} = {(int)AttachmentFileType.Video}
               THEN {ATTACHMENT}.{nameof(Attachment.FileUrl)} 
            ELSE NULL
        END AS {nameof(BenefitOfferMobileView.TeaserVideoUrl)},
        CASE 
            WHEN {ATTACHMENT}.{nameof(Attachment.UploadedFileType)} = {(int)AttachmentFileType.Image}
                THEN {ATTACHMENT}.{nameof(Attachment.FileUrl)}
            WHEN {ATTACHMENT}.{nameof(Attachment.UploadedFileType)} = {(int)AttachmentFileType.Video}
                THEN {THUMBNAIL}.{nameof(Attachment.FileUrl)} 
            ELSE NULL 
        END AS {nameof(BenefitOfferMobileView.TeaserImage)}
    FROM
        [{nameof(Benefit)}] {BENEFIT}
    LEFT JOIN (SELECT COUNT({nameof(RedeemedBenefit.Id)}) AS {nameof(BenefitOfferMobileView.QuantityRemaining)}, {nameof(RedeemedBenefit.BenefitId)}
        FROM [{nameof(RedeemedBenefit)}]
        WHERE {nameof(RedeemedBenefit.Status)} = {(int)RedeemedBenefitType.Available}
        GROUP BY {nameof(RedeemedBenefit.BenefitId)}
    ) {REDEEMED}
        ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
    LEFT JOIN (SELECT {nameof(Attachment.FileUrl)}, {nameof(Attachment.FileLanguage)}, {nameof(Attachment.UploadedFileType)}, {nameof(Attachment.BenefitId)}
        FROM [{nameof(Attachment)}]
        WHERE ({nameof(Attachment.Category)} = {(int)AttachmentCategory.BenefitTeaser } OR {nameof(Attachment.Category)} = {(int)AttachmentCategory.BenefitTeaserVideo }) 
            AND {nameof(Attachment.FileLanguage)} = @language
    ) {ATTACHMENT}
        ON {ATTACHMENT}.{nameof(Attachment.BenefitId)} = {BENEFIT}.{nameof(Benefit.Id)}
    LEFT JOIN (SELECT {nameof(Attachment.FileUrl)}, {nameof(Attachment.FileLanguage)}, {nameof(Attachment.UploadedFileType)}, {nameof(Attachment.BenefitId)}
        FROM [{nameof(Attachment)}]
        WHERE {nameof(Attachment.Category)} = {(int)AttachmentCategory.BenefitTeaserVideoThumbnail} 
            AND {nameof(Attachment.FileLanguage)} = @language
    ) {THUMBNAIL}
        ON {THUMBNAIL}.{nameof(Attachment.BenefitId)} = {BENEFIT}.{nameof(Benefit.Id)}
    WHERE {BENEFIT}.{nameof(Benefit.Status)} = {(int)BenefitStatus.Active}
);
";

        public static string BenefitGuestOfferSqlFunction(string functionName) => $@"
        IF Object_ID('dbo.{functionName}', N'IF') IS NOT NULL
           DROP FUNCTION {functionName}

        GO

        CREATE FUNCTION {functionName} (@language int)
        RETURNS TABLE  
        AS  
        RETURN  
        (   
            SELECT
            {BENEFIT}.{nameof(Benefit.Id)} AS {nameof(BenefitGuestOfferMobileView.Id)},
            {BENEFIT}.{nameof(Benefit.EndDate)} AS {nameof(BenefitGuestOfferMobileView.EndDate)},
            {BENEFIT}.{nameof(Benefit.AppleStoreUrlEN)} AS {nameof(BenefitGuestOfferMobileView.AppStoreUrlEN)},
            {BENEFIT}.{nameof(Benefit.AppleStoreUrlAR)} AS {nameof(BenefitGuestOfferMobileView.AppStoreUrlAR)},
            {BENEFIT}.{nameof(Benefit.GooglePlayUrlEN)} AS {nameof(BenefitGuestOfferMobileView.GooglePlayUrlEN)},
            {BENEFIT}.{nameof(Benefit.GooglePlayUrlAR)} AS {nameof(BenefitGuestOfferMobileView.GooglePlayUrlAR)},
            {BENEFIT}.{nameof(Benefit.ButtonUrlEN)} AS {nameof(BenefitGuestOfferMobileView.BrowserUrlEN)},
            {BENEFIT}.{nameof(Benefit.ButtonUrlAR)} AS {nameof(BenefitGuestOfferMobileView.BrowserUrlAR)},
            {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(BenefitGuestOfferMobileView.Type)},
            {BENEFIT}.{nameof(Benefit.NameAR)} AS {nameof(BenefitGuestOfferMobileView.NameAR)},
            {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitGuestOfferMobileView.NameEN)},
            {BENEFIT}.{nameof(Benefit.DescriptionTeaserAR)} AS {nameof(BenefitGuestOfferMobileView.DescriptionTeaserAR)},
            {BENEFIT}.{nameof(Benefit.DescriptionTeaserEN)} AS {nameof(BenefitGuestOfferMobileView.DescriptionTeaserEN)},
            {BENEFIT}.{nameof(Benefit.DescriptionAR)} AS {nameof(BenefitGuestOfferMobileView.DescriptionAR)},
            {BENEFIT}.{nameof(Benefit.DescriptionEN)} AS {nameof(BenefitGuestOfferMobileView.DescriptionEN)},
            {BENEFIT}.{nameof(Benefit.Category)} AS {nameof(BenefitGuestOfferMobileView.Category)},
            {BENEFIT}.{nameof(Benefit.IsShownToGuests)} AS {nameof(BenefitGuestOfferMobileView.IsShownToGuests)},
            {BENEFIT}.{nameof(Benefit.IsFeatured)} AS {nameof(BenefitGuestOfferMobileView.IsFeatured)},
            {REDEEMED}.{nameof(BenefitGuestOfferMobileView.QuantityRemaining)},
                CASE 
                    WHEN {ATTACHMENT}.{nameof(Attachment.UploadedFileType)} = {(int)AttachmentFileType.Video}
                       THEN {ATTACHMENT}.{nameof(Attachment.FileUrl)} 
                       ELSE NULL 
                END AS {nameof(BenefitGuestOfferMobileView.TeaserVideoUrl)},
                CASE 
                    WHEN {ATTACHMENT}.{nameof(Attachment.UploadedFileType)} = {(int)AttachmentFileType.Image}
                        THEN {ATTACHMENT}.{nameof(Attachment.FileUrl)}
                    WHEN {ATTACHMENT}.{nameof(Attachment.UploadedFileType)} = {(int)AttachmentFileType.Video}
                        THEN {THUMBNAIL}.{nameof(Attachment.FileUrl)} 
                    ELSE NULL 
                END AS {nameof(BenefitGuestOfferMobileView.TeaserImage)}
            FROM
                [{nameof(Benefit)}] {BENEFIT}
            LEFT JOIN (SELECT COUNT({nameof(RedeemedBenefit.Id)}) AS {nameof(BenefitGuestOfferMobileView.QuantityRemaining)}, {nameof(RedeemedBenefit.BenefitId)}
                FROM [{nameof(RedeemedBenefit)}]
                WHERE {nameof(RedeemedBenefit.Status)} = {(int)RedeemedBenefitType.Available}
                GROUP BY {nameof(RedeemedBenefit.BenefitId)}
            ) {REDEEMED}
                ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
            LEFT JOIN (SELECT {nameof(Attachment.FileUrl)}, {nameof(Attachment.FileLanguage)}, {nameof(Attachment.UploadedFileType)}, {nameof(Attachment.BenefitId)}
                FROM [{nameof(Attachment)}]
                WHERE ({nameof(Attachment.Category)} = {(int)AttachmentCategory.BenefitTeaser } OR {nameof(Attachment.Category)} = {(int)AttachmentCategory.BenefitTeaserVideo }) 
                    AND {nameof(Attachment.FileLanguage)} = @language
            ) {ATTACHMENT}
                ON {ATTACHMENT}.{nameof(Attachment.BenefitId)} = {BENEFIT}.{nameof(Benefit.Id)}
            LEFT JOIN (SELECT {nameof(Attachment.FileUrl)}, {nameof(Attachment.FileLanguage)}, {nameof(Attachment.UploadedFileType)}, {nameof(Attachment.BenefitId)}
                FROM [{nameof(Attachment)}]
                WHERE {nameof(Attachment.Category)} = {(int)AttachmentCategory.BenefitTeaserVideoThumbnail} 
                    AND {nameof(Attachment.FileLanguage)} = @language
            ) {THUMBNAIL}
                ON {THUMBNAIL}.{nameof(Attachment.BenefitId)} = {BENEFIT}.{nameof(Benefit.Id)}
            WHERE {BENEFIT}.{nameof(Benefit.Status)} = {(int)BenefitStatus.Active}
            AND {BENEFIT}.{nameof(Benefit.IsShownToGuests)} = 1
        );
        ";

        // Title is EN by default
        public static string RedeemedBenefitHistoryMobileSql = $@"
SELECT
    {REDEEMED}.{nameof(RedeemedBenefit.Id)} AS {nameof(RedeemedBenefitRecordMobile.Id)},
    {REDEEMED}.{nameof(RedeemedBenefit.AddedDate)} AS {nameof(RedeemedBenefitRecordMobile.RedeemedDate)},
    {REDEEMED}.{nameof(RedeemedBenefit.PromoCode)} AS {nameof(RedeemedBenefitRecordMobile.PromoCode)},
    {REDEEMED}.{nameof(RedeemedBenefit.ClientId)} AS {nameof(RedeemedBenefitRecordMobile.ClientId)},
    {REDEEMED}.{nameof(RedeemedBenefit.DeliveryAddress)} AS {nameof(RedeemedBenefitRecordMobile.DeliveryAddress)},
    {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(RedeemedBenefitRecordMobile.Title)},
    {BENEFIT}.{nameof(Benefit.NameAR)} AS {nameof(RedeemedBenefitRecordMobile.NameAR)},
    {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(RedeemedBenefitRecordMobile.NameEN)},
    {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(RedeemedBenefitRecordMobile.Type)},
    {CLIENT}.{nameof(Client.Email)} AS {nameof(RedeemedBenefitRecordMobile.RedeemedByEmail)},
    {CLIENT}.{nameof(RedeemedBenefitRecordMobile.RedeemedBy)}
FROM
    [{nameof(RedeemedBenefit)}] {REDEEMED}
LEFT JOIN [{nameof(Benefit)}] {BENEFIT} ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
LEFT JOIN (SELECT {nameof(Client.Id)}, 
CONCAT ({nameof(Client.FirstName)}, ' ', {nameof(Client.LastName)}) AS {nameof(RedeemedBenefitRecordMobile.RedeemedBy)},
{nameof(Client.Email)}
FROM [{nameof(Client)}]) {CLIENT} ON {CLIENT}.{nameof(Client.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.ClientId)}
";

        public static string RedeemedBenefitCounts = $@"
SELECT {REDEEMED}.{nameof(RedeemedBenefit.Status)} AS {nameof(RedeemedBenefitStatusView.Status)},
    COUNT({REDEEMED}.{nameof(RedeemedBenefit.Id)}) AS {nameof(RedeemedBenefitStatusView.Count)},
    {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)} AS {nameof(RedeemedBenefitStatusView.BenefitId)}
FROM [{nameof(RedeemedBenefit)}] {REDEEMED}
GROUP BY
{REDEEMED}.{nameof(RedeemedBenefit.Status)},
{REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
;
";

        public static string BenefitDetailsMobileSql => $@"
    SELECT
    {BENEFIT}.{nameof(Benefit.Id)} AS {nameof(BenefitDetailsMobileView.Id)},
    {BENEFIT}.{nameof(Benefit.EndDate)} AS {nameof(BenefitDetailsMobileView.EndDate)},
    {BENEFIT}.{nameof(Benefit.AppleStoreUrlEN)} AS {nameof(BenefitDetailsMobileView.AppStoreUrlEN)},
    {BENEFIT}.{nameof(Benefit.AppleStoreUrlAR)} AS {nameof(BenefitDetailsMobileView.AppStoreUrlAR)},
    {BENEFIT}.{nameof(Benefit.GooglePlayUrlEN)} AS {nameof(BenefitDetailsMobileView.GooglePlayUrlEN)},
    {BENEFIT}.{nameof(Benefit.GooglePlayUrlAR)} AS {nameof(BenefitDetailsMobileView.GooglePlayUrlAR)},
    {BENEFIT}.{nameof(Benefit.ButtonUrlEN)} AS {nameof(BenefitDetailsMobileView.ButtonUrlEN)},
    {BENEFIT}.{nameof(Benefit.ButtonUrlAR)} AS {nameof(BenefitDetailsMobileView.ButtonUrlAR)},
    {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(BenefitDetailsMobileView.Type)},
    {BENEFIT}.{nameof(Benefit.NameAR)} AS {nameof(BenefitDetailsMobileView.NameAR)},
    {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitDetailsMobileView.NameEN)},
    {BENEFIT}.{nameof(Benefit.DescriptionTeaserAR)} AS {nameof(BenefitDetailsMobileView.DescriptionTeaserAR)},
    {BENEFIT}.{nameof(Benefit.DescriptionTeaserEN)} AS {nameof(BenefitDetailsMobileView.DescriptionTeaserEN)},
    {BENEFIT}.{nameof(Benefit.DescriptionAR)} AS {nameof(BenefitDetailsMobileView.DescriptionAR)},
    {BENEFIT}.{nameof(Benefit.DescriptionEN)} AS {nameof(BenefitDetailsMobileView.DescriptionEN)},
    {BENEFIT}.{nameof(Benefit.SecondaryDescriptionAR)} AS {nameof(BenefitDetailsMobileView.SecondaryDescriptionAR)},
    {BENEFIT}.{nameof(Benefit.SecondaryDescriptionEN)} AS {nameof(BenefitDetailsMobileView.SecondaryDescriptionEN)},
    {BENEFIT}.{nameof(Benefit.SkuCode)} AS {nameof(BenefitDetailsMobileView.SkuCode)},
    {BENEFIT}.{nameof(Benefit.LocationEN)} AS {nameof(BenefitDetailsMobileView.LocationEN)},
    {BENEFIT}.{nameof(Benefit.LocationAR)} AS {nameof(BenefitDetailsMobileView.LocationAR)},
    {BENEFIT}.{nameof(Benefit.StartDate)} AS {nameof(BenefitDetailsMobileView.StartDate)},
    {BENEFIT}.{nameof(Benefit.EventDate)} AS {nameof(BenefitDetailsMobileView.EventDate)},
    
    {REDEEMED}.{nameof(BenefitDetailsMobileView.QuantityRemaining)}
FROM
    [{nameof(Benefit)}] {BENEFIT}
LEFT JOIN (SELECT COUNT({nameof(RedeemedBenefit.Id)}) AS {nameof(BenefitDetailsMobileView.QuantityRemaining)}, {nameof(RedeemedBenefit.BenefitId)}
    FROM [{nameof(RedeemedBenefit)}]
    WHERE {nameof(RedeemedBenefit.Status)} = {(int)RedeemedBenefitType.Available}
    GROUP BY {nameof(RedeemedBenefit.BenefitId)}
) {REDEEMED}
    ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
    WHERE {BENEFIT}.{nameof(Benefit.Status)} = {(int)BenefitStatus.Active}
;
";

        public static string EligibleBenefitViewSql = $@"
        SELECT DISTINCT
            {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)} AS {nameof(BenefitEligibleSelectView.Id)},
            {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitEligibleSelectView.BenefitName)},
            {REDEEMED_2}.{nameof(RedeemedBenefit.ClientId)} AS {nameof(BenefitEligibleSelectView.RegisteredUsers)},
            {BENEFIT}.{nameof(Benefit.Status)} AS {nameof(BenefitEligibleSelectView.Status)}
        FROM
            [{nameof(RedeemedBenefit)}] {REDEEMED}
        LEFT JOIN 
            (SELECT {BENEFIT}.{nameof(Benefit.Id)}, {BENEFIT}.{nameof(Benefit.NameEN)}, {BENEFIT}.{nameof(Benefit.Status)} 
            FROM [{nameof(Benefit)}]) AS {BENEFIT} 
        ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)} 
        LEFT JOIN
            (SELECT {nameof(RedeemedBenefit.BenefitId)} AS {nameof(RedeemedBenefit.Id)}, COUNT({nameof(RedeemedBenefit.ClientId)}) {nameof(RedeemedBenefit.ClientId)}
            FROM [{nameof(RedeemedBenefit)}] {REDEEMED}
            GROUP BY {nameof(RedeemedBenefit.BenefitId)}) AS {REDEEMED_2}
        ON {REDEEMED_2}.{nameof(RedeemedBenefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
        ";

        public static string RedeemedBenefitEmailViewSql = $@"
        SELECT
            {REDEEMED}.{nameof(RedeemedBenefit.Id)} AS {nameof(RedeemedBenefitEmailView.RedeemedBenefitId)},
            {BENEFIT}.{nameof(Benefit.Id)} AS {nameof(RedeemedBenefitEmailView.BenefitId)},
            {REDEEMED}.{nameof(RedeemedBenefit.ClientId)} AS {nameof(RedeemedBenefitEmailView.ClientId)},
            {BENEFIT}.{nameof(Benefit.Type)} AS {nameof(RedeemedBenefitEmailView.Type)},
            {REDEEMED}.{nameof(RedeemedBenefit.Status)} AS {nameof(RedeemedBenefitEmailView.Status)},
            {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(RedeemedBenefitEmailView.BenefitNameEN)},
            {BENEFIT}.{nameof(Benefit.NameAR)} AS {nameof(RedeemedBenefitEmailView.BenefitNameAR)},
            {BENEFIT}.{nameof(Benefit.LocationEN)} AS {nameof(RedeemedBenefitEmailView.LocationEN)},
            {BENEFIT}.{nameof(Benefit.LocationAR)} AS {nameof(RedeemedBenefitEmailView.LocationAR)},
            {CLIENT}.{nameof(Client.FirstName)} AS {nameof(RedeemedBenefitEmailView.ClientFirstName)},
            {CLIENT}.{nameof(Client.Language)} AS {nameof(RedeemedBenefitEmailView.ClientLanguage)},
            {CLIENT}.{nameof(Client.Email)} AS {nameof(RedeemedBenefitEmailView.ClientEmail)},
            
            {ATTACHMENT}.{nameof(Attachment.FileUrl)} AS {nameof(RedeemedBenefitEmailView.QrCodeUrl)},
            {REDEEMED}.{nameof(RedeemedBenefit.DeliveryAddress)} AS {nameof(RedeemedBenefitEmailView.DeliveryAddress)},
            {REDEEMED}.{nameof(RedeemedBenefit.PromoCode)} AS {nameof(RedeemedBenefitEmailView.PromoCode)}
        FROM
            [{nameof(RedeemedBenefit)}] {REDEEMED}
        LEFT JOIN [{nameof(Benefit)}] {BENEFIT}
            ON {BENEFIT}.{nameof(Benefit.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.BenefitId)}
        LEFT JOIN [{nameof(Client)}] {CLIENT}
            ON {CLIENT}.{nameof(Client.Id)} = {REDEEMED}.{nameof(RedeemedBenefit.ClientId)}
        LEFT JOIN [{nameof(Attachment)}] {ATTACHMENT}
            ON {ATTACHMENT}.{nameof(Attachment.ClientId)} = {REDEEMED}.{nameof(RedeemedBenefit.ClientId)}
        ";

        public static string PushBenefitNotificationViewSql = $@"
        SELECT 
            {BENEFIT}.{nameof(Benefit.Id)} AS {nameof(BenefitPushNotificationView.BenefitId)},
            {BENEFIT}.{nameof(Benefit.StartDate)} AS {nameof(BenefitPushNotificationView.StartDate)},
            {BENEFIT}.{nameof(Benefit.EventDate)} AS {nameof(BenefitPushNotificationView.EventDate)},
            {BENEFIT}.{nameof(Benefit.EndDate)} AS {nameof(BenefitPushNotificationView.EndDate)},
            {BENEFIT}.{nameof(Benefit.NameEN)} AS {nameof(BenefitPushNotificationView.NameEN)},
            {BENEFIT}.{nameof(Benefit.NameAR)} AS {nameof(BenefitPushNotificationView.NameAR)}
        FROM 
            [{nameof(Benefit)}] {BENEFIT}
        ";
    }
}