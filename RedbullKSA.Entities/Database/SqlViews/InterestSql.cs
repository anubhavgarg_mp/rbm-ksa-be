﻿using RedbullKSA.Entities.API.Models.ClientModels;
using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.Database.Models;

namespace RedbullKSA.Entities.Database.SqlViews
{
    public static class InterestSql
    {
        private const string INTEREST = "dbo_interest";
        private const string COUNT = "dbo_count";
        private const string CLIENT_INTEREST = "dbo_clientInterest";
        private const string CLIENT = "dbo_client";

        public static string InterestGridViewSql = $@"
SELECT 
    {INTEREST}.{nameof(Interest.Id)},
    {INTEREST}.{nameof(Interest.EnglishName)},
    {INTEREST}.{nameof(Interest.ArabicName)},
    {INTEREST}.{nameof(Interest.Status)},
    {COUNT}.{nameof(InterestGridView.NumberOfClients)}
FROM
    [{nameof(Interest)}] {INTEREST}
    LEFT JOIN (SELECT {CLIENT_INTEREST}.{nameof(ClientInterest.InterestId)}, COUNT({CLIENT_INTEREST}.{nameof(ClientInterest.ClientId)}) AS {nameof(InterestGridView.NumberOfClients)}
                FROM [{nameof(ClientInterest)}] {CLIENT_INTEREST}
                GROUP BY {nameof(ClientInterest.InterestId)}) AS {COUNT} 
    ON {COUNT}.{nameof(ClientInterest.InterestId)} = {INTEREST}.{nameof(Interest.Id)}
";

        public static string ClientInterestsViewSql = $@"
SELECT
    {INTEREST}.{nameof(ClientInterestsView.Id)},
    {INTEREST}.{nameof(ClientInterestsView.EnglishName)},
    {CLIENT_INTEREST}.{nameof(ClientInterestsView.ClientId)},
    {CLIENT}.{nameof(ClientInterestsView.FirstName)},
    {CLIENT}.{nameof(ClientInterestsView.LastName)},
    {CLIENT}.{nameof(ClientInterestsView.Email)},
    {CLIENT}.{nameof(ClientInterestsView.PhoneNumber)},
    {CLIENT}.{nameof(ClientInterestsView.Gender)},
    {CLIENT}.{nameof(ClientInterestsView.BirthDate)},
    {CLIENT}.{nameof(ClientInterestsView.RegistrationDate)}
FROM
    [{nameof(Interest)}] {INTEREST}
    LEFT JOIN [{nameof(ClientInterest)}] {CLIENT_INTEREST} ON {INTEREST}.{nameof(Interest.Id)} = {CLIENT_INTEREST}.{nameof(ClientInterest.InterestId)}
    LEFT JOIN [{nameof(Client)}] {CLIENT} ON {CLIENT_INTEREST}.{nameof(ClientInterest.ClientId)} = {CLIENT}.{nameof(Client.Id)}
";
    }
}