﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.Database.Base
{
    public interface IEntity
    {
        [Key]
        int Id { get; set; }
    }
}
