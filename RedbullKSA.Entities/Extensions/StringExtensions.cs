﻿using RedbullKSA.Entities.Attributes;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Text;

namespace RedbullKSA.Entities.Extensions
{
    public static class StringExtensions
    {
        public static string AppendTo(this ImageFormat format, string fileName)
            => fileName + "." + format.ToString().ToLower();
    }
}
