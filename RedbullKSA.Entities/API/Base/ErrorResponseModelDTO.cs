﻿using RedbullKSA.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Base
{
    /// <summary>
    /// Generic error response object
    /// </summary>
    public class ErrorResponseModelDTO
    {
        /// <summary>
        /// Numerical error code. See <see cref="KSAExceptionCode"></see>
        /// </summary>
        [Required]
        public int ErrorCode { get; set; }

        /// <summary>
        /// Short error description
        /// </summary>
        [Required]
        public string Message { get; set; }

        public ErrorResponseModelDTO() { }

        public ErrorResponseModelDTO(KSAException ex)
        {
            ErrorCode = Convert.ToInt32(ex.Code);
            Message = ex.Message;
        }

        public ErrorResponseModelDTO(Exception ex)
        {
            ErrorCode = (int)KSAExceptionCode.General.UnknownError;
            Message = ex.Message;
        }
    }
}
