﻿using System;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.WebParams
{
    public class BenefitParameters : WebParameters
    {
        [ExpressionParsing(nameof(BenefitGridView.Type), ComparisonOperator.In)]
        public IEnumerable<BenefitType> Type { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.DistributionType), ComparisonOperator.In)]
        public IEnumerable<DistributionMethod> DistributionType { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.Category), ComparisonOperator.In)]
        public IEnumerable<BenefitCategory> Category { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.Status), ComparisonOperator.In)]
        public IEnumerable<BenefitStatus> Status { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.AvailableQuantity), ComparisonOperator.GreaterThanOrEquals)]
        public int? AvailableQuantityFrom { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.AvailableQuantity), ComparisonOperator.LessThanOrEquals)]
        public int? AvailableQuantityTo { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.Name), ComparisonOperator.Contains)]
        public string Name { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.StartDate), ComparisonOperator.GreaterThanOrEquals)]
        public DateTime? StartDateFrom { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.StartDate), ComparisonOperator.LessThanOrEquals)]
        public DateTime? StartDateTo { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.EndDate), ComparisonOperator.GreaterThanOrEquals)]
        public DateTime? EndDateFrom { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.EndDate), ComparisonOperator.LessThanOrEquals)]
        public DateTime? EndDateTo { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.IsFeatured), ComparisonOperator.In)]
        public IEnumerable<bool?> IsFeatured { get; set; }

        [ExpressionParsing(nameof(BenefitGridView.EnglishTagNames), ComparisonOperator.In)]
        public IEnumerable<string> EnglishTagName { get; set; }
    }
}
