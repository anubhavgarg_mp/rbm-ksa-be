﻿using RedbullKSA.Entities.API.Models.InterestModels;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.WebParams
{
    public class InterestParameters : WebParameters
    {
        [ExpressionParsing(nameof(InterestGridView.EnglishName), ComparisonOperator.Contains)]
        public string EnglishName { get; set; }

        [ExpressionParsing(nameof(InterestGridView.NumberOfClients), ComparisonOperator.GreaterThanOrEquals)]
        public int? NumberOfClientsFrom { get; set; }

        [ExpressionParsing(nameof(InterestGridView.NumberOfClients), ComparisonOperator.LessThanOrEquals)]
        public int? NumberOfClientsTo { get; set; }

        [ExpressionParsing(nameof(InterestGridView.Status), ComparisonOperator.In)]
        public IEnumerable<ActiveStatus> Status { get; set; }
    }
}
