﻿using RedbullKSA.Entities.API.Models.UserModels;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.WebParams
{
    public class AdministratorParameters : WebParameters
    {
        [ExpressionParsing(nameof(AdministratorGridView.FullName), ComparisonOperator.Contains)]
        public string FullName { get; set; }

        [ExpressionParsing(nameof(AdministratorGridView.Email), ComparisonOperator.Contains)]
        public string Email { get; set; }

        [ExpressionParsing(nameof(AdministratorGridView.PhoneNumber), ComparisonOperator.Contains)]
        public string PhoneNumber { get; set; }

        [ExpressionParsing(nameof(AdministratorGridView.Status), ComparisonOperator.In)]
        public IEnumerable<ActiveStatus> Status { get; set; }
    }
}
