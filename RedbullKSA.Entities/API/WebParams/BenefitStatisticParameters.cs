﻿using System;
using System.Collections.Generic;
using System.Text;
using RedbullKSA.Entities.API.Models.BenefitModels;
using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.WebParams
{
    public class BenefitStatisticParameters : WebParameters
    {
        [ExpressionParsing(nameof(BenefitStatisticGrid.Id), ComparisonOperator.Equals)]
        public int? Id { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.Type), ComparisonOperator.In)]
        public IEnumerable<BenefitType> Type { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.Category), ComparisonOperator.In)]
        public IEnumerable<BenefitCategory> Category { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.DistributionType), ComparisonOperator.In)]
        public IEnumerable<DistributionMethod> DistributionType { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.BenefitName), ComparisonOperator.Contains)]
        public string BenefitName { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.ClientId), ComparisonOperator.Equals)]
        public int? ClientId { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.BenefitId), ComparisonOperator.Equals)]
        public int? BenefitId { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.ClientFullName), ComparisonOperator.Contains)]
        public string ClientFullName { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.AddedDate), ComparisonOperator.GreaterThanOrEquals)]
        public DateTime? AddedDateFrom { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.AddedDate), ComparisonOperator.LessThanOrEquals)]
        public DateTime? AddedDateTo { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.SkuCode), ComparisonOperator.Contains)]
        public string SkuCode { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.VoucherCode), ComparisonOperator.Contains)]
        public string VoucherCode { get; set; }

        [ExpressionParsing(nameof(BenefitStatisticGrid.IsLottery), ComparisonOperator.In)]
        public IEnumerable<bool> IsLottery { get; set; }
    }
}
