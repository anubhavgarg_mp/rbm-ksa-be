﻿namespace RedbullKSA.Entities.API.WebParams.Base
{
    public enum LogicalOperator
    {
        AND,
        OR
    }
}
