﻿using System.ComponentModel;

namespace RedbullKSA.Entities.API.WebParams.Base
{
    public enum ComparisonType
    {
        [Description("Default")]
        Default = 0,
        [Description("STDistance")]
        StDistance = 1
    }
}
