﻿namespace RedbullKSA.Entities.API.WebParams.Base
{
    public class PagingParameters
    {
        /// <summary>
        /// Page No. to fetch. Defaults to 1 if not provided.
        /// </summary>
        public int? Page { get; set; } = 1;

        /// <summary>
        /// Record count per page. Defaults to 25 if not provided.
        /// </summary>
        public int? PerPage { get; set; } = 25;
    }
}
