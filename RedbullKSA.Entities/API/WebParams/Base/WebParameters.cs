﻿namespace RedbullKSA.Entities.API.WebParams.Base
{
    public class WebParameters : PagingParameters
    {
        /// <summary>
        /// Field to sort by. Defaults to "Id" if not provided
        /// </summary>
        public string Sort { get; set; } = "Id";

        /// <summary>
        /// Sort direction. Defaults to "DESC" if not provided
        /// </summary>
        public string SortDir { get; set; } = "DESC";

        public void StripPaging()
        {
            Page = null;
            PerPage = null;
        }
    }
}
