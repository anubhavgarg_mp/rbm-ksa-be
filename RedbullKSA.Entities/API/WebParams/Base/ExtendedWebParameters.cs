﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.API.WebParams.Base
{
    public class ExtendedWebParameters : WebParameters
    {
        public long? DateFrom { get; set; }
        public long? DateTo { get; set; }
    }
}
