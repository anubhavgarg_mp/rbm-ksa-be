﻿using RedbullKSA.Entities.API.WebParams.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.API.WebParams
{
    public class TagParameters : WebParameters
    {
        [ExpressionParsing(nameof(Tag.Status), ComparisonOperator.In)]
        public IEnumerable<ActiveStatus> Status { get; set; }
    }
}
