﻿namespace RedbullKSA.Entities.API.Models.Base
{
    /// <summary>
    /// Model used for storing an english and an arabic string in a single object.
    /// </summary>
    public class MultiString
    {
        public string English { get; set; }

        public string Arabic { get; set; }

        public MultiString(string englishName, string arabicName)
        {
            English = englishName;
            Arabic = arabicName;
        }
    }
}
