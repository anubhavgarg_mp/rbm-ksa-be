﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.API.Models.Base
{
    public class TotalsBase
    {
        public int Count { get; set; }
        public int PageCount { get; set; }
    }
}
