﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class RechargeDTO
    {
        [Required]
        public double OldBalanceAmt { get; set; }

        [Required]
        public double NewBalanceAmt { get; set; }

        [Required]
        public int Currency { get; set; }

        public RechargeDTO(double oldBalanceAmt, double newBalanceAmt, int currency)
        {
            OldBalanceAmt = oldBalanceAmt;
            NewBalanceAmt = newBalanceAmt;
            Currency = currency;
        }
    }
}
