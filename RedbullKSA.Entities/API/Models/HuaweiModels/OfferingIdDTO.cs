﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class OfferingIdDTO
    {
        public string OfferingId { get; set; }

        public string PurchaseSeq { get; set; }

        public OfferingIdDTO() { }

        public OfferingIdDTO(OfferingIdModel offeringIdModel)
        {
            if (offeringIdModel == null)
            {
                return;
            }

            OfferingId = offeringIdModel.OfferingId;
            PurchaseSeq = offeringIdModel.PurchaseSeq;
        }
    }
}
