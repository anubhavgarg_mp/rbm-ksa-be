﻿namespace RedbullKSA.Entities.API.Models.HuaweiModels.Common
{
    public class TaxInfo
    {
        public string TaxCode;

        public string TaxName;

        public string Amount;

        public string TaxRate;

        public string TaxableAmount;

        public string ExemptionType;
    }
}
