﻿namespace RedbullKSA.Entities.API.Models.HuaweiModels.Common
{
    /// <summary>
    /// Huawei docs SimpleProperty type
    /// </summary>
    public class SimpleProperty
    {
        public string Code { get; set; }

        public string Value { get; set; }
    }
}
