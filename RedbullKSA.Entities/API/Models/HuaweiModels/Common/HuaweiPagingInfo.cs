﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels.Common
{
    public class HuaweiPagingInfo
    {
        /// <summary>
        /// It's filled 0 in the first query, 
        /// and then filled the totalRowNum returned in 
        /// the next queries
        /// </summary>
        [Required]
        public int TotalRowNumber { get; set; }

        /// <summary>
        /// Row number (in Huawei's system) to begin fetching from.
        /// </summary>
        [Required]
        public int BeginRowNumber { get; set; }

        /// <summary>
        /// The row number (starting from BeginRowNumber) to fetch.
        /// </summary>
        [Required]
        public int FetchRowNumber { get; set; }

        public HuaweiPagingInfo(int beginRowNumber, int fetchRowNumber)
        {
            TotalRowNumber = fetchRowNumber;
            BeginRowNumber = beginRowNumber;
            FetchRowNumber = fetchRowNumber;
        }

    }
}
