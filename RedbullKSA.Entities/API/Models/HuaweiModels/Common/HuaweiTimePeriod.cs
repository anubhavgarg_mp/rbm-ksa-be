﻿using Newtonsoft.Json;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels.Common
{
    public class HuaweiTimePeriod
    {
        /// <summary>
        /// The start date of log
        /// </summary>
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// The end date of log
        /// </summary>
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime EndTime { get; set; }
    }
}
