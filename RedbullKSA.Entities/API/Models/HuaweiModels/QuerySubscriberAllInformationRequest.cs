﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class QuerySubscriberAllInformationRequest
    {
        /// <summary>
        /// The subscriber you want to query
        /// </summary>
        [Required]
        public ObjectAccessInfo AccessInfo { get; set; }

        /// <summary>
        /// Include offerring flag
        /// </summary>
        [Required]
        public bool IncludeOfferFlag { get; set; }
    }
}
