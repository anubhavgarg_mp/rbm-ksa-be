﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class CalOneOffFeeRequest
    {
        /// <summary>
        /// Business code
        /// </summary>
        public string BusinessCode { get; set; }

        /// <summary>
        /// Primary offering Id
        /// </summary>
        public OfferingIdDTO PrimaryOfferingIdDTO { get; set; }

        /// <summary>
        /// Supplementary offering Id
        /// </summary>
        public OfferingIdDTO SupplementaryOfferingIdDTO { get; set; }

        /// <summary>
        /// MSISDN
        /// </summary>
        public string ServiceNumber { get; set; }

        /// <summary>
        /// Item code (SKU) of SIM
        /// </summary>
        public string ICCIDItemCode { get; set; }

        /// <summary>
        /// List of additional Properties
        /// </summary>
        public IEnumerable<SimpleProperty> AdditionalProperties { get; set; }
    }
}
