﻿using RedbullKSA.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class UnbilledAmountRequest
    {
        /// <summary>
        /// The subscriber you want to query
        /// </summary>
        [Required]
        public ObjectAccessInfo AccessInfo { get; set; }

        /// <summary>
        /// Only for a hybrid user
        /// </summary>
        public PaymentType? PaymentType { get; set; }
    }
}
