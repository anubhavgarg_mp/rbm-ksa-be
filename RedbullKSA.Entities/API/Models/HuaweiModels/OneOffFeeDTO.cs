﻿using RedbullKSA.Entities.Database.Models.UtilityServices;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class OneOffFeeDTO
    {
        public string FeeItemCode { get; set; }

        public string FeeType { get; set; }

        public string FeeName { get; set; }

        public string TotalAmount { get; set; }

        public string OriginalAmount { get; set; }

        public string DiscountAmount { get; set; }

        public TaxInfoDTO TaxInfo { get; set; }

        public OfferingIdDTO OfferingIdDTO { get; set; }

        public IEnumerable<AdditionalPropertyDTO> AdditionalProperty { get; set; }

        public OneOffFeeDTO() { }

        public OneOffFeeDTO(CalOneOffFeeRspMsgOneOffFeeListOneOffFee oneOffFee)
        {
            if (oneOffFee == null)
            {
                return;
            }

            ICollection<AdditionalPropertyDTO> additionalProperties = new List<AdditionalPropertyDTO>();
            if (oneOffFee.AdditionalProperty.Length > 0)
            {
                foreach (var additionalProperty in oneOffFee.AdditionalProperty)
                {
                    additionalProperties.Add(new AdditionalPropertyDTO(additionalProperty));
                }
            }

            FeeItemCode = oneOffFee.FeeItemCode;
            FeeName = oneOffFee.FeeName;
            FeeType = oneOffFee.FeeType;
            TotalAmount = oneOffFee.TotalAmt;
            OriginalAmount = oneOffFee.OriginalAmt;
            DiscountAmount = oneOffFee.DiscountAmt;
            AdditionalProperty = additionalProperties;
            TaxInfo = new TaxInfoDTO(oneOffFee.TaxInfo);

            if (oneOffFee.OfferingId != null)
            {
                OfferingIdDTO = new OfferingIdDTO(oneOffFee.OfferingId);
            }
        }
    }
}
