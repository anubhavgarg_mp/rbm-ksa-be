﻿using RedbullKSA.Entities.Database.Models.AccountServices;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class AccountBalanceAndFreeUnitDTO
    {
        /// <summary>
        /// Information about queried account
        /// </summary>
        public AccountDetailsDTO AccountDetails { get; set; }

        /// <summary>
        /// Details of account free units.
        /// </summary>
        public List<AccountFreeUnitDTO> FreeUnit { get; set; } = new List<AccountFreeUnitDTO>(0);

        public AccountBalanceAndFreeUnitDTO()
        {
        }

        public AccountBalanceAndFreeUnitDTO(QueryBalanceAndFreeUnitRspMsg response)
        {
            if (response == null || response.Account == null || response.Account.AcctBalance == null)
            {
                return;
            }

            AccountDetails = new AccountDetailsDTO(response);

            if (response.FreeUnits == null || response.FreeUnits.Length <= 0)
            {
                return;
            }

            FreeUnit = new List<AccountFreeUnitDTO>();

            foreach (var freeUnit in response.FreeUnits)
            {
                FreeUnit.Add(new AccountFreeUnitDTO(freeUnit));
            }
        }
    }

    public class AccountDetailsDTO
    {
        /// <summary>
        /// The account ID generated in the NGBSS system.
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Total amount of account
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// If this balance item is deposit or not.1: Yes. 2: No.
        /// </summary>
        public int IsDeposit { get; set; }

        /// <summary>
        /// If this balance item can refund. 1: can refund. 2: can't refund, but can change to coupon. 3: can't refund.
        /// </summary>
        public int IsRefund { get; set; }

        /// <summary>
        /// The currency is defined in ISO 4217.
        /// </summary>
        public int CurrencyCode { get; set; }
        public decimal? TotalCreditAmount { get; set; }
        public decimal? TotalUsageCreditAmount { get; set; }
        public decimal? TotalRemainCreditAmount { get; set; }

        public AccountDetailsDTO()
        {
        }

        public AccountDetailsDTO(QueryBalanceAndFreeUnitRspMsg response)
        {
            if (response == null)
            {
                return;
            }

            Id = response.Account.AcctId;
            TotalAmount = response.Account.AcctBalance.TotalAmount;
            IsDeposit = response.Account.AcctBalance.IsDeposit;
            IsRefund = response.Account.AcctBalance.IsRefund;
            CurrencyCode = response.Account.AcctBalance.Currency;
            TotalCreditAmount = response.Account.AcctCreditAmt?.TotalCreditAmt;
            TotalUsageCreditAmount = response.Account.AcctCreditAmt?.TotalUsageAmt;
            TotalRemainCreditAmount = response.Account.AcctCreditAmt?.TotalRemainAmt;
        }
    }

    public class AccountFreeUnitDTO
    {
        /// <summary>
        /// Id of the free unit's type.
        /// </summary>
        public string TypeId { get; set; }

        /// <summary>
        /// Name of the free unit's type.
        /// </summary>
        public string TypeName { get; set; }

        /// <summary>
        /// Unit that measures the free unit.
        /// </summary>
        public string MeasureUnit { get; set; }

        /// <summary>
        /// Total amount of the free unit.
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Unused amount of the free unit.
        /// </summary>
        public decimal UnusedAmount { get; set; }

        /// <summary>
        /// Id of unit
        /// </summary>
        public long UnitId { get; set; }

        public AccountFreeUnitDTO()
        {
        }

        public AccountFreeUnitDTO(QueryBalanceAndFreeUnitRspMsgFreeUnit freeUnit)
        {
            if (freeUnit == null)
            {
                return;
            }

            TypeId = freeUnit.TypeId;
            TypeName = freeUnit.TypeName;
            MeasureUnit = freeUnit.MeasureUnit;
            TotalAmount = freeUnit.TotalAmt;
            UnusedAmount = freeUnit.UnusedAmt;
        }
    }
}
