﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class RechargeModel
    {
        /// <summary>
        /// Amount to recharge by
        /// </summary>
        [Required]
        public double Amount { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        public int Currency { get; set; }

        /// <summary>
        /// Object ID to operate upon
        /// </summary>
        [Required]
        public string ObjectId { get; set; }
    }
}
