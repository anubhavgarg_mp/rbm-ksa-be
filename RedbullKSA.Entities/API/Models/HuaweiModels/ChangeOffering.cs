﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class ChangeOffering {

        /// <summary>
        /// User's service number
        /// </summary>
        [Required]
        public string ObjectId { get; set; }

        // <summary>
        /// New offer id
        /// </summary>
        public string NewOfferId { get; set; }
    }
}
