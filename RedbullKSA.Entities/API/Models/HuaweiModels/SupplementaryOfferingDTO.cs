﻿using RedbullKSA.Entities.Database.Models.OfferingServices;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class SupplementaryOfferingDTO : AvailableOfferingDTO
    {
        public string PrimaryOfferingId { get; set; }

        public SupplementaryOfferingDTO(SupplementaryOffering supplementaryOffering) : base(supplementaryOffering)
        {
        }

        public SupplementaryOfferingDTO(
            SupplementaryOffering supplementaryOffering,
            string primaryOfferingId) : base(supplementaryOffering)
        {
            PrimaryOfferingId = primaryOfferingId;
        }
    }
}
