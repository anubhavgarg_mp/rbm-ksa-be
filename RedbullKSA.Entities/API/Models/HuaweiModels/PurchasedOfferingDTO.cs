﻿using Newtonsoft.Json;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Common.Helpers;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class PurchasedOfferingDTO
    {
        [Required]
        public string OfferingId { get; set; }

        public string OfferingName { get; set; }

        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime EffectiveDate { get; set; }

        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime ExpireDate { get; set; }

        [Required]
        public long PurchaseSeq { get; set; }

        public PurchasedOfferingDTO(string offeringId, long purchaseSeq, string offeringName, string effectiveDate, string expireDate)
        {
            OfferingId = offeringId;
            OfferingName = offeringName;
            EffectiveDate = ConversionHelper.ConvertHuaweiDateToDateTime(effectiveDate);
            ExpireDate = ConversionHelper.ConvertHuaweiDateToDateTime(expireDate);
            PurchaseSeq = purchaseSeq;
        }
    }
}
