﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class ObjectAccessInfo
    {
        [Required]
        public ObjectIdType ObjectIdType { get; set; }

        /// <summary>
        /// ID of object you want to operate. If you want to operate customer this is CustomerID;
        /// if account - AccountID.
        /// Customers: CustId; Accounts: AcctId; Subscriptions: SubId; Services: ServiceNum; SIM: ICCID;
        /// </summary>
        [Required]
        public string ObjectId { get; set; }
    }
}
