﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class ChangeSubscriberInformation
    {
        public string Language { get; set; }
        public string WrittenLanguage { get; set; }
        public string IMEI { get; set; }
        public List<SimpleProperty> AdditionalProperties { get; set; }
    }
}
