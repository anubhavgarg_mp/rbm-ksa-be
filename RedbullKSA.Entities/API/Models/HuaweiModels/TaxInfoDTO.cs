﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class TaxInfoDTO
    {
        public string TaxCode { get; set; }

        public string TaxName { get; set; }

        public string Amount { get; set; }

        public string TaxRate { get; set; } 

        public string TaxableAmount { get; set; }

        public string ExemptionType { get; set; }

        public TaxInfoDTO() { }

        public TaxInfoDTO(TaxInfo taxInfo)
        {
            if (taxInfo == null)
            {
                return;
            }

            TaxCode = taxInfo.TaxCode;
            TaxName = taxInfo.TaxName;
            Amount = taxInfo.Amount;
            TaxRate = taxInfo.TaxRate;
            TaxableAmount = taxInfo.TaxableAmount;
            ExemptionType = taxInfo.ExemptionType;
        }
    }
}
