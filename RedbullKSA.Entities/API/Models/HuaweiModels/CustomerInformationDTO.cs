﻿using RedbullKSA.Entities.Database.Models.CustomerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class CustomerInformationDTO
    {
        public CustomerInformationDTO(QueryCustInfoRspMsgCustomer customer)
        {
            CustId = customer.CustId;
            CustLevel = customer.CustLevel;
            DefaultAcctId = customer.DefaultAcctId;
            Title = customer.Title;
            Name = customer.Name.LastName;
            Nationality = customer.Nationality;
            Birthday = customer.Birthday;
            Status = customer.Status;
            IsLastGdprVersion = customer.IsLastGdprVersion;
        }

        public long CustId;
        public int CustLevel;
        public long DefaultAcctId;
        public int Title;
        public string Name;
        public int Nationality;
        public int Birthday;
        public int Status;
        public bool IsLastGdprVersion;
    }
}
