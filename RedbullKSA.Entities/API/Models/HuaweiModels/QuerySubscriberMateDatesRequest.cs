﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class QuerySubscriberMateDatesRequest
    {
        [Required]
        public List<ObjectAccessInfo> Objects { get; set; }
    }
}
