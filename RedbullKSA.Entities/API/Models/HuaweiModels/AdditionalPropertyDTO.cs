﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class AdditionalPropertyDTO
    {
        public string Code { get; set; }

        public string Value { get; set; }

        public AdditionalPropertyDTO() { }

        public AdditionalPropertyDTO(SimpleProperty additionalProperty)
        {
            if (additionalProperty == null)
            {
                return;
            }

            Code = additionalProperty.Code;
            Value = additionalProperty.Value;
        }
    }
}
