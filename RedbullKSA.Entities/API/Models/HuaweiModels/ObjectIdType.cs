﻿namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public enum ObjectIdType : byte
    {
        CustomerId = 1,
        AccountId = 2,
        SubscriberId = 3,
        ServiceNumber = 4,
        ICCID = 5,
    }
}
