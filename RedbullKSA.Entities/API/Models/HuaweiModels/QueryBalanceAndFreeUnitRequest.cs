﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class QueryBalanceAndFreeUnitRequest
    {
        /// <summary>
        /// The account or service number which you want to  query balance.
        /// </summary>
        [Required]
        public ObjectAccessInfo AccessInfo { get; set; }
    }
}
