﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class RechargeVoucherModel
    {       
        /// <summary>
        /// Voucher's PIN
        /// </summary>
        [Required]
        public string PIN { get; set; }

        /// <summary>
        /// Object ID to operate upon
        /// </summary>
        [Required]
        public string ObjectId { get; set; }
    }
}
