﻿using Newtonsoft.Json;
using RedbullKSA.Common.Helpers;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using RedbullKSA.Entities.Database.Models.RechargeServices;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class RechargeLogEntryDTO
    {
        /// <summary>
        /// Serial number or the recharge transaction
        /// </summary>
        [Required]
        public string SerialNumber { get; set; }

        /// <summary>
        /// The time that the end user recharged
        /// </summary>
        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime RechargeTime { get; set; }

        /// <summary>
        /// The amount that is recharged to the user's account
        /// </summary>
        [Required]
        public decimal ActualAmount { get; set; }

        public int Currency { get; set; }

        public RechargeLogEntryDTO(QueryRechargeLogRspMsgRechargeLog rechargeLog)
        {
            if (rechargeLog == null)
                return;

            SerialNumber = rechargeLog.SerialNum;
            RechargeTime = ConversionHelper.ConvertHuaweiDateToDateTime(rechargeLog.RechargeTime);
            ActualAmount = rechargeLog.ActualAmt;
            Currency = rechargeLog.Currency;
        }
    }
}
