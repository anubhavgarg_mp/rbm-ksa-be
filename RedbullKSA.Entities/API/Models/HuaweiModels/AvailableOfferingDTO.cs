﻿using RedbullKSA.Entities.Database.Models.OfferingServices;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class AvailableOfferingDTO
    {
        [Required]
        public string OfferingId { get; set; }

        [Required]
        public string OfferingName { get; set; }

        public decimal OneTimeCost { get; set; }

        public decimal MonthlyCost { get; set; }

        public string DurationUnit { get; set; }

        public int? DurationValue { get; set; }

        public AvailableOfferingDTO(PrimaryOffering primaryOffering)
        {
            if (primaryOffering == null)
                return;

            OfferingId = primaryOffering.OfferingId.OfferingId;            
            OfferingName = primaryOffering.OfferingName;
            OneTimeCost = primaryOffering.OneTimeCost;
            MonthlyCost = primaryOffering.MonthlyCost;
            DurationUnit = primaryOffering.Contract?.DurationUnit;
            DurationValue = primaryOffering.Contract?.DurationValue;
        }

        public AvailableOfferingDTO(SupplementaryOffering supplementaryOffering)
        {
            if (supplementaryOffering == null)
                return;

            OfferingId = supplementaryOffering.OfferingId.OfferingId;
            OfferingName = supplementaryOffering.OfferingName;
            OneTimeCost = supplementaryOffering.OneTimeCost;
            MonthlyCost = supplementaryOffering.MonthlyCost;
        }
    }
}
