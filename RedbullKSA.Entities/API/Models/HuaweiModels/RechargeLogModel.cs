﻿using RedbullKSA.Entities.API.Models.HuaweiModels.Common;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class RechargeLogModel
    {
        /// <summary>
        /// User's service number
        /// </summary>
        [Required]
        public string ObjectId { get; set; }

        [Required]
        public HuaweiTimePeriod TimePeriod { get; set; }

        [Required]
        public HuaweiPagingInfo PagingInfo { get; set; }
    }
}
