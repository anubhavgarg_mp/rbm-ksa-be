﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.HuaweiModels
{
    public class AvailableNumberTierDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
