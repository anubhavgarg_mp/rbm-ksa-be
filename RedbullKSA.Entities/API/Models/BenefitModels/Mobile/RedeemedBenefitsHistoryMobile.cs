﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class RedeemedBenefitsHistoryMobile
    {
        public IEnumerable<RedeemedBenefitRecordMobile> BenefitsHistory { get; set; }

        public RedeemedBenefitsHistoryMobile() { }

        public RedeemedBenefitsHistoryMobile(IEnumerable<RedeemedBenefitRecordMobile> benefits) => BenefitsHistory = benefits;
    }
}
