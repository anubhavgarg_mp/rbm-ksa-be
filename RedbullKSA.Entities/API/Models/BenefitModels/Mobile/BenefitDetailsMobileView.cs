﻿using RedbullKSA.Entities.Enums;
using System;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class BenefitDetailsMobileView
    {
        public int Id { get; set; }

        public string NameAR { get; set; }
        public string NameEN { get; set; }

        public string DescriptionTeaserAR { get; set; }
        public string DescriptionTeaserEN { get; set; }

        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }

        public string SecondaryDescriptionAR { get; set; }
        public string SecondaryDescriptionEN { get; set; }

        public string LocationAR { get; set; }
        public string LocationEN { get; set; }

        public DateTime? EndDate { get; set; }
        public DateTime? EventDate { get; set; }
        public DateTime? StartDate { get; set; }

        public int? QuantityRemaining { get; set; }

        public BenefitType Type { get; set; }

        public string AppStoreUrlEN { get; set; }
        public string AppStoreUrlAR { get; set; }
        public string GooglePlayUrlEN { get; set; }
        public string GooglePlayUrlAR { get; set; }
        public string ButtonUrlEN { get; set; }
        public string ButtonUrlAR { get; set; }

        public string SkuCode { get; set; }

    }
}
