﻿using RedbullKSA.Entities.API.Models.TagModels;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class BenefitOffersViewMobile
    {
        public IEnumerable<OfferBannerMobile> Banners { get; set; }
        public IEnumerable<BenefitOfferMobile> Benefits { get; set; }

        public BenefitOffersViewMobile() { }

        public BenefitOffersViewMobile(IEnumerable<BenefitOfferMobile> benefits) => Benefits = benefits;

        public IEnumerable<TagDetailsMobileView> Tags { get; set; }
        public IEnumerable<BenefitCategoryDetailsMobileView> Categories { get; set; }
    }
}
