﻿using RedbullKSA.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class RedeemedBenefitViewMobile
    {
        /// <summary>
        /// Not available for the moment
        /// </summary>
        public string PromoCode { get; set; }
        public string PromoUsageDescription { get; set; }
        
        [Required]
        public BenefitType Type { get; set; }

        public RedeemedBenefitViewMobile(string promoCode, string description, BenefitType type)
        {
            PromoCode = promoCode ?? "";
            PromoUsageDescription = description;
            Type = type;
        }
    }
}
