﻿using RedbullKSA.Entities.Enums;
using System;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class BenefitOfferMobileView
    {
        public int Id { get; set; }

        public string NameAR { get; set; }
        public string NameEN { get; set; }

        public string DescriptionTeaserAR { get; set; }
        public string DescriptionTeaserEN { get; set; }

        public string DescriptionAR { get; set; }
        public string DescriptionEN { get; set; }

        public DateTime? EndDate { get; set; }

        public int QuantityRemaining { get; set; }

        public bool? IsShownToGuests { get; set; }

        public BenefitType Type { get; set; }
        public BenefitCategory Category { get; set; }

        public string TeaserVideoUrl { get; set; }
        public string TeaserImage { get; set; }

        public string AppStoreUrlEN { get; set; }
        public string AppStoreUrlAR { get; set; }
        public string GooglePlayUrlEN { get; set; }
        public string GooglePlayUrlAR { get; set; }
        public string BrowserUrlEN { get; set; }
        public string BrowserUrlAR { get; set; }

        public bool IsFeatured { get; set; }
    }
}
