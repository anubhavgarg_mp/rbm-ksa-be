﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class GalleryImageMobile
    {
        public string ImageUrl { get; set; }

        public GalleryImageMobile(string image) => ImageUrl = image;
    }
}
