﻿using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class RedeemBenefitAddMobile
    {
        [Required]
        public int BenefitId { get; set; }

        public string DeliveryAddress { get; set; }

        public void ValidateDeliveryAddressByType(BenefitType type)
        {
            if (type == BenefitType.Tangible)
            {
                if (string.IsNullOrWhiteSpace(DeliveryAddress))
                    throw new KSAException(KSAExceptionCode.Benefit.DeliveryAddressIsInvalid);
            }

            else
            {
                DeliveryAddress = null;
            }
        }
    }
}
