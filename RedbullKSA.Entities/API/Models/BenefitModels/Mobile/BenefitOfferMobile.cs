﻿using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.API.Models.TagModels;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using System;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class BenefitOfferMobile
    {
        public int Id { get; set; }

        public string TitleEn { get; set; }

        public string Title { get; set; }

        public string TeaserDescription { get; set; }

        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EndDate { get; set; }

        public int QuantityRemaining { get; set; }

        public BenefitType OfferType { get; set; }
        public BenefitCategory Category { get; set; }

        public string TeaserVideoUrl { get; set; }
        public string TeaserImage { get; set; }

        public string BrowserUrl { get; set; }
        public string AppUrl { get; set; }
        public string StoreUrl { get; set; }

        public IEnumerable<int> TagIds { get; set; }

        public BenefitOfferMobile() { }

        public BenefitOfferMobile(BenefitOfferMobileView view, ServiceLanguage language, IEnumerable<BenefitTagView> benefitTags)
        {
            Id = view.Id;
            TitleEn = view.NameEN;

            if (language == ServiceLanguage.EN)
                SetToEnglishLanguage(view);
            else
                SetToArabicLanguage(view);

            EndDate = view.EndDate;
            OfferType = view.Type;

            TeaserImage = view.TeaserImage;
            TeaserVideoUrl = view.TeaserVideoUrl;

            QuantityRemaining = view.QuantityRemaining;

            Category = view.Category;

            TagIds = benefitTags
                .Where(tag => tag.BenefitId == view.Id)
                .Select(tag => tag.TagId);
        }

        private void SetToArabicLanguage(BenefitOfferMobileView view)
        {
            Title = view.NameAR;
            TeaserDescription = view.DescriptionTeaserAR;
            BrowserUrl = view.BrowserUrlAR;
            AppUrl = view.AppStoreUrlAR;
            StoreUrl = view.GooglePlayUrlAR;
        }

        private void SetToEnglishLanguage(BenefitOfferMobileView view)
        {
            Title = view.NameEN;
            TeaserDescription = view.DescriptionTeaserEN;
            BrowserUrl = view.BrowserUrlEN;
            AppUrl = view.AppStoreUrlEN;
            StoreUrl = view.GooglePlayUrlEN;
        }
    }
}
