﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class OfferBannerMobile
    {
        public int BenefitReferenceId { get; set; }
        public string ImageUrl { get; set; }
    
        public OfferBannerMobile() { }

        public OfferBannerMobile(int referenceId, string imageUrl)
            => (BenefitReferenceId, ImageUrl) = (referenceId, imageUrl);
    }
}
