﻿using RedbullKSA.Entities.Enums;
using System.Runtime.Serialization;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class RedeemedBenefitRecordMobile
    {
        public int Id { get; set; }

        public long RedeemedDate { get; set; }

        public string PromoCode { get; set; }

        public string DeliveryAddress { get; set; }

        public string RedeemedBy { get; set; }

        public string RedeemedByEmail { get; set; }

        public BenefitType Type { get; set; }

        public string Title { get; set; }

        [IgnoreDataMember]
        public string NameAR { get; set; }

        [IgnoreDataMember]
        public string NameEN { get; set; }

        [IgnoreDataMember]
        public int ClientId { get; set; }

        public void SetTitle(ServiceLanguage language) => Title = language == ServiceLanguage.EN ? NameEN : NameAR;
    }
}
