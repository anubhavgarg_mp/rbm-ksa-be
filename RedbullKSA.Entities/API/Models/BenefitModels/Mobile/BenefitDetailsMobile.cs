﻿using Newtonsoft.Json;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Mobile
{
    public class BenefitDetailsMobile
    {
        [Required]
        public int Id { get; set; }

        public string TitleEn { get; set; }
        public string Title { get; set; }

        public string HeaderVideoUrl { get; set; }

        [Required]
        public string HeaderImageUrl { get; set; }

        public string DescriptionVideoUrl { get; set; }

        [Required]
        public string DescriptionImageUrl { get; set; }

        [Required]
        public BenefitType OfferType { get; set; }

        public string GalleryCoverUrl { get; set; }

        public string SkuNumber { get; set; }

        public int? QuantityRemaining { get; set; }

        public string DescriptionTeaser { get; set; }
        public string Description { get; set; }
        public string SecondaryDescription { get; set; }
        
        public string EventAddress { get; set; }

        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EventTime { get; set; }

        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? DealEndingTime { get; set; }

        [Required]
        private ServiceLanguage Language { get; set; }

        public string AppStoreUrl { get; set; }
        public string GooglePlayUrl { get; set; }
        public string BrowserUrl { get; set; }

        public IEnumerable<GalleryImageMobile> GalleryImageUrls { get; set; } = new List<GalleryImageMobile>();

        public BenefitDetailsMobile(BenefitDetailsMobileView benefit, ServiceLanguage language)
        {
            Id = benefit.Id;
            TitleEn = benefit.NameEN;
            Language = language;

            if (Language == ServiceLanguage.EN)
                SetToEnglishLanguage(benefit);

            else
                SetToArabicLanguage(benefit);

            OfferType = benefit.Type;
            EventTime = benefit.EventDate;
            DealEndingTime = benefit.EndDate;

            QuantityRemaining = benefit.QuantityRemaining;
            SkuNumber = benefit.SkuCode;
        }

        private void SetToArabicLanguage(BenefitDetailsMobileView benefit)
        {
            Title = benefit.NameAR;
            DescriptionTeaser = benefit.DescriptionTeaserAR;
            Description = benefit.DescriptionAR;
            EventAddress = benefit.LocationAR;
            SecondaryDescription = benefit.SecondaryDescriptionAR;
            BrowserUrl = benefit.ButtonUrlAR;
            AppStoreUrl = benefit.AppStoreUrlAR;
            GooglePlayUrl = benefit.GooglePlayUrlAR;
        }

        private void SetToEnglishLanguage(BenefitDetailsMobileView benefit)
        {
            Title = benefit.NameEN;
            DescriptionTeaser = benefit.DescriptionTeaserEN;
            Description = benefit.DescriptionEN;
            EventAddress = benefit.LocationEN;
            SecondaryDescription = benefit.SecondaryDescriptionEN;
            BrowserUrl = benefit.ButtonUrlEN;
            AppStoreUrl = benefit.AppStoreUrlEN;
            GooglePlayUrl = benefit.GooglePlayUrlEN;
        }

        public void AssignAttachments(IEnumerable<Attachment> attachments)
        {
            //header
            var headerAttachment = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitHeader);

            if (headerAttachment.UploadedFileType == AttachmentFileType.Image)
                HeaderImageUrl = headerAttachment.FileUrl;

            else
                HeaderVideoUrl = headerAttachment.FileUrl;

            //description
            var descriptionAttachment = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitMain);

            if (descriptionAttachment.UploadedFileType == AttachmentFileType.Image)
                DescriptionImageUrl = descriptionAttachment.FileUrl;

            else
                DescriptionVideoUrl = descriptionAttachment.FileUrl;

            //gallery cover
            GalleryCoverUrl = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitGalleryCover).FileUrl;

            //gallery images
            GalleryImageUrls = attachments
                .Where(x => x.Category == AttachmentCategory.BenefitGalleryImage)
                .Select(x => new GalleryImageMobile(x.FileUrl));
        }
    }
}
