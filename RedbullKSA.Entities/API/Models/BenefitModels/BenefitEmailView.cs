﻿using RedbullKSA.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitEmailView
    {
        [Key]
        public int Id { get; set; }

        public int BenefitId { get; set; }

        public BenefitType? Type { get; set; }

        public string BenefitNameEN { get; set; }
        public string IntroEN { get; set; }
        public string InstructionTitleEN { get; set; }
        public string InstructionDescriptionEN { get; set; }

        public string BenefitNameAR { get; set; }
        public string IntroAR { get; set; }
        public string InstructionTitleAR { get; set; }
        public string InstructionDescriptionAR { get; set; }

        public string BenefitHeaderImageUrl { get; private set; }

        public void SetBenefitHeader(string url)
        {
            BenefitHeaderImageUrl = url;
        }
    }
}
