﻿using System;
using System.Collections.Generic;
using System.Text;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class RedeemedBenefitStatusView
    {
        public int Count { get; set; }

        public RedeemedBenefitType Status { get; set; }

        public int BenefitId { get; set; }
    }
}
