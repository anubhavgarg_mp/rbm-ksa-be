﻿using RedbullKSA.Common.Exceptions;
using RedbullKSA.Entities.API.Models.AttachmentModels;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.Linq;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitModel
    {
        public string Name { get; set; }
        public string DescriptionTeaser { get; set; }
        public string Description { get; set; }
        public string SecondaryDescription { get; set; }
        public string Location { get; set; }
        public string ButtonUrl { get; set; }
        public string AppleStoreUrl { get; set; }
        public string GooglePlayUrl { get; set; }

        public AttachmentModel Teaser { get; set; }
        public AttachmentModel Main { get; set; }
        public AttachmentModel GalleryCover { get; set; }
        public AttachmentModel Header { get; set; }
        public IEnumerable<AttachmentModel> GalleryImages { get; set; }

        public BenefitModel() { }

        public static BenefitModel FromBenefitEN(BenefitDetailsView details, IEnumerable<AttachmentModel> attachments)
        {
            return new BenefitModel
            {
                Name = details.NameEN,
                DescriptionTeaser = details.DescriptionTeaserEN,
                Description = details.DescriptionEN,
                SecondaryDescription = details.SecondaryDescriptionEN,
                Location = details.LocationEN,
                ButtonUrl = details.ButtonUrlEN,
                AppleStoreUrl = details.AppleStoreUrlEN,
                GooglePlayUrl = details.GooglePlayUrlEN,
                Main = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitMain),
                Header = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitHeader),
                GalleryCover = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitGalleryCover),
                GalleryImages = attachments.Where(x => x.Category == AttachmentCategory.BenefitGalleryImage)
            };
        }

        public static BenefitModel FromBenefitAR(BenefitDetailsView details, IEnumerable<AttachmentModel> attachments)
        {
            return new BenefitModel
            {
                Name = details.NameAR,
                DescriptionTeaser = details.DescriptionTeaserAR,
                Description = details.DescriptionAR,
                SecondaryDescription = details.SecondaryDescriptionAR,
                Location = details.LocationAR,
                ButtonUrl = details.ButtonUrlAR,
                AppleStoreUrl = details.AppleStoreUrlAR,
                GooglePlayUrl = details.GooglePlayUrlAR,
                Main = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitMain),
                Header = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitHeader),
                GalleryCover = attachments.FirstOrDefault(x => x.Category == AttachmentCategory.BenefitGalleryCover),
                GalleryImages = attachments.Where(x => x.Category == AttachmentCategory.BenefitGalleryImage)
            };
        }

        public IEnumerable<AttachmentModel> GetAttachments()
        {
            var attachments = new List<AttachmentModel>();

            if (Teaser.Id == 0 || GalleryCover.Id == 0 || Main.Id == 0 || Header.Id == 0 || (GalleryImages.Any(x => x.Id == 0) || !GalleryImages.Any()))
                throw new KSAException(KSAExceptionCode.Benefit.NoImagesAreChosen);

            attachments.Add(Teaser);
            attachments.Add(GalleryCover);
            attachments.Add(Main);
            attachments.Add(Header);
            attachments.AddRange(GalleryImages);

            return attachments;
        }
    }
}
