﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitRedeemedDetails : IEntity
    {
        [Key]
        public int Id { get; set; }

        public BenefitType Type { get; set; }
        public BenefitCategory Category { get; set; }
        public DistributionMethod DistributionType { get; set; }

        public string BenefitName { get; set; }

        public string ClientFullName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public string DeliveryAddress { get; set; }

        public DateTime AddedDate { get; set; }

        public string SkuCode { get; set; }
        public string VoucherCode { get; set; }
    }
}
