﻿using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.BenefitModels.Email
{
    public class RedeemedBenefitEmailView
    {
        public int BenefitId { get; set; }

        public int RedeemedBenefitId { get; set; }

        public int ClientId { get; set; }

        public ServiceLanguage ClientLanguage { get; set; }

        public BenefitType Type { get; set; }

        public RedeemedBenefitType Status { get; set; }

        public string BenefitNameEN { get; set; }

        public string BenefitNameAR { get; set; }

        public string LocationEN { get; set; }

        public string LocationAR { get; set; }

        public string ClientFirstName { get; set; }

        public string ClientEmail { get; set; }

        //public string ClientMemberId { get; set; }

        public string DeliveryAddress { get; set; }

        public string QrCodeUrl { get; set; }

        public string PromoCode { get; set; }
    }
}
