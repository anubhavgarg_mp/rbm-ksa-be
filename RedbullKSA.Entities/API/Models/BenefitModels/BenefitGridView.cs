﻿using System;
using RedbullKSA.Entities.Database.Base;
using System.ComponentModel.DataAnnotations;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitGridView : IEntity
    {
        [Key]
        public int Id { get; set; }

        public BenefitType Type { get; set; }
        public DistributionMethod DistributionType { get; set; }
        public BenefitCategory Category { get; set; }

        public int? AvailableQuantity { get; set; }

        public string Name { get; set; }

        public BenefitStatus Status { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public bool IsFeatured { get; set; }

        public string EnglishTagNames { get; set; }
    }
}
