﻿using RedbullKSA.Entities.API.Models.Base;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitStatisticsTotals : TotalsBase
    {
        public int ClientCount { get; set; }

        public int BenefitCount { get; set; }

        public int BenefitTypeCount { get; set; }

        public int BenefitDistributionTypeCount { get; set; }
    }
}
