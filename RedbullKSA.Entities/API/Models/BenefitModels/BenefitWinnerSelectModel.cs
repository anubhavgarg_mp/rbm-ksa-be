﻿namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitWinnerSelectModel
    {
        public int NumberOfWinners { get; set; }
    }
}
