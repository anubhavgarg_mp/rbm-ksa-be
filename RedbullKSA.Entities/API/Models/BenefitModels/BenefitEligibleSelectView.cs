﻿using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitEligibleSelectView
    {
        public int Id { get; set; }

        public string BenefitName { get; set; }

        public int RegisteredUsers { get; set; }

        public BenefitStatus Status { get; set; }
    }
}
