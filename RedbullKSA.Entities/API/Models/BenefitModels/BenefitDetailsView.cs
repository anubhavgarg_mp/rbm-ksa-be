﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitDetailsView : IEntity
    {
        [Key]
        public int Id { get; set; }

        public string NameEN { get; set; }
        public string NameAR { get; set; }

        public string DescriptionTeaserEN { get; set; }
        public string DescriptionTeaserAR { get; set; }

        public string SecondaryDescriptionEN { get; set; }
        public string SecondaryDescriptionAR { get; set; }

        public string DescriptionEN { get; set; }
        public string DescriptionAR { get; set; }

        public string LocationEN { get; set; }
        public string LocationAR { get; set; }

        public int? AvailableQuantity { get; set; }

        public BenefitType Type { get; set; }
        public BenefitCategory Category { get; set; }
        public BenefitStatus Status { get; set; }
        public DistributionMethod? DistributionType { get; set; }
        public NotificationMethod? NotificationType { get; set; }

        public bool? IsLimitedToOneUser { get; set; }

        public bool? IsShownToGuests { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? EventDate { get; set; }

        public string SkuCode { get; set; }

        public string ButtonUrlEN { get; set; }
        public string ButtonUrlAR { get; set; }
        public string AppleStoreUrlEN { get; set; }
        public string AppleStoreUrlAR { get; set; }
        public string GooglePlayUrlEN { get; set; }
        public string GooglePlayUrlAR { get; set; }

        public bool IsFeatured { get; set; }
    }
}
