﻿using RedbullKSA.Entities.Attributes;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitXlsModel
    {
        [XlsFormat("Type", ExcelCellType.General, 1)]
        public string Type { get; set; }

        [XlsFormat("Category", ExcelCellType.General, 2)]
        public string Category { get; set; }

        [XlsFormat("Name", ExcelCellType.General, 3)]
        public string Name { get; set; }

        [XlsFormat("Start Date", ExcelCellType.DateTime, 4)]
        public DateTime StartDate { get; set; }

        [XlsFormat("End Date", ExcelCellType.DateTime, 5)]
        public DateTime? EndDate { get; set; }

        [XlsFormat("Avail. Quantity", ExcelCellType.Integer, 6)]
        public int AvailableQuantity { get; set; }

        [XlsFormat("Distribution Type", ExcelCellType.General, 7)]
        public string DistributionType { get; set; }

        [XlsFormat("Status", ExcelCellType.General, 9)]
        public string Status { get; set; }

        public BenefitXlsModel() { }

        public BenefitXlsModel(BenefitGridView benefit)
        {
            Type = benefit.Type.ToString();
            DistributionType = benefit.DistributionType.ToString();
            Category = benefit.Category.ToString();

            if (benefit.AvailableQuantity.HasValue)
                AvailableQuantity = benefit.AvailableQuantity.Value;

            Name = benefit.Name;
            Status = benefit.Status.ToString();
            StartDate = benefit.StartDate;
            EndDate = benefit.EndDate;
        }
    }
}
