﻿using Newtonsoft.Json;
using RedbullKSA.Entities.API.Models.AttachmentModels;
using RedbullKSA.Entities.API.Models.TagModels;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitWithAttachmentsDetails
    {
        public int? Id { get; set; }

        public int? AvailableQuantity { get; set; }
        public BenefitType Type { get; set; }
        public BenefitCategory Category { get; set; }
        public BenefitStatus Status { get; set; }
        public DistributionMethod? DistributionType { get; set; }
        public NotificationMethod? NotificationType { get; set; }
        public IEnumerable<BenefitTagModel> BenefitTags { get; set; }

        public bool IsFeatured { get; set; }

        //FE
        public bool LotteryIsRandom { get; set; }

        public bool? IsShownToGuests { get; set; }

        public bool? IsLimitedToOneUser { get; set; }

        public bool IsIndefinite { get; set; }

        [Required]
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime StartDate { get; set; }

        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EndDate { get; set; }

        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? EventDate { get; set; }

        public string SkuCode { get; set; }

        public BenefitModel BenefitEN { get; set; }
        public BenefitModel BenefitAR { get; set; }

        public BenefitEmailView Email { get; set; }

        public IEnumerable<BenefitTagView> Tags { get; set; }

        public BenefitWithAttachmentsDetails(BenefitDetailsView details, IEnumerable<AttachmentModel> attachments, IEnumerable<BenefitTagView> benefitTagViews)
        {
            Id = details.Id;

            BenefitEN = BenefitModel.FromBenefitEN(details, attachments.Where(x => x.Language == ServiceLanguage.EN));
            BenefitAR = BenefitModel.FromBenefitAR(details, attachments.Where(x => x.Language == ServiceLanguage.AR));

            AvailableQuantity = details.AvailableQuantity;
            IsLimitedToOneUser = details.IsLimitedToOneUser;
            IsIndefinite = !details.EndDate.HasValue;
            IsShownToGuests = details.IsShownToGuests;
            StartDate = details.StartDate;
            EndDate = details.EndDate;
            Type = details.Type;
            Category = details.Category;
            Status = details.Status;
            DistributionType = details.DistributionType;
            EventDate = details.EventDate;
            SkuCode = details.SkuCode;
            LotteryIsRandom = IsRandomSelection();
            NotificationType = details.NotificationType;

            IsFeatured = details.IsFeatured;

            Tags = benefitTagViews;
        }

        public BenefitWithAttachmentsDetails() { }

        private bool IsRandomSelection()
        {
            if (Type == BenefitType.Voucher || Type == BenefitType.Explore)
                return false;

            if (AvailableQuantity.HasValue)
                return false;

            return true;
        }
    }
}
