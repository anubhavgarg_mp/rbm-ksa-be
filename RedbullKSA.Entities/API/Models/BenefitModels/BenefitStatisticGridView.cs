﻿using RedbullKSA.Entities.Database.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitStatisticGrid : IEntity
    {
        [Key]
        public int Id { get; set; }

        public BenefitType Type { get; set; }

        public BenefitCategory Category { get; set; }

        public DistributionMethod DistributionType { get; set; }

        public string BenefitName { get; set; }

        public int ClientId { get; set; }
        public int BenefitId { get; set; }

        public string ClientFullName { get; set; }

        public DateTime AddedDate { get; set; }

        public string SkuCode { get; set; }

        public string VoucherCode { get; set; }

        public bool IsLottery { get; set; }
    }
}
