﻿using RedbullKSA.Entities.Enums;
using System;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitPushNotificationView
    {
        public int BenefitId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EventDate { get; set; }

        public DateTime EndDate { get; set; }

        public string NameEN { get; set; }

        public string NameAR { get; set; }

        public int ClientId { get; set; }

        public ServiceLanguage Language { get; set; }

        public string NotificationToken { get; set; }

    }
}
