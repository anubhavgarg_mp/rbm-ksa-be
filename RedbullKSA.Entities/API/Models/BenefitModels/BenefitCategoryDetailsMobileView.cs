﻿using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.BenefitModels
{
    public class BenefitCategoryDetailsMobileView
    {
        public BenefitCategoryDetailsMobileView() { }

        public BenefitCategoryDetailsMobileView(short category)
            => new BenefitCategoryDetailsMobileView
            {
                Id = category,
                Name = ((BenefitCategory)category).ToString()
            };

        public short Id { get; set; }
        public string Name { get; set; }
    }
}
