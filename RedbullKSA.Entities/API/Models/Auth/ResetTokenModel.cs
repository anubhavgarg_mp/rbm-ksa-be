﻿namespace RedbullKSA.Entities.API.Models.Auth
{
    public class ResetTokenModel
    {
        public string ResetToken { get; set; }
    }
}
