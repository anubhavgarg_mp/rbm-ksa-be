﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.Linq;

namespace RedbullKSA.Entities.API.Models.Auth
{
    public class AdminClaimModel
    {
        public SessionTokenType Type { get; set; }

        public IEnumerable<AdminClaimType> Permissions { get; set; }

        public AdminClaimModel() { }

        public AdminClaimModel(IEnumerable<UserClaim> claims)
        {
            Type = SessionTokenType.Admin;

            Permissions = claims.Select(x => x.ClaimType);
        }
    }
}
