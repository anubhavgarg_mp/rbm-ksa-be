﻿namespace RedbullKSA.Entities.API.Models.Auth
{
    public class ResetPasswordModel : ResetTokenModel
    {
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
    }
}
