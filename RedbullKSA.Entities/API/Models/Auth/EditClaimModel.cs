﻿using RedbullKSA.Entities.Enums;
using System.Collections.Generic;

namespace RedbullKSA.Entities.API.Models.Auth
{
    public class EditClaimModel
    {
        public int UserId { get; set; }
        public IEnumerable<AdminClaimType> Claims { get; set; }
    }
}
