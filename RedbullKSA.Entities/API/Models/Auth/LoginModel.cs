﻿using RedbullKSA.Entities.API.Base;

namespace RedbullKSA.Entities.API.Models.Auth
{
    public class LoginModel : CleanUpModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public override void CleanUpInputs()
        {
            Email = Email?.Trim();
            Password = Password?.Trim();
        }
    }
}
