﻿using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Base;

namespace RedbullKSA.Entities.API.Models.Auth.Mobile
{
    public class LoginMobileWithPassword : CleanUpModel
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public override void CleanUpInputs()
        {
            Login = Login?.Trim();
        }
    }
}
