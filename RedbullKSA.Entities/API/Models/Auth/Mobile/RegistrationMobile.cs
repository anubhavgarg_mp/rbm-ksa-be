﻿using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Base;

namespace RedbullKSA.Entities.API.Models.Auth.Mobile
{
    public class RegistrationMobile : CleanUpModel
    {
        public string BorderId { get; set; }
        public string NationalityId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

        public override void CleanUpInputs()
        {
            NationalityId = NationalityId?.Trim();
            BorderId = BorderId?.Trim();
            PhoneNumber = PhoneNumber?.PhoneNumberToSaudiNational();
            Email = Email?.Trim();
        }

        public static void ValidateMobileUserInputs(RegistrationMobile registration)
        {
            KSAException exception = new KSAException();

            if (string.IsNullOrWhiteSpace(registration.BorderId) && string.IsNullOrWhiteSpace(registration.NationalityId))
                exception.AddFieldException(KSAExceptionCode.Client.ClientIdNotProvided);

            if (!string.IsNullOrWhiteSpace(registration.Email) && !registration.Email.IsValidEmail())
                exception.AddFieldException(KSAExceptionCode.Client.EmailInvalid);

            if (!registration.PhoneNumber.IsValidPhoneNumber())
                exception.AddFieldException(KSAExceptionCode.Client.PhoneInvalid);

            if (!registration.Password.Equals(registration.ConfirmPassword))
                exception.AddFieldException(KSAExceptionCode.General.PasswordsDoNotMatch);

            exception.ThrowExceptionIfExists();
        }
    }
}