﻿using RedbullKSA.Common.Exceptions;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Base;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.Auth.Mobile
{
    public class PhoneNumberMobile : CleanUpModel
    {
        [Required]
        public string PhoneNumber { get; set; }

        public override void CleanUpInputs() => PhoneNumber = PhoneNumber.PhoneNumberToSaudiNational();

        /// <summary>
        /// Validates if number is not null or whitespace. Converts to national  aswell.
        /// </summary>
        public void ValidateNumber()
        {
            if (string.IsNullOrWhiteSpace(PhoneNumber))
                throw new KSAException(KSAExceptionCode.Invitation.InvalidPhoneNumber);
        }
    }
}
