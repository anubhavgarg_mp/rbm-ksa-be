﻿using Newtonsoft.Json;
using RedbullKSA.Entities.Database.Models;
using System;

namespace RedbullKSA.Entities.API.Models.Auth.Mobile
{
    public class TokenMobile
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string AuthToken { get; set; }
        public DateTime? Expires { get; set; }

        public TokenMobile() { }

        public TokenMobile(SessionToken token)
        {
            AuthToken = token?.Token;
            Expires = token?.Expires;
        }
    }
}
