﻿using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Base;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.Auth.Mobile
{
    public class LoginMobile : CleanUpModel
    {
        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public string Code { get; set; }

        public override void CleanUpInputs()
        {
            PhoneNumber = PhoneNumber.PhoneNumberToSaudiNational();
            Code = Code.Trim();
        }
    }
}
