﻿using Newtonsoft.Json;
using RedbullKSA.Entities.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.Auth.Mobile
{
    public class TokenGuest
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public string AuthToken { get; set; }

        public TokenGuest() { }

        public TokenGuest(SessionToken token) => AuthToken = token?.Token;
    }
}
