﻿using RedbullKSA.Entities.Database.Models;
using System;

namespace RedbullKSA.Entities.API.Models.Auth
{
    public class TokenModel
    {
        public int Id { get; set; }

        public string Token { get; set; }
        public DateTime? Expires { get; set; }

        public TokenModel() { }

        public TokenModel(SessionToken token)
        {
            Token = token.Token;
            Expires = token.Expires;
        }
    }
}
