﻿using RedbullKSA.Entities.API.Base;

namespace RedbullKSA.Entities.API.Models.Auth.Open
{
    public class OpenLoginModel : CleanUpModel
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public override void CleanUpInputs()
        {
            Username = Username?.Trim();
            Password = Password?.Trim();
        }
    }
}
