﻿using RedbullKSA.Entities.Database.Models;
using System;

namespace RedbullKSA.Entities.API.Models.Auth.Open
{
    public class ApiKeyModel
    {
        public string ApiKey { get; set; }
        public DateTime? Started { get; set; }
        public DateTime? Expires { get; set; }

        public ApiKeyModel() { }

        public ApiKeyModel(SessionToken sessionToken, bool exposeKey = false)
        {
            if (exposeKey)
                ApiKey = sessionToken.Token;

            Started = sessionToken?.Started;
            Expires = sessionToken?.Expires;
        }
    }
}
