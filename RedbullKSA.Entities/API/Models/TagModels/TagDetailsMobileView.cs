﻿using RedbullKSA.Entities.API.Models.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.TagModels
{
    public class TagDetailsMobileView
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static TagDetailsMobileView FormTag(Tag tag, ServiceLanguage language)
        {
            return new TagDetailsMobileView
            {
                Id = tag.Id,
                Name = language == ServiceLanguage.EN ? tag.NameEN : tag.NameAR
            };
        }
    }
}
