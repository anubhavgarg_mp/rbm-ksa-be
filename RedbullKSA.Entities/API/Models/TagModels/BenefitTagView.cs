﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.TagModels
{
    public class BenefitTagView
    {
        [Key]
        public int Id { get; set; }
        public int BenefitId { get; set; }
        public int TagId { get; set; }
        public string TagNameEN { get; set; }
        public string TagNameAR { get; set; }
        public int OrderPosition { get; set; }
    }
}
