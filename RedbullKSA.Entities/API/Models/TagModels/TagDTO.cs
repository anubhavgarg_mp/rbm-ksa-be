﻿using RedbullKSA.Entities.API.Models.Base;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.TagModels
{
    public class TagDTO
    {
        public int Id { get; set; }

        public MultiString Name { get; set; }

        public ActiveStatus Status { get; set; }
    }
}
