﻿using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.ClientModels
{
    public class ClientInterestsView
    {
        // Interest
        [Key]
        public int Id { get; set; }

        public string EnglishName { get; set; }

        // Client
        public int ClientId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public DateTime? BirthDate { get; set; }

        public DateTime RegistrationDate { get; set; }

        public Gender? Gender { get; set; }

    }
}
