﻿using Newtonsoft.Json;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.ClientModels.Mobile
{
    public class ClientProfileMobile
    {
        [Required]
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public Gender? Gender { get; set; }

        public ClientProfileMobile(Client client)
        {
            Id = client.Id;
            FirstName = client.FirstName;
            LastName = client.LastName;
            DateOfBirth = client.BirthDate;
            PhoneNumber = client.PhoneNumber;
            Email = client.Email;
            Gender = client.Gender;
        }
    }
}
