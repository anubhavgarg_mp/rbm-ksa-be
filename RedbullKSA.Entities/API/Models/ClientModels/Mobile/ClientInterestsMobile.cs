﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.Linq;

namespace RedbullKSA.Entities.API.Models.ClientModels.Mobile
{
    public class ClientInterestsMobile
    {
        public IEnumerable<ClientInterestMobile> Interests { get; set; }

        public ClientInterestsMobile() { }

        public static ClientInterestsMobile FromInterestList(IEnumerable<ClientIntrestViewMobile> clientInterests, ServiceLanguage serviceLanguage)
            =>  new ClientInterestsMobile()
            {
                Interests = clientInterests.Select(c => ClientInterestMobile.FromClientInterestView(c, serviceLanguage))
            };

        public static ClientInterestsMobile FromInterestList(
            IEnumerable<Interest> interests, 
            IEnumerable<ClientInterestsView> selectedInterests,
            ServiceLanguage serviceLanguage)
            =>  new ClientInterestsMobile()
            {
                Interests = interests.Select(c => ClientInterestMobile.FromInterest(c, serviceLanguage, selectedInterests.FirstOrDefault(x => x.Id == c.Id) != null))
            };
    }
}
