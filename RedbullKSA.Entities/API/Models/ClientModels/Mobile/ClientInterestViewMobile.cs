﻿using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RedbullKSA.Entities.API.Models.ClientModels.Mobile
{
    public class ClientIntrestViewMobile
    {
        [Key]
        public int Id { get; set; }
        public string ArabicName { get; set; }
        public string EnglishName { get; set; }
        public string ImageEN { get; set; }
        public string ImageAR { get; set; }
    }
}
