﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.ClientModels.Mobile
{
    public class ClientInterestMobile
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string Title { get; set; }
        public bool IsSelected { get; set; }

        public static ClientInterestMobile FromClientInterestView(ClientIntrestViewMobile interestsView, ServiceLanguage serviceLanguage)
        {
            return new ClientInterestMobile()
            {
                Id = interestsView.Id,
                Title = serviceLanguage == ServiceLanguage.AR ? interestsView.ArabicName : interestsView.EnglishName,
                ImageUrl = serviceLanguage == ServiceLanguage.AR ? interestsView.ImageAR : interestsView.ImageEN,
            };
        }

        public static ClientInterestMobile FromInterest(Interest interest, ServiceLanguage serviceLanguage, bool isSelected)
        {
            return new ClientInterestMobile()
            {
                Id = interest.Id,
                Title = serviceLanguage == ServiceLanguage.AR ? interest.ArabicName : interest.EnglishName,
                ImageUrl = serviceLanguage == ServiceLanguage.AR ? interest.ImageAR : interest.ImageEN,
                IsSelected = isSelected
            };
        }
    }
}
