﻿using RedbullKSA.Entities.Attributes;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RedbullKSA.Entities.API.Models.ClientModels
{
    public class ClientXlsModel
    {
        [XlsFormat("Full Name", ExcelCellType.General, 1)]
        public string FullName { get; }

        [XlsFormat("Email", ExcelCellType.General, 2)]
        public string Email { get; }

        [XlsFormat("Phone number", ExcelCellType.General, 3)]
        public string PhoneNumber { get; }

        [XlsFormat("Date of Birth", ExcelCellType.Date, 4)]
        public DateTime? BirthDate { get; }

        [XlsFormat("Registered", ExcelCellType.DateTime, 5)]
        public DateTime RegistrationDate { get; }

        [XlsFormat("Gender", ExcelCellType.General, 6)]
        public string Gender { get; }

        public ClientXlsModel() { }

        public ClientXlsModel(ClientGridView client)
        {
            FullName = client.FullName;
            Email = client.Email;
            PhoneNumber = client.PhoneNumber;
            BirthDate = client.BirthDate;
            RegistrationDate = client.RegistrationDate;
            Gender = client.Gender.ToString();
        }

        public ClientXlsModel(ClientInterestsView clientInterest)
        {
            FullName = $"{clientInterest.FirstName} {clientInterest.LastName}";
            Email = clientInterest.Email;
            PhoneNumber = clientInterest.PhoneNumber;
            BirthDate = clientInterest.BirthDate;
            RegistrationDate = clientInterest.RegistrationDate;
            Gender = clientInterest.Gender.ToString();
        }
    }
}
