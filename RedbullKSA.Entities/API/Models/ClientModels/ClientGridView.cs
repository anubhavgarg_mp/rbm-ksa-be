﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RedbullKSA.Entities.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.ClientModels
{
    public class ClientGridView
    {
        [Key]
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime? BirthDate { get; set; }

        public DateTime RegistrationDate { get; set; }

        public Gender Gender { get; set; }

        public bool IsActive { get; set; }
    }
}
