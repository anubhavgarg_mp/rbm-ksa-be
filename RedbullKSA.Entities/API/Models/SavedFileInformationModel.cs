﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models
{
    public class SavedFileInformationModel
    {
        public SavedFileInformationModel() { }

        public SavedFileInformationModel(Guid guid, string name, string url)
            => (Guid, Name, Url) = (guid, name, url);

        public Guid Guid { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
