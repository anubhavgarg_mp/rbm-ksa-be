﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PaymentModels.Interfaces
{
    public interface ICreateSdkToken
    {
        [Required]
        [JsonProperty("service_command")]
        public string ServiceCommand { get; set; }

        [Required]
        [JsonProperty("device_id")]
        public string DeviceId { get; set; }
    }
}
