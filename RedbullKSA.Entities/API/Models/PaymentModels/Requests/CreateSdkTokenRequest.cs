﻿using Newtonsoft.Json;
using RedbullKSA.Entities.API.Models.PaymentModels.Base;
using RedbullKSA.Entities.API.Models.PaymentModels.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PaymentModels.Requests
{
    public class CreateSdkTokenRequest : PayFortRequestBase, ICreateSdkToken
    {
        [Required]
        [JsonProperty("service_command")]
        public string ServiceCommand { get; set; }

        [Required]
        [JsonProperty("device_id")]
        public string DeviceId { get; set; }
    }
}
