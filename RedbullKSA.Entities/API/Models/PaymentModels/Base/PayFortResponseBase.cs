﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PaymentModels.Base
{
    public abstract class PayFortResponseBase : PayFortQueryBase
    {
        [Required]
        [JsonProperty("response_message")]
        public string ResponseMessage { get; set; }

        [Required]
        [JsonProperty("response_code")]
        public string ResponseCode { get; set; }

        [Required]
        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
