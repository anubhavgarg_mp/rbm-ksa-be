﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PaymentModels.Base
{
    public abstract class PayFortQueryBase
    {
        [Required]
        [JsonProperty("access_code")]
        public string AccessCode { get; set; }

        [Required]
        [JsonProperty("merchant_identifier")]
        public string MerchantIdentifier { get; set; }

        [Required]
        [JsonProperty("language")]
        public string Language { get; set; }

        [Required]
        [JsonProperty("signature")]
        public string HashedSignature { get; set; }
    }
}
