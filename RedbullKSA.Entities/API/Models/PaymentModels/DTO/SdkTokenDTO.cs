﻿namespace RedbullKSA.Entities.API.Models.PaymentModels.DTO
{
    public class SdkTokenDTO
    {
        public string SdkToken { get; set; }
    }
}
