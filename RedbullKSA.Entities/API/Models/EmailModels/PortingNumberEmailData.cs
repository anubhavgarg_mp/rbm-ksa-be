﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.EmailModels
{
    public class PortingNumberEmailData
    {
        public int ClientId { get; set; }
        public int PortingRequestId { get; set; }
        public string ContactMsisdn { get; set; }
        public string PortedMsisdn { get; set; }
    }
}
