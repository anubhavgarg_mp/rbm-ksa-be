﻿using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.FAQModels.Mobile
{
    public class FAQMobile
    {
        public int Id { get; set; }

        public int? FAQTagId { get; set; }

        public string FAQTagName { get; set; }

        public string Question { get; set; }

        public string Answer { get; set; }

        public bool TopXIndicator { get; set; }

        public FAQMobile(FAQView view, ServiceLanguage language)
        {
            if (view == null)
            {
                return;
            }

            Id = view.Id;
            FAQTagId = view.FAQTagId;
            FAQTagName = language == ServiceLanguage.EN ? view.FAQTagNameEN : view.FAQTagNameAR;
            Question = language == ServiceLanguage.EN ? view.QuestionEN : view.QuestionAR;
            Answer = language == ServiceLanguage.EN ? view.AnswerEN : view.AnswerAR;
            TopXIndicator = view.TopXIndicator;
        }
    }
}
