﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.FAQModels
{
    public class CreateFAQRequest : BaseFAQRequest
    {
        [Required]
        public string QuestionEN { get; set; }

        [Required]
        public string QuestionAR { get; set; }

        [Required]
        public string AnswerEN { get; set; }

        [Required]
        public string AnswerAR { get; set; }
    }
}
