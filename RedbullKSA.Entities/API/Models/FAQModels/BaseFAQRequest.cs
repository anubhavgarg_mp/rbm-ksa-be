﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.FAQModels
{
    public abstract class BaseFAQRequest
    {
        public int? FAQTagId { get; set; }

        [Required]
        public bool TopXIndicator { get; set; }
    }
}
