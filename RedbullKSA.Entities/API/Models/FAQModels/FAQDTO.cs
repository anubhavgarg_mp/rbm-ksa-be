﻿using RedbullKSA.Entities.Database.Models;

namespace RedbullKSA.Entities.API.Models.FAQModels
{
    public class FAQDTO : BaseFAQRequest
    {
        public int Id { get; set; }

        public string QuestionEN { get; set; }

        public string QuestionAR { get; set; }

        public string AnswerEN { get; set; }

        public string AnswerAR { get; set; }

        public FAQDTO(FAQ faq)
        {
            if (faq == null)
            {
                return;
            }

            Id = faq.Id;
            FAQTagId = faq.FAQTagId;
            QuestionEN = faq.QuestionEN;
            QuestionAR = faq.QuestionAR;
            AnswerEN = faq.AnswerEN;
            AnswerAR = faq.AnswerAR;
            TopXIndicator = faq.TopXIndicator;
        }
    }
}
