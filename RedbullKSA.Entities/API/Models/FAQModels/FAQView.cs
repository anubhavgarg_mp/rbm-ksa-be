﻿namespace RedbullKSA.Entities.API.Models.FAQModels
{
    public class FAQView
    {
        public int Id { get; set; }

        public int? FAQTagId { get; set; }

        public string FAQTagNameEN { get; set; }

        public string FAQTagNameAR { get; set; }

        public string QuestionEN { get; set; }

        public string QuestionAR { get; set; }

        public string AnswerEN { get; set; }

        public string AnswerAR { get; set; }

        public bool TopXIndicator { get; set; }
    }
}
