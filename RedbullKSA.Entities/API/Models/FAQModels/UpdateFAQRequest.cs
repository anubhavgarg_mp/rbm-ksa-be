﻿namespace RedbullKSA.Entities.API.Models.FAQModels
{
    public class UpdateFAQRequest : BaseFAQRequest
    {
        public int Id { get; set; }

        public string QuestionEN { get; set; }

        public string QuestionAR { get; set; }

        public string AnswerEN { get; set; }

        public string AnswerAR { get; set; }
    }
}
