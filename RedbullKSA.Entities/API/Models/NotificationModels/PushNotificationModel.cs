﻿namespace RedbullKSA.Entities.API.Models.NotificationModels
{
    public class PushNotificationModel
    {
        public string Msisdn { get; set; }

        public PushNotificationContent English { get; set; }

        public PushNotificationContent Arabic { get; set; }
    }

    public class PushNotificationContent
    {
        public string Title { get; set; }

        public string Body { get; set; }
    }
}
