﻿using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.OrderModels
{
    public class SimOrderRequest
    {
        /// <summary>
        /// Selected SIM delivery type
        /// </summary>
        [Required]
        public SimPickupType DeliveryType { get; set; }

        /// <summary>
        /// Selected number tier
        /// </summary>
        [Required]
        public NumberTiers NumberTier { get; set; }

        /// <summary>
        /// Type of SIM
        /// </summary>
        [Required]
        public SimType SimType { get; set; }

        /// <summary>
        /// Selected phone number
        /// </summary>
        [Required]
        public string MSISDN { get; set; }

        /// <summary>
        /// Delivery address if HomeDelivery was selected
        /// </summary>
        public DeliveryAddress DeliveryAddress {get; set;}
    }

    public class DeliveryAddress
    {
        [Required]
        public string City { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public string Address { get; set; }
        public string District { get; set; }
        [Required]
        public string ZipCode { get; set; }
    }
}
