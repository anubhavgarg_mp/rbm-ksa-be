﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.Portability
{
    public class NumberPortingResponse
    {
        /// <summary>
        /// Porting request ID
        /// </summary>
        public int Id { get; set; }
    }
}
