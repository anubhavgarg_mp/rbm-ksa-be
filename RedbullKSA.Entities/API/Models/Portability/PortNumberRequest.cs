﻿using RedbullKSA.Entities.Enums.Customers;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.Portability
{
    public class PortNumberRequest
    {
        [Required]
        public CustomerTitleStatus Title { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// MSISDN which will be ported
        /// </summary>
        [Required]
        public string CurrentMsisdn { get; set; }

        /// <summary>
        /// MSISDN used to contact user through the process
        /// </summary>
        [Required]
        public string ContactMsisdn { get; set; }

        [Required]
        public string NationalityId { get; set; }

        public string CurrentOperator { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }
    }
}
