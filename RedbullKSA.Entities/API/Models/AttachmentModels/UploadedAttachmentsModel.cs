﻿using System.Collections.Generic;

namespace RedbullKSA.Entities.API.Models.AttachmentModels
{
    public class UploadedAttachmentsModel
    {
        public IEnumerable<UploadAttachmentModel> Attachments { get; set; }
    }
}
