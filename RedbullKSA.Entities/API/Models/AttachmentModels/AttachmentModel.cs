﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.AttachmentModels
{
    public class AttachmentModel
    {
        public int Id { get; set; }
        public int? OldId { get; set; }

        public int? ThumbnailId { get; set; }
        public int? OldThumbnailId { get; set; }

        public string FileUrl { get; set; }

        public AttachmentCategory? Category { get; set; }
        public ServiceLanguage? Language { get; set; }

        public AttachmentFileType? Type { get; set; }

        public AttachmentModel(Attachment attachment)
        {
            Id = attachment.Id;
            FileUrl = attachment.FileUrl;
            Category = attachment.Category;
            Language = attachment.FileLanguage;
            Type = attachment.UploadedFileType;
        }

        public AttachmentModel(Attachment attachment, int thumbnailId)
        {
            Id = attachment.Id;
            FileUrl = attachment.FileUrl;
            Category = attachment.Category;
            Language = attachment.FileLanguage;
            Type = attachment.UploadedFileType;
            ThumbnailId = thumbnailId;
        }

        public AttachmentModel() { }
    }
}
