﻿using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.AttachmentModels
{
    public class UploadAttachmentModel
    {
        public string FileUrl { get; set; }

        public AttachmentFileType Type { get; set; }

        public AttachmentCategory Category { get; set; }

        public ServiceLanguage Language { get; set; }
    }
}
