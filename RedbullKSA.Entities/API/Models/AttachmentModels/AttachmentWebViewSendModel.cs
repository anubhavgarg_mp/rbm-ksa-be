﻿using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.AttachmentModels
{
    public class AttachmentWebViewSendModel
    {
        public string Url { get; set; }

        public ServiceLanguage Language { get; set; }

        public AttachmentCategory Category { get; set; }
    }
}
