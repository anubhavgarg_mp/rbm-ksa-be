﻿using RedbullKSA.Entities.Attributes;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.InterestModels
{
    public class InterestXlsModel
    {
        [XlsFormat("Name", ExcelCellType.General, 1)]
        public string Name { get; }

        [XlsFormat("Number of qualifying users", ExcelCellType.Integer, 2)]
        public int NumberOfClients { get; }

        [XlsFormat("Status", ExcelCellType.General, 3)]
        public string Status { get; }

        public InterestXlsModel() { }

        public InterestXlsModel(InterestGridView interests)
        {
            Name = interests.EnglishName;
            NumberOfClients = interests.NumberOfClients;
            Status = interests.Status.ToString();
        }
    }
}
