﻿using RedbullKSA.Entities.API.Models.Base;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.InterestModels
{
    public class InterestDTO
    {
        public int Id { get; set; }

        public MultiString Name { get; set; }

        public ActiveStatus Status { get; set; }

        public int? EnglishImageId { get; set; }
        public int? ArabicImageId { get; set; }
    }
}
