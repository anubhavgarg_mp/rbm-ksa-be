﻿using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System;

namespace RedbullKSA.Entities.API.Models.InterestModels
{
    public class InterestDetailsView
    {
        public int Id { get; set; }

        public MultiString Name { get; set; }

        public ActiveStatus Status { get; set; }

        public string EnglishImageUrl { get; set; }
        public string ArabicImageUrl { get; set; }

        public InterestDetailsView() { }

        public static InterestDetailsView FromInterest(Interest interest)
        {
            return new InterestDetailsView
            {
                Id = interest.Id,
                Name = new MultiString(interest.EnglishName, interest.ArabicName),
                EnglishImageUrl = interest.ImageEN,
                ArabicImageUrl = interest.ImageAR,
                Status = interest.Status
            };
        }
    }
}
