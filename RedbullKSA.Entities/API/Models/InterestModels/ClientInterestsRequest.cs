﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.InterestModels
{
    public class ClientInterestsRequest
    {
        public List<int> Interests { get; set; }
    }
}
