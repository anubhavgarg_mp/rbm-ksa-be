﻿using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.InterestModels
{
    public class InterestGridView
    {
        public int Id { get; set; }

        public string EnglishName { get; set; }

        public int NumberOfClients { get; set; }

        public ActiveStatus Status { get; set; }

        public InterestGridView() { }
    }
}
