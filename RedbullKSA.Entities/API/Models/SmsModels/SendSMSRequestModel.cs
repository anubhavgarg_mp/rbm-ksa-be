﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.SmsModels
{
    public class SendSMSRequestModel
    {
        public string Message { get; set; }
        public string SerialNumber { get; set; }
        public string LaunchContactNumber { get; set; }
    }
}
