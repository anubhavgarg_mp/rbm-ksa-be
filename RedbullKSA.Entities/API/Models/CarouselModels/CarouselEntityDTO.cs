﻿using RedbullKSA.Entities.API.Models.Base;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.CarouselModels
{
    public class CarouselEntityDTO
    {
        public MultiString Title { get; set; }
        public MultiString Description { get; set; }
        public ActiveStatus Status { get; set; }
        [Required]
        public int? EnglishImageId { get; set; }
        [Required]
        public int? ArabicImageId { get; set; }
    }
}
