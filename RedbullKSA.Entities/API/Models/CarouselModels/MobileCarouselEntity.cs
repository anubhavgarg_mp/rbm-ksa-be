﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.CarouselModels
{
    public class MobileCarouselEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }

        public MobileCarouselEntity(CarouselEntity entity, ServiceLanguage language)
        {
            if (entity == null)
                return;

            Id = entity.Id;

            if (language == ServiceLanguage.EN)
            {
                Title = entity.TitleEN;
                ImageUrl = entity.ImageUrlEn;
            }
            else
            {
                Title = entity.TitleAR;
                ImageUrl = entity.ImageUrlAr;
            }
        }
    }
}
