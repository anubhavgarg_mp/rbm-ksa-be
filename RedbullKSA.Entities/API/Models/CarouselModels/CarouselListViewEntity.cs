﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.CarouselModels
{
    public class CarouselListViewEntity
    {
        public int Id { get; set; }
        public string TitleEN { get; set; }
        public ActiveStatus Status { get; set; }

        public CarouselListViewEntity(CarouselEntity entity)
        {
            Id = entity.Id;
            TitleEN = entity.TitleEN;
            Status = entity.Status;
        }
    }
}
