﻿using RedbullKSA.Entities.API.Models.Base;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.API.Models.CarouselModels
{
    public class CarouselEntityDetailsView
    {
        public int Id { get; set; }
        public MultiString Title { get; set; }
        public MultiString Description { get; set; }
        public ActiveStatus Status { get; set; }
        public string EnglishImageUrl { get; set; }
        public string ArabicImageUrl { get; set; }
        public CarouselEntityDetailsView(CarouselEntity entity)
        {
            if (entity == null)
                return;

            Id = entity.Id;
            Title = new MultiString(entity.TitleEN, entity.TitleAR);
            Description = new MultiString(entity.DescriptionEn, entity.DescriptionAr);
            Status = entity.Status;
            EnglishImageUrl = entity.ImageUrlEn;
            ArabicImageUrl = entity.ImageUrlAr;
        }
    }
}
