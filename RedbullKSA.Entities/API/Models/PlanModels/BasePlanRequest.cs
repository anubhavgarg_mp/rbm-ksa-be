﻿using Newtonsoft.Json;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Enums.Plan;
using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace RedbullKSA.Entities.API.Models.PlanModels
{
    public abstract class BasePlanRequest
    {
        /// <summary>
        /// English plan name
        /// </summary>
        [Required]
        public string NameEN { get; set; }

        /// <summary>
        /// Arabic plan name
        /// </summary>
        [Required]
        public string NameAR { get; set; }

        /// <summary>
        /// Plan type
        /// </summary>
        [Required]
        public PlanType PlanType { get; set; }

        /// <summary>
        /// New - 1
        /// </summary>
        [Required]
        public bool IsNew { get; set; } = false;

        /// <summary>
        /// Before this date a plan is new
        /// </summary>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? NewEndDate { get; set; }

        /// <summary>
        /// English price text
        /// </summary>
        [Required]
        public string PriceTextEN { get; set; }

        /// <summary>
        /// Arabic price text
        /// </summary>
        [Required]
        public string PriceTextAR { get; set; }

        /// <summary>
        /// Measurement of plan
        /// </summary>
        [Required]
        public MeasurementType MeasurementId { get; set; } = MeasurementType.GB;

        /// <summary>
        /// Active/Inactive
        /// </summary>
        [Required]
        public bool IsActive { get; set; }

        /// <summary>
        /// Comment 1
        /// </summary>
        public string Comment1EN { get; set; }

        /// <summary>
        /// Comment 2
        /// </summary>
        public string Comment2EN { get; set; }

        /// <summary>
        /// Arabic Comment 1
        /// </summary>
        public string Comment1AR { get; set; }

        /// <summary>
        /// Arabic Comment 2
        /// </summary>
        public string Comment2AR { get; set; }

        [IgnoreDataMember]
        public int UserId { get; set; }
    }
}
