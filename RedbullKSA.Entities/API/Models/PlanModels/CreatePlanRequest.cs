﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PlanModels
{
    public class CreatePlanRequest : BasePlanRequest
    {
        /// <summary>
        /// Plan tag id
        /// </summary>
        [Required]
        public int PlanTagId { get; set; }

        /// <summary>
        /// Supplementary offering id
        /// </summary>
        [Required]
        public string OfferingId { get; set; }
    }
}
