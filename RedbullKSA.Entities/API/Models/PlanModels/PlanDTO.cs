﻿using Newtonsoft.Json;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Enums.Plan;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PlanModels
{
    public class PlanDTO
    {
        /// <summary>
        /// Plan Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Plan tag id
        /// </summary>
        [Required]
        public int PlanTagId { get; set; }

        /// <summary>
        /// Supplementary offering id
        /// </summary>
        [Required]
        public string OfferingId { get; set; }

        /// <summary>
        /// English plan name
        /// </summary>
        [Required]
        public string NameEN { get; set; }

        /// <summary>
        /// Arabic plan name
        /// </summary>
        [Required]
        public string NameAR { get; set; }

        /// <summary>
        /// Plan type
        /// </summary>
        [Required]
        public PlanType PlanType { get; set; }

        /// <summary>
        /// New - 1
        /// </summary>
        [Required]
        public bool IsNew { get; set; }

        /// <summary>
        /// Before this date a plan is new
        /// </summary>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? NewEndDate { get; set; }

        /// <summary>
        /// Price in GB
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// English price text
        /// </summary>
        [Required]
        public string PriceTextEN { get; set; }

        /// <summary>
        /// Arabic price text
        /// </summary>
        [Required]
        public string PriceTextAR { get; set; }

        /// <summary>
        /// Measurement of plan
        /// </summary>
        [Required]
        public MeasurementType MeasurementId { get; set; }

        /// <summary>
        /// Active/Inactive
        /// </summary>
        [Required]
        public bool IsActive { get; set; }

        /// <summary>
        /// Plan activation start date
        /// </summary>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? ActivationDate { get; set; } 

        /// <summary>
        /// Comment 1
        /// </summary>
        public string Comment1EN { get; set; }

        /// <summary>
        /// Comment 2
        /// </summary>
        public string Comment2EN { get; set; }

        /// <summary>
        /// Arabic Comment 1
        /// </summary>
        public string Comment1AR { get; set; }

        /// <summary>
        /// Arabic Comment 2
        /// </summary>
        public string Comment2AR { get; set; }

        public PlanDTO(Plan plan)
        {
            if (plan == null)
            {
                return;
            }

            Id = plan.Id;
            PlanTagId = plan.PlanTagId;
            OfferingId = plan.OfferingId;
            NameEN = plan.NameEN;
            NameAR = plan.NameAR;
            PlanType = plan.PlanType;
            IsNew = plan.IsNew;
            NewEndDate = plan.NewEndDate;
            Price = plan.Price;
            PriceTextEN = plan.PriceTextEN;
            PriceTextAR = plan.PriceTextAR;
            MeasurementId = plan.MeasurementId;
            IsActive = plan.IsActive;
            ActivationDate = plan.ActivationStartDate;
            Comment1EN = plan.Comment1EN;
            Comment2EN = plan.Comment2EN;
            Comment1AR = plan.Comment1AR;
            Comment2AR = plan.Comment2AR;
        }
    }
}
