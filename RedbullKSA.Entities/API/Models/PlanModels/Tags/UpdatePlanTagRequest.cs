﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PlanModels.Tags
{
    public class UpdatePlanTagRequest : BasePlanTagRequest
    {
        /// <summary>
        /// Plan tag id
        /// </summary>
        [Required]
        public int Id { get; set; }
    }
}
