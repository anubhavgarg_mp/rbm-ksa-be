﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PlanModels.Tags
{
    public abstract class BasePlanTagRequest
    {
        /// <summary>
        /// English tag name
        /// </summary>
        [Required]
        public string NameEN { get; set; }

        /// <summary>
        /// Arabic tag name
        /// </summary>
        [Required]
        public string NameAR { get; set; }
    }
}
