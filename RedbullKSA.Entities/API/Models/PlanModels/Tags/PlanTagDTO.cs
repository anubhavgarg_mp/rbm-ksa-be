﻿using RedbullKSA.Entities.Database.Models;

namespace RedbullKSA.Entities.API.Models.PlanModels.Tags
{
    public class PlanTagDTO
    {
        /// <summary>
        /// Plan tag id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// English tag name
        /// </summary>
        public string NameEN { get; set; }

        /// <summary>
        /// Arabic tag name
        /// </summary>
        public string NameAR { get; set; }

        public PlanTagDTO(PlanTag tag)
        {
            if (tag == null)
            {
                return;
            }

            Id = tag.Id;
            NameEN = tag.NameEN;
            NameAR = tag.NameAR;
        }
    }
}
