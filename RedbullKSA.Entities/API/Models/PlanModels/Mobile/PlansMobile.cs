﻿
using Newtonsoft.Json;
using RedbullKSA.Entities.Converters.RedbullKSA.Entities.Converters;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Enums.Plan;
using System;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PlanModels.Mobile
{
    public class PlansMobile
    {
        /// <summary>
        /// Plan Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Plan tag id
        /// </summary>
        [Required]
        public int PlanTagId { get; set; }

        /// <summary>
        /// PlanTagName
        /// </summary>
        [Required]
        public string PlanTagName { get; set; }

        /// <summary>
        /// Plan name
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Plan type
        /// </summary>
        [Required]
        public PlanType PlanType { get; set; }

        /// <summary>
        /// New - 1
        /// </summary>
        [Required]
        public bool IsNew { get; set; }

        /// <summary>
        /// Before this date a plan is new
        /// </summary>
        [JsonConverter(typeof(DateTimeJsonConverter))]
        public DateTime? NewEndDate { get; set; }

        /// <summary>
        /// Price in GB
        /// </summary>
        [Required]
        public decimal Price { get; set; }

        /// <summary>
        /// Price text
        /// </summary>
        [Required]
        public string PriceText { get; set; }

        /// <summary>
        /// Active/Inactive
        /// </summary>
        [Required]
        public bool IsActive { get; set; }

        /// <summary>
        /// Plan activation start date
        /// </summary>
        public DateTime? ActivationDate { get; set; }

        /// <summary>
        /// Comment 1
        /// </summary>
        public string Comment1 { get; set; }

        /// <summary>
        /// Comment 2
        /// </summary>
        public string Comment2 { get; set; }


        public PlansMobile(Plan plan, PlanTag tag, ServiceLanguage language)
        {
            if (plan == null)
            {
                return;
            }

            Id = plan.Id;
            PlanTagId = plan.PlanTagId;
            PlanTagName = language == ServiceLanguage.EN ? tag.NameEN : tag.NameAR;
            Name = language == ServiceLanguage.EN ? plan.NameEN : plan.NameAR;
            PlanType = plan.PlanType;
            IsNew = plan.IsNew;
            NewEndDate = plan.NewEndDate;
            Price = plan.Price;
            PriceText = language == ServiceLanguage.EN ? plan.PriceTextEN : plan.PriceTextAR;
            IsActive = plan.IsActive;
            ActivationDate = plan.ActivationStartDate;
            Comment1 = language == ServiceLanguage.EN ? plan.Comment1EN : plan.Comment1AR;
            Comment2 = language == ServiceLanguage.EN ? plan.Comment2EN : plan.Comment2AR;
        }
    }
}
