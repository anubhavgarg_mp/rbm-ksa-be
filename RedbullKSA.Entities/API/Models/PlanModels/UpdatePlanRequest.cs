﻿namespace RedbullKSA.Entities.API.Models.PlanModels
{
    public class UpdatePlanRequest : BasePlanRequest
    {
        /// <summary>
        /// Plan Id
        /// </summary>
        public int Id { get; set; }
    }
}
