﻿using Newtonsoft.Json;
using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.API.Models.HuaweiModels;
using RedbullKSA.Entities.Database.Models;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.PlanModels
{
    public class PlanGridView
    {
        /// <summary>
        /// Plan
        /// </summary>
        [Required]
        public PlanDTO Plan { get; set; }

        /// <summary>
        /// Huawei plan name
        /// </summary>
        [Required]
        public string OfferingName { get; set; }

        public PlanGridView(Plan plan, SupplementaryOfferingDTO offering)
        {
            if (offering == null)
            {
                return;
            }

            plan ??= new Plan();
            Plan = new PlanDTO(plan);
            Plan.Price = offering.OneTimeCost.ConvertToGBPrice();
            Plan.OfferingId = string.IsNullOrEmpty(Plan.OfferingId) ? offering.OfferingId : Plan.OfferingId;

            OfferingName = offering.OfferingName;
        }
    }
}
