﻿using RedbullKSA.Entities.Enums;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.UserModels
{
    public class AdministratorGridView
    {
        [Key]
        public int Id { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public ActiveStatus Status { get; set; }

        public bool IsApiUser { get; set; }
    }
}
