﻿using RedbullKSA.Common.Extensions;
using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using System.Collections.Generic;
using System.Linq;

namespace RedbullKSA.Entities.API.Models.UserModels
{
    public class AdministratorDetailView
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public ActiveStatus Status { get; set; }

        public bool IsApiUser { get; set; }


        public IEnumerable<AdminClaimType> Permissions { get; set; }

        public AdministratorDetailView() { }

        public static AdministratorDetailView FromUserAndClaims(User user, IEnumerable<UserClaim> claims)
        {
            return new AdministratorDetailView
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Status = user.Status,
                Permissions = claims.Select(x => x.ClaimType),
                IsApiUser = user.IsApiUser
            };
        }

        public void CleanDetails()
        {
            FirstName.StripNonAlphaNumeric();
            LastName.StripNonAlphaNumeric();
            PhoneNumber.StripNonNumeric();
        }
    }
}
