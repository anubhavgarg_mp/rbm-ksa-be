﻿using RedbullKSA.Entities.Attributes;
using RedbullKSA.Entities.Enums;

namespace RedbullKSA.Entities.API.Models.UserModels
{
    public class AdministratorXlsModel
    {
        [XlsFormat("Full Name", ExcelCellType.General, 1)]
        public string FullName { get; }

        [XlsFormat("Email", ExcelCellType.General, 2)]
        public string Email { get; }

        [XlsFormat("Phone", ExcelCellType.General, 3)]
        public string PhoneNumber { get; }

        [XlsFormat("Status", ExcelCellType.General, 4)]
        public string Status { get; }

        public AdministratorXlsModel() { }

        public AdministratorXlsModel(AdministratorGridView administrator)
        {
            FullName = administrator.FullName;
            Email = administrator.Email;
            PhoneNumber = administrator.PhoneNumber;
            Status = administrator.Status.ToString();
        }
    }
}
