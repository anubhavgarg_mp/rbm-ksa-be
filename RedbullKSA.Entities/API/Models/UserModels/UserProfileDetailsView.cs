﻿using RedbullKSA.Entities.Database.Models;

namespace RedbullKSA.Entities.API.Models.UserModels
{
    public class UserProfileDetailsView
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public UserProfileDetailsView(User user)
        {
            FullName = $"{user.FirstName} {user.LastName}";
            Phone = user.PhoneNumber;
            Email = user.Email;
        }
    }
}
