﻿using RedbullKSA.Entities.Database.Models;
using RedbullKSA.Entities.Enums;
using RedbullKSA.Entities.Enums.Address;

namespace RedbullKSA.Entities.API.Models.AddressModels
{
    public class ClientAddressDTO
    {
        public AddressType? AddressType { get; set; }

        /// <summary>
        /// Name of the address
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Address line 1 (street)
        /// </summary>
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Address line 2
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// State/Province
        /// </summary>
        public string Province { get; set; }

        /// <summary>
        /// Zip code
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Shipping address if true
        /// </summary>
        public bool IsShippingAddress { get; set; }

        /// <summary>
        /// API Client Id
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// Address id
        /// </summary>
        public int Id { get; set; }

        public ClientAddressDTO(ClientAddress address, ServiceLanguage language)
        {
            if (address == null)
            {
                return;
            }

            Id = address.Id;
            ClientId = address.ClientId;
            Name = language == ServiceLanguage.EN ? address.NameEN : address.NameAR;
            AddressType = address.AddressType;
            AddressLine1 = address.Street;
            AddressLine2 = address.AddressLine4;
            City = address.City;
            Province = address.Province;
            ZipCode = address.PostCode;
            IsShippingAddress = address.IsDefault;
        }
    }
}
