﻿using RedbullKSA.Entities.Enums.Address;
using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.AddressModels
{
    public class BaseClientAddressRequest
    {
        [Required]
        public AddressType AddressType { get; set; }

        /// <summary>
        /// Name of the address
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Address line 1 (street)
        /// </summary>
        [Required]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Address line 2
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// City
        /// </summary>
        [Required]
        public string City { get; set; }

        /// <summary>
        /// State/Province
        /// </summary>
        [Required]
        public string Province { get; set; }

        /// <summary>
        /// Zip code
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Shipping address if true
        /// </summary>
        [Required]
        public bool IsShippingAddress { get; set; }
    }
}
