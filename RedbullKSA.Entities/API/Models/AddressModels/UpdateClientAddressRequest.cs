﻿using System.ComponentModel.DataAnnotations;

namespace RedbullKSA.Entities.API.Models.AddressModels
{
    public class UpdateClientAddressRequest : BaseClientAddressRequest
    {
        /// <summary>
        /// Address id
        /// </summary>
        [Required]
        public int Id { get; set; }
    }
}
