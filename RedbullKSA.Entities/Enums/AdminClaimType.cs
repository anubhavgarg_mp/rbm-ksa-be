﻿namespace RedbullKSA.Entities.Enums
{
    public enum AdminClaimType
    {
        /// Read
        
        PlansView = 1,
        BenefitsView = 2,
        InterestsView = 3,
        AdministratorsView = 4,
        FAQView = 5,
        CarouselView = 6,

        /// Write
        PlansWrite = 101,
        BenefitsWrite = 102,
        InterestsWrite = 103,
        AdministratorsWrite = 104,
        FAQWrite = 105,  
        CarouselWrite = 106,
    }
}
