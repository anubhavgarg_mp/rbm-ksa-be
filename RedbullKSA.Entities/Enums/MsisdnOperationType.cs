﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Enums
{
    public enum MsisdnOperationType : short
    {
        Pick = 1029,
        Unpick = 1030
    }
}
