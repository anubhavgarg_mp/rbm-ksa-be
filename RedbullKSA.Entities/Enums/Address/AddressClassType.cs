﻿namespace RedbullKSA.Entities.Enums.Address
{
    public enum AddressClassType : byte
    {
        Customer = 1,
        Account = 2,
        Subscriber = 3
    }
}
