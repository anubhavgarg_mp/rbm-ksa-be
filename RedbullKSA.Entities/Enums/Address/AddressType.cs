﻿namespace RedbullKSA.Entities.Enums.Address
{
    public enum AddressType : byte
    {
        Legal = 0,
        Office = 1,
        Base = 2,
        Home = 3,
        Account = 4,
        Emergency = 5,
        Other = 6,
        Delivery = 7,
        Correspondence = 8,
        Register = 9,
        AnotherDelivery = 11
    }
}
