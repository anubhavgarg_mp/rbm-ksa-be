﻿namespace RedbullKSA.Entities.Enums
{
    public enum AttachmentCategory : short
    {
        InterestImage = 1,
        BenefitTeaser = 2,
        BenefitMain = 3,
        BenefitGalleryCover = 4,
        BenefitGalleryImage = 5,
        BenefitHeader = 6,
        BenefitTeaserVideo = 7,
        BenefitTeaserVideoThumbnail = 8,
        CarouselImage = 9
    }
}
