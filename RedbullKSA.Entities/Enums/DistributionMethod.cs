﻿namespace RedbullKSA.Entities.Enums
{
    public enum DistributionMethod : byte
    {
        MembershipId = 1,
        Post = 2,
        PromoCode = 3
    }
}
