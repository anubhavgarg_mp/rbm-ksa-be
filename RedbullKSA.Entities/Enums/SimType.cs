﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Enums
{
    public enum SimType : byte
    {
        Regular,
        eSIM
    }
}
