﻿using RedbullKSA.Entities.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace RedbullKSA.Entities.Enums
{
    public enum TransferUnit
    {
        [TransferUnitDescription("D")]
        Data = 1,
        [TransferUnitDescription("C")]
        Credit = 2,
        [TransferUnitDescription("M")]
        Minutes = 3,
    }
}
