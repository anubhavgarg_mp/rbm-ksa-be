﻿using System;

namespace RedbullKSA.Entities.Enums
{
    [Flags]
    public enum BenefitCategory : short
    {
        Giveaways = 1,
        Goodies = 2,
        Discover = 4,
        Benefits = 8,
        Special = 16
    }
}
