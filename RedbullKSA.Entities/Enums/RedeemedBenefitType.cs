﻿namespace RedbullKSA.Entities.Enums
{
    public enum RedeemedBenefitType : byte
    {
        Available = 0,
        Winner = 1,
        Participant = 2
    }
}