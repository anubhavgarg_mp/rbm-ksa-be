﻿using System;

namespace RedbullKSA.Entities.Enums
{
    [Flags]
    public enum SessionTokenType
    {
        Admin = 1,
        App = 2,
        Api = 4,
        Web = 8
    }
}
