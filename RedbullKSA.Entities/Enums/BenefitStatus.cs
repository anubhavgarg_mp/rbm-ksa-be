﻿namespace RedbullKSA.Entities.Enums
{
    public enum BenefitStatus : byte
    {
        Inactive = 0,
        Active = 1
    }
}
