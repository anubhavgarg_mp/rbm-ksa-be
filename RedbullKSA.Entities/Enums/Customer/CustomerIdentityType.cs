﻿namespace RedbullKSA.Entities.Enums.Customer
{
    public enum CustomerIdentityType : byte
    {
        Citizen = 1,
        Resident = 2,
        Visitor = 3,
        GccPassport = 4,
        GccNationalId = 5,
        PilgrimPassport = 6,
        PilgrimBorder = 7,
        UmrahPassport = 8,
        VisitorVisa = 9,
        UmrahVisa = 10,
        HajVisa = 11
    }
}
