﻿namespace RedbullKSA.Entities.Enums.Customers
{
    public enum CustomerStatus : byte
    {
        Prospect = 1,
        Active = 2,
        Deleted = 3
    }
}
