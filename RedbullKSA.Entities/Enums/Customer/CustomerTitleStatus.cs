﻿namespace RedbullKSA.Entities.Enums.Customers
{
    public enum CustomerTitleStatus : byte
    {
        None = 0,
        Mr = 1,
        Mrs = 2,
        Ms = 3
    }
}
