﻿namespace RedbullKSA.Entities.Enums.Customer
{
    public enum CustomerLevel : byte
    {
        None = 0,
        Standard = 1,
        VIP = 2,
        VVIP = 3
    }
}
