﻿namespace RedbullKSA.Entities.Enums
{
    public enum SubscriberStatus : byte
    {
        Idle = 1, 
        Active = 2,
        Baring = 3,
        Suspended = 4,
        PreDeactivate = 5, 
        Deactive = 9
    }
}
