﻿namespace RedbullKSA.Entities.Enums
{
    public enum AttachmentFileType : byte
    {
        Image = 0,
        WebView = 1,
        Video = 2
    }
}
