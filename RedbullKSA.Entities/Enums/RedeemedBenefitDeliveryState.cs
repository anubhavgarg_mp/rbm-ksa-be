﻿namespace RedbullKSA.Entities.Enums
{
    public enum RedeemedBenefitDeliveryState : byte
    {
        Undelivered = 0,
        EmailSent = 1
    }
}
