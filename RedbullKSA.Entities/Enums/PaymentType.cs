﻿namespace RedbullKSA.Entities.Enums
{
    public enum PaymentType
    {
        Prepaid = 0,
        Postpaid = 1
    }
}
