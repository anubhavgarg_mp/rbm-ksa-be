﻿namespace RedbullKSA.Entities.Enums
{
    public enum NotificationStatus
    {
        Failure = 0,
        Success = 1
    }
}
