﻿namespace RedbullKSA.Entities.Enums.Plan
{
    public enum PlanType
    {
        International = 1,
        Local = 2
    }
}
