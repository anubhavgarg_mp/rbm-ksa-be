using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Enums
{
    public enum ActiveStatus : byte
    {
        Inactive = 0,
        Active = 1
    }
}
