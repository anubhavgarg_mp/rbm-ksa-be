﻿using System;

namespace RedbullKSA.Entities.Enums
{
    [Flags]
    public enum NotificationMethod : short
    {
        Sms = 1,
        Email = 2
    }
}
