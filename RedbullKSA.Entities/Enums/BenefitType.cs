﻿using System.ComponentModel;

namespace RedbullKSA.Entities.Enums
{
    public enum BenefitType : byte
    {
        [Description("Tangible")]
        Tangible = 0,

        [Description("Experience")]
        Experience = 1,

        [Description("Voucher")]
        Voucher = 2,

        [Description("Explore")]
        Explore = 3
    }
}
