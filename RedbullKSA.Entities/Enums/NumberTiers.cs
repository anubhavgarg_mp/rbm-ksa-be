﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Enums
{
    public enum NumberTiers
    {
        Platinum = 1,
        Gold = 2,
        Silver = 3,
        Standard = 4
    }
}
