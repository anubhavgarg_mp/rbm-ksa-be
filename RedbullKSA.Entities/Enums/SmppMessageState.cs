﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedbullKSA.Entities.Enums
{
    public enum SmppMessageState : short
    {
        None = 0,
        //
        // Summary:
        //     The message is in enroute state.
        Enroute = 1,
        //
        // Summary:
        //     Message is delivered to destination
        Delivered = 2,
        //
        // Summary:
        //     Message validity period has expired.
        Expired = 3,
        //
        // Summary:
        //     Message has been deleted.
        Deleted = 4,
        //
        // Summary:
        //     Message is undeliverable
        Undeliverable = 5,
        //
        // Summary:
        //     Message is in accepted state (i.e. has been manually read on behalf of the subscriber
        //     by customer service)
        Accepted = 6,
        //
        // Summary:
        //     Message is in invalid state
        Unknown = 7,
        //
        // Summary:
        //     Message is in a rejected state
        Rejected = 8
    }
}
