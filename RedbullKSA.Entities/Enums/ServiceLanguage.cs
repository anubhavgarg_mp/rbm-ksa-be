﻿using System.ComponentModel;
using RedbullKSA.Entities.Attributes;

namespace RedbullKSA.Entities.Enums
{
    public enum ServiceLanguage : byte
    {
        [Description("en")]
        [LanguageDescription("English")]
        EN = 0,

        [Description("ar")]
        [LanguageDescription("Arabic")]
        AR = 1
    }
}
