﻿namespace RedbullKSA.Entities.Constants
{
    public static class HuaweiPaths
    {
        public const string Inventory = "apiaccess/InventoryService/InventoryService";
        public const string Customer = "apiaccess/CustomerServices/CustomerServices";
        public const string Recharge = "apiaccess/RechargeServices/RechargeServices";
        public const string Offerings = "apiaccess/OfferingServices/OfferingServices";
        public const string Subscriber = "apiaccess/SubscriberServices/SubscriberServices";
        public const string Utility = "apiaccess/UtilityServices/UtilityServices";
        public const string Billing = "apiaccess/BillingServices/BillingServices";
        public const string Account = "apiaccess/AccountServices/AccountServices";
    }
}