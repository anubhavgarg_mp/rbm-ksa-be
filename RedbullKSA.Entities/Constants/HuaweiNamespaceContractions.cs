﻿namespace RedbullKSA.Entities.Constants
{
    public static class HuaweiNamespaceContractions
    {
        public const string Offering = "off";
        public const string Recharge = "rec";
        public const string Billing = "bil";
        public const string Subscriber = "sub";
        public const string Account = "acc";
    }
}
